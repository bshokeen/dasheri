Organize anything or everything you deal with this handy app. 
This is a unique powerful application to capture your tasks, thoughts, activities, action items and all kind of lists. 
Have your assistant always with you and give the load of tracking and managing to this app.

It provides lot of features as a standalone app on iPhone.

What's New?
• Collapsible Lists with all tasks in same UI
• Ability to track task progress in Percentage
• Allows setting up estimated time and then actual time for completion
• Dynamic ability to see your tasks in many different ways
• Collect them under Group of your own choice
• Tag the tasks as necessary and filter based on tags
• Intuitive interaction with tasks as Due On today, this week, over due or without any due date.
• Manage tasks as you prefer i.e. By Due Date, Daily/Weekly Schedules, Project/Group of tasks, Gain/Priority of tasks and so on.
• Set Reminder with wide range of choices
• Store Notes along with tasks with Bullet List Support

#######
ARCHIVE
#######
16-Nov: Implementing state restoration
* https://developer.apple.com/library/ios/documentation/iphone/conceptual/iphoneosprogrammingguide/StatePreservation/StatePreservation.html
* http://www.techotopia.com/index.php/An_iOS_6_iPhone_State_Preservation_and_Restoration_Tutorial
* http://ideveloper.co/introduction-to-ui-state-preservation-restoration-in-ios6/ (testing method - stop from xcode)
* http://www.slideshare.net/robby_brown/ios-state-preservation-and-restoration

16-Nov: XIB implementation reference
* http://onedayitwillmake.com/blog/2013/07/ios-creating-reusable-uiviews-with-storyboard/

17-Nov: Swipe
* http://idevrecipes.com/2011/04/14/how-does-the-twitter-iphone-app-implement-side-swiping-on-a-table/

29-Nov: Google integration
* Copy the SDK
* Add the frameworks
* Project Info -> capabilities -> background fetch
* Code - Appdeletehate.h: Include the header and define the variable. .m: add variables and copy hte code. Generate the app * code on analytics web site
* Inside the common class add the google & flurry title API to be called in the individual classed on load
* Remove flurry calls if needed.
  I have kept the logapi a direct call from app utilities
App Name  : iGotTodo
TrackingID: UA-57208081-1

###########
Parser.com
############
    Application Keys
    Application ID This is the main identifier that uniquely specifies your application.
    This is paired with a key to provide your clients access to your application's data.

    6N9mRm1Sh0sLc0vFctenTsFj17lgHuCLQvM6akcB
        Client Key This key should be used in consumer clients, like the iOS or Android SDK.
        It adheres to object level permissions.

    i96WdFxEpGZnAcHP8PJrpTUtPt67QOgJczyvF2Md
        Javascript Key This key should be used when making requests from JavaScript running on a user's machine.
        It adheres to object-level permissions.

    2faRfXr9jAQ0qKj53LPd6qrCpLcXuviVyuM3TJiH
        .NET Key This key should be used when making requests from a Windows application.
        It adheres to object-level permissions.

    DkxN9aMSMCiExHIf1bovtHDxz5bL1YrDqE3PHjp0
        REST API Key This key should be used when making requests to the REST API.
        It also adheres to object level permissions.

    UJD2ZG2001q7VoMLnTqpa91iTVJyXa6Sz1o25Icf
        Master Key This key is only allowed to access the REST API and does not adhere to object level permissions.
        This should be kept secret.

    Q2Y0rpMMSRve2TSZnwrZXpxRAvfPSpmU7s2VRr8G

    #### CONFIGURE:

    AudioToolbox.framework
    CFNetwork.framework
    CoreGraphics.framework
    CoreLocation.framework
    libz.dylib
    MobileCoreServices.framework
    QuartzCore.framework
    Security.framework
    StoreKit.framework
    SystemConfiguration.framework

    Connect your app to Parse

    Open up your AppDelegate.m file and add the following import to the top of the file:
    #import <Parse/Parse.h>

    Before continuing, select your Parse app from the menu at the right. These steps are for your "iGotTodo" app.

    Then paste the following inside the application:didFinishLaunchingWithOptions: function:
    [Parse setApplicationId:@"6N9mRm1Sh0sLc0vFctenTsFj17lgHuCLQvM6akcB"
    clientKey:@"i96WdFxEpGZnAcHP8PJrpTUtPt67QOgJczyvF2Md"];

    And to track statistics around application opens, add the following below that:
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];

    Compile and run!


30-Nov: Setup cocoapods
    web:
        http://cocoapods.org/
        http://www.raywenderlich.com/64546/introduction-to-cocoapods-2
        http://guides.cocoapods.org/syntax/podfile.html
        http://www.melissadbain.com/quick-and-dirty-how-to-install-parse-and-facebook-sdk-to-your-xcode-project/
    install:
        $ sudo gem install cocoapods
        goto the project folder and setup pod file
        $ pod init
        open pod file using xcode to avoid quotes issue in textedit
        $ open -a Xcode Podfile

        include this line in the pod file and run the next command
        pod 'Parse'
        $ pod install

    Sample
        source 'https://github.com/CocoaPods/Specs.git'

        platform :ios, '6.0'
        inhibit_all_warnings!

        xcodeproj 'MyProject'

        pod 'ObjectiveSugar', '~> 0.5'

        target :test do
            pod 'OCMock', '~> 2.0.1'
        end

        post_install do |installer|
            installer.project.targets.each do |target|
                puts "#{target.name}"
            end
        end



