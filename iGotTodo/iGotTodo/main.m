//
//  main.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 28/01/13.
//  Copyright (c) 2013 bshokeen@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
