//
//  ToolbarCell.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/22.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

/** This class is mapped to a collection ui cell used as toolbar */

#import <UIKit/UIKit.h>

@interface ToolbarCell : UICollectionViewCell

// Controls
@property (weak, nonatomic) IBOutlet UIImageView *mainImg;
@property (weak, nonatomic) IBOutlet UIImageView *highlightImg;
@property (weak, nonatomic) IBOutlet UILabel *name;

// Operations
-(void) resetInitialText;

// Action


@end
