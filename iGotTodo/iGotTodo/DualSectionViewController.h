//
//  DualSectionViewController.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/08.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDualViewController.h"
#import "AppDelegate.h"

@interface DualSectionViewController : CommonDualViewController
                            <UISearchBarDelegate, UIGestureRecognizerDelegate>



@property (strong, nonatomic) IBOutlet UISearchBar *uiSearchBar; //converted to strong to let show/hide work on scroll
@property (weak, nonatomic) IBOutlet UIView *totalRowsView;
@property (weak, nonatomic) IBOutlet UILabel *totalRowsLabel;
@property (weak, nonatomic) IBOutlet UIButton *totalRowsBtn;



@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *SwipeGesture_DOWN;
- (IBAction)actOnSwipeGesture:(UISwipeGestureRecognizer *)sender;

- (IBAction)leftNavBarItemClicked:(id)sender;
- (IBAction)totalRecordsBtnClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtnClick:(id)sender;
- (IBAction)addBtnClick:(id)sender;
- (IBAction)filterBtnClick:(id)sender;

@end
