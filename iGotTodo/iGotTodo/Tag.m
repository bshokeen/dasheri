//
//  Tag.m
//  iGotTodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "Tag.h"
#import "Task.h"


@implementation Tag

@dynamic color;
@dynamic created_on;
@dynamic deleted_on;
@dynamic dirty;
@dynamic icon_id;
@dynamic modified_on;
@dynamic name;
@dynamic sort_order;
@dynamic tag_id;
@dynamic taskS;

@end
