//
//  SectionHeaderView.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/11/16.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//

#import "SectionHeaderView.h"
#import <QuartzCore/QuartzCore.h>
#import "AppUtilities.h"

@implementation SectionHeaderView

@synthesize collapseImg=_collapseImg, sectionIndexLbl = _sectionIndexLbl, sectionNameLbl = _sectionNameLbl, sectionCountBtn = _sectionCountBtn, sectionIsOpen=_sectionIsOpen, sectionIdx = _sectionIdx, sectionItemsCount = _sectionItemsCount, groupManagedObjectID = _groupManagedObjectID;

-(void) updateSectionViewData:(BOOL)isOpen Index:(NSInteger)sectionIndex Name:(NSString *)sectionName Count:(NSInteger)sectionItemsCount GrpMgdObjID:(NSString *)groupManagedObjectID Tag:(NSInteger) tagNumber Delegate:(id<SectionHeaderViewDelegate>)delegate {
    
    DLog(@"SECTION INIT: Open:%@ section:%i name:%@ count:%i Tag:%i Delegate:%@ ", isOpen?@"YES":@"NO", sectionIndex, sectionName, sectionItemsCount, tagNumber, [delegate description]);
    // Attributes
    self.sectionIsOpen = isOpen;
    self.sectionIdx = sectionIndex;
    self.sectionItemsCount = sectionItemsCount;
    self.groupManagedObjectID = groupManagedObjectID;
    self.tag = tagNumber;
    self.delegate = delegate; // To invoke the delegate methods and notify section open/close
    
    // Populate Values
//    self.selected = self.sectionIsOpen;
    if (self.sectionIsOpen) {
        self.backgroundColor = [UIColor colorWithHue:0.612 saturation:.04 brightness:.96 alpha:1.0];
    }
    else {
        ///DDO
    }
    if (self.sectionIsOpen) {
        UIImage *img = [UIImage imageNamed:[AppUtilities getImageName:@"sectionOpen"]];
        [self.collapseImg setImage:img];
    }
    else {
        UIImage *img = [UIImage imageNamed:[AppUtilities getImageName:@"sectionClosed"]];
        [self.collapseImg setImage:img];
    }
    
//    self.sectionIndexLbl.text = [[NSString stringWithFormat:@"%d", (self.sectionIdx + 1)] stringByAppendingString:@": "]; // Increasing section index by 1
    self.sectionIndexLbl.text = [NSString stringWithFormat:@"%d", (self.sectionIdx + 1)];
    self.sectionNameLbl.text = [sectionName uppercaseString];
    [self.sectionCountBtn setTitle:[NSString stringWithFormat:@"%d", self.sectionItemsCount] forState:UIControlStateNormal];
    
    // Handle the line separators
    if (self.sectionIdx == 0) {
        self.groupTopLineLbl.hidden = YES;
    }
    else {
        self.groupTopLineLbl.hidden = NO;
    }
    
    if (self.sectionIsOpen) {
        self.groupBottomLineLbl.hidden = NO;
    }
    else {
        self.groupBottomLineLbl.hidden = YES;
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOpen:)];
    [self addGestureRecognizer:tapGesture];
 
    // Set Gradience: it was not automatically expanding with orientation, so set up the highlight color and tint colors
    //    [self setGradience];
}

// Added to get a tap across the cell/section header
-(void) toggleOpen:(UITapGestureRecognizer *) gesture {
    CGPoint pt = [gesture locationOfTouch:0 inView:self.sectionCountBtn];
    if( pt.x>0 && pt.y>0) { // Clicked Inside the Count Button - Notifiy to add item
        NSLog(@"Clicked inside the tbutton");
        if ([self.delegate respondsToSelector:@selector(uitableSectionHeaderView:Section:Operation:)]) {
            [self.delegate uitableSectionHeaderView:self Section:self.sectionIdx Operation: SECTION_ADD_ITEM];
        }
    }
    else { // Clicked Outside the Count Button - Toggle Section
            [self toggleOpenWithUserAction:YES];
    }
}
             

-(void) toggleOpenWithUserAction:(BOOL)userAction {
    //Toggle the section Image control state
    self.sectionIsOpen = !self.sectionIsOpen;
    
    // If this was a user action, send the delegate the appropriate message
    if (userAction && [self.delegate respondsToSelector:@selector(uitableSectionHeaderView:Section:Operation:)]) {
        if (self.sectionIsOpen) {
            [self.delegate uitableSectionHeaderView:self Section:self.sectionIdx Operation: SECTION_OPEN];
            NSLog(@"Toggle Section: OPENING - NewState:%@", self.sectionIsOpen ? @"YES" : @"NO");
        }
        else {
            [self.delegate uitableSectionHeaderView:self Section:self.sectionIdx Operation: SECTION_CLOSE];
            NSLog(@"Toggle Section: CLOSING - NewState:%@", self.sectionIsOpen ? @"YES" : @"NO");
        }
    }
    //    NSLog(@"Self: bounds: %f", self.bounds.size.height);
}

-(void) updateCountOnDelete:(int)recordsDeleted {
    self.sectionItemsCount = self.sectionItemsCount - recordsDeleted ;
    self.sectionCountBtn.titleLabel.text = [NSString stringWithFormat:@"%d", self.sectionItemsCount];
    DLog(@"updateCountOnDelete: Section: %@ Idx:%i ItemCount:%i", _sectionNameLbl.text, self.sectionIdx, self.sectionItemsCount);
}


- (IBAction)test:(id)sender {
    ULog(@"H");
}
@end
