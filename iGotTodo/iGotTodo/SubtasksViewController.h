//
//  NoteListViewController.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/24.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "SubtaskCell.h"
#import "Subtask.h"

@interface SubtasksViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,NSFetchedResultsControllerDelegate, SubtaskCellDelegate>

@property (weak, nonatomic) IBOutlet UITextField *addTxt;
@property (weak, nonatomic) IBOutlet UIButton *addSubtaskBtn;
@property (strong, nonatomic) IBOutlet UITableView *leftTable;

@property (strong, nonatomic) Task *task;


- (IBAction)addSubtaskBtnClick:(id)sender;

@end
