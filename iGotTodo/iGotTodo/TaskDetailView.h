//
//  TaskDetailView.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/16.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

/**
 *
 * This call was added to deal with controls displayed in the task details view.
 */


#import <UIKit/UIKit.h>
#import "Task.h"
#import "AppUtilities.h"


// Pulled code form subtasks to support subtask here also.
// Had to connect the view in storyboard to delegate as the Dual Seciton
// Table delegates were set to this view
@interface TaskDetailView : UIScrollView <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) IBOutlet UILabel *tagsLbl;
@property (nonatomic, strong) IBOutlet UILabel *reminderLbl;
@property (nonatomic, strong) IBOutlet UITextView *notesTxt;

@property (nonatomic, strong) IBOutlet UILabel *dayLbl;
@property (nonatomic, strong) IBOutlet UILabel *monthLbl;
@property (nonatomic, strong) IBOutlet UILabel *yearLbl;

// Added to display and interact with subtask
@property (weak, nonatomic) IBOutlet UITableView *subtaskTableView;
// Added to fetch the subtasks
@property (nonatomic, strong) Task *task;

-(void) reset;
-(void) initializeForTask:(Task *) task;

@end
