//
//  AFilterSettingsViewController.m
//  realtodo
//
//  Created by Balbir Shokeen on 9/3/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TabFilterViewController.h"
#import "AppDelegate.h"
#import "AppSharedData.h"
#import "TabTagsViewController.h"


@interface TabFilterViewController ()

@property(nonatomic, retain) NSArray *sortListArray;
@property (nonatomic, strong) NSString *selectedCateg;


@end

@implementation TabFilterViewController

@synthesize sortListArray, selectedCateg = _selectedCateg;

@synthesize selectedTags = _selectedTags;
@synthesize selectedOrderBy = _selectedOrderBy;
@synthesize selectedCollapseOn = _selectedCollapseOn;
@synthesize selectedDisplayType = _selectedDisplayType;
@synthesize orderBy_btn = _orderBy_btn;
@synthesize selectedGroups = _selectedGroups;
@synthesize recentItemsOnlySwitch = _recentItemsOnlySwitch;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
	[super viewDidUnload];
}

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: Filter"];
    
    self.navigationController.navigationBar.tintColor = nil;
    sortListArray = [AppSharedData getInstance].dynamicViewData.sortCategoriesValArr;
    // On change directly update the values, same can be done in settings view
    [_recentItemsOnlySwitch addTarget:self action:@selector(didChangeRecentItemsOnlySwitchControl:) forControlEvents:UIControlEventValueChanged];
    
    // Added this button so when we open this view from dual section, we have a button to perform cancel
    if ( self.filterLaunchedFrom == ModelView ) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(leftNavCancelBtnClick:)];
    }
    
    //self.selectedGroup = APP_SHARED_DATA.lastSelectedGroup;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
            
        case 0: {
            switch (indexPath.row) {
                case 0:
                case 1:
                case 2: {
                    [self performSegueWithIdentifier:@"ShowFilterItemDetailsViewController" sender:self];
                    break;
                }
                case 3: {
                    // segue is defined directly on cell.
                    break;
                }
                case 4: {
//                    [self performSegueWithIdentifier:@"ShowGroupViewController" sender:self]; // As the segue is defined from tableview sell, no need to call it for row selection.
                    break;
                }
                default:
                    DLog(@"Error, Not expected to come here.");
                    break;
            }
            break;
        }
            
        case 1: {
            switch (indexPath.row) {
                case 0: {
                    [self resetToDefaults]; //Added a btn click to do this but leaving this old code for reference.
                    break;
                }
                default:
                    break;
            }
        }
        default:
            break;
    }
}


- (void) resetToDefaults {
    [self AnimateView];
    
    [[AppSharedData getInstance].dynamicViewData resetToDefaultValues];
    [self viewWillAppear:YES];
}

-(void) AnimateView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.view cache:NO];
    [UIView commitAnimations];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowTagViewController"] ) {
        
        TabTagsViewController *tvCtrl = (TabTagsViewController *) [segue destinationViewController];
        [tvCtrl initWithoutTask:SOURCE_UI_FILTERUI];
    }
    
    else if ( [[segue identifier] isEqualToString:@"ShowGroupViewController"] ) {
        
        TabGroupsViewController *grpEditViewCntlr = (TabGroupsViewController *)[segue destinationViewController];
        //                grpEditViewCntlr.task = self.task;
        
        [grpEditViewCntlr initializeWithMode:ReadOnlyMultipleSelection 
                                      LastSelGroupSet: [AppSharedData getInstance].dynamicViewData.groupSet
                                           Context:APP_DELEGATE_MGDOBJCNTXT
                                          Delegate:nil];
    }
    
    else if ([[segue identifier] isEqualToString:@"ShowFilterItemDetailsViewController"]) {
        FilterItemDetailsViewController *fiCtrl = (FilterItemDetailsViewController *) [segue destinationViewController];
        fiCtrl.delegate = self;
        
        NSIndexPath *selIdxP = [self.tableView indexPathForSelectedRow];
        if (selIdxP && selIdxP.section == 0) {
            int sortItemsIdx = selIdxP.row;
            [AppSharedData getInstance].dynamicViewData.sortCategoriesIdx = sortItemsIdx;
            
            NSString *selectedSectionTitle = [[AppSharedData getInstance].dynamicViewData.sortCategoriesValArr objectAtIndex:sortItemsIdx];
            
            [fiCtrl setSectionTitle:selectedSectionTitle];
            [fiCtrl setSortItemIdx:sortItemsIdx];
        }
    }
}


- (void) viewWillAppear:(BOOL)animated {
    
    NSSet *tags = [AppSharedData getInstance].dynamicViewData.tagSet;
    _selectedTags.text = [AppUtilities getSelectedTagsLabelText:tags];
    
    NSSet *groups = [AppSharedData getInstance].dynamicViewData.groupSet;
    _selectedGroups.text = [AppUtilities getSelectedTagsLabelText:groups];
    
    int orderByIdx = [AppSharedData getInstance].dynamicViewData.orderByIdx;
    _selectedOrderBy.text = [[AppSharedData getInstance].dynamicViewData.orderByUIValueArr objectAtIndex:orderByIdx];
    
    _selectedCollapseOn.text = [[AppSharedData getInstance].dynamicViewData.collapsibleOnUIValueArr objectAtIndex:[AppSharedData getInstance].dynamicViewData.collapsibleOnIdx];
    
    _selectedDisplayType.text = [[AppSharedData getInstance].dynamicViewData.displayTypeValArr objectAtIndex:[AppSharedData getInstance].dynamicViewData.displayTypeIdx];
    
    _selectedGroups.text = [AppUtilities getSelectedTagsLabelText:[AppSharedData getInstance].dynamicViewData.groupSet];
    
    [self setOrderBtnImage];
    
    [self.recentItemsOnlySwitch setOn:[AppSharedData getInstance].dynamicViewData.recentItemsOnly];
}


//#pragma mark - GroupEditViewControllerDelegate API Implementation
//-(void) groupEditViewControllerDidFinish:(id)controller SelectedGroup:(NSMutableSet *) groupSet
//{
//    [AppSharedData getInstance].dynamicViewData.groupSet = groupSet;
//
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (void) groupEditViewControllerDidCancel:(__autoreleasing id *)controller {
//    //    //no need to save any input when the user taps Cancel, so this method simply dismisses the add scene
//    //    [self dismissViewControllerAnimated:YES completion:nil];
//    // Don't pass current value to the edited object, just pop.
//    [self.navigationController popViewControllerAnimated:YES];
//}


#pragma mark - FilterItemViewControllerDelegate API Implementation
- (void)filterItemTVControllerDidCancel:(FilterItemDetailsViewController *)controller
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


-(void) setOrderBtnImage {

    // For idx 1 = Gain data with low value is high visually. So sorting direction is reversed for showing on UI to match user expectations and internally works the same like other attributes when used for query building.
    
    if ([AppSharedData getInstance].dynamicViewData.orderByDirection)
    {
        if ([AppSharedData getInstance].dynamicViewData.orderByIdx == 1) {
             [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderUP"]] forState:UIControlStateNormal];
        }
        else {
            [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderDOWN"]] forState:UIControlStateNormal];
        }
        
    }
    else {
        if ([AppSharedData getInstance].dynamicViewData.orderByIdx == 1) {
             [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderDOWN"]] forState:UIControlStateNormal];
        }
        else {
            [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderUP"]] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)recentItemsBtnClick:(id)sender {
    [AppSharedData getInstance].dynamicViewData.recentItemsOnly = YES;
    [self switchOver];
}

- (IBAction)setOrderDirection:(id)sender {
    
    // For idx 1 = Gain data with low value is high visually. So sorting direction is reversed for showing on UI to match user expectations and internally works the same like other attributes when used for query building.

    if ([AppSharedData getInstance].dynamicViewData.orderByDirection) {
        [AppSharedData getInstance].dynamicViewData.orderByDirection = NO;
        
        if ([AppSharedData getInstance].dynamicViewData.orderByIdx == 1) {
            [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderDOWN"]] forState:UIControlStateNormal];
        }
        else {
            [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderUP"]] forState:UIControlStateNormal];
        }
    }
    else {
        
        [AppSharedData getInstance].dynamicViewData.orderByDirection = YES;
        
        if ([AppSharedData getInstance].dynamicViewData.orderByIdx == 1) {
            [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderUP"]] forState:UIControlStateNormal];
        }
        else {
            [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderDOWN"]] forState:UIControlStateNormal];
        }
    }

}

- (void)didChangeRecentItemsOnlySwitchControl:(UISwitch *)control {
    
    [AppSharedData getInstance].dynamicViewData.recentItemsOnly = control.isOn;
    
}

// Set the highligth color same as used by the application across
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.selectedBackgroundView = [AppUtilities getSelectedBackgroundView];
    cell.textLabel.highlightedTextColor    = [AppUtilities getSelectedTextHighlightColor];
}

// Commmon operation handling ot go back the navigation controller or switch to tab.
// If called by clicking a button use View Mode else Tab Mode.
-(void) switchOver {
    
    if (self.filterLaunchedFrom == View) {
            [self.navigationController popViewControllerAnimated:YES];
    }
    else if ( self.filterLaunchedFrom == ModelView) {
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDel switchFocusToTabIdx:MANAGE_TAB_IDX];
        
    }
}

- (IBAction)rightNavItemResetBtnClick:(id)sender {
    
    [self resetToDefaults];
}

// Allow keeping the Filter tab in the main tab and switch over to main view soon on the selection or reset.
- (IBAction)bottomResetBtnClick:(id)sender {
    [self resetToDefaults];

    [self switchOver];
}

- (IBAction)bottomSaveBtnClick:(id)sender {

    [self switchOver];
}

- (IBAction)leftNavCancelBtnClick:(id)sender {
    [self switchOver];
}

@end

