//
//  MasterViewController.m
//  iGotTodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabAllTasksSortViewController.h"
#import "AppDelegate.h" // Included to get the manged object context macro
/*
 @interface MasterViewController () {
 NSMutableArray *_objects;
 }
 @end
 */
@interface TabAllTasksSortViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic) BOOL showAll;
@property NSInteger prevSortSelectedIndex;
@property (nonatomic, strong) NSString *origTitle; // To allow showing the title of the caller or the original title


// This is a temp var, set in the child when gesture is swiped and then used in the segue defined in this class to process data on this item
@property (weak, nonatomic) Task *passToSegueSwipedOnIdxPath;

@end


@implementation TabAllTasksSortViewController

@synthesize sortSegment = _sortSegment;
@synthesize operationMode = _operationMode;

//@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController=_fetchedResultsController;
@synthesize showAll = _showAll;
@synthesize prevSortSelectedIndex = _prevSortSelectedIndex;

@synthesize lastSelGroupTogetDetails = _lastSelGroupTogetDetails;
@synthesize lastSelTagTogetDetails = _lastSelTagTogetDetails;
@synthesize origTitle = _origTitle;

@synthesize passToSegueSwipedOnIdxPath = _passToSegueSwipedOnIdxPath;

#pragma mark View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: All Tasks"];
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.

    [self.sortSegment setTitle:NSLocalizedString(@"Gain", @"Gain") forSegmentAtIndex:1];
    [self.sortSegment setTitle:NSLocalizedString(@"Star", @"Star") forSegmentAtIndex:2];
    [self.sortSegment setTitle:NSLocalizedString(@"%", @"%") forSegmentAtIndex:3];
    [self.sortSegment setTitle:NSLocalizedString(@"Alert", @"Alert") forSegmentAtIndex:4];
    [self.sortSegment setTitle:NSLocalizedString(@"Due", @"Due") forSegmentAtIndex:5];

    self.origTitle = self.title; // Backup title during load to avoid switching between the original and custom
    
//    DLog(@"SortViewController: MgdObj: %@", APP_DELEGATE_MGDOBJCNTXT);
//    [self.tableView setContentOffset:CGPointMake(0,self.sortSegment.bounds.size.height) animated:YES];
    
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    // Ability to tap on the tile bar item.
    UITapGestureRecognizer* tapNavTitleDoubleRecon = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(navigationBarTitleDoubleTap:)];

    tapNavTitleDoubleRecon.numberOfTapsRequired = 2;
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] setUserInteractionEnabled:YES];
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] addGestureRecognizer:tapNavTitleDoubleRecon];
    
    [self.sortSegment addTarget:self
                          action:@selector(didChangeSortSegmentControl:)
                forControlEvents:UIControlEventValueChanged];

    // Register swipe
    [self registerSwipe];
}

-(void) registerSwipe{
    
    // Adding ability to perform swipe on task cell
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(oneFingerSwipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
}

- (void)oneFingerSwipeLeft:(UITapGestureRecognizer *)recognizer {
    //    ULog(@"swipe left"); // IT show a message when swiped on section header, so disabled
}

- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    /* DISABLING IT AS IT IS NOT SO USEFUL HERE and rather taking user to sublist view
     // Insert your own code to handle swipe right
     // Get the point of swipe, find the indexpath and from that index path find the actual task. Task is updated and gets refreshed in tableview via the nsfetchresultcontroller changes
     
     CGPoint location = [recognizer locationInView:self.leftTable];
     NSIndexPath *swipedIndexPath = [self.leftTable indexPathForRowAtPoint:location];
     UITableViewCell *swipedCell  = [self.leftTable cellForRowAtIndexPath:swipedIndexPath];
     TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
     
     [AppUtilities markUnmarkTaskComplete: taskViewCell.task ];*/
    
     /* Disabled all this aslo as swiping ot go to next view was not convienent and intiuitive, so made an area to tap and do this segue
    CGPoint location = [recognizer locationInView:self.tableView];
    NSIndexPath *swipedIndexPath = [self.tableView indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [self.tableView cellForRowAtIndexPath:swipedIndexPath];
    TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
    //Now cache the task identified for passing to segue api
    self.passToSegueSwipedOnIdxPath = taskViewCell.task;
    
//    NSLog(@"Debug: Task:%@ Cell:%@ X:%f Y:%f", taskViewCell.task.name, [taskViewCell description], location.x, location.y );
    if (self.passToSegueSwipedOnIdxPath) { //Bug Fix: if swipe is unable to produce desired task, section closed may be, do nothing)
        [self performSegueWithIdentifier:@"ShowSubtasksViewController" sender:self];
    }
      */
}



- (void)navigationBarTitleDoubleTap:(UIGestureRecognizer*)recognizer {
    //    [self.tableView setContentOffset:CGPointMake(0,-self.sortSegment.bounds.size.height) animated:YES];
    self.sortSegment.hidden = !self.sortSegment.hidden;
    if (self.sortSegment.hidden) {
        [self.tableView setContentOffset:CGPointMake(0,self.sortSegment.bounds.size.height) animated:YES];
    }
    else {
        [self.tableView setContentOffset:CGPointMake(0,0) animated:YES]; 
    }
    
    //    self.tableView.contentOffset = CGPointMake(0, -100);
    //    self.tableView.contentInset = UIEdgeInsetsMake(-30, 0, 0, 0);
}

- (void) viewWillAppear:(BOOL)animated {
    
    // if running in filter mode, back original title and set the custom title (keep original title already backuped up)
    switch (self.operationMode) {
        case FilterGroup:
        {
            self.title     = self.lastSelGroupTogetDetails.name;
        }
            break;
        case FilterTag:
        {
            self.title     = self.lastSelTagTogetDetails.name;
        }
            break;
            
        default:
        {
            // revert to original title
            self.title = self.origTitle;
        }
            break;
    }
    
    // Load the data on first run
    [self forceReloadThisUI];
}

- (void) viewWillDisappear:(BOOL)animated {
//    self.lastSelGroupTogetDetails = nil; // clear of the selection, //Don't clear now as otherwise when we come back it was not showing proper data. So using modes to ensure we use this variable appropriately
}
/*
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.contentOffset.y <= -40) {
        self.sortSegment.hidden = NO;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
       
    }
    if (scrollView.contentOffset.y > 40 ) {
//        if (self.sortSegment.hidden) {
//        } else {
        self.sortSegment.hidden = YES;
//        [self.tableView setContentOffset:CGPointMake(0,self.sortSegment.bounds.size.height) animated:YES];
//        }
    }
}*/

- (void)didChangeSortSegmentControl:(UISegmentedControl *)control {
    
    if (self.sortSegment.selectedSegmentIndex == 0 ) {
        
        if (self.showAll) {
            [self.sortSegment setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOff"]] forSegmentAtIndex:0];                
        }
        else {
            [self.sortSegment setImage:[UIImage imageNamed:[AppUtilities getImageName: @"checkOn"]]  forSegmentAtIndex:0];
        }
        self.showAll = !self.showAll;
        // Earlier not sure why it was commented out but again activating this operation.
        [self.sortSegment setSelectedSegmentIndex:self.prevSortSelectedIndex];
    }
    else {
        self.prevSortSelectedIndex = self.sortSegment.selectedSegmentIndex;
    }

    [self forceReloadThisUI];
}

// Copied from CoreDataBooks example
- (void)viewWillAppear
{
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
    [self setSortSegment:nil];
    // Release any properties that are loaded in viewDidLoad or can be recreated lazily.
    self.fetchedResultsController = nil;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = LANDSCAPE_WIDTH_FACTOR;
    else {
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = 0;
    }
    [self forceReloadThisUI];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return IS_PAD || (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    return YES;
}


#pragma mark Table view data source methods
/*
 The data source methods are handled primarily by the fetch results controller
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

// Customize the appearance of table view cells.

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    // Configure the cell to show the Tasks's details
    TaskViewCell *mvaCell = (TaskViewCell *) cell;
    [mvaCell resetInitialText];
    
    Task *activityAtIndex =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    [AppUtilities configureCell:mvaCell Task:activityAtIndex];
    //TODO: Make is a sinlge api call with congirecell in TaskViewCell itself. Bad code,  but adding to set the delgate for notifications back
    mvaCell.delegate = self;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"TaskViewCell";
    
    TaskViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    /*  // This is not being invoked in my case but still retained as code here.
    if (cell == nil) {
        NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (TaskViewCell *) [nib objectAtIndex:0];
    }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma mark Table view editing

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    // return YES;
    return YES;
}

/*
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
 //    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
 //        NSDate *object = [_objects objectAtIndex:indexPath.row];
 //        self.detailViewController.detailItem = object;
 //    }
 NSLog(@"MasterViewCOntroller: didSelectRowAtIndexPatch: I wanted to see if it is called");
 }*/

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        DLog(@"SortViewController: commitEditingStyle: Starting Delete");
        [AppUtilities deleteTask:[[self fetchedResultsController] objectAtIndexPath:indexPath]];        
    } 
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

#pragma mark Fetched results controller
//
//-(id) initWithMgdObj: (NSManagedObjectContext *)mgdobjctx {
//    DLog(@"SortViewController: MgdObj from CalendarVC: %@", mgdobjctx);
//    self.managedObjectContext = mgdobjctx;
//    
//    int count = [self fetchedResultsController].fetchedObjects.count;
//    DLog(@"SortViewController: %d", count);
//    return self;
//}
/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    NSString *sortKeyName = nil;
    BOOL sortOrderAscending = NO;
    switch( self.sortSegment.selectedSegmentIndex) {
        case 1:
        {
            sortOrderAscending = YES;
            sortKeyName = @"gain";
            break;
        }
        case 2:
        {
            sortOrderAscending = NO;
            sortKeyName = @"flag";
            break;
        }
        case 3:
        {
            sortOrderAscending = NO;
            sortKeyName = @"progress";
            break;
        }
        case 4:
        {
             sortOrderAscending = NO;
            sortKeyName = @"remind_on";
            break;
        }
        case 0:
        case 5:
        {
            sortOrderAscending = NO;
            sortKeyName = @"dueOn";
            break;
        }
        default:
        { // Somehow the selectedIndex was set as -1 when All Task UI is opened after using UISegment few times in previuos attempts. So considering that case setting this default value to prevent this bug.
            sortOrderAscending = NO;
            sortKeyName = @"dueOn";
            break;
        }
    }
    
    // Create the sort descriptors array.
    NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKeyName ascending:sortOrderAscending];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dueOnDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Using array to generated and of predicates
    NSMutableArray *predicates = [[NSMutableArray alloc] init ];
    if (self.showAll == YES) {
       [predicates addObject:[NSPredicate predicateWithFormat:@"(done == 0 || done == 1)"]];
    }
    else {
        [predicates addObject:[NSPredicate predicateWithFormat:@"(done == 0)"]];
    }
    // If called from Groups UI and group is set, add its filter.
    if (self.lastSelGroupTogetDetails != nil && self.operationMode == FilterGroup) { // if call is from Groups/specific UI check if the group is set and include this group filter to show tasks only for this group
        [predicates addObject: [NSPredicate predicateWithFormat:@"group == %@", self.lastSelGroupTogetDetails]];
    }

    // if called from Tags UI and tag is set, add its filter
    if (self.lastSelTagTogetDetails != nil && self.operationMode == FilterTag) { // if call is from Groups/specific UI check if the group is set and include this group filter to show tasks only for this group
        [predicates addObject: [NSPredicate predicateWithFormat:@" ANY tagS IN [cd] %@", [[NSMutableArray alloc] initWithObjects: self.lastSelTagTogetDetails, nil]]];
    }
    
    // Compound Predicates as AND Conditions
    if ([predicates count] > 0 ) {
        NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setPredicate:p];
    }
    
    // Create and initialize the fetch results controller.
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                    managedObjectContext:APP_DELEGATE_MGDOBJCNTXT
                                                                      sectionNameKeyPath:nil
                                                                                cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    // Memory management.
    return _fetchedResultsController;
    
}  

/*
 NSFetchedResultsController delegate methods to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    UITableView *tableView = self.tableView;
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        NSLog(@"SortViewController: Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI 
{    
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsController = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}




#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowUpdateTaskViewController"] )
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Task *task = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        UpdateTaskViewController *updateTaskViewController = [segue destinationViewController];
        updateTaskViewController.delegate = self;
        updateTaskViewController.task = task;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddTaskViewController"])
    {
        AddTaskViewController *addTaskViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        
        addTaskViewController.managedObjectContext = APP_DELEGATE_MGDOBJCNTXT;
        
        // Feature:See if viewing tasks of a specific group, if so, when adding a new task use that group as the group to add task
        if ( self.lastSelGroupTogetDetails != nil)
            [AppSharedData getInstance].lastSelectedGroup = self.lastSelGroupTogetDetails;
        
        /*if (self.lastSelTagTogetDetails != nil )
            [AppSharedData getInstance].lastSelectedTagsOnNewTask = [[NSMutableSet alloc] initWithObjects:self.lastSelTagTogetDetails, nil]; NOT WORKING*/
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowSubtasksViewController"])  {
        
        if (self.passToSegueSwipedOnIdxPath) { // if swipe is unable to produce desired task, section closed may be, do nothing)
            SubtasksViewController *nlVC = (SubtasksViewController *) [segue destinationViewController];
            nlVC.task = self.passToSegueSwipedOnIdxPath;
        }
    }
}

#pragma mark - Add/Update controller delegate
- (void) updateTaskViewControllerDidCancel:(UpdateTaskViewController *)controler {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) updateTaskViewControllerDidFinish:(UpdateTaskViewController *)controller task:(Task *)task WithDelete:(BOOL)isDeleted
{
    
    if (task) {
        if (isDeleted) {
            [AppUtilities deleteTask:task];
        }
        else {
            [AppUtilities saveTask:task];
        }
    }
    //    [self dismissModalViewControllerAnimated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
    //    [self.tableView reloadData];
    
    [self forceReloadThisUI];
}

#pragma mark - TaskViewCell Delegate
-(void) notifyShowSubtasksClicked:(Task *)task Cell:(TaskViewCell *)cell {
    //Now cache the task identified for passing to segue api
    self.passToSegueSwipedOnIdxPath = task;
    
    //    NSLog(@"Debug: Task:%@ Cell:%@ X:%f Y:%f", taskViewCell.task.name, [taskViewCell description], location.x, location.y );
    if (self.passToSegueSwipedOnIdxPath) { //Bug Fix: if swipe is unable to produce desired task, section closed may be, do nothing)
        [self performSegueWithIdentifier:@"ShowSubtasksViewController" sender:self];
    }
}

@end
