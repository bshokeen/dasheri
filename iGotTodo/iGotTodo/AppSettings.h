//
//  AppSettings.h
//  iGotTodo
//  This class is meant to wrap apis on top of the settings stored in plist file. 
//  Created by Balbir Shokeen on 2013/08/05.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject

-(bool) settingExists;

// Show tasks which are completed
-(int) showCompletedTask;
// Auto Delete the completed tasks
-(int) autoDeleteCompletedTask;
// User Selected Default Gain when adding new tasks
-(int) defaultGainIdx;
// User selected effort count index
-(int) defaultEffortCountIdx;
// User selected effort unit
-(int) defaultEffortUnitIdx;
// RUn once on this device to fix the sort order of all the tasks
-(int) runOnceOnDeviceFixSortOrderOfAllTasks;
// Run Once for all devices using this app by inserting the default data
-(int) runOnceAcrossDeviceInsertedDefaultData;
// Selected user language preference is stored
-(int) addOnLanguageUserSelection;
// Run once on this device to upgrade notes
-(int) runOnceOnDeviceUpgradeNotesToSubtasks;


-(void) saveSettingForShowCompletedTask       : (int) showCompletedTask;
-(void) saveSettingForAutoDeleteCompletedTask : (int) autoDeleteCompletedTask ;
-(void) saveSettingForDefaultGainIdx          : (int) defaultGainIdx;
-(void) saveSettingForDefaultEffortUnitIdx    : (int) defaultEffortUnitIdx;
-(void) saveSettingForDefaultEffortCountIdx   : (int) defaultEffortCount;
-(void) saveSettingForRunOnceOnDeviceFixSortOrderOfAllTasks: (int) runOnceOnDeviceFixSortOrderOfAllTasks;
-(void) saveSettingForRunOnceAcrossDeviceInsertedDefaultData: (int) runOnceAcrossDeviceInsertedDefaultData;
-(void) saveSettingForAddOnLanguageUserSelection: (int) addOnLanguageUserSelection;
-(void) saveSettingForRunOnceOnDeviceUpgradeNotesToSubtasks:(int)runOnceOnDeviceUpgradeNotesToSubtasks;
@end
