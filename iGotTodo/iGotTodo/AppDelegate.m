//
//  AppDelegate.m
//  iGotTodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AppUtilities.h"



/******* Set your tracking ID here *******/
static NSString *const kTrackingId       = @"UA-57208081-1";
static NSString *const kAllowTracking    = @"allowTracking";
static NSString *const kFlurryTrackingID = @"GCVJQP6XPFGK6MX5WMVZ";

/******* Set your Parse ID here    *******/
static NSString *const kParseAppID       = @"6N9mRm1Sh0sLc0vFctenTsFj17lgHuCLQvM6akcB";
static NSString *const kParseClientKey   = @"i96WdFxEpGZnAcHP8PJrpTUtPt67QOgJczyvF2Md" ;


@interface AppDelegate ()



// Used for sending Google Analytics traffic in the background.
@property(nonatomic, assign) BOOL okToWait;
@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);


@end


@implementation AppDelegate

@synthesize window = _window;
@synthesize tabBarController = _uiTabBarController;

@synthesize managedObjectContext = __managedObjectContext, managedObjectModel = __managedObjectModel, persistentStoreCoordinator = __persistentStoreCoordinator;

-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    // Flurry Integration
    [self activateFlurryIntegration];
    
    // Google Integration
    [self activateGoogleAnalytics];
    
    // Parse Integration
    [self activateParseFramework: application WithOptions: launchOptions];
    
    // Set up the locale language if user has setup their own language
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *locale = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    
    //    NSLog(@"Language Setting: Lang:%@ Locale:%@ All:%@", language, locale, [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);

        DLog(@"AppDelegate: Language Setting: Lang:%@ Locale:%@", language, locale);

    // Handle launching from a notification
    UILocalNotification *notif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    // If not nil, the application was based on an incoming notifiation
    if (notif)
    {
        DLog(@"AppDelegate: Notification initiated app startup");
        
        // Access the payload content
        DLog(@"AppDelegate: Notification payload: %@", [notif.userInfo objectForKey:@"payload"]);
        
        [self.tabBarController setSelectedIndex: ALERT_TAB_IDX]; // Start the app with a specific view open.
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Task Reminder", @"Task Reminder")
                                                        message:notif.alertBody
                                                       delegate:self cancelButtonTitle:OK
                                              otherButtonTitles:nil];
        [alert show];
        
        // As alert has fired now handle the reset of the alert
        [self resetReminder:notif ShowAlertUI:YES];
    }
    
    /*
    TabAlertViewController *alertViewController;
    UINavigationController *alertViewCtrlNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:ALERT_TAB_IDX];
    alertViewController = (TabAlertViewController *) [[alertViewCtrlNavigCntrl viewControllers] objectAtIndex:0];
  
    
    NSManagedObjectContext *context = [self managedObjectContext];
    alertViewController.mgdObjContext = context;
    */
    return YES;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Prepare required, mandatory initializations
    self.tabBarController = (UITabBarController *) self.window.rootViewController; // Kept here before being used anywhere else
    
    // Exclude the alert tab from being rearranged else it was getting difficult to update the badge on it.
    NSMutableArray *mutArr = [self.tabBarController.customizableViewControllers mutableCopy];
    [mutArr removeObjectAtIndex:ALERT_TAB_IDX];
    self.tabBarController.customizableViewControllers = [mutArr copy];
    
    // Restore the order of tabs. It was slowing down but for now feel better keep disabled. There is no reset back to default option so not enabling this feature for now.
    //[self restoreSavedTabOrderSequnce: self.tabBarController]; // Kept here before alert index is tried

    /* Dynamic Way finding worked fine but badge udpate was having issues
    int alertTabIdx = [self findIndexOfAlertTab1:self.tabBarController];
    self.tabBarController.customizableViewControllers = [[NSArray alloc] initWithObjects:[[self.tabBarController viewControllers] objectAtIndex:alertTabIdx], nil];
    */
    
    // One time init - not aware if it is really needed here but added
    [AppSharedData getInstance];
    
      // Initialized shared data holder - not sure if needed
    [self managedObjectContext];
    
    // BEFORE APPLICATION STARTS UP, RUNONCE Operations like performing the upgrade.
    [AppUtilities runOnceOperations];

    // Prepare notifications
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];

        DLog(@"AppDelegate: List all the notifications at startup");
        // Get list of local notifications
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (UILocalNotification *item in notificationArray) {
            DLog(@"AppDelegate: Scheduled Notif Item - Body: %@, fire: %@ repeat:%d", item.alertBody, [item.fireDate description], item.repeatInterval);
        }

        // On start of the application the alert badge count to be updated from the database.
        [AppUtilities updateAlertBadge:2];
    
    return YES;
}

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    // Handle the notificaton when the app is running
    DLog(@"Recieved Notification %@",notif);
    
    DLog(@"Show the list of notifications...");
    // Get list of local notifications
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *item in notificationArray) {
        DLog(@"Notif Body: %@, fire: %@ repeat:%d", item.alertBody, [item.fireDate description], item.repeatInterval);
    }
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    AudioServicesPlaySystemSound(1005);
    
    // Access the payload content
    //	NSLog(@"Notification payload: %@", [notification.userInfo objectForKey:@"payload"]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Task Reminder", @"Task Reminder")
                                                    message:notif.alertBody
                                                   delegate:self cancelButtonTitle:OK
                                          otherButtonTitles:nil];
    [alert show];
    
    // As alert has fired now handle the reset of the alert. Enabling the UI in this mode was having issue if we save the taks and show the alert immediately. View siwtch to alert but managedcontext is not updated.
    [self resetReminder:notif ShowAlertUI:NO]; 
        
	DLog(@"Incoming notification in running app");
}

-(void) resetReminder: (UILocalNotification *) notif ShowAlertUI:(bool) showAlertUI {
    
    NSDictionary *infoDict = (NSDictionary *) notif.userInfo;
    NSString *taskObjectID = [infoDict objectForKey:@"TaskObjectID"];
    
    if (taskObjectID.length == 0) {
        DLog(@"Error, unable to find the task Object ID for this reminder");
    }
    else {
        NSManagedObject *managedObjectTask= [[self managedObjectContext] objectWithID:[[self persistentStoreCoordinator] managedObjectIDForURIRepresentation:[NSURL URLWithString:taskObjectID]]];
        
        Task *task = (Task *) managedObjectTask;
        
        // TODO: As some of the tasks were deleted by the time alert was notified we got an exception. Ideally it should not happen and all deleted tasks to delete their notifications. 
//        to let this issue keep breaking the code and get addressed properly, not putting a check and putting a TODO
        /* TODO: to see how it behaves if alerts are not cleared commenting it temporarily
        if ([task.remind_repeat_interval intValue] == 0) {
            task.remind_on = nil;   
            task.remind_repeat_interval = [NSNumber numberWithInt: NSEraCalendarUnit];
        }
        */
        // Added 2 additional attributes to track if the notificaiton is ON and the time of notification
        task.alert_notification = [NSNumber numberWithInt: 1];
        task.alert_notification_on = [NSDate date];
        
        // Not required so far but additionally added to save immediately
        [AppUtilities saveTask:task];
        
        // Increment the alertBadge
        [AppUtilities updateAlertBadge:1];

        //Moved showing of alert ui code from here else it was switching to alert ui while we submit from update and want to go back to manage UI and giving error. So disabling switch to alert tab when app is running
        if (showAlertUI) {
            [self showAlertNotificationWithHighlight];
        }
    }
}

-(void) showAlertNotificationWithHighlight {
    [self.tabBarController setSelectedIndex:ALERT_TAB_IDX]; // Start the app with a specific view open.
    // Code to assist highlighting the recently notified item
    UINavigationController *alertViewCtrlNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:ALERT_TAB_IDX];
    TabAlertViewController *alertViewController = (TabAlertViewController *) [[alertViewCtrlNavigCntrl viewControllers] objectAtIndex:0];
    if ( alertViewController != nil)
        [alertViewController selectTopItemNotifiedRecently];
    DLog(@"AppDelegate: Highlight tiem on Alert UI");
    
    
}


- (void)application:(UIApplication *)app
didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    DLog(@"Error in registration. Error: %@", err);
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self saveContext];
    
    //Google
    [self sendHitsInBackground];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if ([[AppSharedData getInstance].appSettings autoDeleteCompletedTask] == 1 )  {
        [AppUtilities deleteCompletedTasks:DAYS_60];
    }
    
    //Google
    [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error;
    if (__managedObjectContext != nil) {
        if ([__managedObjectContext hasChanges] && ![__managedObjectContext save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            ALog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc]
                                  //initWithConcurrencyType:NSMainQueueConcurrencyType];
                                  init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iGotTodo" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    DLog(@"Path: %@", modelURL);
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"iGotTodo.sqlite"];
    
    // To all lightweight migration adding these options. Passing it as options for Persistent Store Cordinator. Read the apple guide.
    //    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                       [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:optionsDictionary error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - State Restoration 
-(BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

-(BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

#pragma mark - Restore State of More Tab Reordering sequence
-(void) restoreSavedTabOrderSequnce: (UITabBarController *) tabBarControlr {
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SAVED_TAB_ORDER];
//    [[NSUserDefaults standardUserDefaults] synchronize];

    @try {
        NSArray *initialViewControllers = [NSArray arrayWithArray:tabBarControlr.viewControllers];
        
        NSUserDefaults *defaults           = [NSUserDefaults standardUserDefaults];
        NSArray *tabBarOrderRestorationIDs = [defaults arrayForKey:SAVED_TAB_ORDER];
        
        
        if (tabBarOrderRestorationIDs && [tabBarOrderRestorationIDs count] > 0 && initialViewControllers && [initialViewControllers count] > 0 ) {
            NSLog(@"AppDelegate: Trying to restore tab order from:%@", tabBarOrderRestorationIDs);
            
            NSMutableArray *newViewControllers = [NSMutableArray arrayWithCapacity:initialViewControllers.count];
            
            for (NSString *restorationID in tabBarOrderRestorationIDs) {
                bool foundSomething = NO;
                
                for (UIViewController *vc in initialViewControllers) {
                    if ( [restorationID isEqualToString: [vc restorationIdentifier]] ) {
                        
                        foundSomething = YES;
                        [newViewControllers addObject:vc]; // If found add it and break the loop to continue with others
                        
                        break;
                    }
                }
                if (!foundSomething) {
                    NSAssert(!foundSomething, @"One or more restoration ID missing so countine without restoring the tab order");
                    return;
                }
            }
            
            tabBarControlr.viewControllers = newViewControllers;
            
            NSLog(@"AppDelegate: Completed Tab order restore");
            
        } else {
            NSLog(@"AppDelegate: Skipped Tab OrderRestoring for:%@", tabBarOrderRestorationIDs);
        }
    }
    @catch (NSException *exception) {
        NSLog(@"AppDelegate: Encountered EXCEPTION on tab order restore. Removing the current saved value and continuing. %@", [exception reason]);
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SAVED_TAB_ORDER];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    @finally {
        //
    }
}

// Can no longer use a predefined index so need to search for the alert ui tab. But later preserved alert tab index and kept it on hold
-(int) findIndexOfTab: (UITabBarController *) tabBarControlr ForTabVC: (Class) tabVCClass {
    
    NSArray *initialViewControllers = [NSArray arrayWithArray:tabBarControlr.viewControllers];
    
    for (int i = 0; i < [initialViewControllers count]; i++) {
        UINavigationController *item = [initialViewControllers objectAtIndex:i];
        
        NSAssert([item isKindOfClass:[UINavigationController class]], @"Expectation that each and every tab is wrapped under navigation controller");
    
        UIViewController *uivc =[[item viewControllers] objectAtIndex:0];
        
        NSLog(@"Kind: %@", [[ uivc class] description]);
        if ( [uivc isKindOfClass:tabVCClass] ) {
            NSLog(@"AppDelegate: Found TabIndex: %i", i);
            return i;
        }
    }
    
    return 0; // If nothing found return 0;
}

-(bool) switchFocusToTabIdx:(int)tabVCClassType {
    
    int alertTabIdx=0;
    switch (tabVCClassType) {
        case MANAGE_TAB_IDX:
        {
            alertTabIdx = [self findIndexOfTab:self.tabBarController ForTabVC:[TabManageViewController class]];
        }
            break;
        case ALERT_TAB_IDX:
        {
            alertTabIdx = [self findIndexOfTab:self.tabBarController ForTabVC:[TabAlertViewController class]];
        }
        default:
            alertTabIdx = 0;
            break;
    }
    
    [self.tabBarController setSelectedIndex: alertTabIdx]; // Select the index found

    return YES;
}


/*** FLURRY ***/

- (void)activateFlurryIntegration {
    [Flurry setCrashReportingEnabled:NO];
    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
    [Flurry startSession:kFlurryTrackingID];
}

/*** Google ***/

- (void)activateGoogleAnalytics {
    
    // Ask User to opt in or opt out.
    //    [self askUserOptionForGoogleAnalytics];
    [GAI sharedInstance].optOut = NO; // No sensitive info transferred
    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:kAllowTracking];
    
    // If your app runs for long periods of time in the foreground, you might consider turning
    // on periodic dispatching.  This app doesn't, so it'll dispatch all traffic when it goes
    // into the background instead.  If you wish to dispatch periodically, we recommend a 120
    // second dispatch interval.
    // [GAI sharedInstance].dispatchInterval = 120;
    [GAI sharedInstance].dispatchInterval = 120;
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
    
    //    self.tracker = [[GAI sharedInstance] trackerWithName:@"Kees"
    //                                              trackingId:kTrackingId];
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
    
}
- (void) askUserOptionForGoogleAnalytics {
    
    if ( [[NSUserDefaults standardUserDefaults] objectForKey:kAllowTracking] == nil ) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Google Analytics" message:@"With your permission usage information will be collected to improve the application." delegate:self cancelButtonTitle:@"Opt Out" otherButtonTitles:@"Opt In", nil];
        [av show];
    } else {
        // User must be able to opt out of tracking
        [GAI sharedInstance].optOut =     ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0: {
            NSDictionary *appDefaults = @{kAllowTracking: @(NO)};
            [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
            [[NSUserDefaults standardUserDefaults] setObject:@(NO) forKey:kAllowTracking]; // without this it was not storing
            
            [[GAI sharedInstance] setOptOut:YES];
            break;
        }
        case 1: {
            NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
            [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
            [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:kAllowTracking];
            
            [[GAI sharedInstance] setOptOut:NO];
            break;
        }
        default:
            break;
    }
}

// In case the app was sent into the background when there was no network connection, we will use
// the background data fetching mechanism to send any pending Google Analytics data.  Note that
// this app has turned on background data fetching in the capabilities section of the project.
-(void)application:(UIApplication *)application
performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    //Google
    [self sendHitsInBackground];
    completionHandler(UIBackgroundFetchResultNewData);
}

// This method sends hits in the background until either we're told to stop background processing,
// we run into an error, or we run out of hits.  We use this to send any pending Google Analytics
// data since the app won't get a chance once it's in the background.
- (void)sendHitsInBackground {
    self.okToWait = YES;
    __weak AppDelegate *weakSelf = self;
    __block UIBackgroundTaskIdentifier backgroundTaskId =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        weakSelf.okToWait = NO;
    }];
    
    if (backgroundTaskId == UIBackgroundTaskInvalid) {
        return;
    }
    
    self.dispatchHandler = ^(GAIDispatchResult result) {
        // If the last dispatch succeeded, and we're still OK to stay in the background then kick off
        // again.
        if (result == kGAIDispatchGood && weakSelf.okToWait ) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskId];
        }
    };
    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
}

// Parse.com

-(void) activateParseFramework: (UIApplication *) application WithOptions:(NSDictionary *) launchOptions {
    
    [Parse setApplicationId:kParseAppID 
                  clientKey:kParseClientKey];
    
    // Parse analytics around application open
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    /** Enable Test Code below and click on their website:  https://www.parse.com/apps/quickstart#parse_data/mobile/ios/native/existing **/
    /*
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"foo"] = @"bar";
    [testObject saveInBackground];
     */
    
    // Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}
// Upload your .p12 certificate in the Apple Push certificates section of your app's push settings. If you don't have your .p12 certificate file, head on over to our provisioning tutorial and go through sections 1 through 4 to correctly configure your app and obtain the .p12 certificate file.


@end
