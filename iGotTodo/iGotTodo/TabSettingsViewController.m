//
//  SettingsViewController.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 9/6/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TabSettingsViewController.h"
#import "AppSharedData.h"

@interface TabSettingsViewController ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *gainSegmentControl;
@property (weak, nonatomic) IBOutlet UISwitch *showCompletedTask;
@property (weak, nonatomic) IBOutlet UISwitch *autoDeleteCompletedTask;
@property (weak, nonatomic) IBOutlet UILabel *effortLbl;
@property (weak, nonatomic) IBOutlet UIButton *gainImage;

@property (nonatomic)  NSUInteger gainIdx;
@property (nonatomic)  NSUInteger defaultEffortCountIdx;
@property (nonatomic)  NSUInteger defaultEffortUnitIdx;
@property (weak, nonatomic)  NSString *effortEstimation;


@end

@implementation TabSettingsViewController

@synthesize gainIdx;
@synthesize defaultEffortCountIdx = _defaultEffortCountIdx;
@synthesize defaultEffortUnitIdx = _defaultEffortUnitIdx;
@synthesize effortLbl = _effortLbl;
@synthesize effortEstimation = _effortEstimation;
@synthesize gainSegmentControl = _gainSegmentControl;
@synthesize gainImage = _gainImage;
@synthesize showCompletedTask = _showCompletedTask;
@synthesize autoDeleteCompletedTask = _autoDeleteCompletedTask;

@synthesize versionLabel = _versionLabel;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) setGainButtonImage:(NSUInteger) gainIndex {
    
    [_gainImage setImage:[AppUtilities getGainImage:gainIdx] forState:UIControlStateNormal];
    
}

-(void) customizeControlSegment {
    
    NSUInteger defaultGainIdx = [AppSharedData getInstance].defaultGainIdx;
    _gainSegmentControl.selectedSegmentIndex = defaultGainIdx;
    
    [self setGainButtonImage:defaultGainIdx];
    
}

- (void)didChangeGainSegmentControl:(UISegmentedControl *)control {
    
    [self setGainButtonImage:control.selectedSegmentIndex];
    
    //[self customizeControlSegment];
    //[_gainSegmentControl titleForSegmentAtIndex:control.selectedSegmentIndex];
    
}


- (void)viewDidLoad
{
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: Settings"];
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.gainSegmentControl setTitle:NSLocalizedString(@"High", @"High") forSegmentAtIndex:0];
    [self.gainSegmentControl setTitle:NSLocalizedString(@"Medium", @"Medium") forSegmentAtIndex:1];
    [self.gainSegmentControl setTitle:NSLocalizedString(@"Normal", @"Normal") forSegmentAtIndex:2];
    [self.gainSegmentControl setTitle:NSLocalizedString(@"Low", @"Low") forSegmentAtIndex:3];
   
    
    [self customizeControlSegment];
    
    [_gainSegmentControl addTarget:self action:@selector(didChangeGainSegmentControl:) forControlEvents:UIControlEventValueChanged];
    
    _gainSegmentControl.selectedSegmentIndex =[AppSharedData getInstance].defaultGainIdx;
    _defaultEffortUnitIdx = [AppSharedData getInstance].defaultEffortUnitIdx;
    _defaultEffortCountIdx = [AppSharedData getInstance].defaultEffortCountIdx;
    
    [_showCompletedTask setOn:NO];
    
    [super viewDidLoad];
    
}


-(void) viewWillAppear:(BOOL)animated {
    
    if (![AppSharedData getInstance].dynamicViewData.excludeConditionFinishedTasks) {
        [_showCompletedTask setOn:YES];
    }
    
    if (![AppSharedData getInstance].dynamicViewData.autoDeleteFinishedTasks) {
        [_autoDeleteCompletedTask setOn:NO];
    }
    
    _effortLbl.text = [NSString stringWithFormat:@"%@ %@", [[AppSharedData getInstance].effortCountArray objectAtIndex:_defaultEffortCountIdx], [[AppSharedData getInstance].effortUnitArray objectAtIndex:_defaultEffortUnitIdx]];
    
    
    // Update the version number
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersionName = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    DLog(@"Version Info: %@", majorVersionName );
    [self.versionLabel setText: [@"v" stringByAppendingString:majorVersionName]];
}

- (void)viewDidUnload
{
    [self setVersionLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewDidDisappear:(BOOL)animated {
    
    if ([_showCompletedTask isOn]) {
        [AppSharedData getInstance].dynamicViewData.excludeConditionFinishedTasks = NO;
        [[AppSharedData getInstance].appSettings saveSettingForShowCompletedTask: 1];
    }
    else {
        [AppSharedData getInstance].dynamicViewData.excludeConditionFinishedTasks = YES;
        
        if ([_showCompletedTask isOn])
            [[AppSharedData getInstance].appSettings saveSettingForShowCompletedTask: 1];
        else
            [[AppSharedData getInstance].appSettings saveSettingForShowCompletedTask: 0];
    }
    
    
    if ([_autoDeleteCompletedTask isOn]) {
        [AppSharedData getInstance].dynamicViewData.autoDeleteFinishedTasks = YES;
        [[AppSharedData getInstance].appSettings saveSettingForAutoDeleteCompletedTask: 1];
    }
    else {
        [AppSharedData getInstance].dynamicViewData.autoDeleteFinishedTasks = NO;
        
        if ([_autoDeleteCompletedTask isOn])
            [[AppSharedData getInstance].appSettings saveSettingForAutoDeleteCompletedTask: 1];
        else
            [[AppSharedData getInstance].appSettings saveSettingForAutoDeleteCompletedTask: 0];
    }
    
    [[AppSharedData getInstance].appSettings saveSettingForDefaultGainIdx:_gainSegmentControl.selectedSegmentIndex];
    [[AppSharedData getInstance].appSettings saveSettingForDefaultEffortUnitIdx:_defaultEffortUnitIdx];
    [[AppSharedData getInstance].appSettings saveSettingForDefaultEffortCountIdx: _defaultEffortCountIdx];

    APP_SHARED_DATA.lastSelGain =    _gainSegmentControl.selectedSegmentIndex;
    
}

- (IBAction) resetToDefaults:(id)sender {
    
    [self AnimateView];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"iGotTodo.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"iGotTodo.plist"] ];
    }
    
    
    NSMutableDictionary *data;
    
    if ([fileManager fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    [AppSharedData getInstance].dynamicViewData.excludeConditionFinishedTasks = YES;
    [[AppSharedData getInstance].appSettings saveSettingForShowCompletedTask: 0];
        
    [AppSharedData getInstance].dynamicViewData.autoDeleteFinishedTasks = YES;
    [[AppSharedData getInstance].appSettings saveSettingForAutoDeleteCompletedTask: 1];
    
    [[AppSharedData getInstance].appSettings saveSettingForDefaultGainIdx: 2];
    [[AppSharedData getInstance].appSettings saveSettingForDefaultEffortUnitIdx: 0];
    [[AppSharedData getInstance].appSettings saveSettingForDefaultEffortCountIdx: 10];
    
    APP_SHARED_DATA.lastSelGain =    2;
    APP_SHARED_DATA.lastSelEffortCount = 10;
    APP_SHARED_DATA.lastSelEffortUnit = 0;
    
    //To insert the data into the plist
    [data writeToFile:path atomically:YES];
    [self loadDefaultsInUI];
}

- (void) loadDefaultsInUI {
    
    _gainSegmentControl.selectedSegmentIndex =2;
    [self setGainButtonImage: 2];
    _defaultEffortUnitIdx = 0;
    _defaultEffortCountIdx = 10;
    [_autoDeleteCompletedTask setOn:YES];
    [_showCompletedTask setOn:NO];
    
    _effortLbl.text = [NSString stringWithFormat:@"%@ %@", [[AppSharedData getInstance].effortCountArray objectAtIndex:10], [[AppSharedData getInstance].effortUnitArray objectAtIndex:0]];
}

-(void) AnimateView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.20];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.view cache:NO];
    [UIView commitAnimations];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            // Google Custom
            [AppUtilities logAnalytics:@"Clicked: RateApp"];
            
            // Option to allow rating this app
            NSString* url = [NSString stringWithFormat: @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", @"598951800"];
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
            break;
        }
        case 3:
        {
            switch (indexPath.row) {
                case 0: {
                    [self exportTasksToEmail];
                    
                    // Google Custom
                    [AppUtilities logAnalytics:@"Clicked: Export"];

                    }
                    break;
                case 1: {
                    [self deleteFinishedTasks];

                    // Google Custom
                    [AppUtilities logAnalytics:@"Clicked: DeleteFinishedTasks"];

                }
                    break;
                default:
                    break;
            }
            break;
        }
        case 4:
        {
            switch (indexPath.row) {
                case 0: {
                    [self mailToSupport];
                    
                    // Google Custom
                    [AppUtilities logAnalytics:@"Clicked: Support"];


                }
                    break;
                case 1:
                    break;// version number
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"defaultEffortSetting"]) {
        
        EffortViewController *effortCtrl = (EffortViewController *) [segue destinationViewController];
        effortCtrl.delegate = self;
        [effortCtrl initForKeyName:@"setDefaultEffort" WithCountIdx:[AppSharedData getInstance].lastSelEffortCount WithUnitIdx:[AppSharedData getInstance].lastSelEffortUnit Delegate:self];
        
    }
    
}

- (void) effortViewControllerDidFinish:(EffortViewController *)controller WithKeyName:(NSString *)keyName WithEffortCount:(int)countIdx WithEffortUnit:(int)unitIdx {
    
    // change these lines to save button click function
    _defaultEffortUnitIdx = unitIdx;
    _defaultEffortCountIdx = countIdx;
    
}

#pragma mark - Email For Support Option
- (void) mailToSupport {
    
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"iGotTodo: Support!"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"dasheridev@gmail.com"]];
        //        [mailCont setMessageBody:@"Don't ever want to give you up" isHTML:NO];
        [mailCont setMessageBody:@"" isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
        
    }
}

#pragma mark - Email Export Option
- (void) exportTasksToEmail  {
    
    // MOVE This code the TaskDataController
    NSManagedObjectContext *moc = APP_DELEGATE_MGDOBJCNTXT;
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *taskEntity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:moc];
    
    [request setEntity:taskEntity];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"dueOn" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error= nil;
    
    NSMutableArray *mutableFetchResults = [[moc executeFetchRequest:request error:&error]  mutableCopy];
    
    // To avoid memory crash using Mutable String
    NSMutableString *fetchedData = [NSMutableString string];
    
    [fetchedData appendFormat:@"GroupName, TaskName, DueOn, Gain, Done, progress, CreatedOn, remind_on, notes \n"];
    
//    int i=0;
    for (Task *taskItem in mutableFetchResults) {
//        NSLog(@"i: %i", i++);
       [ fetchedData appendFormat:@"%@", [self prepareForDump:taskItem.group.name]];
       [ fetchedData appendFormat:@"\t, "];
       [ fetchedData appendFormat:@"%@",[self prepareForDump:taskItem.name]];
       [ fetchedData appendFormat:@", \t"];        
       [ fetchedData appendFormat:@"%@",[self handleNull:[taskItem.dueOn description]]];
       [ fetchedData appendFormat:@", \t"];        
       [ fetchedData appendFormat:@"%@",[NSString stringWithFormat:@"%i", [taskItem.gain intValue] ] ];
       [ fetchedData appendFormat:@", \t"];        
       [ fetchedData appendFormat:@"%@",[NSString stringWithFormat:@"%i", [taskItem.done  intValue]] ];
       [ fetchedData appendFormat:@", \t"];        
       [ fetchedData appendFormat:@"%@",[NSString stringWithFormat:@"%f", [taskItem.progress doubleValue]] ];
       [ fetchedData appendFormat:@", \t"];       
       [ fetchedData appendFormat:@"%@",[self handleNull: [taskItem.createdOn description]]];
       [ fetchedData appendFormat:@", \t"];       
       [ fetchedData appendFormat:@"%@",[self handleNull: [taskItem.remind_on description]]];
       [ fetchedData appendFormat:@", \t"];   
        
        // Include the Note Field //TODO: INCLUDE THE SUBTASKS NOW
        [ fetchedData appendFormat:@"%@",[self prepareForDump:taskItem.note]];
        
        [ fetchedData appendFormat:@"\n"];
    }
    
   
    
    //    NSLog(@"Count of data: %d", [mutableFetchResults count]);
    //    UIAlertView *someError = [[UIAlertView alloc] initWithTitle: @"Network error" message: @"Error sending your info to the server" delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    
    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"iGotTodo: Tasks Backup!"];
        //[mailCont setToRecipients:[NSArray arrayWithObject:@"changme@domain.com"]];
        //        [mailCont setMessageBody:@"Don't ever want to give you up" isHTML:NO];
        //        [mailCont setMessageBody:fetchedData isHTML:NO];     
        
        // Attach the data as a CSV attachment
        NSData *myData = [fetchedData dataUsingEncoding:NSUTF8StringEncoding]; 
        [mailCont addAttachmentData:myData mimeType:@"text/csv" fileName:@"ExportUserData.csv"];
        
        [mailCont setMessageBody:NSLocalizedString(@"Dear User, Your Data is extracted and attached as CSV file. Thanks!", @"Dear User, Your Data is extracted and attached as CSV file. Thanks!")
                          isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
    
    DLog(@"Email Sent...Log handling" );
    
}

-(void) deleteFinishedTasks {
    
    int recordsDeleted = [AppUtilities deleteCompletedTasks:DAYS_30];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Purge Tasks", @"Purge Tasks")
                                                    message:[NSString stringWithFormat:@"%@%d", NSLocalizedString(@"Deleted Record Count:", @"Deleted Record Count:"), recordsDeleted]
                                                   delegate:self cancelButtonTitle:OK
                                          otherButtonTitles:nil];
    [alert show];
}

- (NSString *) handleNull: (NSString *) str {
    if (str == nil|| [str length] == 0) {
        return @"";
    }
    else {
        return str;
    }
}
- (NSString *) prepareForDump: (NSString *) inputStr {
    
    NSString *NEWLINE = @"\n";
    NSString *LINEFEED= @"\t";
    
    NSString *COMMA = @",";
    NSString *SPACE = @" ";
    NSString *EMPTY1 = @"";
    
    NSString *tmpStr = inputStr;
    if (tmpStr == nil|| [tmpStr length] == 0) {
        tmpStr = EMPTY1;
    }
    else {
        tmpStr = [tmpStr stringByReplacingOccurrencesOfString:NEWLINE   withString:LINEFEED];
        tmpStr = [tmpStr stringByReplacingOccurrencesOfString:COMMA     withString:SPACE];
    }
    
    return  tmpStr;
}



// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            DLog(@"Mail Sending - Canceled");
            break;
        case MFMailComposeResultSaved:
            DLog(@"Mail Sending - Saved");
            break;
        case MFMailComposeResultSent:
            DLog(@"Mail Sending - Sent");        
            break;
        case MFMailComposeResultFailed:
            DLog(@"Mail Sending - Failed");            
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed."
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
            [alert show];
        }
            
            break;
    }
        [self dismissViewControllerAnimated:YES completion:nil];
}
@end
