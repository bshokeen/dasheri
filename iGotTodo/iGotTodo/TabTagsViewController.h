//
//  TagViewController.h
//  iGotTodo
//
//  Created by Me on 20/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tag.h"
#import "Task.h"
#import "AppUtilities.h"
#import "AppSharedData.h"
#import "ItemDetailsCell.h"
#import "BVReorderTableView.h"

@interface TabTagsViewController : UITableViewController
<NSFetchedResultsControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate, ReorderTableViewDelegate, ItemDetailsCellDelegate>

@property (nonatomic, strong) Task *task;
@property (nonatomic, strong) NSString *sourceUI; // To track where to set the selected tags as Task is not available.
@property (nonatomic, strong) NSMutableSet *pickedTags;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;


@property (weak, nonatomic) IBOutlet UITextField *tagNameText;


- (IBAction)rightNavigBtnClick:(id)sender;


- (void) initWithTask:(Task *) task;
- (void)initWithoutTask: (NSString *) sourceUI;

@end
