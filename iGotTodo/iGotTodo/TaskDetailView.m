//
//  TaskDetailView.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/16.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//  A LOT OF CODE IS DUPLICATED FROM SubtaskViewCOntroller

#import "TaskDetailView.h"
#import "AppUtilities.h"
#import "AppDelegate.h"

#define STD_CELL_HEIGHT 30.0 //used 30,35 but when text was in 2 lines it was not coming up well on the UI, so increased back to standard cell height.
// NOT SAME AS THE OTHER TO SAVE SPACE using 30 here

@interface TaskDetailView ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;


// To disable the keyboard editing in a cell, if tap is done on another cell.
@property (nonatomic, strong) SubtaskCell *cellUnderEditing;

@end


@implementation TaskDetailView

@synthesize fetchedResultsController=_fetchedResultsController, cellUnderEditing = _cellUnderEditing;

-(void) reset {
    
    self.task = nil;
    self.tagsLbl.text     = @"";
    self.reminderLbl.text = @"";
    self.notesTxt.text    = @"Select a task to show additional information.";
    
    self.dayLbl.text   = @"";
    self.monthLbl.text = @"";
    self.yearLbl.text  = @"";
}

-(void) initializeForTask:(Task *) task {
    
    self.task = task; //used for loading the subtasks
    
    self.tagsLbl.text     = [AppUtilities getSelectedTagsLabelText:task.tagS];
    self.reminderLbl.text = [AppUtilities uiFormatedDate: task.remind_on];
    
    /*NSString *note = @"";
    if (task.note == nil)
        note = [NSString stringWithFormat:@"     "];
    else
        note = [NSString stringWithFormat:@"       %@", task.note];
    
    self.notesTxt.text    = note;*/
    self.notesTxt.text = task.note;
    
    self.dayLbl.text   = [AppUtilities mFormatedDateDay:task.dueOn];
    self.monthLbl.text = [AppUtilities mFormatedDateMonthName:task.dueOn];
    self.yearLbl.text = [AppUtilities mFormatedDateYearOnly:task.dueOn];
    
    // Load subtasks
    // Have to do reload even if task is nil to clear the table. Doing checks was either causing failure to fetch or no refresh
        [self forceReloadThisUI];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


#pragma mark Subtasks handling
// All below code is for subtasks
-(void) hideCellKeyboard {
    [self.cellUnderEditing hideKeyboard ];
}

#pragma mark Table view data source methods
/*
 The data source methods are handled primarily by the fetch results controller
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

// Customize the appearance of table view cells.

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Subtask *subtask =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    // Configure the cell to show the Tasks's details
    SubtaskCell *noteCell = (SubtaskCell *) cell;
    [noteCell resetInitialText];
    [noteCell initializeWith:subtask Delegate:self FontSize:10]; //had to pass font size from here
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"SubtaskCell";
    
    SubtaskCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    /*  // This is not being invoked in my case but still retained as code here.
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (TaskViewCell *) [nib objectAtIndex:0];
     }*/
    
    // Configure the cell to show the Tasks's details
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Subtask *subtask =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    NSString *txt = subtask.data;
    
    CGSize size = [txt sizeWithFont:[UIFont fontWithName:@"Verdana" size:10] constrainedToSize:CGSizeMake(188, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat height = size.height;
    if (height < STD_CELL_HEIGHT) {
        height = STD_CELL_HEIGHT;
    }
    else {
        height = height + 10;
    }
    
//    NSLog(@"TESETING: FONT SIze: %f : new:%f", height, size.height);
    return height;
    /*
     
     Note *note =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
     NSString *txt = note.data;
     
     NotesCell *noteCell = [(NotesCell *)[NotesCell alloc] init];
     UIFont *cellFont =     noteCell.dataTxt.font;
     CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
     CGSize size = [txt sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
     
     return size.height + 5;*/
}

// Set the highligth color same as used by the application across
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIView *selectedView = [[UIView alloc]init];
    //    selectedView.backgroundColor = [UIColor colorWithHue:0.612 saturation:.04 brightness:.96 alpha:1.0];
    selectedView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = selectedView;
    cell.textLabel.highlightedTextColor    = [UIColor clearColor];// [AppUtilities getSelectedTextHighlightColor];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Subtask" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Create the sort descriptors array.
    NSString *sortKeyName   = @"created_on";
    BOOL sortOrderAscending = NO;
    NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKeyName ascending:sortOrderAscending];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dueOnDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    // Using array to generated and of predicates
    NSMutableArray *predicates = [[NSMutableArray alloc] init ];
    if (self.task) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"task == %@", self.task]];
    }
    
    // Compound Predicates as AND Conditions
    if ([predicates count] > 0 ) {
        NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setPredicate:p];
    }
    
    // Create and initialize the fetch results controller.
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:APP_DELEGATE_MGDOBJCNTXT
                                                                      sectionNameKeyPath:nil
                                                                               cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    // Memory management.
    return _fetchedResultsController;
    
}

/*
 NSFetchedResultsController delegate methods to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.subtaskTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.subtaskTableView;
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.subtaskTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.subtaskTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.subtaskTableView endUpdates];
}

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        NSLog(@"SortViewController: Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI
{
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsController = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.subtaskTableView reloadData];
}


#pragma mark - SubtaskEditFinish
-(void) notifyEditFinish:(NSObject *)cellObject NewValue:(NSString *)newValue {
    
}
-(void) notifyCellEditOnFor:(Subtask *)subtask Cell:(SubtaskCell *)subtasksCell {
    self.cellUnderEditing = subtasksCell;
}




@end
