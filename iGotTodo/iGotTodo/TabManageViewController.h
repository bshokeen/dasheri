//
//  TabManageViewController.h
//  iGotTodo
//
//  Created by Me on 12/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "CommonViewController.h"
#import "DragHelper.h"

@interface TabManageViewController : CommonViewController
                                    < UISearchBarDelegate, UIGestureRecognizerDelegate, DragHelperDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *uiSearchBar;
@property (weak, nonatomic) IBOutlet UIView *totalRowsView;
@property (weak, nonatomic) IBOutlet UILabel *totalRowsLabel;
@property (weak, nonatomic) IBOutlet UIButton *totalRowsBtn;

// Property to holde the help class, required for supporting drag n drop
@property (nonatomic, strong) DragHelper* helper;
@property (weak, nonatomic) IBOutlet UICollectionView *dummy; //hidden

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *SwipeGesture_DOWN;
- (IBAction)actOnSwipeGesture:(UISwipeGestureRecognizer *)sender;

- (IBAction)leftNavBarItemClicked:(id)sender;
- (IBAction)totalRecordsBtnClick:(id)sender;

@end
