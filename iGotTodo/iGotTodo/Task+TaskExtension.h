//
//  Task+TaskExtension.h
//  iGotTodo
//
//  Created by Me on 29/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "Task.h"

@interface Task (TaskExtension)

// Custom Code
@property (nonatomic, retain) NSDate *primitiveDueOn;
@property (nonatomic, retain) NSString *primitiveDueOnSectionName;
@property (nonatomic, retain) NSNumber *primitiveProgress;
@property (nonatomic, retain) NSString *primitiveProgressSectionName;
@property (nonatomic, retain) NSNumber *primitiveGain;
@property (nonatomic, retain) NSString *primitiveGainSectionName;
@property (nonatomic, retain) NSNumber *primitiveDone;
@property (nonatomic, retain) NSString *primitiveDoneSectionName;
@property (nonatomic, retain) NSNumber *primitiveFlag;
@property (nonatomic, retain) NSString *primitiveFlagSectionName;
@property (nonatomic, retain) NSNumber *primitiveStatus;
@property (nonatomic, retain) NSString *primitiveStatusSectionName;
@property (nonatomic, retain) NSString *primitiveDateSectionName;

@end
