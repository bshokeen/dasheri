//
//  FilterGroupViewController.m
//  iGotTodo
//
//  Created by Me on 08/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabDueOnViewController.h"
#import "AppDelegate.h" // Included to get the manged object context macro

@interface TabDueOnViewController ()


@end


@implementation TabDueOnViewController

@synthesize filterSegmentControl = _filterSegmentControl;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{ 
        return YES;
}

- (void)viewDidLoad
{
    self.thisInterfaceEntityName = @"TabManageViewController";
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: DueOn"];
    
    [self.filterSegmentControl addTarget:self
                                  action:@selector(didChangeGroupSegmentControl:)
                        forControlEvents:UIControlEventValueChanged];

    [super viewDidLoad];    
    
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

- (void)didChangeGroupSegmentControl:(UISegmentedControl *)control {
    
    self.selSegmentIdx = self.filterSegmentControl.selectedSegmentIndex;
    [self forceReloadThisUI];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setFilterSegmentControl:nil];   
}

- (void) viewDidAppear:(BOOL)animated {
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.filterSegmentControl setTitle:NSLocalizedString(@"Today", @"Today") forSegmentAtIndex:0];
    [self.filterSegmentControl setTitle:NSLocalizedString(@"Month", @"Month") forSegmentAtIndex:1];
    [self.filterSegmentControl setTitle:NSLocalizedString(@"Over Due", @"Over Due") forSegmentAtIndex:2];
    [self.filterSegmentControl setTitle:NSLocalizedString(@"No Due", @"No Due") forSegmentAtIndex:3];
    
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
        [self forceReloadThisUI];
    }
}

// Using did rotate to ensure we do what we want after rotation of parent view is done, no problem found but protective check.
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if(self.interfaceOrientation == UIDeviceOrientationPortrait || self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown){
        NSLog(@"UNHANDLED");
    } else  if(self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight){
        
        [AppUtilities showDualSectionView: self];
    }
}

#pragma mark Fetched results controller

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    NSString *sectionNameKeyPath = @"group.sort_order"; // most cases for this UI use this
    NSString *sortDescriptorKey  = @"group.sort_order";
    NSPredicate *new1 = [AppUtilities generatePredicate];
    
    switch( self.filterSegmentControl.selectedSegmentIndex) {
        case 0:
        {
            // Google Custom
            [AppUtilities logAnalytics:@"Tab Access: Due On: Today"];
            
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *tomorrow = [AppUtilities getStartOfNextDayDate:today];

            NSMutableArray *parr = [NSMutableArray array];

            NSPredicate *filterPredicate1 = [NSPredicate predicateWithFormat:@" (dueOn >= %@ && dueOn < %@)", today, tomorrow];
            NSPredicate *new1 = [AppUtilities generatePredicate];
            
            [parr addObject: filterPredicate1];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
            break;
        }
        case 1:
        {
            // Google Custom
            [AppUtilities logAnalytics:@"Tab Access: Due On: Month"];

            
            // Want to group it differently
            sectionNameKeyPath = @"dateSectionName";
            sortDescriptorKey  = @"dueOn";
            
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startOfMonth     = [AppUtilities getStartOfMonthDate:today];
            NSDate *startOfNextMth   = [AppUtilities getStartOfNextMonthDate:today];
            
            NSMutableArray *parr = [NSMutableArray array];
            
            NSPredicate *filterPredicate1 = [NSPredicate predicateWithFormat:@" (dueOn >= %@ && dueOn < %@)", startOfMonth, startOfNextMth];
            //NSPredicate *new1 = [NSPredicate predicateWithFormat:@"(done == 0)"];
            
            [parr addObject: filterPredicate1];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: Month: StartDate: %@, EndDate: %@", startOfMonth, startOfNextMth);
            break;
        }
        case 2:
        {
            // Google Custom
            [AppUtilities logAnalytics:@"Tab Access: Due On: OverDue"];

            
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(dueOn <= %@)", today];
            
            NSMutableArray *parr = [NSMutableArray array];
            [parr addObject:filterPredicate];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: OverDue: %@", today);
            break;
        }
        case 3:
        {
            // Google Custom
            [AppUtilities logAnalytics:@"Tab Access: Due On: No Due"];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(dueOn == nil)"];
            
            NSMutableArray *parr = [NSMutableArray array];
            [parr addObject:filterPredicate];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: No Due Set");
            break;
        }
        default:
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];
            
            break;
        }
    }
    

    // Create the sort descriptors array.
    NSSortDescriptor *grpSortOrderDescriptor = [[NSSortDescriptor alloc]initWithKey:sortDescriptorKey ascending:YES];
    NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
    
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:
                       grpSortOrderDescriptor,
                       dueOnDescriptor, nil];
    
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Create and initialize the fetch results controller.
    self.fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                           managedObjectContext:APP_DELEGATE_MGDOBJCNTXT
                                                                             sectionNameKeyPath:sectionNameKeyPath
                                                                                      cacheName:nil];
    self.fetchedResultsControllerObj.delegate = self;
    
    DLog(@"FILTERGroupViewController: getFetchResultsCtroller: RE - QUERY");
    // Memory management.
    return self.fetchedResultsControllerObj;
}  


- (NSString *) computeSectionNameFromRawName: (NSInteger) section 
{
    if ( self.filterSegmentControl.selectedSegmentIndex == 1) { // startOfMonth, use the name as extracted
        id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self fetchedResultsController] sections] objectAtIndex:section];
        return [sectionInfo name];
    }
    
    DLog(@"computeSectionName: Start");
    id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self fetchedResultsController] sections] objectAtIndex:section];
    
    // TODO: Code Clean up and bettername
    NSString *sectionName = nil;
    
    // NSString *groupName = task.group.name; //sectionInfo.name
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
    
    sectionName = task.group.name;
    sectionName =  ([sectionName length] == 0) ? EMPTY : sectionName;
    
    DLog(@"ComputeName: Section: %i, SectionInfoName: %@ newSectionName: %@: END", section, sectionInfo.name, sectionName);
    
    return sectionName;
}

// Save & Restore state api implementation

-(void) encodeRestorableStateWithCoder:(NSCoder *)coder {
    
    [super encodeRestorableStateWithCoder:coder];
    
    [coder encodeInt:self.filterSegmentControl.selectedSegmentIndex   forKey:@"TabDueOnVC_filterSegmentControl"];
}

- (void) decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    int oldIdx = [coder decodeIntForKey:@"TabDueOnVC_filterSegmentControl"];
    [self.filterSegmentControl setSelectedSegmentIndex:oldIdx];
}

@end
