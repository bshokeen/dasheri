//
//  Task.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/25.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "Task.h"
#import "Category.h"
#import "Group.h"
#import "Subtask.h"
#import "Tag.h"


@implementation Task

@dynamic actual_time;
@dynamic actual_time_unit;
@dynamic alert_notification;
@dynamic alert_notification_on;
@dynamic child_count;
@dynamic completed_on;
@dynamic createdOn;
@dynamic deleted_on;
@dynamic dirty;
@dynamic done;
@dynamic doneSectionName;
@dynamic dueOn;
@dynamic dueOnSectionName;
@dynamic effort_time;
@dynamic effort_time_unit;
@dynamic extra1;
@dynamic flag;
@dynamic flagSectionName;
@dynamic gain;
@dynamic gainSectionName;
@dynamic group_id;
@dynamic icon_name;
@dynamic location_alert;
@dynamic location_id;
@dynamic modified_on;
@dynamic name;
@dynamic note;
@dynamic parent_task_id;
@dynamic priority;
@dynamic progress;
@dynamic progressSectionName;
@dynamic projectName;
@dynamic remind_on;
@dynamic remind_repeat_interval;
@dynamic remind_via;
@dynamic remindOnCustomDays;
@dynamic sort_order;
@dynamic status;
@dynamic statusSectionName;
@dynamic tags;
@dynamic task_id;
@dynamic task_type;
@dynamic dateSectionName;
@dynamic category;
@dynamic group;
@dynamic subtaskS;
@dynamic tagS;

@end
