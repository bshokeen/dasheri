//
//  NotesViewController.h
//  realtodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDTO.h"
#import <QuartzCore/QuartzCore.h>

@protocol NotesViewControllerDelegate;


@interface NotesViewController : UIViewController <UIActionSheetDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *dataTextView;
// Added a view for carrying buttons on top of keyboard, used as of now as white space by hiding the buttons
@property (nonatomic, weak) IBOutlet UIView *accessoryView;

@property (nonatomic, strong) CommonDTO *commonDTOObj;

@property(weak, nonatomic) id <NotesViewControllerDelegate> delegate;


- (IBAction)axsryBtn_1:(id)sender;

@end

@protocol NotesViewControllerDelegate <NSObject>

-(void) notesViewControllerDidFinish:(CommonDTO *) commonDTO;

@end