//
//  AFilterItemsViewController.m
//  realtodo
//
//  Created by Balbir Shokeen on 9/5/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "ReminderViewController.h"
#import "AppUtilities.h"
#import "AppSharedData.h"

@interface ReminderViewController ()
@property (nonatomic, strong) NSDate         *selRemindOn;
@property (nonatomic) NSCalendarUnit selnscuRemindRepeatInterval;
@end

@implementation ReminderViewController
@synthesize yearQuickOptionSegment;
@synthesize monthQuickOptionSegment;
@synthesize timeQuickOptionSegment;
@synthesize dateQuickOptionSegment;
@synthesize remindOnChoiceSegment;

@synthesize delegate, datePicker, timePicker, displayDateTimeLabel, displayCustomDaysLabel;
@synthesize selRemindOn = _selRemindOn, selnscuRemindRepeatInterval = _selnscuRemindRepeatInterval;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) initWithArgumentsRemindOn: (NSDate *) remindOn RemindRepeatInterval: (NSCalendarUnit) remindRepeatInterval {
    
    _selRemindOn                 = remindOn;
    _selnscuRemindRepeatInterval = remindRepeatInterval;
    
    if (_selRemindOn == nil) {
        _selRemindOn = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
    }
    DLog(@"ReminderViewController: Loading: RemindOn: %@  RemindInterval: %d", self.selRemindOn, self.selnscuRemindRepeatInterval);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Google Custom
    [AppUtilities logAnalytics:@"View Opened: Reminder"];
    
    [self.datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    [self.timePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    
    int i = self.selnscuRemindRepeatInterval;// it was giving warning (as 0 is not in this type) so moving to a variable.
    switch (i) {
        case 0:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:0];
            break;
        case NSDayCalendarUnit:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:1];
            break;
        case NSWeekCalendarUnit:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:2];
            break;
        case NSMonthCalendarUnit:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:3];
            break;
        case NSYearCalendarUnit:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:4];
            break;
        case NSEraCalendarUnit:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:5];
            break;
        default:
            [self.remindOnChoiceSegment setSelectedSegmentIndex:5];
            break;
    }
    
    //    self.remindOnChoiceSegment.transform = CGAffineTransformMakeScale(.9f, .9f);
    UIFont *Boldfont = [UIFont boldSystemFontOfSize:12.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:Boldfont forKey:UITextAttributeFont];
    [self.remindOnChoiceSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    // To captue button click again for the selected segment setting this field to yes. Later moved to Storyboard setting.
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.remindOnChoiceSegment setTitle:NSLocalizedString(@"Day", @"Day") forSegmentAtIndex:0];
    [self.remindOnChoiceSegment setTitle:NSLocalizedString(@"Daily", @"Daily") forSegmentAtIndex:1];
    [self.remindOnChoiceSegment setTitle:NSLocalizedString(@"Weekly", @"Weekly") forSegmentAtIndex:2];
    [self.remindOnChoiceSegment setTitle:NSLocalizedString(@"Monthly", @"Monthly") forSegmentAtIndex:3];
    [self.remindOnChoiceSegment setTitle:NSLocalizedString(@"Year", @"Year") forSegmentAtIndex:4];
    [self.remindOnChoiceSegment setTitle:NEVER forSegmentAtIndex:5];

    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.dateQuickOptionSegment setTitle:NSLocalizedString(@"Today", @"Today") forSegmentAtIndex:0];
    [self.dateQuickOptionSegment setTitle:NSLocalizedString(@"+1 D", @"+1 D") forSegmentAtIndex:1];
    [self.dateQuickOptionSegment setTitle:NSLocalizedString(@"-1 D", @"-1 D") forSegmentAtIndex:2];
    [self.dateQuickOptionSegment setTitle:NSLocalizedString(@"+7 D", @"+7 D") forSegmentAtIndex:3];
    [self.dateQuickOptionSegment setTitle:NSLocalizedString(@"-7 D", @"-7 D") forSegmentAtIndex:4];
 
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.timeQuickOptionSegment setTitle:NSLocalizedString(@"Now", @"Now") forSegmentAtIndex:0];
    [self.timeQuickOptionSegment setTitle:NSLocalizedString(@"9 AM", @"9 AM") forSegmentAtIndex:1];
    [self.timeQuickOptionSegment setTitle:NSLocalizedString(@"1 PM", @"1 PM") forSegmentAtIndex:2];
    [self.timeQuickOptionSegment setTitle:NSLocalizedString(@"5 PM", @"5 PM") forSegmentAtIndex:3];
    [self.timeQuickOptionSegment setTitle:NSLocalizedString(@"9 PM", @"9 PM") forSegmentAtIndex:4];
    
      
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.monthQuickOptionSegment setTitle:NSLocalizedString(@"Today", @"Today") forSegmentAtIndex:0];
    [self.monthQuickOptionSegment setTitle:NSLocalizedString(@"Day 1", @"Day 1") forSegmentAtIndex:1];
    [self.monthQuickOptionSegment setTitle:NSLocalizedString(@"+7 D", @"+7 D") forSegmentAtIndex:2];
    [self.monthQuickOptionSegment setTitle:NSLocalizedString(@"-7 D", @"-7 D") forSegmentAtIndex:3];
    [self.monthQuickOptionSegment setTitle:NSLocalizedString(@"Last", @"Last") forSegmentAtIndex:4];
    
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.yearQuickOptionSegment setTitle:NSLocalizedString(@"Today", @"Today") forSegmentAtIndex:0];
    [self.yearQuickOptionSegment setTitle:NSLocalizedString(@"Jan", @"Jan") forSegmentAtIndex:1];
    [self.yearQuickOptionSegment setTitle:NSLocalizedString(@"Apr", @"Apr") forSegmentAtIndex:2];
    [self.yearQuickOptionSegment setTitle:NSLocalizedString(@"Jul", @"Jul") forSegmentAtIndex:3];
    [self.yearQuickOptionSegment setTitle:NSLocalizedString(@"Oct", @"Oct") forSegmentAtIndex:4];
    
}

- (void)viewDidUnload
{
    [self setRemindOnChoiceSegment:nil];
    [self setDateQuickOptionSegment:nil];
    [self setDateQuickOptionSegment:nil];
    [self setTimeQuickOptionSegment:nil];
    [self setYearQuickOptionSegment:nil];
    [self setMonthQuickOptionSegment:nil];
    [super viewDidUnload];
}

- (void) dateChanged {
    
    int selIdx = [self.remindOnChoiceSegment selectedSegmentIndex];
    
    NSDate *datePickerDate = [AppUtilities getDateWithSecondsResetForDate:self.datePicker.date];
    NSDate *timePickerDate = [AppUtilities getDateWithSecondsResetForDate:self.timePicker.date];
    AppSharedData *appS = [AppSharedData getInstance];
    
    // Most cases this need to happen so putting common here
    switch (selIdx) {
        case 0:   // Day
        case 2:   // WEEKLY
        case 3:   // MONTHLY
        case 4:   // YEARLY
        {
            self.displayDateTimeLabel.text = [appS getRemindOnLabelTextFromSelSegIdx:selIdx RemindDate:datePickerDate];
            self.selRemindOn = datePickerDate;
            break;
        }
        case 1: {   // Daily
            self.displayDateTimeLabel.text = [appS getRemindOnLabelTextFromSelSegIdx:selIdx RemindDate:timePickerDate];
            self.selRemindOn = timePickerDate;
            break;
        }
        case 5: {   // NEVER
            self.displayDateTimeLabel.text = NEVER;
            // self.selRemindOn = nil; // Don't nullify here but when you save from this UI. For better UI experience retain the old date as use navigates to different option.
            break;
        }
        default:
            break;
    }      
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) viewWillAppear:(BOOL)animated {
    int selIdx = [[AppSharedData getInstance] getSegmentIdxForCalenderUnit:self.selnscuRemindRepeatInterval];
    [self.remindOnChoiceSegment setSelectedSegmentIndex:selIdx];
    
    [self updateUIInterfaceWithRemindOn: self.selRemindOn RemindRepeatInterval: self.selnscuRemindRepeatInterval]; 
}

- (void) updateUIInterfaceWithRemindOn: (NSDate *) remindOn RemindRepeatInterval: (NSCalendarUnit) remindRepeatInterval {
    
    DLog(@"ReminderViewController: Refresh: RemindOn: %@  RemindInterval: %d ", remindOn, remindRepeatInterval);
    
    // Day segment settings are common and applicable in most cases, hence kept here generic.
    self.dateQuickOptionSegment.hidden  = FALSE;
    self.datePicker.hidden              = FALSE;       
    self.timePicker.hidden              = TRUE;
    self.timeQuickOptionSegment.hidden  = TRUE;
    self.monthQuickOptionSegment.hidden = TRUE;
    self.yearQuickOptionSegment.hidden  = TRUE;
    
    self.datePicker.enabled             = TRUE; 
    self.dateQuickOptionSegment.enabled = TRUE;
    
    self.datePicker.minimumDate = nil; // Reset the restriction first.
    
    int i = remindRepeatInterval; // it was giving warning (as 0 is not in this type) so moving to a variable.
    switch (i) {
        case 0:                 { // Day
            self.datePicker.date = remindOn;
            
            self.datePicker.minimumDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
            break;
        }
        case NSDayCalendarUnit: { // Daily
            self.dateQuickOptionSegment.hidden  = TRUE;
            self.datePicker.hidden              = TRUE;       
            self.timePicker.hidden              = FALSE;
            self.timeQuickOptionSegment.hidden  = FALSE;
            
            self.timePicker.date = remindOn;
            
            self.timePicker.minimumDate = nil;
            break;
        }
        case NSWeekCalendarUnit: { // WEEKLY
            self.datePicker.date = remindOn;
            
            // Try to prepare the minimum date restriction
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            NSDateComponents *dateComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit)
                                                      fromDate:self.datePicker.date];
            [dateComps setWeekday:1];
            self.datePicker.minimumDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case NSMonthCalendarUnit: { // MONTHLY
            self.dateQuickOptionSegment.hidden = TRUE;
            self.monthQuickOptionSegment.hidden = FALSE;
            
            self.datePicker.date = remindOn;
            
            // Try to prepare the minimum date restriction
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            NSDateComponents *dateComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit)
                                                      fromDate:self.datePicker.date];
            [dateComps setDay:1];
            self.datePicker.minimumDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case NSYearCalendarUnit: { // YEARLY
            self.dateQuickOptionSegment.hidden = TRUE;
            self.yearQuickOptionSegment.hidden = FALSE;
            
            self.datePicker.date = remindOn;
            
            // Try to prepare the minimum date restriction
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            NSDateComponents *dateComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit)
                                                      fromDate:self.datePicker.date];
            [dateComps setDay:1];
            [dateComps setMonth:1];
            self.datePicker.minimumDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case NSEraCalendarUnit: { // NEVER
            self.datePicker.enabled             = FALSE; // Only in this case it not enabled.
            self.dateQuickOptionSegment.enabled = FALSE; // Only in this case it not enabled.       
            
            self.datePicker.date           = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
            
            // Try to prepare the minimum date restriction
            NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
            NSDateComponents *dateComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit)
                                                      fromDate:self.datePicker.date];
            [dateComps setYear:[dateComps year] + 1];
            self.datePicker.minimumDate = [calendar dateFromComponents:dateComps];
            break;
        }
        default:
            break;
    }
    [self dateChanged]; // Use only in else block otherwise it was having a conflicting condition. Reusing this API.
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowCustomDaysViewController"]) {
        
        SelectCustomDaysViewController *fiCtrl = (SelectCustomDaysViewController *) [segue destinationViewController];  
        fiCtrl.delegate = self;
    }
}


- (IBAction)save:(id)sender { 
    self.selnscuRemindRepeatInterval = [[AppSharedData getInstance] 
                                        getCalenderUnitForSegmentIndex:self.remindOnChoiceSegment.selectedSegmentIndex];
    
    // For better UI experience remind on date was not set to nil on selecting the NEVER option. So putting this check to set to nil before saving.
    if (self.selnscuRemindRepeatInterval == NSEraCalendarUnit) {
        self.selRemindOn = nil;
    }
    [self.delegate RemindViewControllerDidFinish: self.selRemindOn 
                                  RepeatInterval:self.selnscuRemindRepeatInterval 
                               FormatedLabelText: self.displayDateTimeLabel.text];    
    
    [self.navigationController popViewControllerAnimated:YES];  
    
    DLog(@"ReminderViewController: Saved  : RemindOn: %@  RemindInterval: %d", [self.selRemindOn description], self.selnscuRemindRepeatInterval);
}

- (void)customDaysTVControllerDidFinish:(SelectCustomDaysViewController *)controller
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


// MAIN SEGMENT to SELECT REMINDER INTERVAL TYPE
- (IBAction)remindOnChoiceSegmentValueChanged:(id)sender 
{
    int selIdx = [sender selectedSegmentIndex];
    self.selnscuRemindRepeatInterval = [[AppSharedData getInstance] getCalenderUnitForSegmentIndex:selIdx];
    
    // Use the sel date as it keeps on chaning when it is changed from the date picker controls.
    [self updateUIInterfaceWithRemindOn:self.selRemindOn RemindRepeatInterval:self.selnscuRemindRepeatInterval];
}

// CHILD SEGMENT OPERATIONS
- (IBAction)dateQuickOptionSegment_ValueChanged:(id)sender {
    int selIdx = [sender selectedSegmentIndex];
    NSDate *datePickerDate = self.datePicker.date;
    
    switch (selIdx) {
        case 0: {
            //            self.datePicker.date = [AppUtilities getDateWithSecondsResetForDate:[AppUtilities uiUNFormatedDate:nil WithDayAdd:YES]];
            datePickerDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]]; // it is easy to understand if it is today.
            break;
        }
        case 1: {
            datePickerDate = [datePickerDate dateByAddingTimeInterval:(1*24*60*60)];
            break;
        }
        case 2:{
            datePickerDate = [datePickerDate dateByAddingTimeInterval:-(1*24*60*60)];
            break;
        }
        case 3:{
            datePickerDate = [datePickerDate dateByAddingTimeInterval:(7*24*60*60)];
            break;
        }
        case 4: {
            datePickerDate = [datePickerDate dateByAddingTimeInterval:-(7*24*60*60)];
            //            [self performSegueWithIdentifier:@"ShowCustomDaysViewController" sender:self];   
            break;
        }
    }
    // Finally set it over the selected date variable
    self.datePicker.date = datePickerDate;
    self.selRemindOn = self.datePicker.date; // Only date will change and not the NSCalendarUnit on this segment.
    
    [self updateUIInterfaceWithRemindOn: self.selRemindOn RemindRepeatInterval: self.selnscuRemindRepeatInterval ]; 
}

- (IBAction)timeQuickOption_ValueChanged:(id)sender {
    
    int selIdx = [sender selectedSegmentIndex];
    NSDate *timePickerDate = self.timePicker.date;
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:timePickerDate];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    
    [dateComps setMinute:0];
	[dateComps setSecond:0];
    
    switch (selIdx) {
        case 0: {
            timePickerDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
            break;
        }
        case 1: {
            [dateComps setHour:9];
            
            timePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 2:{
            [dateComps setHour:13];
            
            timePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 3:{
            [dateComps setHour:17];
            
            timePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 4: {
            [dateComps setHour:21];
            
            timePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
    }
    
    self.timePicker.date = timePickerDate;
    self.selRemindOn = self.timePicker.date;// Only date will change and not the NSCalendarUnit on this segment.
    
    [self updateUIInterfaceWithRemindOn: self.selRemindOn RemindRepeatInterval: self.selnscuRemindRepeatInterval]; 
}

- (IBAction)monthQuickOptionSegment_ValueChanged:(id)sender {
    
    int selIdx = [sender selectedSegmentIndex];
    NSDate *datePickerDate = self.datePicker.date;
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    // Break the date up into components
    NSDateComponents *dateComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit)
                                              fromDate:datePickerDate];
    switch (selIdx) {
        case 0: {
            datePickerDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
            break;
        }
        case 1: {
            [dateComps setDay:1];
            [dateComps setMonth:[dateComps month]+1];
            
            datePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 2:{
            datePickerDate = [datePickerDate dateByAddingTimeInterval:(7*24*60*60)];
            break;
        }
        case 3:{
            datePickerDate = [datePickerDate dateByAddingTimeInterval:-(7*24*60*60)];
            break;
        }
        case 4: {
            [dateComps setMonth:[dateComps month] + 1 ];
            [dateComps setDay:1];
            
            NSDate *tmp = [calendar dateFromComponents:dateComps];
            datePickerDate = [tmp dateByAddingTimeInterval:-1*24*60*60 ];
            break;
        }
    }
    self.datePicker.date = datePickerDate;
    self.selRemindOn = self.datePicker.date; // Only date will change and not the NSCalendarUnit on this segment.
    
    [self updateUIInterfaceWithRemindOn: self.selRemindOn RemindRepeatInterval: self.selnscuRemindRepeatInterval]; 
}

- (IBAction)yearQuickOptionSegment_ValueChanged:(id)sender {
    
    int selIdx = [sender selectedSegmentIndex];
    NSDate *datePickerDate = self.datePicker.date;
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    // Break the date up into components
    NSDateComponents *dateComps = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit)
                                              fromDate:datePickerDate];
    switch (selIdx) {
        case 0: {
            datePickerDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
            break;
        }
        case 1: {
            [dateComps setMonth:1];
            
            datePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 2:{
            [dateComps setMonth:4];
            
            datePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 3:{
            [dateComps setMonth:7];
            
            datePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
        case 4: {
            [dateComps setMonth:10];
            
            datePickerDate = [calendar dateFromComponents:dateComps];
            break;
        }
    }
    self.datePicker.date = datePickerDate;
    self.selRemindOn = self.datePicker.date;// Only date will change and not the NSCalendarUnit on this segment.
    
    [self updateUIInterfaceWithRemindOn: self.selRemindOn RemindRepeatInterval: self.selnscuRemindRepeatInterval]; 
}

- (IBAction)bottomSaveBtnClick:(id)sender {
    [self save:sender];
}

@end
