//
//  ToolbarItem.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/22.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "ToolbarItem.h"

@implementation ToolbarItem

- (instancetype)initWithIndex:(int)idx Title:(NSString*)titleLbl IconImg:(NSString*)iconImage Count:(int)cnt
{
    self = [super init];
    if (self) {
        self.index   = idx;
        self.title   = titleLbl;
        self.iconImg = iconImage;
        self.count   = cnt;
    }
    return self;
}

@end