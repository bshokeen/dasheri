//
//  CommonViewController.m
//  iGotTodo
//
//  Created by Me on 31/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "CommonViewController.h"
#import "SectionHeaderCell.h"
#import "AppDelegate.h" // Included to get the manged object context macro

@interface CommonViewController ()

// <SegmentID, <SectionIDs OrderedSet>>
// NSMutableDictionary => <NSNumber:SegmentNo <NSMutableOrderedSet: CollapsedSectionNumber(NSNumber)>
@property (nonatomic, strong) NSMutableDictionary *colpsedSectionNameDict; // sections in collapsed state
@property (nonatomic, strong) NSMutableDictionary *segmentSectionsLoaded; // Segment Number {Section on first load was loaded as collapsed). Section in this list are already loaded once.

@property (nonatomic) int direction; // Direction: Add/Delete/Move indicator to allow fixing the cache in the same order
@property (nonatomic) int deletedOrInsertedSection;

// This is a temp var, set in the child when gesture is swiped and then used in the segue defined in this class to process data on this item
@property (weak, nonatomic) Task *passToSegueSwipedOnIdxPath;

@end

@implementation CommonViewController

//@synthesize mgdObjContext = _mgdObjContext;
@synthesize fetchedResultsControllerObj = _fetchedResultsControllerObj;
@synthesize selSegmentIdx = _selSegmentIdx;

@synthesize colpsedSectionNameDict = _colpsedSectionNameDict, segmentSectionsLoaded = _segmentSectionsLoaded, direction = _direction, deletedOrInsertedSection = _deletedOrInsertedSection;

@synthesize thisInterfaceEntityName = _thisInterfaceEntityName;
@synthesize passToSegueSwipedOnIdxPath = _passToSegueSwipedOnIdxPath;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.colpsedSectionNameDict        = [[NSMutableDictionary alloc] init ];
    self.segmentSectionsLoaded         = [[NSMutableDictionary alloc] init ];    
    
    self.leftTable.sectionHeaderHeight = HEADER_HEIGHT;

    [self registerSwipe];
    
//    // Load the data on first run
//    [self fetchData];
}

- (void) viewWillAppear:(BOOL)animated {
    // Added this line to fix the bug which was crashing the app when filter ui is selected on a group and try to add the task
    self.fetchedResultsControllerObj = nil;
    
    // Load the data on first run
    [self forceReloadThisUI];
}

- (void) viewWillDisappear:(BOOL)animated {
        // Added this line to fix the bug which was crashing the app when filter ui is selected on a group and try to add the task
    self.fetchedResultsControllerObj = nil;
}
- (void)viewDidUnload
{
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    self.fetchedResultsControllerObj = nil;
    
}

-(void) registerSwipe{
    
    // Adding ability to perform swipe on task cell
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(oneFingerSwipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
}

- (void)oneFingerSwipeLeft:(UITapGestureRecognizer *)recognizer {
    //    ULog(@"swipe left"); // IT show a message when swiped on section header, so disabled
}

- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    /* DISABLING IT AS IT IS NOT SO USEFUL HERE and rather taking user to sublist view
     // Insert your own code to handle swipe right
     // Get the point of swipe, find the indexpath and from that index path find the actual task. Task is updated and gets refreshed in tableview via the nsfetchresultcontroller changes
     
     CGPoint location = [recognizer locationInView:self.leftTable];
     NSIndexPath *swipedIndexPath = [self.leftTable indexPathForRowAtPoint:location];
     UITableViewCell *swipedCell  = [self.leftTable cellForRowAtIndexPath:swipedIndexPath];
     TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
     
     [AppUtilities markUnmarkTaskComplete: taskViewCell.task ];*/
    
    
    /* Disabled all this aslo as swiping ot go to next view was not convienent and intiuitive, so made an area to tap and do this segue
    CGPoint location = [recognizer locationInView:self.leftTable];
    NSIndexPath *swipedIndexPath = [self.leftTable indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [self.leftTable cellForRowAtIndexPath:swipedIndexPath];
    TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
    //Now cache the task identified for passing to segue api
    self.passToSegueSwipedOnIdxPath = taskViewCell.task;
    
//    NSLog(@"Debug: Task:%@ Cell:%@ X:%f Y:%f", taskViewCell.task.name, [taskViewCell description], location.x, location.y );
    if (self.passToSegueSwipedOnIdxPath) { //Bug Fix: if swipe is unable to produce desired task, section closed may be, do nothing)
        [self performSegueWithIdentifier:@"ShowSubtasksViewController" sender:self];
    }
    */
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = LANDSCAPE_WIDTH_FACTOR;
    else {
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = 0;
    }
    [self forceReloadThisUI];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return IS_PAD || (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 36; // Setting the height on the storyboard was not coming up so manually setting it here.
}


#pragma mark - Section Name API

-(BOOL) isSectionUserCollapsed: (NSInteger)sectionNumber  {

    BOOL isSectionUserCollapsed = NO;
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
    
//    NSString *computedSectionName = [self getManagedObjID:sectionNumber];// [NSString stringWithFormat:@"%i", sectionNumber] ; //[self computeSectionNameFromRawName:sectionNumber];

//    NSString *grpSectionNo =[NSString stringWithFormat:@"%i", sectionNumber];    
    NSNumber *grpSectionNo = [NSNumber numberWithInt:sectionNumber];

    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:key];
    if ([valueSet containsObject:grpSectionNo] )
        isSectionUserCollapsed = YES;
    else {
        isSectionUserCollapsed = NO;
    }
    
    DLog(@"isSectionUserCollapsed:%@ Section:%i, Segment:%@  ComputedName:%@   %@: colpsedSectionNameDict", isSectionUserCollapsed? @"YES": @"NO", sectionNumber, key,  grpSectionNo, [AppUtilities cleanDictSetPrint: _colpsedSectionNameDict]);
    return isSectionUserCollapsed;
}

- (NSString *) getManagedObjID: (NSInteger) section {
    
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
    
    NSString *objectIDStr = [[[task.group objectID] URIRepresentation] absoluteString];
    if ( objectIDStr == nil || [objectIDStr length] == 0 ) {
        objectIDStr = EMPTY;
    }
    
    return objectIDStr;
}

- (NSString *) computeSectionNameFromRawName: (NSInteger) section 
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self fetchedResultsController] sections] objectAtIndex:section];
    
    
    // TODO: Code Clean up and bettername
    NSString *sectionName = nil;
    
    // NSString *groupName = task.group.name; //sectionInfo.name
    NSString *dbQryGrpName = sectionInfo.name;
    
    switch ( self.selSegmentIdx ) {
        case 0:
        {
            /*
             sectionName = dbQryGrpName;//[sectionInfo name];   
             
             // AS I was not able to get the right sorting for group name, getting it sorted by sort_order and finding the group name selectively.
             Group *grp = [AppUtilities fetchGroupBySortOrder:groupName  ManagedContext:self.mgdObjContext];
             groupName = grp.name;
             //            DLog(@"Section Name%@ %@", sectionName, groupName);
             
             // Display the project name as section headings.
             sectionName =  ([groupName length] == 0) ? @"Empty" : groupName;
             */
            // TODO: Check if the above one is faster or there is another faster method to do this
            NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
            Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
            
            sectionName = task.group.name;
            sectionName =  ([sectionName length] == 0) ? EMPTY : sectionName;
            
            break;
        }
        case 1: {
            sectionName = dbQryGrpName;
            break;
        }
        case 2:
            sectionName = dbQryGrpName;
            break;
        case 3: 
            sectionName = dbQryGrpName;
            break;
        case 4:
            sectionName = dbQryGrpName;
            break;
        default:
            sectionName = dbQryGrpName;
            break;
    }
    
    DLog(@"ComputeName:              Section:%i, SectionInfoName: %@ newSectionName: %@: selSegIdx:%i END", section, sectionInfo.name, sectionName, self.selSegmentIdx);
    
    return sectionName;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    int count = [[[self fetchedResultsController] sections] count];
    DLog(@"numberOfSectionsInTableView: %i", count);
    return count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"noOfRowsInsection: START  Section:%i", section);   
    
    BOOL val = [self isSectionUserCollapsed:section];
    if (val) {
        DLog(@"noOfRowsInsection: END    Section:%i isUserCollapsed rows:0", section);
        return 0;
    }
    
    // Get the value from the array maintained per groupSegment
    if ([self isSectionCollapsedAsFirstTimeLoad: section]) {
        // mark these sections so that they don't appear as first load again
        [self markSectionLoadedCollapsedOnFirstLoad:section];
        
        // Also put it in the closed section names to get the section as collapsed
//        NSString *sectionName = [self getManagedObjID:section];//[self computeSectionNameFromRawName:section];
        NSString *sectionName = [NSString stringWithFormat:@"%i", section];
        [self addClosedSectionName:sectionName SectionNumber:section];
        
        DLog(@"noOfRowsInsection: END    Section:%i isSectionCollapsedAsFirstTimeLoad rows:0", section);        
        return 0;
    }
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
        DLog(@"noOfRowsInSection: END    Section:%i Returned Actual Row Count (%i).", section, [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
    
}
// Ensuring update is alwasy avaialble on selection by adding in common view
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"ShowUpdateTaskViewController" sender:self];
}


- (void) markSectionLoadedCollapsedOnFirstLoad: (NSInteger) section {
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
//    NSString *item = [NSString stringWithFormat:@"%i", section];
    NSNumber *item = [NSNumber numberWithInt:section];
    
    NSMutableOrderedSet *value = [self.segmentSectionsLoaded objectForKey:key];
    if (value == nil) {
        value = [[NSMutableOrderedSet alloc] init];
        [value addObject:item];
        
        [self.segmentSectionsLoaded setObject:value forKey:key];
    }
    else {
        [value addObject:item];
    }
    DLog("MarkSectionLoadedCollapsedOnFirstLoad: Section: %i %@: segmentSectionsLoaded", section, [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
}
- (BOOL) isSectionCollapsedAsFirstTimeLoad: (NSInteger) sectionNumber {
   /* 
    int selCollapsibleOnIdx = self.selSegmentIdx;
    
    BOOL valueAtIdex = [[self.isSegmentSectionsFirstLoadArr objectAtIndex:selCollapsibleOnIdx] boolValue];
    
    //    DLog(@"Check if SelectedCollonFirstLoad: %@", self.isSectionFirstLoadArray);
    */
    
    BOOL isSectionFirstLoad = NO;

    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
//    NSString *item = [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *item = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *value = [self.segmentSectionsLoaded objectForKey:key];
    
    if (value !=nil && [value containsObject:item])
        isSectionFirstLoad = NO;
    else {
        isSectionFirstLoad = YES;
    }
    
    DLog(@"isSectionCollapsedAtFirstTimeLoad: %@ ForSection:%@ selSegmentIdx:%@    %@: segmentSectionsLoaded", isSectionFirstLoad ? @"Yes" : @"NO", item, key,  [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
   
    return isSectionFirstLoad;    
}


-(UIView*)FOUR_tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = @"@BA"; //[self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL isSectionOpen = YES;
    if ([self isSectionUserCollapsed:section]){
        isSectionOpen = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    
    // Initialize custom header section view using custom prototype cell
    static NSString *cellID = @"SectionTVCell";
    SectionHeaderCell *sectionHeaderCell = (SectionHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (sectionHeaderCell == nil) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SectionHTVCell" owner:self options:nil];
        sectionHeaderCell = [subviewArray objectAtIndex:0];
//        NSLog(@"TEMP: INIT From Nib");
    }else {
//        NSLog(@"TEMP: INIT From QUEUE");
    }
    
    [sectionHeaderCell updateSectionViewData:isSectionOpen
                                       Index:section
                                        Name:sectionName
                                       Count:sectionItemCount
                                 GrpMgdObjID:grpManagedObjectID
                                         Tag:(SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                                    Delegate:self]; // To inform open/close of section
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    
    //    return sectionHeaderCell;
    
    UIView *view = [[UIView alloc] initWithFrame:[sectionHeaderCell frame]];
    [view addSubview:sectionHeaderCell];
    
    return view;
}

-(UIView*)THREE_tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL selectState = YES; // TODO: Change this name select name to be more meanigful - groupBtnStateCollapsed
    if ([self isSectionUserCollapsed:section]){
        selectState = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    UITableSectionHeaderView *ghsv = [[UITableSectionHeaderView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.leftTable.bounds.size.width, self.leftTable.bounds.size.height) title:sectionName section:section sectionItemCount: sectionItemCount selectState: selectState HeaderIndex: (section +1) groupMngedObjID:grpManagedObjectID delegate:self ];
    
    // To identify a group section for updates later
    ghsv.tag = SECTION_NUMBER_STARTFROM + section;
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    return ghsv;
}



-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL isSectionOpen = YES;
    if ([self isSectionUserCollapsed:section]){
        isSectionOpen = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    
    // Initialize custom header section view using custom prototype cell
    SectionHeaderView *sectionHeaderView = nil;
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SectionHeaderView" owner:self options:nil];
    sectionHeaderView = [subviewArray objectAtIndex:0];
    
    
    [sectionHeaderView updateSectionViewData:isSectionOpen
                                       Index:section
                                        Name:sectionName
                                       Count:sectionItemCount
                                 GrpMgdObjID:grpManagedObjectID
                                         Tag:(SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                                    Delegate:self]; // To inform open/close of section
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    
    return sectionHeaderView;
    
}


-(UIView*)ONE_tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL isSectionOpen = YES;
    if ([self isSectionUserCollapsed:section]){
        isSectionOpen = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];

    // Initialize custom header section view using custom prototype cell
    static NSString *cellID = @"SectionHeaderCell";
    SectionHeaderCell *sectionHeaderCell = (SectionHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
//    NSLog(@"TEMP: Queued Section: %i", section);
    
    [sectionHeaderCell updateSectionViewData:isSectionOpen
                         Index:section
                          Name:sectionName
                         Count:sectionItemCount
                   GrpMgdObjID:grpManagedObjectID
                           Tag:(SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                      Delegate:self]; // To inform open/close of section
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    
//    return sectionHeaderCell;
    
    UIView *view = [[UIView alloc] initWithFrame:[sectionHeaderCell frame]];
    [view addSubview:sectionHeaderCell];
    
    return view;
}

// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    // Configure the cell to show the Tasks's details
    TaskViewCell *mvaCell = (TaskViewCell *) cell;
    [mvaCell resetInitialText];
    
    Task *activityAtIndex =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    [AppUtilities configureCell:mvaCell Task:activityAtIndex];
    //TODO: Make is a sinlge api call with congirecell in TaskViewCell itself. Bad code,  but adding to set the delgate for notifications back
    mvaCell.delegate = self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TaskViewCell";
    
    TaskViewCell *cell = (TaskViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /*     // Tried using TaskViewCell.xib, but found issue of setting segues, initializatin so leave it unless gain more knowledge
    if (cell == nil) {
        NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (TaskViewCell *) [nib objectAtIndex:0];
    }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        DLog(@"Dynamic: commitEditingStyle: Starting Delete");
        [AppUtilities deleteTask:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
        
        // After deleting the record update teh section count value. Added tags for this purpose and finding the section to upate values
//        SectionHeaderCell *view =  (SectionHeaderCell *) [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)];
        SectionHeaderView *view =  (SectionHeaderView *) [self.leftTable viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)];
        //  DLog(@"Descrit: %@", [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)]);
        
//        if ( [view isKindOfClass:[ SectionHeaderCell class]] ) {
        if ( [view isKindOfClass:[ SectionHeaderView class]] ) {
            [view updateCountOnDelete:1]; // update name for rows deleted arg            
        }
        else {
            DLog(@"Error, unable to find the GroupHeaderSectionView, need to debug.");
        }
        //        id x = self.tableView;
        //        id y = [self.tableView viewWithTag:indexPath.section];
        //        id z = [self.tableView viewWithTag:0];
        //        id a = [self.tableView viewWithTag:3];
        
        
        
        
        // TODO: I NEED TO UPDATE THE COUNT ON THE SECTIONS HERE.
        //        // Delete the managed object.
        //        NSManagedObjectContext *context = [[self fetchedResultsController] managedObjectContext];
        //        [context deleteObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
        //        
        //        NSError *error;
        //        if (![context save:&error]) {
        //            /*
        //             Replace this implementation with code to handle the error appropriately.
        //             
        //             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
        //             */
        //            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //            abort();
        //        }
        //        
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
    DLog(@"Dynamic: commitEditingStyle: FINISHED Operation");
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.   
    [self.leftTable beginUpdates];
    DLog(@"Table End Update");
}


/* This API will add/delete/move (delete +add) a record in the table */
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    
    DLog(@"Dynamic: didChangeObject: Type: %i, atIndexSection: %i atIndexRow: %i, newIndexSection: %i newIndexRow: %i", type, indexPath.section, indexPath.row,newIndexPath.section, newIndexPath.row);
    //    DTLog(@"Debug: %@, %@, %@", indexPath, newIndexPath, anObject);
    UITableView *tableView = self.leftTable;
    switch(type) {
        // Had to put checks for section being in closed state else it was giving error as rows before and after update don't match. NoOfRows will continue return 0 even after add/update/delete and fail. To prevent it doing these checks. Additionally saving on same context was impacting tableview on other controllers and one section open in this view might be closed on other. To handle them had to put these checks. 
        case NSFetchedResultsChangeInsert: {
            // before using the newindex reloading the cache to ensure the isSectionUserCollapsed: check happens on the newer section number (say section is added, so wd not b there in cache but reload cache will handle this by shifting the cached section number
            [self reloadUserCollapsedListOnSectionDelete:newIndexPath.section Direction:self.direction];
            
            self.direction=0;
            self.deletedOrInsertedSection = newIndexPath.section;
            
            if (![self isSectionUserCollapsed:newIndexPath.section]) {
                [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
        }
        case NSFetchedResultsChangeDelete:
            if (![self isSectionUserCollapsed:indexPath.section]) {                
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
        case NSFetchedResultsChangeUpdate:
            if ( ![self isSectionUserCollapsed:indexPath.section] ) {
                [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];                
            }
            break;
            
        case NSFetchedResultsChangeMove: {
//            if (indexPath.section == newIndexPath.section) {
//                // even though there is a move somehow the old and new section numbers were same, don't know why. BUGGY
//                [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
//                
//                self.direction=0;
//                self.deletedOrInsertedSection = -1;
//                
////                NSIndexPath *reloadedIndexPath = [NSIndexPath indexPathForRow:(indexPath.row + self.direction) inSection:indexPath.section];
//              //  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
////                [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];                
//            }
//            else {
            // As we have open & closed sections we need to check the section is closed or open to see if actual deleted or open operation is required or not.
                if (! [self isSectionUserCollapsed:indexPath.section] ) {
                    DLog(@"Deleting Row: section: %i row:%i", indexPath.section, indexPath.row);
                    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
            
            // before doing a section open/close check on newIndexPath reloading the cache to adjust for add/delete of section 
            [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
            
            self.direction=0; // operation move is direction=0
            self.deletedOrInsertedSection = newIndexPath.section; // just following the process to set the available section number. this value assign should not matter as of today though

                if ( ![self isSectionUserCollapsed:newIndexPath.section] ) {
                    DLog(@"Inserting Row: section: %i row:%i", newIndexPath.section, newIndexPath.row);
                    [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
//            }
            break;
        }
    }
    
    DLog(@"Dynamic: didChangeObject: Finished Update.");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{    
    DLog(@"Dynamic: didChangeSection: Type: %i, atIndexSection: %i ", type, sectionIndex);
    switch(type) {
        case NSFetchedResultsChangeInsert: {
            // MOVED LATER
            // To prevent getting an error because of it being marked as collapsed and item count returning as 0, adding it as if it is already collapsed
//            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
            
            [self.leftTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            self.direction = 1;
            self.deletedOrInsertedSection = sectionIndex;
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.leftTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            self.direction = -1;
            self.deletedOrInsertedSection = sectionIndex;
            break;
        }
    }

    DLog(@"Dynamic: didChangeSection: Section Did change: Finished Update.");
}

/* Added this api to update the cache storing the closed sections. When a section is inserted or deleted the section numbers are
 * no longer valid and should be readjusted (BETTER USE ARRAY TO ADD/DELETE ITEMS, LATER)
 * Since we need to start from top or bottom using the direction for insert/delete operation
 * Moved the marking API here as wanted it to be run just before begining updates ... forget why?
 * calling this API before the updates and sometimes when objectchanged and somehow the new and old section was coming same and creating problem
 */

-(void) reloadUserCollapsedListOnSectionDelete: (int) sectionIndex Direction:(int) direction 
{    
    DLog(@"ReloadUserCollapsedListOnSectionDelete: Start SectionIdx: %i Direction_1Add_-1Del_0Move:%i %@: colapsedSectionNameDict  %@: segmentSectionsLoaded", sectionIndex, direction, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict], [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
    if (sectionIndex != -1 ) {
        int sectionCount = [self.leftTable numberOfSections];
        if (direction == -1 ) {
            for (int i = sectionIndex; i < sectionCount; i++) {
                NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
                //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
                //            NSString *subKeySectionInx = [NSString stringWithFormat:@"%i", i];
                //            NSString *newSubKeySectionIdx = [NSString stringWithFormat:@"%i", i + direction];        
                NSNumber *subKeySectionInx = [NSNumber numberWithInt:i];
                NSNumber *newSubKeySectionIdx = [NSNumber numberWithInt:(i + direction)];
                
                NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
                if (valueSet && [valueSet containsObject:subKeySectionInx]) {
                    [valueSet removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case. 
                        [valueSet addObject:newSubKeySectionIdx];
                }
                
                // Update the dict holding sections which are loaded on first load else it will show different index already loaded
                NSMutableOrderedSet *valueSet2 = [ self.segmentSectionsLoaded objectForKey:keySegmentNo];
                if (valueSet2 && [valueSet2 containsObject:subKeySectionInx]) {
                    [valueSet2 removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case. 
                        [valueSet2 addObject:newSubKeySectionIdx];
                }
            }     
        }
        else if (direction == 1) {
            for (int i = sectionCount; i >= sectionIndex; i--) {
                NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
                //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
                //            NSString *subKeySectionInx = [NSString stringWithFormat:@"%i", i];
                //            NSString *newSubKeySectionIdx = [NSString stringWithFormat:@"%i", i + direction];
                NSNumber *subKeySectionInx = [NSNumber numberWithInt:i];
                NSNumber *newSubKeySectionIdx = [NSNumber numberWithInt:(i + direction)];
                
                NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
                if (valueSet && [valueSet containsObject:subKeySectionInx]) {
                    [valueSet removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case. 
                        [valueSet addObject:newSubKeySectionIdx];
                }
                // Update the dict holding sections which are loaded on first load else it will show different index already loaded
                NSMutableOrderedSet *valueSet2 = [ self.segmentSectionsLoaded objectForKey:keySegmentNo];
                if (valueSet2 && [valueSet2 containsObject:subKeySectionInx]) {
                    [valueSet2 removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case. 
                        [valueSet2 addObject:newSubKeySectionIdx];
                }
            }
            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
            // Also add this newly added section as collapsed by default
            NSString *sectionName = [NSString stringWithFormat:@"%i", sectionIndex];
            [self addClosedSectionName:sectionName SectionNumber:sectionIndex];
        }    
        else if (direction == 0) { // HACK to be tested out if it really works in all cases
            // Adding a new section is not giving an add in some cases and hence section is getting flagged as load first time and count is not matching with old count and 0. So force adding this section as already loaded first time.
            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
        }
    }

         
    DLog(@"ReloadUserCollapsedListOnSectionDelete: END   SectionIdx: %i Direction_1Add_-1Del_0Move:%i %@: colapsedSectionNameDict  %@: segmentSectionsLoaded", sectionIndex, direction, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict], [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    DLog(@"controllerDidChangeContent: START"); 
    
    [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
    
    self.direction=0;
    self.deletedOrInsertedSection = -1;
    
    @try {
        // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
        [self.leftTable endUpdates];
    }
    
    @catch (NSException *exception) {
        DLog(@"main: Caught %@: %@", [exception name], [exception reason]);
        // Tried few things for recovery but did not find something useful.
//        [self.tableView reloadData];
//        [super setEditing:YES animated:YES]; 
//        [self.tableView setEditing:YES animated:YES];
        [self forceReloadThisUI];
    }
}


#pragma mark - DB Operations

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI 
{    
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    [self fetchData];
    
    // After adding a new record we refereshing the existing data to show added record. So loaded new data from DB and refreshing the UI.
    [self.leftTable reloadData];
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowUpdateTaskViewController"] )
    {
        
        NSIndexPath *indexPath = [self.leftTable indexPathForSelectedRow];
        Task *task = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        UpdateTaskViewController *updateTaskViewController = [segue destinationViewController];
        updateTaskViewController.delegate = self;
        updateTaskViewController.task = task;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddTaskViewController"])
    {
        
        AddTaskViewController *addTaskViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];

        addTaskViewController.managedObjectContext = APP_DELEGATE_MGDOBJCNTXT;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowTabFilterViewController"]) 
    {
        TabFilterViewController *tabFilterVC = (TabFilterViewController *) [segue destinationViewController];
        tabFilterVC.filterLaunchedFrom = View;
        
        [segue destinationViewController];
    }
    if ([[segue identifier] isEqualToString:@"ShowSubtasksViewController"])  {
        
        if (self.passToSegueSwipedOnIdxPath) { // if swipe is unable to produce desired task, section closed may be, do nothing)
            SubtasksViewController *nlVC = (SubtasksViewController *) [segue destinationViewController];
            nlVC.task = self.passToSegueSwipedOnIdxPath;
        }
        
    }
    else {
//        @throw Exc
    }
}


#pragma Section Open Close API Handling on FilterDictionary

// Rename this to markSectionAsClosed
- (void) addClosedSectionName: (NSString *) sectionName SectionNumber: (NSInteger) sectionNumber {
    
    NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
//    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
//    NSString *subKeySectionNo = [NSString stringWithFormat:@"%i", sectionNumber];
//    NSString *subValueSectionNo= [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *subKeySectionNo  = [NSNumber numberWithInt:sectionNumber];
//    NSNumber *subValueSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
    if (valueSet )
        //        [ dictValue setObject:sectionNumberStr forKey:sectionName];
        [valueSet addObject: subKeySectionNo];        
    else {
        valueSet = [[NSMutableOrderedSet alloc] init ];
        //        [dictValue setObject:sectionNumberStr forKey:sectionName];
        [valueSet addObject: subKeySectionNo];        
        [self.colpsedSectionNameDict setObject:valueSet forKey:keySegmentNo];
    }
    DLog(@"AddClosedSecName:         Section:%@ keySegmentNo: %@   %@: colpsedSectionNameDict", subKeySectionNo, keySegmentNo,  [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}

#pragma Section Open Close API Handling on FilterDictionary
- (void) removeOpenSectionName: (NSString *) sectionName  Section: (NSInteger) sectionNumber{
    
    NSNumber *groupSegmentIndex = [NSNumber numberWithInteger: self.selSegmentIdx];
//    NSString *grpSectionNo = [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *grpSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    //    NSString *groupSectionNameOpened = sectionName;
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:groupSegmentIndex];
    if (valueSet )
//        [ dictValue removeObjectForKey: [NSString stringWithFormat:@"%i", sectionNumber]];
        [ valueSet removeObject: grpSectionNo];
    else {
        DLog(@"Unable to find the object to remove for key: %@", grpSectionNo);
    }
    DLog(@"RemoveOpenSecName: Key:%@ Item:%@   %@: colpsedSectionNameDict", groupSegmentIndex, grpSectionNo, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}


#pragma mark - GroupHeaderSectionView Delegate API

- (void) uitableSectionHeaderView:(SectionHeaderView *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.sectionNameLbl.text Section:sectionHeaderView.sectionIdx];
            
            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.sectionNameLbl.text SectionNumber:sectionHeaderView.sectionIdx];
            break;
        }
        case SECTION_ADD_ITEM:{
            [self launchAddItemOnSection:section];
            break;
        }
        default:
            break;
    }
    
    [self forceReloadThisUI];
}

- (void) THREE_uitableSectionHeaderView:(UITableSectionHeaderView *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.titleLabel.text Section:sectionHeaderView.section];
            //            [self removeOpenSectionName:sectionHeaderView.groupManagedObjectID Section:sectionHeaderView.section];
            
            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.titleLabel.text SectionNumber:sectionHeaderView.section];
            //            [self addClosedSectionName:sectionHeaderView.groupManagedObjectID SectionNumber:sectionHeaderView.section];
            break;
        }
        default:
            break;
    }
    
    [self forceReloadThisUI];
}


- (void) ONE_uitableSectionHeaderView:(SectionHeaderCell *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.sectionNameLbl.text Section:sectionHeaderView.sectionIdx];
//            [self removeOpenSectionName:sectionHeaderView.groupManagedObjectID Section:sectionHeaderView.section];

            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.sectionNameLbl.text SectionNumber:sectionHeaderView.sectionIdx];
//            [self addClosedSectionName:sectionHeaderView.groupManagedObjectID SectionNumber:sectionHeaderView.section];            
            break;
        }
        default:
            break;
    }
    
    [self forceReloadThisUI];
}

// Added to record the way of launching add new task on a section
-(void) launchAddItemOnSection:(int) section {
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
    
    [AppSharedData getInstance].lastSelectedGroup = task.group;
    
    [self performSegueWithIdentifier:@"ShowAddTaskViewController" sender:self];
}

#pragma mark - Add/Update controller delegate
- (void) updateTaskViewControllerDidCancel:(UpdateTaskViewController *)controler {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) updateTaskViewControllerDidFinish:(UpdateTaskViewController *)controller task:(Task *)task WithDelete:(BOOL)isDeleted
{
    if (task) {
        if (isDeleted) {
            [AppUtilities deleteTask:task];
        }
        else {
            [AppUtilities saveTask:task];
        }
    }
    //    [self dismissModalViewControllerAnimated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
    //    [self.tableView reloadData];
    
    [self forceReloadThisUI];
}

#pragma mark - TaskViewCell Delegate
-(void) notifyShowSubtasksClicked:(Task *)task Cell:(TaskViewCell *)cell {
    //Now cache the task identified for passing to segue api
    self.passToSegueSwipedOnIdxPath = task;
    
    //    NSLog(@"Debug: Task:%@ Cell:%@ X:%f Y:%f", taskViewCell.task.name, [taskViewCell description], location.x, location.y );
    if (self.passToSegueSwipedOnIdxPath) { //Bug Fix: if swipe is unable to produce desired task, section closed may be, do nothing)
        [self performSegueWithIdentifier:@"ShowSubtasksViewController" sender:self];
    }
}

#pragma mark - FetchResuls controller
-(NSFetchedResultsController *) fetchedResultsController {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return  nil;
}

#pragma mark: Save & Restore state api implementation
-(void) encodeRestorableStateWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.colpsedSectionNameDict   forKey:@"colpsedSectionNameDict"];
    [coder encodeObject:self.segmentSectionsLoaded   forKey:@"segmentSectionsLoaded"];
    DLog(@"CommonViewController:Encode:%@ - colpsedSectionNameDict %@", self.thisInterfaceEntityName, self.colpsedSectionNameDict);
    DLog(@"CommonViewController:Encode:%@ - segmentSectionsLoaded %@", self.thisInterfaceEntityName, self.colpsedSectionNameDict);
}

- (void) decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSDictionary *colpSec = [coder decodeObjectForKey:@"colpsedSectionNameDict"];
    NSDictionary *segSec = [coder decodeObjectForKey:@"segmentSectionsLoaded"];

    self.colpsedSectionNameDict = [self copyCacheDictionary:colpSec];
    self.segmentSectionsLoaded  = [self copyCacheDictionary:segSec];
  
    DLog(@"CommonViewController:Decode:%@ - colpsedSectionNameDict %@", self.thisInterfaceEntityName,self.colpsedSectionNameDict);
    DLog(@"CommonViewController:Decode:%@ - segmentSectionsLoaded %@", self.thisInterfaceEntityName, self.colpsedSectionNameDict);
}

// Not a general purpose API but understand the types used the cache object to copy
-(NSMutableDictionary *) copyCacheDictionary:(NSDictionary *) inputDict {
    NSMutableDictionary *outDict = [[NSMutableDictionary alloc] init];
    for (NSNumber *n in inputDict) {
        // We were getting type exception when restoring directly on expand/collapse ([__NSOrderedSetI addObject:]: unrecognized selector sent to instance) so added manual copying to exact types
        NSMutableOrderedSet *tmOSet = [[NSMutableOrderedSet alloc] init];
        
        for (id d in [inputDict objectForKey:n]) {
            [tmOSet addObject:d];
        }
    }
    
    return outDict;
}


// Launching add view on swipe down (DISABLED USE AS OF NOW)
#pragma mark - Swipe Gesture Recognizers
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.contentOffset.y <= -40) {
//        [self performSegueWithIdentifier:@"ShowAddTaskViewController" sender:self];
    }
    if (scrollView.contentOffset.y > 40 ) {
//        ULog(@"GLAD");
    }
}

@end
