//
//  CommonDualViewController.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/08.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//


#import "CommonDualViewController.h"
#import "SectionHeaderCell.h"
#import "AppDelegate.h" // Included to get the manged object context macro
#import "CoreDataOperations.h"

static NSString* tableViewReusableCell = @"TaskViewCell";
static NSString* collnViewReusableCell = @"GroupCollViewCell";
static NSString* toolbarReusableCell   = @"ToolbarCell";
static int NO_ROW_WAS_HIGHLIGHTED = -1;

@interface CommonDualViewController ()

// <SegmentID, <SectionIDs OrderedSet>>
// NSMutableDictionary => <NSNumber:SegmentNo <NSMutableOrderedSet: CollapsedSectionNumber(NSNumber)>
@property (nonatomic, strong) NSMutableDictionary *colpsedSectionNameDict; // sections in collapsed state
@property (nonatomic, strong) NSMutableDictionary *segmentSectionsLoaded; // Segment Number {Section on first load was loaded as collapsed). Section in this list are already loaded once.

@property (nonatomic) int direction; // Direction: Add/Delete/Move indicator to allow fixing the cache in the same order
@property (nonatomic) int deletedOrInsertedSection;

// Created to help find the right section number when we try scrolling left table based on the group selection. We need to remove the groups of zero count, hence tracking them here to. Using ordered to allow break as soon as find an item
@property (nonatomic, strong) NSMutableOrderedSet *sectionWithZeroCount;

// To deselect the last highlighted cell, store the value when highlighted to allow resetting the highlight on lost focus or move of drag to another item
@property (nonatomic) int lastHighlightedCollnViewCell;
// To track the last selected toolbar item
@property (nonatomic) int lastHighlightedToolbarCell;

// DataSource Arrays for ModeGain and ModeDue
@property (nonatomic, strong) NSMutableArray *dataSrc_Gain;
@property (nonatomic, strong) NSMutableArray *dataSrc_Due;
/// DataSource Array to store the precreated 
@property (nonatomic, strong) NSMutableArray *dataSrc_ToolbarItems;

//Keyboard show/hide, just ocpied did not attempt inside http://stackoverflow.com/questions/13845426/generic-uitableview-keyboard-resizing-algorithm
@property BOOL keyboardShown;
@property CGFloat keyboardOverlap;
@end

@implementation CommonDualViewController

//@synthesize mgdObjContext = _mgdObjContext;
@synthesize leftTableFetchedResultsControllerObj = _leftTableFetchedResultsControllerObj, rightColnFetchedResultsControllerObj = _rightColnFetchedResultsControllerObj;
@synthesize selSegmentIdx = _selSegmentIdx;

@synthesize colpsedSectionNameDict = _colpsedSectionNameDict, segmentSectionsLoaded = _segmentSectionsLoaded, direction = _direction, deletedOrInsertedSection = _deletedOrInsertedSection, lastHighlightedCollnViewCell = _lastHighlightedCollnViewCell, dataSrc_Gain = _dataSrc_Gain, dataSrc_Due = _dataSrc_Due, dataSrc_ToolbarItems = _dataSrc_ToolbarItems, lastHighlightedToolbarCell = _lastHighlightedToolbarCell;

@synthesize thisInterfaceEntityName = _thisInterfaceEntityName;

@synthesize keyboardShown = _keyboardShown, keyboardOverlap = _keyboardOverlap;

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.colpsedSectionNameDict        = [[NSMutableDictionary alloc] init ];
    self.segmentSectionsLoaded         = [[NSMutableDictionary alloc] init ];
    
    self.leftTable.sectionHeaderHeight = HEADER_HEIGHT;
    
    //    // Load the data on first run
    //    [self fetchData];
    
    // Load Drag N Drop Feature
    [self registerDragNDrop];
    
    // INIT
    self.sectionWithZeroCount = [[NSMutableOrderedSet alloc] init];
    
    // Set the lastselectedrow = -1 to avoid its folder open state
    self.lastHighlightedCollnViewCell = NO_ROW_WAS_HIGHLIGHTED; // -1 indicates no last highlighted cell
    // Set the lastSelectedCell = 0 i.e. default mode
    self.lastHighlightedToolbarCell = 0;
    
    // Clear the task detail view to show blank values, don't want to blank in storyboard as values there help readability.
    [self.taskDetailView reset];
    
    // Load Data Source
    [self loadDataSource_Gain];
    [self loadDataSource_Due];
    
    [self loadDataSource_ToolbarItems];
    
//    [self registerKeyboardShowHide]; // NOt working well here
}

//TODO:
-(void) switchFocusToMode:(TypeModeShow) selMode {
    
}


-(void) loadDataSource_Gain {
    self.dataSrc_Gain = [[NSMutableArray alloc] initWithObjects:
                         NSLocalizedString(@"High"   , @"High")
                         ,NSLocalizedString(@"Medium", @"Medium")
                         ,NSLocalizedString(@"Normal", @"Normal")
                         ,NSLocalizedString(@"Low"   , @"Low")
                         ,nil];

}

-(void) loadDataSource_Due {
    self.dataSrc_Due = [[NSMutableArray alloc] initWithObjects:
                         NSLocalizedString(@"Today", @"High")
                         ,NSLocalizedString(@"Tomorrow", @"Tommorrow")
                         ,NSLocalizedString(@"Weekend", @"Weekend")
                         ,NSLocalizedString(@"Next Week", @"Next Week")
                         ,NSLocalizedString(@"Next Month", @"Next Month")
                         ,nil];
    
}
-(void) loadDataSource_ToolbarItems {
    self.dataSrc_ToolbarItems = [[NSMutableArray alloc] init];
    
    ToolbarItem *groupTBItem  = [[ToolbarItem alloc] initWithIndex:0 Title:NSLocalizedString(@"Group", @"Group")   IconImg:@"Group" Count:-1];
    ToolbarItem *tagTBItem    = [[ToolbarItem alloc] initWithIndex:1 Title:NSLocalizedString(@"Tags", @"Tags")     IconImg:@"Tag"   Count:-1];
    ToolbarItem *gainTBItem   = [[ToolbarItem alloc] initWithIndex:2 Title:NSLocalizedString(@"Gain",@"Gain")      IconImg:@"Gain"  Count:-1];
    ToolbarItem *dueTBItem    = [[ToolbarItem alloc] initWithIndex:3 Title:NSLocalizedString(@"Due", @"Due")       IconImg:@"Due"   Count:-1];
    ToolbarItem *detailTBItem = [[ToolbarItem alloc] initWithIndex:4 Title:NSLocalizedString(@"Detail", @"Detail") IconImg:@"Detail"  Count:-1];
    
    [self.dataSrc_ToolbarItems addObject:groupTBItem];
    [self.dataSrc_ToolbarItems addObject:tagTBItem];
    [self.dataSrc_ToolbarItems addObject:gainTBItem];
    [self.dataSrc_ToolbarItems addObject:dueTBItem];
    [self.dataSrc_ToolbarItems addObject:detailTBItem];
    
}

// Do the keyboard adjustments
-(void) registerKeyboardShowHide {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    if(self.keyboardShown)
        return;
    
    self.keyboardShown = YES;
    
    // Get the keyboard size
    UIScrollView *tableView;
    if([self.leftTable.superview isKindOfClass:[UIScrollView class]])
        tableView = (UIScrollView *)self.leftTable.superview;
    else
        tableView = self.leftTable;
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [tableView.superview convertRect:[aValue CGRectValue] fromView:nil];
    
    // Get the keyboard's animation details
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    
    // Determine how much overlap exists between tableView and the keyboard
    CGRect tableFrame = tableView.frame;
    CGFloat tableLowerYCoord = tableFrame.origin.y + tableFrame.size.height;
    self.keyboardOverlap = tableLowerYCoord - keyboardRect.origin.y;
    if(self.inputAccessoryView && self.keyboardOverlap>0)
    {
        CGFloat accessoryHeight = self.inputAccessoryView.frame.size.height;
        self.keyboardOverlap -= accessoryHeight;
        
        tableView.contentInset = UIEdgeInsetsMake(0, 0, accessoryHeight, 0);
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, accessoryHeight, 0);
    }
    
    if(self.keyboardOverlap < 0)
        self.keyboardOverlap = 0;
    
    if(self.keyboardOverlap != 0)
    {
        tableFrame.size.height -= self.keyboardOverlap;
        
        NSTimeInterval delay = 0;
        if(keyboardRect.size.height)
        {
            delay = (1 - self.keyboardOverlap/keyboardRect.size.height)*animationDuration;
            animationDuration = animationDuration * self.keyboardOverlap/keyboardRect.size.height;
        }
        
        [UIView animateWithDuration:animationDuration delay:delay
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{ tableView.frame = tableFrame; }
                         completion:^(BOOL finished){ [self tableAnimationEnded:nil finished:nil contextInfo:nil]; }];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    if(!self.keyboardShown)
        return;
    
    self.keyboardShown = NO;
    
    UIScrollView *tableView;
    if([self.leftTable.superview isKindOfClass:[UIScrollView class]])
        tableView = (UIScrollView *)self.leftTable.superview;
    else
        tableView = self.leftTable;
    if(self.inputAccessoryView)
    {
        tableView.contentInset = UIEdgeInsetsZero;
        tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
    
    if(self.keyboardOverlap == 0)
        return;
    
    // Get the size & animation details of the keyboard
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [tableView.superview convertRect:[aValue CGRectValue] fromView:nil];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    
    CGRect tableFrame = tableView.frame;
    tableFrame.size.height += self.keyboardOverlap;
    
    if(keyboardRect.size.height)
        animationDuration = animationDuration * self.keyboardOverlap/keyboardRect.size.height;
    
    [UIView animateWithDuration:animationDuration delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{ tableView.frame = tableFrame; }
                     completion:nil];
}

- (void) tableAnimationEnded:(NSString*)animationID finished:(NSNumber *)finished contextInfo:(void *)context
{
    //    // Scroll to the active cell
    //    if(self.activeCellIndexPath)
    //    {
    //        [self.leftTable scrollToRowAtIndexPath:self.activeCellIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    //        [self.leftTable selectRowAtIndexPath:self.activeCellIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    //    }
}


- (void) viewWillAppear:(BOOL)animated {
    // Added this line to fix the bug which was crashing the app when filter ui is selected on a group and try to add the task
    self.leftTableFetchedResultsControllerObj = nil;
    self.rightColnFetchedResultsControllerObj = nil;
    
    // Load the data on first run
    [self forceReloadThisUI];
}

- (void) viewWillDisappear:(BOOL)animated {
    // Added this line to fix the bug which was crashing the app when filter ui is selected on a group and try to add the task
    self.leftTableFetchedResultsControllerObj = nil;
    self.rightColnFetchedResultsControllerObj = nil;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    self.leftTableFetchedResultsControllerObj = nil;
    self.rightColnFetchedResultsControllerObj = nil;
    
}


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = LANDSCAPE_WIDTH_FACTOR;
    else {
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = 0;
    }
    [self forceReloadThisUI];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return IS_PAD || (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 36; // Setting the height on the storyboard was not coming up so manually setting it here.
}


#pragma mark - Section Name API

-(BOOL) isSectionUserCollapsed: (NSInteger)sectionNumber  {
    
    BOOL isSectionUserCollapsed = NO;
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
    
    //    NSString *computedSectionName = [self getManagedObjID:sectionNumber];// [NSString stringWithFormat:@"%i", sectionNumber] ; //[self computeSectionNameFromRawName:sectionNumber];
    
    //    NSString *grpSectionNo =[NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *grpSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:key];
    if ([valueSet containsObject:grpSectionNo] )
        isSectionUserCollapsed = YES;
    else {
        isSectionUserCollapsed = NO;
    }
    
    DLog(@"isSectionUserCollapsed:%@ Section:%i, Segment:%@  ComputedName:%@   %@: colpsedSectionNameDict", isSectionUserCollapsed? @"YES": @"NO", sectionNumber, key,  grpSectionNo, [AppUtilities cleanDictSetPrint: _colpsedSectionNameDict]);
    return isSectionUserCollapsed;
}

- (NSString *) getManagedObjID: (NSInteger) section {
    
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self leftTableFetchedResultsController] objectAtIndexPath:newPath];
    
    NSString *objectIDStr = [[[task.group objectID] URIRepresentation] absoluteString];
    if ( objectIDStr == nil || [objectIDStr length] == 0 ) {
        objectIDStr = EMPTY;
    }
    
    return objectIDStr;
}

- (NSString *) computeSectionNameFromRawName: (NSInteger) section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self leftTableFetchedResultsController] sections] objectAtIndex:section];
    
    
    // TODO: Code Clean up and bettername
    NSString *sectionName = nil;
    
    // NSString *groupName = task.group.name; //sectionInfo.name
    NSString *dbQryGrpName = sectionInfo.name;
    
    switch ( self.selSegmentIdx ) {
        case 0:
        {
            /*
             sectionName = dbQryGrpName;//[sectionInfo name];
             
             // AS I was not able to get the right sorting for group name, getting it sorted by sort_order and finding the group name selectively.
             Group *grp = [AppUtilities fetchGroupBySortOrder:groupName  ManagedContext:self.mgdObjContext];
             groupName = grp.name;
             //            NSLog(@"Section Name%@ %@", sectionName, groupName);
             
             // Display the project name as section headings.
             sectionName =  ([groupName length] == 0) ? @"Empty" : groupName;
             */
            // TODO: Check if the above one is faster or there is another faster method to do this
            NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
            Task *task =     [[self leftTableFetchedResultsController] objectAtIndexPath:newPath];
            
            sectionName = task.group.name;
            sectionName =  ([sectionName length] == 0) ? EMPTY : sectionName;
            
            break;
        }
        case 1: {
            sectionName = dbQryGrpName;
            break;
        }
        case 2:
            sectionName = dbQryGrpName;
            break;
        case 3:
            sectionName = dbQryGrpName;
            break;
        case 4:
            sectionName = dbQryGrpName;
            break;
        default:
            sectionName = dbQryGrpName;
            break;
    }
    
    DLog(@"ComputeName:              Section:%i, SectionInfoName: %@ newSectionName: %@: selSegIdx:%i END", section, sectionInfo.name, sectionName, self.selSegmentIdx);
    
    return sectionName;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ( tableView == self.leftTable) {
        // Return the number of sections.
        int count = [[[self leftTableFetchedResultsController] sections] count];
        DLog(@"numberOfSectionsInTableView: %i", count);
        return count;
    }
    else {
        // kept most of the code in TaskDetailView to handle subtask
        return 0;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"noOfRowsInsection: START  Section:%i", section);
    
    BOOL val = [self isSectionUserCollapsed:section];
    if (val) {
        DLog(@"noOfRowsInsection: END    Section:%i isUserCollapsed rows:0", section);
        return 0;
    }
    
    // Get the value from the array maintained per groupSegment
    if ([self isSectionCollapsedAsFirstTimeLoad: section]) {
        // mark these sections so that they don't appear as first load again
        [self markSectionLoadedCollapsedOnFirstLoad:section];
        
        // Also put it in the closed section names to get the section as collapsed
        //        NSString *sectionName = [self getManagedObjID:section];//[self computeSectionNameFromRawName:section];
        NSString *sectionName = [NSString stringWithFormat:@"%i", section];
        [self addClosedSectionName:sectionName SectionNumber:section];
        
        DLog(@"noOfRowsInsection: END    Section:%i isSectionCollapsedAsFirstTimeLoad rows:0", section);
        return 0;
    }
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self leftTableFetchedResultsController] sections] objectAtIndex:section];
    DLog(@"noOfRowsInSection: END    Section:%i Returned Actual Row Count (%i).", section, [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
    
}

// Ensuring update is alwasy avaialble on selection by adding in common view
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.leftTable)
    {
        // When we added a new detail view, we wanted a way to edit a task, so allowing to edit if the current mode is not detail mode.
        if (self.selDataModeForCollnView == ModeShowTaskDetail) { //Show teh details of the current task selected
            Task *task = (Task *)[[self leftTableFetchedResultsController] objectAtIndexPath:indexPath];
            [self.taskDetailView initializeForTask:task];
    }
        else {
          [self performSegueWithIdentifier:@"ShowUpdateTaskViewController" sender:self]; //Allow edit of the selected task
        }
    }
    else {
        
    }
}

/* Allow changing the focus of tableview to the group which is selected.
 * As of now supporting on click on group as other modes are not implemented or not relevant.
 */
-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if (collectionView == self.rightCollection) {
        switch (self.selDataModeForCollnView) {
            case ModeShowGroups: // Group
            {
                /* http://stackoverflow.com/questions/159821/how-do-i-scroll-a-uitableview-to-a-section-that-contains-no-rows
                 * Scroll tableview to the desired location.
                 * NSIndexPath *groupSectionInxPath = [NSIndexPath indexPathForItem:0 inSection:indexPath.row];
                 * [self.leftTable scrollToRowAtIndexPath:groupSectionInxPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
                 * This method was working only for open sections so used below height attributed to refer closed sections also.
                 */
                
                // Scroll tableview to the desired location on click/select of the group
                
                // As teh section may have zero records and hence may not be appearing on the lefttable. So moving to left table section based on row number could be wrong. So we need to remove the count of sections with 0 from row count.
                int scrollToSection = 0; // start with zero
                scrollToSection     = indexPath.row; // init with row
                
                if ([self.sectionWithZeroCount containsObject:[NSNumber numberWithInt:scrollToSection]]) {
                    // this is a section which is having zero count, so we don't need to do navigation on the left table
                    return;
                }
                else {
                    for (int i=0; i<indexPath.row; i++) {
                        if ([self.sectionWithZeroCount containsObject:[NSNumber numberWithInt:i]]) {
                            scrollToSection = scrollToSection - 1;
                        }
                    }
                    CGRect sectionRect = [self.leftTable rectForSection:scrollToSection];
                    sectionRect.size.height = self.leftTable.frame.size.height;
                    [self.leftTable scrollRectToVisible:sectionRect animated:YES];
                }
                
                //    NSLog(@"didSelect Coln: move to LeftTable Section:%d for coll row:%d", scrollToSection, indexPath.row);
            }
                break;
            case ModeShowGain: //Gain
            {
                break;
            }
            case ModeShowDue: //Due
            {
                break;
            }
            case ModeShowTags: // Tags
            {
                break;
            }
            default:
                break;
        }
    }
    else if ( collectionView == self.rightToolbarCollnView) {
        
        // Keep the detail view hidden until required
        [self.taskDetailView setHidden:YES];
        
        // Dehighlight the cell in the right collection
        [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
        
        // Deselect the last tool bar item
        NSIndexPath *oldIdxPath = [NSIndexPath indexPathForRow:self.lastHighlightedToolbarCell inSection:0];
        ToolbarCell *lastCell = (ToolbarCell*) [self.rightToolbarCollnView cellForItemAtIndexPath:oldIdxPath];
        [lastCell setHighlighted:NO];
        
        // Switch to appropriate mode
        switch (indexPath.row) {
            case 0: //ModeShowGroups
            {
                self.selDataModeForCollnView        = ModeShowGroups;
                break;
            }
            case 1: {//ModeShowTags
                
                self.selDataModeForCollnView        = ModeShowTags;
                break;
            }
            case 2: { //ModeShowGain
                self.selDataModeForCollnView         = ModeShowGain;
                break;
            }
            case 3: {//ModeShowDue
                self.selDataModeForCollnView        = ModeShowDue;
                break;
            }
            case 4: {//ModeShowTaskDetail
                [self.taskDetailView setHidden:NO]; // Additionally enable this view
                self.selDataModeForCollnView = ModeShowTaskDetail;
                break;
            }
            default:
                break;
        }
        
        [self forceReloadThisUI]; // had to refresh both to show the changed table cell for tags
        
        // Enable the highlight
        ToolbarCell *tbcell = (ToolbarCell*) [self.rightToolbarCollnView cellForItemAtIndexPath:indexPath];
        tbcell.highlighted = YES;
        self.lastHighlightedToolbarCell = indexPath.row;
    }
}

- (void) markSectionLoadedCollapsedOnFirstLoad: (NSInteger) section {
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
    //    NSString *item = [NSString stringWithFormat:@"%i", section];
    NSNumber *item = [NSNumber numberWithInt:section];
    
    NSMutableOrderedSet *value = [self.segmentSectionsLoaded objectForKey:key];
    if (value == nil) {
        value = [[NSMutableOrderedSet alloc] init];
        [value addObject:item];
        
        [self.segmentSectionsLoaded setObject:value forKey:key];
    }
    else {
        [value addObject:item];
    }
    DLog(@"MarkSectionLoadedCollapsedOnFirstLoad: Section: %i %@: segmentSectionsLoaded", section, [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
}
- (BOOL) isSectionCollapsedAsFirstTimeLoad: (NSInteger) sectionNumber {
    /*
     int selCollapsibleOnIdx = self.selSegmentIdx;
     
     BOOL valueAtIdex = [[self.isSegmentSectionsFirstLoadArr objectAtIndex:selCollapsibleOnIdx] boolValue];
     
     //    NSLog(@"Check if SelectedCollonFirstLoad: %@", self.isSectionFirstLoadArray);
     */
    
    BOOL isSectionFirstLoad = NO;
    
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
    //    NSString *item = [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *item = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *value = [self.segmentSectionsLoaded objectForKey:key];
    
    if (value !=nil && [value containsObject:item])
        isSectionFirstLoad = NO;
    else {
        isSectionFirstLoad = YES;
    }
    
    DLog(@"isSectionCollapsedAtFirstTimeLoad: %@ ForSection:%@ selSegmentIdx:%@    %@: segmentSectionsLoaded", isSectionFirstLoad ? @"Yes" : @"NO", item, key,  [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
    
    return isSectionFirstLoad;
}


-(UIView*)FOUR_tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = @"@BA"; //[self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self leftTableFetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL isSectionOpen = YES;
    if ([self isSectionUserCollapsed:section]){
        isSectionOpen = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    
    // Initialize custom header section view using custom prototype cell
    static NSString *cellID = @"SectionTVCell";
    SectionHeaderCell *sectionHeaderCell = (SectionHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (sectionHeaderCell == nil) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SectionHTVCell" owner:self options:nil];
        sectionHeaderCell = [subviewArray objectAtIndex:0];
//        NSLog(@"TEMP: INIT From Nib");
    }else {
//        NSLog(@"TEMP: INIT From QUEUE");
    }
    
    [sectionHeaderCell updateSectionViewData:isSectionOpen
                                       Index:section
                                        Name:sectionName
                                       Count:sectionItemCount
                                 GrpMgdObjID:grpManagedObjectID
                                         Tag:(SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                                    Delegate:self]; // To inform open/close of section
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    
    //    return sectionHeaderCell;
    
    UIView *view = [[UIView alloc] initWithFrame:[sectionHeaderCell frame]];
    [view addSubview:sectionHeaderCell];
    
    return view;
}

-(UIView*)THREE_tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self leftTableFetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL selectState = YES; // TODO: Change this name select name to be more meanigful - groupBtnStateCollapsed
    if ([self isSectionUserCollapsed:section]){
        selectState = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    UITableSectionHeaderView *ghsv = [[UITableSectionHeaderView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.leftTable.bounds.size.width, self.leftTable.bounds.size.height) title:sectionName section:section sectionItemCount: sectionItemCount selectState: selectState HeaderIndex: (section +1) groupMngedObjID:grpManagedObjectID delegate:self ];
    
    // To identify a group section for updates later
    ghsv.tag = SECTION_NUMBER_STARTFROM + section;
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    return ghsv;
}



-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self leftTableFetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL isSectionOpen = YES;
    if ([self isSectionUserCollapsed:section]){
        isSectionOpen = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    
    // Initialize custom header section view using custom prototype cell
    SectionHeaderView *sectionHeaderView = nil;
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SectionHeaderView" owner:self options:nil];
    sectionHeaderView = [subviewArray objectAtIndex:0];
    
    
    [sectionHeaderView updateSectionViewData:isSectionOpen
                                       Index:section
                                        Name:sectionName
                                       Count:sectionItemCount
                                 GrpMgdObjID:grpManagedObjectID
                                         Tag:(SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                                    Delegate:self]; // To inform open/close of section
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    
    return sectionHeaderView;
    
}


-(UIView*)ONE_tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self leftTableFetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL isSectionOpen = YES;
    if ([self isSectionUserCollapsed:section]){
        isSectionOpen = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    
    // Initialize custom header section view using custom prototype cell
    static NSString *cellID = @"SectionHeaderCell";
    SectionHeaderCell *sectionHeaderCell = (SectionHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    [sectionHeaderCell updateSectionViewData:isSectionOpen
                                       Index:section
                                        Name:sectionName
                                       Count:sectionItemCount
                                 GrpMgdObjID:grpManagedObjectID
                                         Tag:(SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                                    Delegate:self]; // To inform open/close of section
    
    
    DLog(@"viewForHeaderInSection: %i   ItemCount:%i End", section, sectionItemCount);
    
    //    return sectionHeaderCell;
    
    UIView *view = [[UIView alloc] initWithFrame:[sectionHeaderCell frame]];
    [view addSubview:sectionHeaderCell];
    
    return view;
}

#pragma mark configure cell
// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    TaskViewCell *mvaCell = (TaskViewCell *) cell;
    [mvaCell resetInitialText];
    
    Task *activityAtIndex =     [[self leftTableFetchedResultsController] objectAtIndexPath:indexPath];
    
    [AppUtilities configureCell:mvaCell Task:activityAtIndex];
    
    // If the mode is tags view then change the date label to show the tags instead. This makes it easy to see which tags are there and what changes we making to that.
    if ( self.selDataModeForCollnView == ModeShowTags) {
        // Addiitonal customization which is not standard across app.
        mvaCell.subtitleLabel.text = [AppUtilities getSelectedTagsLabelText:activityAtIndex.tagS];
    }
}

- (void)configureCell_ModeGroup:(GroupCollViewCell *)cell IndexPath: (NSIndexPath *)indexPath {

    [cell resetInitialText];     // Reset the cell
    
    Group *group = (Group *)[self.rightColnFetchedResultsController objectAtIndexPath:indexPath];
    
    //    int totalCount = [group.tasks count]; - USING Active count, is more indicative on the dualsection ui
    int activeCount = 0;
    for (Task *taskObj in group.tasks) {
        if ([taskObj.done intValue] == 0)
            activeCount +=1;
    }
    
    // Before reloading erase the cell's last Tag, as we deque it is possible old tag is there and when we set specialfocus there we might end up using this last set Tag, was useful in tag as we got special cell but added here for safety.
    cell.tag = TypeOfCellIsNormal;
    
    // If we reloading, setting the approriate state for cell.
    if ( indexPath.row == self.lastHighlightedCollnViewCell) {
        [self updateCellImage:cell Format:TypeOfCellIsSpecialFocusOn];
    }
    else {
        [self updateCellImage:cell Format:TypeOfCellIsNormal];
    }

    cell.name.text = group.name;
    // Set the count
//    [cell.countBtn setTitle: [NSString stringWithFormat:@"%i", activeCount] forState:UIControlStateNormal];
    [cell.countBtn setTitle: [NSString stringWithFormat:@"%i", activeCount] forState:UIControlStateDisabled]; // Bug Fix: if we don't set for disabled state on doubleclick on toolbar button, counts were jumbled up
    
    // Store section index of group with zero count and remove it is non zero. Using row as row in collection is equal to section number in left table.
    if (activeCount == 0) {
        [self.sectionWithZeroCount addObject:[NSNumber numberWithInt:indexPath.row]];
    }
    else {
        [self.sectionWithZeroCount removeObject:[NSNumber numberWithInt:indexPath.row]];
    }
}

- (void)configureCell_ModeTags:(GroupCollViewCell *)cell IndexPath:(NSIndexPath *)indexPath {
    
    [cell resetInitialText];     // Reset the cell
    
    // If the item is the manually additionally added we need to handle it manually (set label)
    if (indexPath.row < [[self.rightColnFetchedResultsControllerObj fetchedObjects] count]) {
        Tag *tag = (Tag *)[self.rightColnFetchedResultsController objectAtIndexPath:indexPath];
        
        //  Show the count for active tasks only
        int activeCount = 0;
        for (Task *taskObj in tag.taskS) {
            if ([taskObj.done intValue] == 0)
                activeCount +=1;
        }
        
        // Bug Fix:If we dragged on delete, it was ending up giving a delete icon on some cell after reload. So Before reloading erase the cell's last Tag, as we deque it is possible old tag is there and when we set specialfocus there we might end up using this last set Tag
        cell.tag = TypeOfCellIsNormal;
        
        // If we reloading, setting the approriate state for cell.
        if ( indexPath.row == self.lastHighlightedCollnViewCell) {
            [self updateCellImage:cell Format:TypeOfCellIsSpecialFocusOn];
        }
        else {
            [self updateCellImage:cell Format:TypeOfCellIsNormal];
        }
        
        cell.name.text = tag.name;
        [cell.countBtn setTitle: [NSString stringWithFormat:@"%i", activeCount] forState:UIControlStateDisabled];
    }
    else {
        [self updateCellImage:cell Format:TypeOfCellIsSpecialDelete];
        
        cell.name.text = NSLocalizedString(@"Clear", @"Clear");
        [cell.countBtn setTitle:@"" forState:UIControlStateNormal];
        [cell.countBtn setTitle:@"" forState:UIControlStateDisabled]; //If we don't set this it was not working
    }
}

- (void)configureCell_ModeGain:(GroupCollViewCell *)cell IndexPath:(NSIndexPath *)indexPath {

    [cell resetInitialText];     // Reset the cell
    
    long row = indexPath.row;
    NSString *name = [self.dataSrc_Gain objectAtIndex:indexPath.row];
    
    // Before reloading erase the cell's last Tag, as we deque it is possible old tag is there and when we set specialfocus there we might end up using this last set Tag, was useful in tag as we got special cell but added here for safety.
    cell.tag = TypeOfCellIsNormal;
    
    UIImage *img = [AppUtilities getGainImage:row];
    [cell.iconImg2 setImage:img forState:UIControlStateNormal];

    // If we reloading, setting the approriate state for cell.
    if ( indexPath.row == self.lastHighlightedCollnViewCell) {
        [self updateCellImage:cell Format:TypeOfCellIsSpecialFocusOn];
    }
    else {
        [self updateCellImage:cell Format:TypeOfCellIsNormal];
    }
    
    cell.name.text = name;
    
    // Set the count
    //    [cell.countBtn setTitle: [NSString stringWithFormat:@"%i", activeCount] forState:UIControlStateNormal];
    [cell.countBtn setTitle: [NSString stringWithFormat:@"%ld", row] forState:UIControlStateDisabled]; // Bug Fix: if we don't set for disabled state on doubleclick on toolbar button, counts were jumbled up
    
    // Find the Counts Related to this item
    long cnt = [self countTaskForGain: row];
    [cell.countBtn setTitle:[NSString stringWithFormat:@"%li", cnt] forState:UIControlStateDisabled];
}

-(long) countTaskForGain: (long) gainIdx {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"gain == %@ && done==0", [NSString stringWithFormat:@"%ld",gainIdx]];
    NSNumber *count = [CoreDataOperations aggregateOperation:@"count:" onEntity:@"Task" onAttribute:@"gain" withPredicate:predicate inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    
    return [count longValue];
}
-(long) countTaskForDue: (NSDate *) date {
    NSDate *nextDay = [AppUtilities getStartOfNextDayDate:date];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dueOn >= %@ && dueOn <= %@ && done==0", date, nextDay];
    NSNumber *count = [CoreDataOperations aggregateOperation:@"count:" onEntity:@"Task" onAttribute:@"gain" withPredicate:predicate inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    
    return [count longValue];
}

- (void)configureCell_ModeDue:(GroupCollViewCell *)cell IndexPath:(NSIndexPath *)indexPath {
    
    [cell resetInitialText];     // Reset the cell
    
    long row = indexPath.row;
    NSString *name = [self.dataSrc_Due objectAtIndex:indexPath.row];
    
    // Before reloading erase the cell's last Tag, as we deque it is possible old tag is there and when we set specialfocus there we might end up using this last set Tag, was useful in tag as we got special cell but added here for safety.
    cell.tag = TypeOfCellIsNormal;
    
    // If we reloading, setting the approriate state for cell.
    if ( indexPath.row == self.lastHighlightedCollnViewCell) {
        [self updateCellImage:cell Format:TypeOfCellIsSpecialFocusOn];
    }
    else {
        [self updateCellImage:cell Format:TypeOfCellIsNormal];
    }
    
    cell.name.text = name;
    // Set the count
    // Find the Counts Related to this item
    NSDate *date = [self generateDueOn:row];
    long cnt = [self countTaskForDue:date];
    [cell.countBtn setTitle:[NSString stringWithFormat:@"%li", cnt] forState:UIControlStateDisabled];
    
}

// Added this api for calling it from multiple places to set the right image
-(void) updateCellImage:(GroupCollViewCell *) cell Format:(int) typeOfCell{
    switch (typeOfCell) {
        case TypeOfCellIsNormal:
        {
            if ( self.selDataModeForCollnView ==  ModeShowDue) {//use different icon
                [cell.iconImg setImage:[UIImage imageNamed:[AppUtilities getImageName:@"Btn_Calendar"]]];
                [cell.iconImg setHighlightedImage:[UIImage imageNamed:[AppUtilities getImageName:@"Btn_Calendar"]]];
                
            }else {
                [cell.iconImg setImage:[UIImage imageNamed:[AppUtilities getImageName:@"Folder"]]];
                [cell.iconImg setHighlightedImage:[UIImage imageNamed:[AppUtilities getImageName:@"Folder"]]];
            }

            cell.tag = TypeOfCellIsNormal;
            break;
        }
        case TypeOfCellIsSpecialAdd: {
            cell.tag = TypeOfCellIsSpecialAdd;
            break;
        }
        case TypeOfCellIsSpecialDelete: {
            [cell.iconImg setImage:[UIImage imageNamed:           [AppUtilities getImageName:@"Btn_Delete"]]];
            [cell.iconImg setHighlightedImage:[UIImage imageNamed:[AppUtilities getImageName:@"Btn_Delete"]]];
            
            cell.tag = TypeOfCellIsSpecialDelete;
            
            break;
        }
        case TypeOfCellIsSpecialFocusOn: {
            if (cell.tag == TypeOfCellIsSpecialDelete) {
                // Use a different image for Focus
                [cell.iconImg setImage:[UIImage imageNamed:           [AppUtilities getImageName:@"Btn_Delete_Open"]]];
                [cell.iconImg setHighlightedImage:[UIImage imageNamed:[AppUtilities getImageName:@"Btn_Delete_Open"]]];
                
            }
            else {
                if ( self.selDataModeForCollnView == ModeShowDue ) {
                    [cell.iconImg setImage:[UIImage imageNamed:           [AppUtilities getImageName:@"Btn_Calendar_Open"]]];
                    [cell.iconImg setHighlightedImage:[UIImage imageNamed:[AppUtilities getImageName:@"Btn_Calendar_Open"]]];
                }
                else {
                    [cell.iconImg setImage:[UIImage imageNamed:           [AppUtilities getImageName:@"Btn_Folder_Open"]]];
                    [cell.iconImg setHighlightedImage:[UIImage imageNamed:[AppUtilities getImageName:@"Btn_Folder_Open"]]];

                }
                cell.tag = TypeOfCellIsNormal; // Bug Fix: We still use the type as normal.
            }
            
            break;
        }
        default:
            break;
    }
}

-(void) configureCollectionCell:(GroupCollViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    switch (self.selDataModeForCollnView) {
        case ModeShowGroups: // Group
        {
            [self configureCell_ModeGroup:cell IndexPath:indexPath];
            break;
        }
        case ModeShowGain: //Gain
        {
            [self configureCell_ModeGain:cell IndexPath:indexPath];
            break;
        }
        case ModeShowDue: //Due
        {
            [self configureCell_ModeDue:cell IndexPath:indexPath];
            break;
        }
        case ModeShowTags: // Tags
        {
            [self configureCell_ModeTags:cell IndexPath:indexPath];
            break;
        }
        default:
            break;
    }
}

-(void) configureCollectionTBCell:(ToolbarCell *)cell atIndexPath:(NSIndexPath *) indexPath {
    
    [cell resetInitialText];     // Reset the cell
    
    ToolbarItem *item = [self.dataSrc_ToolbarItems objectAtIndex:indexPath.row];
    
    cell.name.text     = item.title;
    cell.mainImg.image = [UIImage imageNamed:[AppUtilities getImageName:item.iconImg]];
 
    if (indexPath.row == self.lastHighlightedToolbarCell) {
        [cell setHighlighted:YES];
    }
    else {
        [cell setHighlighted:NO];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TaskViewCell";
    
    TaskViewCell *cell = (TaskViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /*     // Tried using TaskViewCell.xib, but found issue of setting segues, initializatin so leave it unless gain more knowledge
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (TaskViewCell *) [nib objectAtIndex:0];
     }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.leftTable){
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            DLog(@"Dynamic: commitEditingStyle: Starting Delete");
            [AppUtilities deleteTask:[[self leftTableFetchedResultsController] objectAtIndexPath:indexPath]];
            
            // After deleting the record update teh section count value. Added tags for this purpose and finding the section to upate values
            //        SectionHeaderCell *view =  (SectionHeaderCell *) [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)];
            SectionHeaderView *view =  (SectionHeaderView *) [self.leftTable viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)];
            //  NSLog(@"Descrit: %@", [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)]);
            
            //        if ( [view isKindOfClass:[ SectionHeaderCell class]] ) {
            if ( [view isKindOfClass:[ SectionHeaderView class]] ) {
                [view updateCountOnDelete:1]; // update name for rows deleted arg
            }
            else {
                NSLog(@"Error, unable to find the GroupHeaderSectionView, need to debug.");
            }
            //        id x = self.tableView;
            //        id y = [self.tableView viewWithTag:indexPath.section];
            //        id z = [self.tableView viewWithTag:0];
            //        id a = [self.tableView viewWithTag:3];
            
            
            
            
            // TODO: I NEED TO UPDATE THE COUNT ON THE SECTIONS HERE.
            //        // Delete the managed object.
            //        NSManagedObjectContext *context = [[self fetchedResultsController] managedObjectContext];
            //        [context deleteObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
            //
            //        NSError *error;
            //        if (![context save:&error]) {
            //            /*
            //             Replace this implementation with code to handle the error appropriately.
            //
            //             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //             */
            //            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            //            abort();
            //        }
            //
            
        }
        else if (editingStyle == UITableViewCellEditingStyleInsert) {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    DLog(@"Dynamic: commitEditingStyle: FINISHED Operation");
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    
    if (controller == self.leftTableFetchedResultsControllerObj) {
        // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
        [self.leftTable beginUpdates];
    }
    else {
        // performbatchupdates
    }
    DLog(@"Table End Update");
}


/* This API will add/delete/move (delete +add) a record in the table */
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    DLog(@"Dynamic: didChangeObject: Type: %i, atIndexSection: %i atIndexRow: %i, newIndexSection: %i newIndexRow: %i", type, indexPath.section, indexPath.row,newIndexPath.section, newIndexPath.row);
    //    DTLog(@"Debug: %@, %@, %@", indexPath, newIndexPath, anObject);
    
      if (controller == self.leftTableFetchedResultsControllerObj) {
          UITableView *tableView = self.leftTable;
          switch(type) {
                  // Had to put checks for section being in closed state else it was giving error as rows before and after update don't match. NoOfRows will continue return 0 even after add/update/delete and fail. To prevent it doing these checks. Additionally saving on same context was impacting tableview on other controllers and one section open in this view might be closed on other. To handle them had to put these checks.
              case NSFetchedResultsChangeInsert: {
                  // before using the newindex reloading the cache to ensure the isSectionUserCollapsed: check happens on the newer section number (say section is added, so wd not b there in cache but reload cache will handle this by shifting the cached section number
                  [self reloadUserCollapsedListOnSectionDelete:newIndexPath.section Direction:self.direction];
                  
                  self.direction=0;
                  self.deletedOrInsertedSection = newIndexPath.section;
                  
                  if (![self isSectionUserCollapsed:newIndexPath.section]) {
                      [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                  }
                  break;
              }
              case NSFetchedResultsChangeDelete:
                  if (![self isSectionUserCollapsed:indexPath.section]) {
                      [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                  }
                  break;
              case NSFetchedResultsChangeUpdate:
                  if ( ![self isSectionUserCollapsed:indexPath.section] ) {
                      [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
                  }
                  break;
                  
              case NSFetchedResultsChangeMove: {
                  //            if (indexPath.section == newIndexPath.section) {
                  //                // even though there is a move somehow the old and new section numbers were same, don't know why. BUGGY
                  //                [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
                  //
                  //                self.direction=0;
                  //                self.deletedOrInsertedSection = -1;
                  //
                  ////                NSIndexPath *reloadedIndexPath = [NSIndexPath indexPathForRow:(indexPath.row + self.direction) inSection:indexPath.section];
                  //              //  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                  ////                [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                  //            }
                  //            else {
                  // As we have open & closed sections we need to check the section is closed or open to see if actual deleted or open operation is required or not.
                  if (! [self isSectionUserCollapsed:indexPath.section] ) {
                      DLog(@"Deleting Row: section: %i row:%i", indexPath.section, indexPath.row);
                      [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                  }
                  
                  // before doing a section open/close check on newIndexPath reloading the cache to adjust for add/delete of section
                  [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
                  
                  self.direction=0; // operation move is direction=0
                  self.deletedOrInsertedSection = newIndexPath.section; // just following the process to set the available section number. this value assign should not matter as of today though
                  
                  if ( ![self isSectionUserCollapsed:newIndexPath.section] ) {
                      DLog(@"Inserting Row: section: %i row:%i", newIndexPath.section, newIndexPath.row);
                      [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                  }
                  //            }
                  break;
              }
          }
      }
      else {
          UICollectionView *colnView = nil;
          if ( controller == self.rightColnFetchedResultsControllerObj){
             colnView = self.rightCollection;
          }
          else {
              colnView = self.rightToolbarCollnView;
          }
         
          switch (type) {
              case NSFetchedResultsChangeInsert: {
                  [colnView insertItemsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]];
                  break;
              }
              case NSFetchedResultsChangeDelete:
                  [colnView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                  break;
              case NSFetchedResultsChangeUpdate: {
                  GroupCollViewCell *cell = (GroupCollViewCell *) [colnView cellForItemAtIndexPath:indexPath];
                  [self configureCollectionCell:cell atIndexPath:indexPath];
                  break;
              }
              case NSFetchedResultsChangeMove: {
                  [colnView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                  [colnView insertItemsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]];
              }
                  break;
          }
      }
    DLog(@"Dynamic: didChangeObject: Finished Update.");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    DLog(@"Dynamic: didChangeSection: Type: %i, atIndexSection: %i ", type, sectionIndex);
    if (controller == self.leftTableFetchedResultsControllerObj) {
        switch(type) {
            case NSFetchedResultsChangeInsert: {
                // MOVED LATER
                // To prevent getting an error because of it being marked as collapsed and item count returning as 0, adding it as if it is already collapsed
                //            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
                
                [self.leftTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
                self.direction = 1;
                self.deletedOrInsertedSection = sectionIndex;
                break;
            }
            case NSFetchedResultsChangeDelete: {
                [self.leftTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
                self.direction = -1;
                self.deletedOrInsertedSection = sectionIndex;
                break;
            }
        }
    }
    else {
        UICollectionView *colnView = nil;
        if ( controller == self.rightColnFetchedResultsControllerObj){
            colnView = self.rightCollection;
        }
        else {
            colnView = self.rightToolbarCollnView;
        }
        
        switch(type) {
            case NSFetchedResultsChangeInsert: {
                [colnView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
                break;
            }
            case NSFetchedResultsChangeDelete: {
                [colnView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
                break;
            }
        }
    }
    
    DLog(@"Dynamic: didChangeSection: Section Did change: Finished Update.");
}

/* Added this api to update the cache storing the closed sections. When a section is inserted or deleted the section numbers are
 * no longer valid and should be readjusted (BETTER USE ARRAY TO ADD/DELETE ITEMS, LATER)
 * Since we need to start from top or bottom using the direction for insert/delete operation
 * Moved the marking API here as wanted it to be run just before begining updates ... forget why?
 * calling this API before the updates and sometimes when objectchanged and somehow the new and old section was coming same and creating problem
 */

-(void) reloadUserCollapsedListOnSectionDelete: (int) sectionIndex Direction:(int) direction
{
    DLog(@"ReloadUserCollapsedListOnSectionDelete: Start SectionIdx: %i Direction_1Add_-1Del_0Move:%i %@: colapsedSectionNameDict  %@: segmentSectionsLoaded", sectionIndex, direction, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict], [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
    if (sectionIndex != -1 ) {
        int sectionCount = [self.leftTable numberOfSections];
        if (direction == -1 ) {
            for (int i = sectionIndex; i < sectionCount; i++) {
                NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
                //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
                //            NSString *subKeySectionInx = [NSString stringWithFormat:@"%i", i];
                //            NSString *newSubKeySectionIdx = [NSString stringWithFormat:@"%i", i + direction];
                NSNumber *subKeySectionInx = [NSNumber numberWithInt:i];
                NSNumber *newSubKeySectionIdx = [NSNumber numberWithInt:(i + direction)];
                
                NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
                if (valueSet && [valueSet containsObject:subKeySectionInx]) {
                    [valueSet removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case.
                        [valueSet addObject:newSubKeySectionIdx];
                }
                
                // Update the dict holding sections which are loaded on first load else it will show different index already loaded
                NSMutableOrderedSet *valueSet2 = [ self.segmentSectionsLoaded objectForKey:keySegmentNo];
                if (valueSet2 && [valueSet2 containsObject:subKeySectionInx]) {
                    [valueSet2 removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case.
                        [valueSet2 addObject:newSubKeySectionIdx];
                }
            }
        }
        else if (direction == 1) {
            for (int i = sectionCount; i >= sectionIndex; i--) {
                NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
                //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
                //            NSString *subKeySectionInx = [NSString stringWithFormat:@"%i", i];
                //            NSString *newSubKeySectionIdx = [NSString stringWithFormat:@"%i", i + direction];
                NSNumber *subKeySectionInx = [NSNumber numberWithInt:i];
                NSNumber *newSubKeySectionIdx = [NSNumber numberWithInt:(i + direction)];
                
                NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
                if (valueSet && [valueSet containsObject:subKeySectionInx]) {
                    [valueSet removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case.
                        [valueSet addObject:newSubKeySectionIdx];
                }
                // Update the dict holding sections which are loaded on first load else it will show different index already loaded
                NSMutableOrderedSet *valueSet2 = [ self.segmentSectionsLoaded objectForKey:keySegmentNo];
                if (valueSet2 && [valueSet2 containsObject:subKeySectionInx]) {
                    [valueSet2 removeObject:subKeySectionInx];
                    
                    if ([newSubKeySectionIdx intValue] != -1) // as the section number cannot be negative hence avoiding to add -1 in that case.
                        [valueSet2 addObject:newSubKeySectionIdx];
                }
            }
            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
            // Also add this newly added section as collapsed by default
            NSString *sectionName = [NSString stringWithFormat:@"%i", sectionIndex];
            [self addClosedSectionName:sectionName SectionNumber:sectionIndex];
        }
        else if (direction == 0) { // HACK to be tested out if it really works in all cases
            // Adding a new section is not giving an add in some cases and hence section is getting flagged as load first time and count is not matching with old count and 0. So force adding this section as already loaded first time.
            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
        }
    }
    
    
    DLog(@"ReloadUserCollapsedListOnSectionDelete: END   SectionIdx: %i Direction_1Add_-1Del_0Move:%i %@: colapsedSectionNameDict  %@: segmentSectionsLoaded", sectionIndex, direction, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict], [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    DLog(@"controllerDidChangeContent: START");
    
     if (controller == self.leftTableFetchedResultsControllerObj) {
         [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
         
         self.direction=0;
         self.deletedOrInsertedSection = -1;
         
         @try {
             // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
             [self.leftTable endUpdates];
         }
         
         @catch (NSException *exception) {
             NSLog(@"main: Caught %@: %@", [exception name], [exception reason]);
             // Tried few things for recovery but did not find something useful.
             //        [self.tableView reloadData];
             //        [super setEditing:YES animated:YES];
             //        [self.tableView setEditing:YES animated:YES];
             [self forceReloadThisUI];
         }
     }
}


#pragma mark - DB Operations

- (void) fetchData_leftTable {
    NSError *error;
    if (![[self leftTableFetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}
- (void) fetchData_rightColn {
    switch (self.selDataModeForCollnView) { //Loading only when relevant
        case ModeShowGroups:
        case ModeShowTags:
        {
            NSError *error;
            if (![[self rightColnFetchedResultsController] performFetch:&error]) {
                /*
                 Replace this implementation with code to handle the error appropriately.
                 
                 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 */
                ALog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark ForceReload
- (void) forceReloadThisUI
{
    // Also clean up the local cache
    [self.sectionWithZeroCount removeAllObjects];
    [self moveModeSelectionIndicator:self.selDataModeForCollnView];
    
    [self forceReloadThisUI_left];
    [self forceReloadThisUI_right];
}

- (void) forceReloadThisUI_left {
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.leftTableFetchedResultsControllerObj = nil;
    
    [self fetchData_leftTable];
    
    [self.leftTable reloadData];
}

-(void) forceReloadThisUI_right {
    
//    [self deSelectColnCellAtRow:self.lastColnRowHighlighted]; 
    
    self.rightColnFetchedResultsControllerObj = nil;
    
    [self fetchData_rightColn];
    
    [self.rightCollection reloadData];
}

/* Reset the location of image bar indicating which button is clicked 
 *
 */
-(void) moveModeSelectionIndicator:(TypeModeShow) dataMode{
    
    // Keep the detail view hidden until required
    [self.taskDetailView setHidden:YES];
    
    // identify the source i.e. the location respective to which we need to move the bar.
    CGRect sourceRect;
    switch (dataMode) {
        case ModeShowGroups:
        {
            sourceRect = self.btn_Group.frame;
            break;
        }
        case ModeShowGain: {
            sourceRect = self.btn_Gain.frame;
            break;
        }
        case ModeShowDue: {
            sourceRect = self.btn_Due.frame;
            break;
        }
        case ModeShowTags: {
            sourceRect = self.btn_Tags.frame;
            break;
        }
        case ModeShowTaskDetail: {
            sourceRect = self.btn_TaskDetails.frame;
            [self.taskDetailView setHidden:NO];
            break;
        }
        default:
        {
            sourceRect = self.btn_Group.frame;
            break;
        }
    }
    
    // Align the center of bar to center of button
    CGFloat imgCenter = self.selBtnIndicatorImg.frame.size.width/2;
    CGFloat imgY      = self.selBtnIndicatorImg.frame.origin.y;
    CGSize size       = self.selBtnIndicatorImg.bounds.size;
    
    // Adjust the bottom bar from the center of the selected button, assuming button width is more than img
    CGFloat btnCenter = sourceRect.size.width/2;
    CGFloat btnX      = sourceRect.origin.x;
    CGFloat newX      = btnX + (btnCenter - imgCenter); // Starting position of button + the delta of size difference between button and the image
    CGPoint newPoint  = CGPointMake(newX, imgY);
    
    // Use the newly generated starting point of bar with bar's current size and finally set its new frame.
    CGRect rect = {newPoint, size};
    self.selBtnIndicatorImg.frame = rect;
    
//    NSLog(@"Test: %f %f %f %f", rect.origin.x, rect.origin.y, rect.size.height, rect.size.width);
}


// TODO: THESE BUTTON APIS are hidden and no longer used, can be cleaned up
#pragma mark Action/Button Clicks
/* Deselect the last highlighted cell as the row number is not valid across the different views. Tried in forceReload but as we call it after update, it was not allowing to show the folder as open where we did the last drag and drop.
 * Update cache to be aware of the mode we using this UI
 * Move the bottom bar to the button clicked
 * Reload the UI to show data specific to the button clicked. Refreshing left and right to show the data updated for a tag.
 */
- (IBAction)btn_GroupClick:(id)sender {
    [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
    
    self.selDataModeForCollnView        = ModeShowGroups;
    [self moveModeSelectionIndicator:ModeShowGroups];
    
    [self forceReloadThisUI]; // had to refresh both to show the changed table cell for tags
}

- (IBAction)btn_GainClick:(id)sender {
    [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
    
    self.selDataModeForCollnView         = ModeShowGain;
    [self moveModeSelectionIndicator:ModeShowGain];
    
    [self forceReloadThisUI];
}

- (IBAction)btn_DueClick:(id)sender {
    [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
    
    self.selDataModeForCollnView        = ModeShowDue;
    [self moveModeSelectionIndicator:ModeShowDue];
    
    [self forceReloadThisUI];
}

- (IBAction)btn_TagClick:(id)sender {
    [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
    
    self.selDataModeForCollnView        = ModeShowTags;
    [self moveModeSelectionIndicator:ModeShowTags];
    
    [self forceReloadThisUI];
}

- (IBAction)btn_TaskDetailClick:(id)sender {
    [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
    
    self.selDataModeForCollnView = ModeShowTaskDetail;
    [self moveModeSelectionIndicator:ModeShowTaskDetail];
    
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowUpdateTaskViewController"] )
    {
        
        NSIndexPath *indexPath = [self.leftTable indexPathForSelectedRow];
        Task *task = (Task *)[[self leftTableFetchedResultsController] objectAtIndexPath:indexPath];
        
        // FOr this view controller we added a navigation bar controller in update like Add View as puhs was not working and required model seque.
        UpdateTaskViewController *updateTaskViewController = (UpdateTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];

        updateTaskViewController.delegate = self;
        updateTaskViewController.task = task;
        
        // If called from dual view, since we launching a model view. Install the cancel button in the UI
        [updateTaskViewController setupCancelNavButton];
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddTaskViewController"])
    {
        
        AddTaskViewController *addTaskViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        
        addTaskViewController.managedObjectContext = APP_DELEGATE_MGDOBJCNTXT;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowTabFilterViewController"])
    {
        if ( [[[segue destinationViewController] viewControllers] count] > 0 ) { // if i saw with storyboard selected in landscape, it was giving crash, so selected portrait views and then saved, but added this check 
            
            //        TabFilterViewController *tabFilterVC = (TabFilterViewController *) [segue destinationViewController]
            TabFilterViewController *tabFilterVC = (TabFilterViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];
            ;
            tabFilterVC.filterLaunchedFrom = ModelView;
            
            [segue destinationViewController];
        }
    }
    else {
        //        @throw Exc
    }
}


#pragma Section Open Close API Handling on FilterDictionary

// Rename this to markSectionAsClosed
- (void) addClosedSectionName: (NSString *) sectionName SectionNumber: (NSInteger) sectionNumber {
    
    NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
    //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
    //    NSString *subKeySectionNo = [NSString stringWithFormat:@"%i", sectionNumber];
    //    NSString *subValueSectionNo= [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *subKeySectionNo  = [NSNumber numberWithInt:sectionNumber];
    //    NSNumber *subValueSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
    if (valueSet )
        //        [ dictValue setObject:sectionNumberStr forKey:sectionName];
        [valueSet addObject: subKeySectionNo];
    else {
        valueSet = [[NSMutableOrderedSet alloc] init ];
        //        [dictValue setObject:sectionNumberStr forKey:sectionName];
        [valueSet addObject: subKeySectionNo];
        [self.colpsedSectionNameDict setObject:valueSet forKey:keySegmentNo];
    }
    DLog(@"AddClosedSecName:         Section:%@ keySegmentNo: %@   %@: colpsedSectionNameDict", subKeySectionNo, keySegmentNo,  [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}

#pragma Section Open Close API Handling on FilterDictionary
- (void) removeOpenSectionName: (NSString *) sectionName  Section: (NSInteger) sectionNumber{
    
    NSNumber *groupSegmentIndex = [NSNumber numberWithInteger: self.selSegmentIdx];
    //    NSString *grpSectionNo = [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *grpSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    //    NSString *groupSectionNameOpened = sectionName;
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:groupSegmentIndex];
    if (valueSet )
        //        [ dictValue removeObjectForKey: [NSString stringWithFormat:@"%i", sectionNumber]];
        [ valueSet removeObject: grpSectionNo];
    else {
        NSLog(@"Unable to find the object to remove for key: %@", grpSectionNo);
    }
    DLog(@"RemoveOpenSecName: Key:%@ Item:%@   %@: colpsedSectionNameDict", groupSegmentIndex, grpSectionNo, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}


#pragma mark - GroupHeaderSectionView Delegate API

- (void) uitableSectionHeaderView:(SectionHeaderView *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.sectionNameLbl.text Section:sectionHeaderView.sectionIdx];
            
            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.sectionNameLbl.text SectionNumber:sectionHeaderView.sectionIdx];
            break;
        }
        case SECTION_ADD_ITEM: {
             [self launchAddItemOnSection:section];
            break;
        }
        default:
            break;
    }
    
//    [self forceReloadThisUI];
    [self.leftTable reloadData]; // avoid full forcereload to only reload, not sure what will i miss
}

- (void) THREE_uitableSectionHeaderView:(UITableSectionHeaderView *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.titleLabel.text Section:sectionHeaderView.section];
            //            [self removeOpenSectionName:sectionHeaderView.groupManagedObjectID Section:sectionHeaderView.section];
            
            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.titleLabel.text SectionNumber:sectionHeaderView.section];
            //            [self addClosedSectionName:sectionHeaderView.groupManagedObjectID SectionNumber:sectionHeaderView.section];
            break;
        }
        default:
            break;
    }
    
    [self forceReloadThisUI];
}


- (void) ONE_uitableSectionHeaderView:(SectionHeaderCell *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.sectionNameLbl.text Section:sectionHeaderView.sectionIdx];
            //            [self removeOpenSectionName:sectionHeaderView.groupManagedObjectID Section:sectionHeaderView.section];
            
            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.sectionNameLbl.text SectionNumber:sectionHeaderView.sectionIdx];
            //            [self addClosedSectionName:sectionHeaderView.groupManagedObjectID SectionNumber:sectionHeaderView.section];
            break;
        }
        default:
            break;
    }
    
    [self forceReloadThisUI];
}

// Added to record the way of launching add new task on a section
-(void) launchAddItemOnSection:(int) section {
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self leftTableFetchedResultsControllerObj] objectAtIndexPath:newPath];
    
    [AppSharedData getInstance].lastSelectedGroup = task.group;
    
    [self performSegueWithIdentifier:@"ShowAddTaskViewController" sender:self];
}

#pragma mark - Add/Update controller delegate
- (void) updateTaskViewControllerDidCancel:(UpdateTaskViewController *)controler {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) updateTaskViewControllerDidFinish:(UpdateTaskViewController *)controller task:(Task *)task WithDelete:(BOOL)isDeleted
{
    if (task) {
        if (isDeleted) {
            [AppUtilities deleteTask:task];
        }
        else {
            [AppUtilities saveTask:task];
        }
    }
    //    [self dismissModalViewControllerAnimated:YES];
    //    [self.navigationController popViewControllerAnimated:YES];
    //    [self.tableView reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil]; // Adding this line as update is not moving off if called from this view.
    
//    [self forceReloadThisUI];
    [self.leftTable reloadData]; // avoid full forcereload to only reload, not sure what will i miss
}

-(NSFetchedResultsController *) leftTableFetchedResultsController {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return  nil;
}

/* Fetch Groups including ones with 0 count, in their sort order, not sorted by name, can make it difficult but keeping the importance of order for now
 */
- (NSFetchedResultsController *) generateFetchedResultsControllerObj_Groups
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Group" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:APP_DELEGATE_MGDOBJCNTXT sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.rightColnFetchedResultsControllerObj = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.rightColnFetchedResultsControllerObj performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    DLog(@"Object: %i", [[aFetchedResultsController fetchedObjects] count]);
    
    return _rightColnFetchedResultsControllerObj;
}

/* Fetch Tags including ones with 0 count, in their sort order, not sorted by name, can make it difficult but keeping the importance of order for now
 */
- (NSFetchedResultsController *)generatedFetchedResultsControllerObj_Tags
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:APP_DELEGATE_MGDOBJCNTXT sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.rightColnFetchedResultsControllerObj = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.rightColnFetchedResultsControllerObj performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    DLog(@"Object: %i", [[aFetchedResultsController fetchedObjects] count]);
    
    return _rightColnFetchedResultsControllerObj;
}


- (NSFetchedResultsController *)rightColnFetchedResultsController
{
    if (_rightColnFetchedResultsControllerObj != nil) {
        return _rightColnFetchedResultsControllerObj;
    }
    
    switch (self.selDataModeForCollnView) {
        case ModeShowGroups: // Group
        {
            _rightColnFetchedResultsControllerObj = [self generateFetchedResultsControllerObj_Groups];
            break;
        }
        case ModeShowGain: //Gain
        {
            break;
        }
        case ModeShowDue: //Due
        {
            break;
        }
        case ModeShowTags: // Tags
        {
            _rightColnFetchedResultsControllerObj = [self generatedFetchedResultsControllerObj_Tags];
            
            break;
        }
        default:
            break;
    }
    
    return _rightColnFetchedResultsControllerObj;
}


#pragma mark: Save & Restore state api implementation
-(void) encodeRestorableStateWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.colpsedSectionNameDict   forKey:@"colpsedSectionNameDict"];
    [coder encodeObject:self.segmentSectionsLoaded   forKey:@"segmentSectionsLoaded"];
    DLog(@"CommonViewController:Encode:%@ - colpsedSectionNameDict %@", self.thisInterfaceEntityName, self.colpsedSectionNameDict);
    DLog(@"CommonViewController:Encode:%@ - segmentSectionsLoaded %@", self.thisInterfaceEntityName, self.colpsedSectionNameDict);
}

- (void) decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSDictionary *colpSec = [coder decodeObjectForKey:@"colpsedSectionNameDict"];
    NSDictionary *segSec = [coder decodeObjectForKey:@"segmentSectionsLoaded"];
    
    self.colpsedSectionNameDict = [self copyCacheDictionary:colpSec];
    self.segmentSectionsLoaded  = [self copyCacheDictionary:segSec];
    
    DLog(@"CommonViewController:Decode:%@ - colpsedSectionNameDict %@", self.thisInterfaceEntityName,self.colpsedSectionNameDict);
    DLog(@"CommonViewController:Decode:%@ - segmentSectionsLoaded %@", self.thisInterfaceEntityName, self.colpsedSectionNameDict);
}

// Not a general purpose API but understand the types used the cache object to copy
-(NSMutableDictionary *) copyCacheDictionary:(NSDictionary *) inputDict {
    NSMutableDictionary *outDict = [[NSMutableDictionary alloc] init];
    for (NSNumber *n in inputDict) {
        // We were getting type exception when restoring directly on expand/collapse ([__NSOrderedSetI addObject:]: unrecognized selector sent to instance) so added manual copying to exact types
        NSMutableOrderedSet *tmOSet = [[NSMutableOrderedSet alloc] init];
        
        for (id d in [inputDict objectForKey:n]) {
            [tmOSet addObject:d];
        }
    }
    
    return outDict;
}


// Launching add view on swipe down (DISABLED USE AS OF NOW)
#pragma mark - Swipe Gesture Recognizers
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.contentOffset.y <= -40) {
        //        [self performSegueWithIdentifier:@"ShowAddTaskViewController" sender:self];
    }
    if (scrollView.contentOffset.y > 40 ) {
        //        ULog(@"GLAD");
    }
}


#pragma mark Drag N Drop API
-(void) registerDragNDrop {
    //    [self.leftTable registerClass:[UITableViewCell class] forCellReuseIdentifier:tableViewReusableCell];
    
    //    [self.rightCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:tableViewReusableCell];
    
    /* Configure the helper */
    
    self.helper = [[DragHelper alloc] initWithSuperview:self.view
                                                srcView:self.leftTable
                                                dstView:self.rightCollection
                                        SwipeOutEnabled:YES];  // Retaining teh ability to swipe out in Dual View controller for smooth swipe out and drag/drop.
    
    self.helper.delegate = self;
    
    /* The source is rearrangeable or exchangeable with the dest but dest is not rearrangeable */
    self.helper.isSrcRearrangeable = YES;
    self.helper.doesSrcRecieveDst = YES;
    self.helper.hideSrcDraggingCell = YES;
    
    self.helper.isDstRearrangeable = YES;
    self.helper.doesDstRecieveSrc = YES;
    self.helper.hideDstDraggingCell = YES;
}

#pragma mark - Drag n drop exchange and rearrange delegate methods
// Drag within the table to another group
-(void) droppedOnSrcAtIndexPath:(NSIndexPath *)to fromSrcIndexPath:(NSIndexPath *)from {
   
    Task *srctask   = (Task *) [self.leftTableFetchedResultsController objectAtIndexPath:from];
    Task *dstTask   = (Task *) [self.leftTableFetchedResultsController objectAtIndexPath:to];
    
    [AppUtilities moveDragedTask:srctask ToATask:dstTask];
    
//    [self forceReloadThisUI]; disabled to avoid reload.
    
}

-(void) droppedOnDstAtIndexPath:(NSIndexPath*) to fromDstIndexPath:(NSIndexPath*) from{
    
    /* NOTHING TO DO AS NOT ALLOWING REORDER ON DES */
    
}

// Dragging from the table to the group box
-(void) droppedOnDstAtIndexPath:(NSIndexPath*) to fromSrcIndexPath:(NSIndexPath*)from{
   
    switch (self.selDataModeForCollnView) {
        case ModeShowGroups: // Group
        {
            /* Update group name of the task dragged and dropped on the respective group */
            Task *task = (Task *) [self.leftTableFetchedResultsController objectAtIndexPath:from];
            Group *group = (Group *) [self.rightColnFetchedResultsController objectAtIndexPath:to];
            
            [AppUtilities moveDragedTask:task ToAGroup:group];
            
//            [self forceReloadThisUI]; // avoid full forcereload to only reload, not sure what will i miss
            [self.leftTable reloadData]; //disabled forcereload and using reload alone. When we forceload we were moving back to the right section but on top of that after each drag drop. if we got lot more items in a group it would keep taking us to top of section to avoid that doing only a reload.
            break;
        }
        case ModeShowGain: //Gain
        {
            Task *task = (Task *) [self.leftTableFetchedResultsController objectAtIndexPath:from];
            long gainIdx= to.row;
            
            task.gain = [NSNumber numberWithLong:gainIdx];
            
            [AppUtilities saveContext];
            //            [self forceReloadThisUI]; // avoid full forcereload to only reload, not sure what will i miss
            [self.leftTable reloadData]; //disabled forcereload and using reload alone. When we forceload we were moving back to the right section but on top of that after each drag drop. if we got lot more items in a group it would keep taking us to top of section to avoid that doing only a reload.
            [self.rightCollection reloadData];
            
            break;
        }
        case ModeShowDue: //Due
        {
            Task *task    = (Task *) [self.leftTableFetchedResultsController objectAtIndexPath:from];
            long dueOnIdx = to.row;
            
            task.dueOn = [self generateDueOn:dueOnIdx];
            
            [AppUtilities saveContext];
            //            [self forceReloadThisUI]; // avoid full forcereload to only reload, not sure what will i miss
            [self.leftTable reloadData]; //disabled forcereload and using reload alone. When we forceload we were moving back to the right section but on top of that after each drag drop. if we got lot more items in a group it would keep taking us to top of section to avoid that doing only a reload.

            [self.rightCollection reloadData];
            break;
        }
        case ModeShowTags: // Tags
        {
            /* If task is dropped on Tags, add the tag on that task, if dropped on Delete Tags, remove the tags */
            Task *task = (Task *) [self.leftTableFetchedResultsController objectAtIndexPath:from];
            if (to.row < [[self.rightColnFetchedResultsControllerObj fetchedObjects] count]) {
                // Include the new tag
              
                Tag *newTag = (Tag *) [self.rightColnFetchedResultsController objectAtIndexPath:to];
                
                NSMutableSet *tmp = [[NSMutableSet alloc] initWithSet:task.tagS];
                [tmp addObject:newTag];
                task.tagS = tmp;

            }
            else {
                // remove all the tags on this object
                task.tagS = nil;
            }
            
            [AppUtilities saveContext];
            
//            [self forceReloadThisUI]; // avoid full forcereload to only reload, not sure what will i miss
            // not even doing a reload as we will not delete or remove section, tasks, groups. when tags will be shown as group will have to do reload
        }
        default:
            break;
    }
    DLog(@"Perform Reload of right table/collection data");
    
}


-(BOOL) droppedOutsideAtPoint:(CGPoint) pointIn fromDstIndexPath:(NSIndexPath*) from{
    
    /* Here we're going to 'clean' the cell before its snapped back. */
    
    //    NSInteger fromIndex = (from.item);
    //    NSMutableDictionary* rightCellData = ([self.leftTableFetchedResultsController objectAtIndex:fromIndex];
    //    [rightCellData setObject:CELL_CLEAN_COLOR forKey:kCellMetaKeyColor];
    
    /* NOTE: We're not updating the cell here - its been hidden so the affects
     won't be visible. Instead we reload the cell in dragFromDstSnappedBackFromIndexPath */
    
    return YES;
}

-(void) dragFromDstSnappedBackFromIndexPath:(NSIndexPath*) path{
    
    /* Reload the newly cleaned cell once its snapped back. */
    
    if(path != nil) { // Bug Fix: it was crashing if we do wild dragging around, so added this nil check
        [self.leftTable reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Undraggable/Unrearrangeable cell implementations

-(BOOL) isCellAtIndexPathDraggable:(NSIndexPath*) index inContainer:(UIView*) container{
    
    if(container == self.leftTable){
        
        /* We only care about cell draggabillity config for the right hand table */
        
        return YES; // Table view is draggable
        
    }
    else {
        return NO; // Collection View is not draggable
    }
}


-(BOOL) isCellInDstAtIndexPathExchangable:(NSIndexPath*) to withCellAtIndexPath:(NSIndexPath*) from{
    
    return NO;// You can decide conditionally, like for few cells allow exchange, not using & overriding just to let know this option is there.
}

-(BOOL) isCellInSrcAtIndexPathExchangable:(NSIndexPath*) to withCellAtIndexPath:(NSIndexPath*) from{
    
    return YES;// You can decide conditionally, like for few cells allow exchange, not using & overriding just to let know this option is there.s there.
}

/* Highlight the cell on which we doing a drag
 * if dragging is within the collection view/destination
 * find the poing where dragging is going on
 * At that point find the indexpath of cell
 * Find that cell and mark that cell as selected
 * if dragging is outside the collection view, deselect the last highlighted cell
 */
-(void) isDragging:(BOOL)onDstView At:(CGPoint)point
{
    if ( onDstView) { // highlight
        NSIndexPath *focusCellIdxPath = [self.rightCollection indexPathForItemAtPoint:point];
        
        if (focusCellIdxPath) {
            int rowOfFocus = focusCellIdxPath.row;
            
            [self highlightCollectionCellAtRow:rowOfFocus];
            
              NSLog(@"Focus is on ROW: %d", rowOfFocus);
        }
    }
    else { // dehighlight
        [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];
    }
}

#pragma mark Highlight Dehighlight
/* If focus is not on required cell, dehighlighted the last highlighted cell
 * Find the cell for a valid row
 * Update the image to the type of the cell, type is set during configurecell and we reuse to idenity what type of image it should default back.
 */
-(void) deHighlightCollectionCellAtRow:(int) row {
    if (row >= 0) {
        NSIndexPath *idxPath = [NSIndexPath indexPathForRow:row inSection:0];
        GroupCollViewCell *cell = (GroupCollViewCell *)[self.rightCollection cellForItemAtIndexPath:idxPath];
        
        [self updateCellImage:cell Format:cell.tag]; // based on its tag time to reset the image
    }
    
    self.lastHighlightedCollnViewCell = NO_ROW_WAS_HIGHLIGHTED; // even if we force a dehighlight on some row we reset the last highlighted value
}

/* Highlight the cell on which we got focus while dragging
 * Dehighlight previously highlighted cell
 */
-(void) highlightCollectionCellAtRow:(int) row {
    
    // Bug fix: Avoid unnecessary highlight/dehighlight if we scrolling over the same cell
    if( row >= 0 && self.lastHighlightedCollnViewCell != row) {

        // Dehighlight previos highlighted cell, first.
        [self deHighlightCollectionCellAtRow:self.lastHighlightedCollnViewCell];

        NSIndexPath *idxPath = [NSIndexPath indexPathForRow:row inSection:0];
    GroupCollViewCell *cell = (GroupCollViewCell *)[self.rightCollection cellForItemAtIndexPath:idxPath];
    
    [self updateCellImage:cell Format:TypeOfCellIsSpecialFocusOn]; // Update Image with Format Focuse On
        
        // remember current highlight for dehighlight later
        self.lastHighlightedCollnViewCell = row;
    }
}

-(NSDate *) generateDueOn:(long) dueOnIdx {
    NSDate *nextDueOn = nil;
    
    switch (dueOnIdx) {
        case 0: // Today
        {
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            
            nextDueOn = [AppUtilities getNextTimeSlot:today];
            break;
        }
        case 1: { //Tomorrow
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *tomorrow = [NSDate dateWithTimeInterval:24*60*60 sinceDate:today];
            
            nextDueOn = [AppUtilities getNextTimeSlot:tomorrow];
            break;
        }
        case 2: { // Week end
            NSDate *today         = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startWeekEnd  = [AppUtilities getStartOfWeekEndDate:today];
            
            nextDueOn = [AppUtilities getNextTimeSlot:startWeekEnd];
            break;
        }
        case 3: { // Next Week
            NSDate *today         = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startNextWeek = [AppUtilities getStartOfNextWeekDate:today];
            
            nextDueOn = [AppUtilities getNextTimeSlot:startNextWeek];
            break;
        }
        case 4: { // Next Month
            NSDate *today          = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startNextMonth = [AppUtilities getStartOfNextMonthDate:today];
            
            nextDueOn = [AppUtilities getNextTimeSlot:startNextMonth];
            break;
        }
        default:
            break;
    }
    return nextDueOn;
}

#pragma mark - Collection view delegate and datasource implementations

-(NSInteger) collectionView:(UICollectionView*) collectionView numberOfItemsInSection:(NSInteger) section{
    NSInteger count = 0;

    if ( collectionView == self.rightCollection) {
        switch (self.selDataModeForCollnView) {
            case ModeShowGroups: //Groups
            {
                id <NSFetchedResultsSectionInfo> sectionInfo = [[[self rightColnFetchedResultsController] sections] objectAtIndex:section];
                count =  [sectionInfo numberOfObjects];
                break;
            }
            case ModeShowGain: // Gain
            {
                count = [self.dataSrc_Gain count];
                break;
            }
            case ModeShowDue: // Due
            {
                count = [self.dataSrc_Due count];
                break;
            }
            case ModeShowTags: // Tags
            {
                id <NSFetchedResultsSectionInfo> sectionInfo = [[[self rightColnFetchedResultsController] sections] objectAtIndex:section];
                count =  [sectionInfo numberOfObjects] + 1; // add one extra item to allow removing tags if we drag drop on delete cell.
                break;
            }
            default:
                break;
        }
    }
    else if ( collectionView == self.rightToolbarCollnView ) {
        count = [self.dataSrc_ToolbarItems count];
    }
    

    return count;
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger count = 0;
    if ( collectionView == self.rightCollection) {
        switch (self.selDataModeForCollnView) {
            case ModeShowGroups: //Groups
            {
                count = [[self.rightColnFetchedResultsController sections] count];
                break;
            }
            case ModeShowGain: // Gain
            {
                count = 1; // Only one section
                break;
            }
            case ModeShowDue: // Due
            {
                count = 1; // Only one section
                break;
            }
            case ModeShowTags: // Tags
            {
                count = [[self.rightColnFetchedResultsController sections] count]; // section count is not incremented by 1 extra
                break;
            }
            default:
                break;
        }
    }
    else if ( collectionView == self.rightToolbarCollnView) {
        count = 1;
    }

     return count;
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = nil;
    if ( collectionView == self.rightCollection) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:collnViewReusableCell   forIndexPath:indexPath];
        
        [self configureCollectionCell:(GroupCollViewCell *)cell atIndexPath:indexPath];
    }
    else if ( collectionView == self.rightToolbarCollnView ) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:toolbarReusableCell   forIndexPath:indexPath];
        
        [self configureCollectionTBCell:(ToolbarCell *)cell atIndexPath:indexPath];
    }
    
    return cell;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if(self.interfaceOrientation == UIDeviceOrientationPortrait || self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown){
        
        //        NSLog(@"REARRANGE PORTRAIT-UNHANDLED");
        //         [[NSNotificationCenter defaultCenter] postNotificationName:@"RotateParent"object:nil];
        
//        [self goBack];
        
    } else  if(self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight){
        
        //        NSLog(@"REARRANGE LANDS-UNHANDLED");
    }
}

@end
