//
//  AppUtilities.h
//  iGotTodo
//
//  Created by Me on 12/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//



#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Task.h"
#import "TaskViewCell.h"
#import "AppSharedData.h"

@interface AppUtilities : NSObject

+ (NSString *) trim: (NSString *) inputString;
+ (NSString *) handleEmptyString: (NSString *) inputStr;
+ (BOOL) ifTaskAlreadyExist: (NSManagedObjectContext *) context TaskName: (NSString *)taskObjectID;
+(BOOL) ifEntityAlreadyExist: (NSString *)entityName AttributeName: (NSString *) attributeName AttributeValue: (NSString *) attributeValue;
//+ (BOOL) deleteTask:         (NSManagedObjectContext *) context TaskName: (NSString *)taskObjectID;
//+ (void) resetReminderFor: (Task *) task;


+ (NSString *)uiFormatedDate:(NSDate *) date;
+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate;
+ (NSString *) mFormatedDateYearOnly: (NSDate *) date;
+ (NSString *) mFormatedDateMonthName: (NSDate *) date;
+ (NSString *) mFormatedDateDay: (NSDate *) date;
    
+ (NSDateFormatter *)lDateFormatter;
+ (NSString*) getFormattedDateOnly: (NSDate *) date; // Date without Time
+ (NSDate *) ifEmptyAddDay: (NSDate *) date;
+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate WithDayAdd: (BOOL) addDay;
+ (NSString *) uiFormatedTime: (NSDate *) timeWithDate; // For Remind at a time get FormattedTimeValue showing only the time part in predefined format
+ (NSString *) dayNameFromDate: (NSDate *) date; // Get the name of the day (in your locale)
+ (NSInteger) dayFromDate: (NSDate *) date; // Get the day part from the date.
+ (NSInteger) monthFromDate: (NSDate *) date; // Get the month part from the date.
+ (NSString *) monthNameFromDate: (NSDate *) date;

// DATE & TIME OPERATIONS
+ (NSString*)getReadableTimeWithStartDate:(NSDate*) startDate WithEndDate: (NSDate*)endDate;
+ (NSString*)getReadableTime:(double)timeInMilliSecs;

+ (NSDate *) getDatePartOnlyFor:(NSDate *)inputDate;
+ (NSDate *) getStartOfWeekMondayDate:(NSDate *)inputDate;
+ (NSDate *) getStartOfNextDayDate:(NSDate *) inputDate;
+ (NSDate *) getStartOfWeekDate:(NSDate *)inputDate;
+ (NSDate *) getStartOfWeekEndDate:(NSDate *)inputDate;
+ (NSDate *) getStartOfNextWeekDate:(NSDate *)inputDate;
+ (NSDate *) getStartOfMonthDate:(NSDate*)inputDate;
+ (NSDate *) getStartOfNextMonthDate:(NSDate*)inputDate;

+ (NSDate *) getRecentDate:(NSDate*)inputDate;
+ (NSDate *) getNextTimeSlot:(NSDate *)inputDate;


+(NSString *) getImageName: (NSString *) imageTag;
+(UIImage *) getGainImage:(NSUInteger) gainIndex;

+(UIView *) getSelectedBackgroundView; // To get a common color view for selected cell
+(UIColor *)  getSelectedTextHighlightColor; // To get a selected text highlight color across

// Api to do some basic string modifications
+(NSAttributedString *) getAtrbutedString_Significant: (NSString *) inputStr Begin:(int)begin End:(int) end;
// StrikesThrough the input string
+(NSAttributedString *) getAtrbutedString_Striked: (NSString *) inputStr;
+(NSAttributedString *) getAtrbutedWith_FontStriked: (NSString *) inputStr FontSize:(CGFloat) fSize;
+(NSAttributedString *) getAtrbutedWith_FontNotStriked: (NSString *) inputStr FontSize:(CGFloat) fSize;

/** This API is added to add schedule an alarm **/
+ (void) scheduleAlarm: (NSString *) groupName AlarmDate: (NSDate *) alarmDate TaskName: (NSString *) taskName TaskObjectID: (NSString *)taskObjectID RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval;

// Added to allow removal of notification for tasks being deleted
+ (BOOL) removeNotificationForTaskObjectID: (NSString *) modifiedTaskObjectID;

// Fetching the group for a sort number to allow displaying grouped data in sorted order but with the name of the group and not the sort number.
+ (Group *) fetchGroupBySortOrder: (NSString *) sortOrder ManagedContext: (NSManagedObjectContext *) context;

+ (void) clearTaskAlertNotifications: (Task *) task; // Remove the alert notification of a task to avoid getting notification when not required (ex: delete/completed task)
+ (BOOL) deleteTask: (Task *) task;
+ (int) deleteCompletedTasks: (int) olderThan;

/* clear alert notification flag from tasks
 */
+ (void) clearAllAlertNotification;
/* clear alert badge, increment the alert badge or update badge count from db record count
 */
+ (void) updateAlertBadge: (int) mode;
/* get count of records which has alert notification on
 */
+ (int) countAlertNotificationItems;

+(NSManagedObjectContext *)createManagedObjectContext;
+ (void) saveContext;
+ (BOOL) saveTask: (Task *) task;

// CoreData: Add Operations
+ (Tag *) addTag:   (NSString *)tagName   SortOrder:(int) sortOrder;
+ (Group *) addGroup: (NSString *)groupName SortOrder:(int) sortOrder;
+ (Task *) addTask:(NSString *) name Gain:(int) gain DueOn:(NSDate *)dueOn RemindOn:remindOn Note:(NSString *)note Flag:(int) flag Group:(Group *)group  EstimCountIdx: (int) estimCountIdx EstimUnitIdx: (int) estimUnitIdx Tags: (NSMutableSet *) selTags RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval ;
+ (Subtask *) addSubtaskForTask:(Task*)task Data: (NSString *)data Status:(int)status SortOrder:(int) sortOrder Type:(int) type;

+ (NSArray *) getTaskGreaterThanSortOrder: (int) sortOrderVal Group:(Group *) group; // Needed in UpdateTask when task is switching groups names.


// CoreData: Extract Data Opertion
+ (NSArray *) taskBetweenSortOrder:(int) sortOrderA AndSortOrder:(int)sortOrderB ForGroup:(Group *) group;

+ (NSDate *) getDateWithSecondsResetForDate: (NSDate *) date;
// Utility API to be used across different places for getting the text to be used on Tag Label Control from selected tags
+ (NSString *)getSelectedTagsLabelText: (NSSet *) tags;

+ (void)configureCell:(TaskViewCell *)mvaCell Task: (Task*) task;
+ (NSString *) cleanUpNotes: (NSString *) note;

+(int) markUnmarkTaskComplete: (Task*)task;

+ (NSString *) cleanDictSetPrint: (NSMutableDictionary *) localDictWithSet ;

+ (NSPredicate *) generatePredicate;

+ (int) getMaxSortOrder: (Group *) group ;

// Reodering of tasks, important API
// This api allows updating sort order of the group from where task is moving out or in a group where task is moving in.
+ (BOOL) moveTaskOutOfGroup:(Group *)srcGroup AtIndex:(int) srcTaskSortorder;
+ (void) moveDragedTask:(Task *)srcTask ToAGroup:(Group *)dstGroup;
+ (void) moveDragedTask:(Task *)srcTask ToATask:(Task *)dstTask;

// Generic API to show dual section view
+(void) showDualSectionView:(id) caller;

// All the run once api are listed here
+ (void) runOnceOperations;
+ (void) runOnceOperations_EachLaunch;

// Google
+ (void) logAnalytics:(NSString *) screenName;

@end
