//
//  ItemDetailsCell.h
//  iGotTodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group.h"


@protocol ItemDetailsCellDelegate;


@interface ItemDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemSeqNumberLbl;
@property (nonatomic, weak) IBOutlet UILabel *itemNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *itemTasksCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UITextField *itemNameEditTxt;

-(void) hideKeyboard;

- (IBAction)editBtnClicked:(id)sender;

// To store the cell  object related ot this cell. Added to notify parent & share this object for which edit button is clicked
@property (strong, nonatomic) NSObject *cellObject;

// TO inform operations back to the caller/delegate
@property (weak, nonatomic) id <ItemDetailsCellDelegate> delegate;
@end

@protocol ItemDetailsCellDelegate <NSObject>


// Notify the caller, cell is under editing mode
-(void) notifyCellEditOnFor: (NSObject *) cellObject Cell:(ItemDetailsCell *) cell;

// Notify the caller/delegate that edit button is clicked and pass on the object which is specific to the cell. Since it is a commonly used class using CellObject and to be typcased as appropriate in the delegate implementation.
-(void) notifyEditBtnClick: (NSObject *) cellObject;

// Notify the caller, cell is double tapped and edit mode is finished
-(void) notifyEditFinish: (NSObject *) cellObject NewValue: (NSString*)newValue;

@end