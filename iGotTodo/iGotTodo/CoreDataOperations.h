//
//  CoreDataOperations.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/08/05.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//
//  Creating this class to start accumulating the operations on the core database. These API should not be directly used in remaining application rather via AppUtilities

#import <Foundation/Foundation.h>
#import "Task.h"
#import "Group.h"
#import "Tag.h"
#import "Subtask.h"

@interface CoreDataOperations : NSObject

+ (int) countGroups;
+ (int) countTasks;

#pragma mark - SaveContext
+ (void) saveContext;
    
// Add Operations
+ (Tag *) addTag: (NSString *)tagName SortOrder:(int) sortOrder;
+ (Group *) addGroup: (NSString *)groupName SortOrder:(int) sortOrder;
+ (Task *)  addTask:  (NSString *) name Gain:(int) gain DueOn:(NSDate *)dueOn RemindOn:remindOn Note:(NSString *)note Flag:(int) flag Group:(Group *)group EstimCountIdx: (int) estimCountIdx EstimUnitIdx: (int) estimUnitIdx Tags: (NSMutableSet *) selTags RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval;
+ (Subtask *) addSubtaskForTask:(Task*)task Data: (NSString *)data Status:(int)status SortOrder:(int) sortOrder Type:(int) type;

// Delete Operations
+(BOOL) deleteItemByEntityName:(NSString *)entityName Item:(NSManagedObject *)item;
+(NSArray *) getItemsGreaterThanSortOrder: (int) sortOrder EntityName:(NSString *) entityName;

// Extract Data Opertion
+(NSNumber *)aggregateOperation:(NSString *)function onEntity:(NSString*)entityName onAttribute:(NSString *)attributeName withPredicate:(NSPredicate *)predicate inManagedObjectContext:(NSManagedObjectContext *)context;

+(NSDate *) getMaxDueOnOnDay:(NSDate *) datePartOnly;
+ (NSArray *) taskBetweenSortOrder:(int) sortOrderA AndSortOrder:(int)sortOrderB ForGroup:(Group *) group;
+ (NSArray *) getTaskGreaterThanSortOrder: (int) sortOrderVal Group:(Group *) group;
+ (Group *) getGroupBySortID:(int) sortOrder;

// Validate Data
+(BOOL) isSortOrderFragmented: (NSString *) entityName;

// Special DB Operations
+ (BOOL) updateAllSortOrder: (NSString *) entityName; // combile tag+group but looks complex so retaining this for reference.
+ (BOOL) updateAllTagsSortOrder;
+ (BOOL) updateAllGroupsSortOrder;
+ (void) insertDefaultData;

// Upgrade API
+ (BOOL) updateAllTasksSortOrder;
+ (BOOL) runOnceOnDeviceUpgradeNotesToSubtasks;

// Data Fixes & Upgrade API
+(void) fixSortOrder;

@end
