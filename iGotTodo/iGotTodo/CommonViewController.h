//
//  CommonViewController.h
//  iGotTodo
//
//  Created by Me on 31/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdateTaskViewController.h"
#import "AddTaskViewController.h"
#import "UITableSectionHeaderView.h"
#import "SectionHeaderCell.h"
#import "SectionHeaderView.h"
#import "TabFilterViewController.h"
#import "Task.h"
#import "Group.h"
#import "AppSharedData.h"
#import "AppUtilities.h"
#import "TaskViewCell.h"


// TODO: Rename, give better name. Maximum number for which we will track open/closed section names.
#define MAX_TRACKED_SEGMENTS        7


// Converted from UITableVC to UIVC, implemented the delegate interface, added a leftTable instance. Connected table to the Storyboad table. If not having a direct table instance, when implementing a drag and drop, scroll was not working on the view though dragging was fine. So moved the table inside a view by converting it to VC and now scroll on tableview was fine.

@interface CommonViewController : UIViewController
                                <UITableViewDataSource, UITableViewDelegate, UpdateTaskViewControllerDelegate, NSFetchedResultsControllerDelegate, SectionHeaderCellDelegate, SectionHeaderViewDelegate, UITableSectionHeaderViewDelegate, TaskViewCellDelegate>

//@property (nonatomic, strong) NSManagedObjectContext        *mgdObjContext;

/** This is the Source collection */
// IMPORTANT: This table should be connected to the table on interface to use this class on subclassing new views based on this.
@property (nonatomic, strong) IBOutlet UITableView* leftTable;

// This object is actually populated by the subclasses
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerObj;

// Idx which is the key to record open/closed groups
//TODO: How do i ensure that whenever a selection changes these variables are updated by caller or user of this controller and also how to ensure the must have methods are overriden in the subclasses
@property (nonatomic) int selSegmentIdx;


@property (nonatomic, strong) NSString *thisInterfaceEntityName;

- (void) forceReloadThisUI;
- (void) fetchData;
- (NSFetchedResultsController *)fetchedResultsController;
- (NSString *) computeSectionNameFromRawName: (NSInteger) section;

@end
