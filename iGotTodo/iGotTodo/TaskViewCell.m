//
//  TaskViewCell.m
//  iGotTodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TaskViewCell.h"
#import "AppUtilities.h"

@implementation TaskViewCell

@synthesize titleLabel = _titleLabel;
@synthesize subtitleLabel = _subtitleLabel;
@synthesize statusButton1 = _statusButton1;
@synthesize statusButton2 = _statusButton2;
@synthesize infoLabel1 = _infoLabel1;
@synthesize infoButton1 = _infoButton1;
@synthesize infoButton2 = _infoButton2;
@synthesize notesButton = _notesButton;
@synthesize tagsButton = _tagsButton;
@synthesize task = _task;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
//        ULog(@"Test");
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) resetInitialText {
    [[self infoButton1] setTitle:@"" forState:UIControlStateNormal];
    [[self infoButton2] setTitle:@"" forState:UIControlStateNormal];
    [[self statusButton1] setTitle:@"" forState:UIControlStateNormal];
    [[self statusButton2] setTitle:@"" forState:UIControlStateNormal];
    [[self notesButton] setTitle:@"" forState:UIControlStateNormal];
    [[self tagsButton] setTitle:@"" forState:UIControlStateNormal];
    
    //TODO: status1 button was not getting the doneClick event called. So added this target to invoke it for respective view controllers. There should be some way to fix it on storyboard, find it out and resolve. As controls are copy pasted it may be sending events to dynamicview only.
    [[self statusButton1] addTarget:self action:@selector(doneClick:) forControlEvents:UIControlEventTouchUpInside];
    // Reset these as we no longer using and default value to be cleared
    [[self dayLbl] setText:@""];
    [[self monthLbl] setText:@""];
    [[self yearLbl] setText:@""];
}

// had to notify as unabel to preparesegue here so asking caller to do the segue
- (IBAction)showSubtaskBtnClick:(id)sender {
    [self.delegate notifyShowSubtasksClicked:self.task Cell:self];
}

// Not connected but rasing this call from the code in reset
- (IBAction)doneClick:(id)sender {
    
    // Moved code to common place to reuse
    [AppUtilities markUnmarkTaskComplete:(Task*)self.task ];
    [AppUtilities saveContext];
}



@end



