//
//  TabSettingsViewController.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 9/6/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EffortViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AppDelegate.h"
#import "AddOnLangViewController.h"


@interface TabSettingsViewController : UITableViewController 
                                        <EffortViewControllerDelegate, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

/* Common fields are extracted into a CSV format and included in email message. Allows to export tasks into a CSV format. */
- (void) exportTasksToEmail;


@end
