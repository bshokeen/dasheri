//
//  Constants.h
//  iGotTodo
//
//  Created by Me on 04/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flurry.h"

#define IS_PAD                     ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)

#define APP_DELEGATE               ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define APP_DELEGATE_MGDOBJCNTXT   ([(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext])

#define DEFAULT_ROW_HEIGHT   78
#define HEADER_HEIGHT        30

// To change header color (if Filter is applied)
#define HUE 1.0
#define SATURATION 0.78
#define BRIGHTNESS 0.68
#define ALPHA 1.0

#define LANDSCAPE_WIDTH_FACTOR 160 // 160 was old value, after autoresizing set in code it is now set to 0. TODO: Try using autoresizing and a UIView instead of using from code.
#define SECTION_NUMBER_STARTFROM 9000
#define TAB_VC_INDEX_STARTFROM 8000 //THough it is set for now on tabs but not really used, rather using restoration ID of view controlers.

// TO help identify the controls in same API
#define TAG_EFFORT_COUNT_PICKERVIEW 10001
#define TAG_EFFORT_UNIT_PICKERVIEW 10002

// Delete Task
#define DAYS_60  60
#define DAYS_30  30

// No of days for reminder
#define REMINDER_DAYS 17

// Index of Alert Tab for managing the badge count
#define ALERT_TAB_IDX  3
#define MANAGE_TAB_IDX 0 // TO allow switching

// TODO: Use Test Flight Simulator Logging
#if DEBUG == 0

#define DLog(inputFrmt, ...) NSLog((@"" inputFrmt), ##__VA_ARGS__)
#define ULog(inputFrmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:inputFrmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }

#define DLog1(inputFrmt, ...) NSLog((@""))
#define DDDDLog(inputFrmt, ...) NSLog((@"%s: " inputFrmt  ),__PRETTY_FUNCTION__, ##__VA_ARGS__)
#define DDLog(inputFrmt, ...) NSLog((inputFrmt @"- \t\t\t  %s: [Line %d] " ), ##__VA_ARGS__, __PRETTY_FUNCTION__, __LINE__)
#define DDDLog(inputFrmt, ...) NSLog((@"%s: [Line %d] - \n\t\t\t" inputFrmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#else
//#define DLog(...) // For now show logs in debug and iphone also ain testing mode
#define DLog(inputFrmt, ...) NSLog((@"" inputFrmt), ##__VA_ARGS__)
#define ULog(inputFrmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:inputFrmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }

#endif
#define ALog(inputFrmt, ...) NSLog((@"%s [Line %d] " inputFrmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);


#define NEVER       NSLocalizedString(@"Never", @"Never")
#define EMPTY       NSLocalizedString(@"Empty", @"Empty")
#define NOT_SET     NSLocalizedString(@"Not Set", @"Not Set")
#define UNDEFINED   NSLocalizedString(@"Undefined", @"Undefined")
#define OK          NSLocalizedString(@"OK", @"OK")
#define ENGLISH_LOCALE @"en"

#define DISPLAYTYPE_ALL       NSLocalizedString(@"All", @"All")
#define DISPLAYTYPE_TODAY     NSLocalizedString(@"Today", @"Today")
#define DISPLAYTYPE_WEEK      NSLocalizedString(@"Week", @"Week")
#define DISPLAYTYPE_OVERDUE   NSLocalizedString(@"Over Due", @"Over Due")
#define DISPLAYTYPE_NODUE     NSLocalizedString(@"No Due", @"No Due")
#define DISPLAYTYPE_STAR      NSLocalizedString(@"Star", @"Star")

#define REMIND_INTERVAL_SELECTED_DATE  @""
#define REMIND_INTERVAL_DAILY          NSLocalizedString(@"Every Day At:", @"Every Day at")
#define REMIND_INTERVAL_WEEKDAYS       NSLocalizedString(@"Week Days At:", @"Week Days at")
#define REMIND_INTERVAL_MONTHLY        NSLocalizedString(@"Every Month At:", @"Every Month at")
#define REMIND_INTERVAL_CUSTOMDAYS     NSLocalizedString(@"Custom Days At:", @"Custom Days at")
#define REMIND_INTERVAL_NEVER          NSLocalizedString(@"Never", @"Never")

// Key used to save the order of the tabs and restore those orders
#define SAVED_TAB_ORDER @"savedTabOrder"


extern NSString * const PREFS_MY_CONSTANT;

extern NSString * const CONFIG_KEY_COLLAPSIBLE_ON;
extern NSString * const CONFIG_KEY_DISPLAYTYPE;
extern NSString * const CONFIG_KEY_ORDER_BY;
extern NSString * const CONFIG_KEY_ORDER_BY_DIRECTION;
extern NSString * const CONFIG_KEY_EXCLUDE_COMPLETED;

extern NSString * const COLLAPSIBLEON_GROUP;
extern NSString * const COLLAPSIBLEON_DATE;
extern NSString * const COLLAPSIBLEON_GAIN;
extern NSString * const COLLAPSIBLEON_DONE;
extern NSString * const COLLAPSIBLEON_PERCENTAGE;
extern NSString * const COLLAPSIBLEON_FLAG;

extern NSString * const ORDERBY_DUEON;
extern NSString * const ORDERBY_GAIN;
extern NSString * const ORDERBY_PERCENTAGE;
extern NSString * const ORDERBY_DONE;

//extern NSString * const ORDERBY_DUEON_ASC;
//extern NSString * const ORDERBY_DUEON_DESC;
//extern NSString * const ORDERBY_GAIN_ASC;
//extern NSString * const ORDERBY_GAIN_DESC;
//extern NSString * const ORDERBY_PERCENTAGE_ASC;
//extern NSString * const ORDERBY_PERCENTAGE_DESC;
//extern NSString * const ORDERBY_DONE_ASC;
//extern NSString * const ORDERBY_DONE_DESC;

extern NSString * const ORDER_BY_DIRECTION_ASC;
extern NSString * const ORDER_BY_DIRECTION_DESC;

extern NSString * const EXCLUDE_COMPLETED;


extern NSString * const SOURCE_UI_ADDUI;
extern NSString * const SOURCE_UI_FILTERUI;

extern NSString * const REMINDON_SELECTED_DATE;
extern NSString * const REMINDON_DAILY;
extern NSString * const REMINDON_WEEKDAYS;
extern NSString * const REMINDON_MONTHLY;
extern NSString * const REMINDON_CUSTOMDAYS;
extern NSString * const REMINDON_NEVER;


// Added for cloud integration
extern NSString * const IGOT_TODO_ICLOUD_APPID;
extern NSString * const IGOT_TODO_APPNAME;
extern NSString * const IGOT_TODO_SQLITE_DBNAME;
extern NSString * const IGOT_TODO_DATANOSYNC_FOLDERNAME;
extern NSString * const IGOT_TODO_DATA_LOG_FOLDERNAME;

// Default Settings Plist File
extern NSString * const IGOT_TODO_PLISTNAME;

// Define the start of the sort_order index. To remind that some places index is not starting with 0 but 1.
#define TASK_SORT_ORDER_START_IDX 1;


