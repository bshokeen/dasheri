//
//  Group.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/02/18.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "Group.h"
#import "Task.h"


@implementation Group

@dynamic color;
@dynamic created_on;
@dynamic deleted_on;
@dynamic dirty;
@dynamic group_id;
@dynamic icon_id;
@dynamic modified_on;
@dynamic name;
@dynamic sort_order;
@dynamic uppercaseFirstLetterOfName;
@dynamic tasks;

@end
