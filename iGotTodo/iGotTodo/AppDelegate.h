//
//  AppDelegate.h
//  iGotTodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabAllTasksSortViewController.h"
#import "TabViewsViewController.h"
#import "TabDueOnViewController.h"
#import "TabManageViewController.h"
#import "DynamicViewData.h"
#import "UpdateTaskViewController.h"
#import "TabAlertViewController.h"
#import "GAI.h"  // Google
#import <Parse/Parse.h> // Parse

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


@property (strong, nonatomic) UITabBarController *tabBarController ;

- (NSURL *)applicationDocumentsDirectory;

// Api to find the index of the tab. Now tabs can be rearranged so predefined index cannot be used.
-(int) findIndexOfTab: (UITabBarController *) tabBarControlr ForTabVC: (Class) tabVCClass;
// Set the focus to tab with the index
-(bool) switchFocusToTabIdx:(int)tabVCClassType;


// Google tracking
@property(nonatomic, strong) id<GAITracker> tracker;

@end
