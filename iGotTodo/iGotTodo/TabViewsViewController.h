//
//  TabViewsViewController.h
//  iGotTodo
//
//  Created by Me on 18/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "BVReorderTableView.h"

@interface TabViewsViewController : CommonViewController <ReorderTableViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *groupSegment;

- (IBAction)editDoneBtnClick:(id)sender;

@end
