//
//  AppSettings.m
//  iGotTodo
// 
//  Created by Balbir Shokeen on 2013/08/05.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//

#import "AppSettings.h"

@interface AppSettings()

@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSMutableDictionary *savedDefaultValues;

@end

@implementation AppSettings

@synthesize path = _path;
@synthesize savedDefaultValues = _savedDefaultValues;

-(id) init {
    self = [super init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    self.path = [documentsDirectory stringByAppendingPathComponent:IGOT_TODO_PLISTNAME];

    if ( [self settingExists] ) {
        //To reterive the data from the plist
        self.savedDefaultValues = [[NSMutableDictionary alloc] initWithContentsOfFile: self.path];
    }
    else {
        // If the file doesn’t exist, create an empty dictionary
        self.savedDefaultValues = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (bool) settingExists {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ( [fileManager fileExistsAtPath: self.path] )     {
        return YES;
    }
    else {
        return NO;
    }
}

- (int) showCompletedTask {
    if ( ![self.savedDefaultValues objectForKey:@"ShowCompletedTask"] )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"ShowCompletedTask"] intValue];
}

- (int) autoDeleteCompletedTask {
    if (![self.savedDefaultValues objectForKey:@"AutoDeleteCompletedTask"] )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"AutoDeleteCompletedTask"] intValue];
}

-(int) defaultGainIdx {
    if (![self.savedDefaultValues objectForKey:@"DefaultGain"] )
        return 2; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"DefaultGain"] intValue];
}

-(int) defaultEffortCountIdx {
    if (![self.savedDefaultValues objectForKey:@"DefaultEffortCount"] )
        return 10; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"DefaultEffortCount"] intValue];
}

-(int) defaultEffortUnitIdx {
    if (![self.savedDefaultValues objectForKey:@"DefaultEffortUnit"]  )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"DefaultEffortUnit"] intValue];
}

-(int) runOnceOnDeviceFixSortOrderOfAllTasks {
    if (![self.savedDefaultValues objectForKey:@"RunOnceOnDeviceFixSortOrderOfAllTasks"]  )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"RunOnceOnDeviceFixSortOrderOfAllTasks"] intValue];
}
-(int) runOnceAcrossDeviceInsertedDefaultData {
    if (![self.savedDefaultValues objectForKey:@"RunOnceAcrossDeviceInsertedDefaultData"]  )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"RunOnceAcrossDeviceInsertedDefaultData"] intValue];
}

-(int) addOnLanguageUserSelection {
    if (![self.savedDefaultValues objectForKey:@"AddOnLanguageUserSelection"]  )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"AddOnLanguageUserSelection"] intValue];
}
-(int) runOnceOnDeviceUpgradeNotesToSubtasks {
    if (![self.savedDefaultValues objectForKey:@"RunOnceOnDeviceUpgradeNotesToSubtasks"]  )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"RunOnceOnDeviceUpgradeNotesToSubtasks"] intValue];
}

-(void) saveSettingForShowCompletedTask:(int) showCompletedTask {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:showCompletedTask] forKey:@"ShowCompletedTask"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForAutoDeleteCompletedTask:(int) autoDeleteCompletedTask {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:autoDeleteCompletedTask] forKey:@"AutoDeleteCompletedTask"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForDefaultGainIdx: (int) defaultGainIdx {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:defaultGainIdx] forKey:@"DefaultGain"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForDefaultEffortUnitIdx: (int) defaultEffortUnitIdx {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:defaultEffortUnitIdx] forKey:@"DefaultEffortUnit"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForDefaultEffortCountIdx: (int) defaultEffortCount {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:defaultEffortCount] forKey:@"DefaultEffortCount"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForRunOnceOnDeviceFixSortOrderOfAllTasks: (int) runOnceOnDeviceFixSortOrderOfAllTasks {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:runOnceOnDeviceFixSortOrderOfAllTasks] forKey:@"RunOnceOnDeviceFixSortOrderOfAllTasks"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForRunOnceAcrossDeviceInsertedDefaultData: (int) runOnceAcrossDeviceInsertedDefaultData {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:runOnceAcrossDeviceInsertedDefaultData] forKey:@"RunOnceAcrossDeviceInsertedDefaultData"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForAddOnLanguageUserSelection: (int) addOnLanguageUserSelection {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:addOnLanguageUserSelection] forKey:@"AddOnLanguageUserSelection"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForRunOnceOnDeviceUpgradeNotesToSubtasks: (int) runOnceOnDeviceUpgradeNotesToSubtasks {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:runOnceOnDeviceUpgradeNotesToSubtasks] forKey:@"RunOnceOnDeviceUpgradeNotesToSubtasks"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
@end
