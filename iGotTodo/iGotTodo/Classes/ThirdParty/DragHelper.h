

#import <UIKit/UIKit.h>




@protocol DragHelperDelegate

@optional


/** Called when the dragging view has been dropped nowhere and the
     snap-back animation has ended. */

-(void) dragFromDstSnappedBackFromIndexPath:(NSIndexPath*) path;


/** Called when the dragging view has been dropped nowhere and the
     snap-back animation has ended. */

-(void) dragFromSrcSnappedBackFromIndexPath:(NSIndexPath*) path;


/** Called if you implement droppedOutsideAtPoint:fromDstIndexPath: to return
     NO after the 'deletion' animation sequence. */

-(void) itemFromDstDeletedAtIndexPath:(NSIndexPath*) path;


/** Called if you implement droppedOutsideAtPoint:fromSrcIndexPath: to return
     NO after the 'deletion' animation sequence. */

-(void) itemFromSrcDeletedAtIndexPath:(NSIndexPath*) path;


/** A drag from the destination table/collection was started */

-(void) dragFromDstStartedAtIndexPath:(NSIndexPath*) path;


/** A drag from the source table/collection was stopped inside of the
     src view. This should be implemented to make data changes. */

-(void) droppedOnSrcAtIndexPath:(NSIndexPath*) to fromDstIndexPath:(NSIndexPath*) from;


/** A drag from the source table/collection was stopped inside of the
     destination view. */

-(void) droppedOnDstAtIndexPath:(NSIndexPath*) to fromDstIndexPath:(NSIndexPath*) from;


/** A drag from the destination table/collection was stopped ouside of 
     both src and dst table/collections. Should return a BOOL indicating
     whether or not to snap the view back or transition it 'out' with
     the default scale animation. */

-(BOOL) droppedOutsideAtPoint:(CGPoint) pointIn fromDstIndexPath:(NSIndexPath*) from;

/** Implemented to determine whether two cells are exchangable inside of
     a the dst collection/table. */

-(BOOL) isCellInDstAtIndexPathExchangable:(NSIndexPath*) to
                      withCellAtIndexPath:(NSIndexPath*) from;



/** A drag from the srource table/collection was started */

-(void) dragFromSrcStartedAtIndexPath:(NSIndexPath*) path;


/** A drag from the source table/collection was stopped inside of the
     src view. This should be implemented to make data changes. */

-(void) droppedOnSrcAtIndexPath:(NSIndexPath*) to fromSrcIndexPath:(NSIndexPath*) from;


/** A drag from the source table/collection was stopped inside of the
     destination view. */

-(void) droppedOnDstAtIndexPath:(NSIndexPath*) to fromSrcIndexPath:(NSIndexPath*) from;


/** A drag from the source table/collection was stopped ouside of both
     src and dst table/collections. Should return a BOOL indicating
     whether or not to snap the view back or transition it 'out' with 
     the shrink animation. */

-(BOOL) droppedOutsideAtPoint:(CGPoint) pointIn fromSrcIndexPath:(NSIndexPath*) from;


/** Implemented to determine whether two cells are exchangable inside of
     a the src collection/table. */

-(BOOL) isCellInSrcAtIndexPathExchangable:(NSIndexPath*) to
                      withCellAtIndexPath:(NSIndexPath*) from;


/** Implemented to determine whether a specific cell is draggable from
     a container. The container will either be srcView or dstView - logic
     should be implemented in the delegate to determine which one the delegate
     applies to. If not implemented assumed YES. */

-(BOOL) isCellAtIndexPathDraggable:(NSIndexPath*) index inContainer:(UIView*) container;


/** Implemented to let the delegate do something while the item is being dragged.
 *  
 * 
 */
-(void) isDragging:(BOOL)onDstView At:(CGPoint) point;

/* Generic view draggin handler */

-(void) dragginAtPoint:(CGPoint) pointIn;




@end




/** Class that handles routing logic for dragging between two table/collection
     views. */

@interface DragHelper : NSObject

@property (atomic, readonly) BOOL isDragging;

@property (nonatomic, readonly, retain) UIPanGestureRecognizer* currentGestureRecognizer;


/* The 'live' draggin properties, these are only valid to
    reference while dragging is taking place. */

@property (nonatomic, readonly, retain) NSIndexPath* draggingIndexPath;

@property (nonatomic, retain) UIView* draggingView;

@property (nonatomic, readonly) CGRect draggingViewPreviousRect;

@property (nonatomic, readonly) BOOL isDraggingFromSrcCollection;


/** CUSTOM: DRAG BY SWIPEOUT ENABLED **/
@property (nonatomic) BOOL isDragBySwipeOutEnabled;

/** Indicates whether the source should be rearrangeable. Does NOT
     check for the delegate's implementation of droppedOnSrcAtIndexPath:fromSrcIndexPath: 
     to determine whether the Src is rearrangeable. */

@property (nonatomic) BOOL isSrcRearrangeable;

/** Indicates whether to hide the original cell for the Src view whilst
     dragging, or not. It does this by setting its alpha to 0 whilst dragging */

@property (nonatomic) BOOL hideSrcDraggingCell;

/** Indicates whether the source should recieve destination
     items. The delegate also needs to have implemented droppedOnDstAtIndexPath:fromSrcIndexPath: 
     for this to have an effect */

@property (nonatomic) BOOL doesSrcRecieveDst;

/** Indicates whether the source should be rearrangeable. Does NOT
     check for the delegate's implementation of droppedOnDstAtIndexPath:fromDstIndexPath:
     to determine whether the Dst is rearrangeable. */

@property (nonatomic) BOOL isDstRearrangeable;

/** Indicates whether to hide the original cell for the Dst view whilst
     dragging, or not. It does this by setting its alpha to 0 whilst dragging */

@property (nonatomic) BOOL hideDstDraggingCell;


/** Indicates whether the destination should recieve source
     items. The delegate also needs to have implemented droppedOnDstAtIndexPath:fromSrcIndexPath:
     for this to have an effect */

@property (nonatomic) BOOL doesDstRecieveSrc;


/** The view that is being dragged - this will always be a copy of one of the 
     src or dst cells */

@property (nonatomic, weak) UIView* superview;

/** Must be an instance of UITableView or UICollectionView */

@property (nonatomic, weak) UIScrollView* srcView;

/** Must be an instance of UITableView or UICollectionView */

@property (nonatomic, weak) UIScrollView* dstView;

/** Delegate object for the drag routing */

@property (nonatomic, weak) NSObject<DragHelperDelegate>* delegate;



/** Initializes a UIPanGestureRecognizer for the superview, src and dst
     views. Should be noted that it throw either of the src or dst views
     aren't table or collection views. 
 
 // CUSTOM: Added customer isDragByswipeoutenabled otherwise in superview PAN was getting registeerd and not helping if we disable after that.
 */

-(id) initWithSuperview:(UIView*) superview
                srcView:(UIScrollView*) srcView
                dstView:(UIScrollView*) dstView
        SwipeOutEnabled:(BOOL) isDragBySwipeOutEnabled;

/* Setters */

/** Implemented to re-iniatlize UIPanGestureRecognizer */
-(void) setSuperview:(UIView*) superview;

/** Implemented to check type for table/collection */

-(void) setSrcView:(UIScrollView*) srcView;

/** Implemented to check type for table/collection */

-(void) setDstView:(UIScrollView*) dstView;



/** Removes UIPanGestureRecognizer from superview */

-(void) dealloc;

@end
