//
//  TabGroupsViewController.m
//  iGotTodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabGroupsViewController.h"
#import "Group.h"
#import "AppUtilities.h"
#import "AppSharedData.h"
#import "AppDelegate.h"

@interface TabGroupsViewController ()
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Group *groupSelected;
@property (nonatomic, strong) NSIndexPath *lastSelectedGroupName;
@property BOOL canEditSelection;

@property Group *tmpSelGroup; // To pass indexp between apis

// To disable the keyboard editing in a cell, if tap is done on another cell.
@property (nonatomic, strong) ItemDetailsCell *cellUnderEditing;


@end

@implementation TabGroupsViewController

@synthesize headerView = _headerView;
@synthesize groupNameText = _groupNameText;
//@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;

@synthesize lastSelGroupSet = _lastSelGroupSet;
@synthesize currentSelGroupSet = _currentSelGroupSet;
@synthesize doneBtn = _doneBtn;
@synthesize groupSelected = _groupSelected;
@synthesize lastSelectedGroupName = _lastSelectedGroupIndexPath;
@synthesize groupOperationMode = _groupOperationMode;
@synthesize canEditSelection = _canEditSelection;
@synthesize tmpSelGroup = _tmpSelGroup;

@synthesize segSortToggle = _segSortToggle;

@synthesize delegate = _delegate;
@synthesize cellUnderEditing = _cellUnderEditing;

#pragma Enable Edit/Add Mode
-(void) enableEditGroupName : (NSIndexPath *) indexPath {
    if (indexPath) {
        Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        self.groupNameText.text = group.name;
        
        self.lastSelectedGroupName = indexPath;
        self.groupSelected = group;
        
        self.groupNameText.placeholder = NSLocalizedString(@"Enter New Group Name...", @"Enter New Group Name...");// Select/Deselect Item to Edit/Add...";
        
    }else {
        
        [self enableAddGroupName];
    }
}

- (void) enableAddGroupName {
    
    self.groupNameText.text = @"";
    self.groupNameText.placeholder = NSLocalizedString(@"Enter New Group Name...", @"Enter New Group Name...");
    self.groupSelected = nil;
    self.lastSelectedGroupName = nil;
}

#pragma mark - InitWith

// init method is not called but retained for now
- (id)init
{
    self = [super init];
    if (self) {
        // Moved initialization from the AppDelegate so setting up default mode in the init.
        [self initializeWithMode:ReadWriteSingleSelection LastSelGroupSet:nil Context:APP_DELEGATE_MGDOBJCNTXT Delegate:nil];
    }
    return self;
}
// The below api is called when groups is launched from Tab. This helps getting the required mode be initialized.
-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self)
        // Moved initialization from the AppDelegate so setting up default mode in the init.
        [self initializeWithMode:ReadWriteSingleSelection LastSelGroupSet:nil Context:APP_DELEGATE_MGDOBJCNTXT Delegate:nil];
    
    return self;
}

- (void) initializeWithMode:(GroupOperationMode)selOperationMode LastSelGroupSet:(NSSet *)lastSelGroupSet Context:(NSManagedObjectContext *)managedContext Delegate:(id<TabGroupsViewControllerDelegate>)delegate {
    
    self.groupOperationMode = selOperationMode;
    
//    self.managedObjectContext = managedContext;
    self.delegate = delegate;
    
    switch (selOperationMode)
    {
        case ReadOnlySingleSelection:
        case ReadOnlyMultipleSelection:
        {
            self.lastSelGroupSet = lastSelGroupSet;
            self.currentSelGroupSet = [[NSMutableSet alloc] init];
            
            for (Group *group in self.lastSelGroupSet)
                [self.currentSelGroupSet addObject:group];
            
            break;
        }
        case ReadWriteSingleSelection:
            // No selected group info needed when being launched from tab bar. Only from tab bar we allow editing to prevent handling several cases when using this controller being launched from other views.
            break;
        default:
            break;
    }
}

- (void) viewDidAppear:(BOOL)animated {
    // Active/Total was not refreshing each time, so had to move fetch here. In case used from update/add it is leading to double call though.
        [self fetchData];
    
    [self focusSelection]; // scroll to selected cell
}

// Helps scroll to the selected item.
-(void) focusSelection {

//    [self.tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    if (self.groupOperationMode == ReadOnlySingleSelection && [self.lastSelGroupSet count] == 1) {
        
        Group *grp = [[self.lastSelGroupSet allObjects] objectAtIndex:0];
        [self focusSelectionToGroup:grp];
    }
}
// To allow reuse of this focus
-(void) focusSelectionToGroup:(Group*)grp {
    if ( grp && self.groupOperationMode == ReadOnlySingleSelection) { // run only when using single selection else keep getting focus to end is not useful
        
        NSIndexPath *selGrpIdx = nil;
        
        if ( [self.segSortToggle selectedSegmentIndex]     == 0) {
            
            selGrpIdx = [NSIndexPath indexPathForItem:([grp.sort_order intValue] -1 )  inSection:0];
        }
        else if ( [self.segSortToggle selectedSegmentIndex] == 1 ) {
            NSString *firstChar = [grp.name substringToIndex:1];
            int sectionIdx = 0;
            int counter = 0;
            for(NSString *character in [self.fetchedResultsController sectionIndexTitles])
            {
                if([character caseInsensitiveCompare:firstChar] == NSOrderedSame)
                {
                    sectionIdx = counter;
                    break;
                }
                counter ++;
            }
            
            selGrpIdx = [NSIndexPath indexPathForItem:0  inSection:sectionIdx];
        }
        
        [self.tableView scrollToRowAtIndexPath:selGrpIdx atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: Groups"];
    
    
    [self.groupNameText setDelegate:self];
    
   /* Commenting the default group creation on click, not a prefered method.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *preloadGroups = [defaults objectForKey:@"preloadGroups"];
    
    if ([preloadGroups isEqualToString:@"loadedGroupsAlready"]) {
    }
    
    else {
        [defaults setObject:@"loadedGroupsAlready" forKey:@"preloadGroups"];
        
        NSMutableArray *preGroupsArray = [[AppSharedData getInstance] preCreateGroups];
        
        for (int i=0; i<[preGroupsArray count]; i++) {
            [self createNewGroup: [preGroupsArray objectAtIndex:i]];
        }
    }*/
    
    
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection:
        case ReadWriteSingleSelection:
        {// Enable Edit Button to allow rearrange group and get it ready to allow add groups.
            [self enableAddGroupName];
//            self.navigationItem.rightBarButtonItem = self.editButtonItem; // No need for an edit button now as we using dynamic ordering by drag and drop
            
            [self.groupNameText addTarget:self
                                   action:@selector(startEditing:)
                         forControlEvents:UIControlEventEditingDidBegin];
            break;
        }
        case ReadOnlyMultipleSelection:
            // No Special handling needed for this mode as their we do not allow add/move/edit in this case.
            self.groupNameText.enabled = NO;
//            self.navigationItem.rightBarButtonItem = nil; // Hide the edit button as it is not required and delete is also disabled
            break;
        default:
            break;
    }
    // Earlier we were sorting by default based on caller in fetchResult controller. later to give flexibility we sorted by default on segment controll but control index is controlled by where it is called or on click of it
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection:
            // Add the ability to sort on name also
            [self.segSortToggle setSelectedSegmentIndex:1];
            break;
        case ReadWriteSingleSelection:
//            [self.segSortToggle setEnabled:NO]; // I think we disabled this control earlier bcz using edit button we were able to edit in both modes(order by name or A-Z). But now with drag and drop feature we do not allow move based on segment index so enabling it again.
        case ReadOnlyMultipleSelection:
        default:
            // Add the ability to sort on name also
            [self.segSortToggle setSelectedSegmentIndex:0];
            break;
    }

    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.segSortToggle setTitle:NSLocalizedString(@"Order", @"Order") forSegmentAtIndex:0];
    [self.segSortToggle setTitle:NSLocalizedString(@"A-Z", @"A-Z") forSegmentAtIndex:1];
    
}

- (void) startEditing: (UITextField *)textField
{
    if (self.tableView.indexPathForSelectedRow == nil) {
        self.lastSelectedGroupName = nil;
        self.groupSelected = nil;
    }
}

- (void)viewDidUnload
{
    [self setHeaderView:nil];
    [self setGroupNameText:nil];
    [self setDoneBtn:nil];
    [self setSegSortToggle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![_fetchedResultsController.managedObjectContext save:&error]) {
            DLog(@"GroupAddEditViewController: Could not save context: %@", error);
        }
    }
}


- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    //    self.task.group = self.groupSelected;
    
    // During Disapper save the global variable to pass values for caller to use and share again. This is to be done only when we have multiple select one.
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection: // Selected Item is informed using delegate so not required.
            break;
        case ReadOnlyMultipleSelection:
        {
            [AppSharedData getInstance].dynamicViewData.groupSet = self.currentSelGroupSet;
            break;
        }
        case ReadWriteSingleSelection:
            // Navigation is not expected to set some valus any where as this is the main starting screen.
            // Change on the group for its name is yet to be saved, if edited. Not doing it here as it has to be saved in the delegate which will be invoked from here.
            // later i saw it was not updated and hence putting up  this change and retaining older comments documented above
            if (self.groupSelected != nil) [self saveUpdatedGroup:self.groupSelected];
            break;
        default:
            break;
    }
    
    // Group order was not refreshing on the manage UI after rearrage, so before leaving out saving the context
    [AppUtilities saveContext];
}

/* On Filter UI where multiple selection is allowed, do not want to have the ability to delete a group */
- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    bool canEditRow = NO;
    
    switch (self.groupOperationMode) {
        case ReadOnlyMultipleSelection:
            canEditRow =  NO;
            break;
        default:
            canEditRow =  YES; // For all other cases we allowing to delete the group (ADD/Update/Group View Listing
            break;
    }
    // Disable sorting if sorted using A-Z and allow sorting only when UI shows data based on order
    if ( self.segSortToggle.selectedSegmentIndex == 0 ) {
        canEditRow = YES; // Allow as UI is sorted on ORder
    }
    else {
        canEditRow = NO; // DO not Allow as UI is sorted by name and it cause crash on switching to different UI.
    }
    
    return canEditRow;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ItemDetailsCell *gaevCell = (ItemDetailsCell *) cell;
    
    Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // UI behaviour based on segmenet selected.
    if ([self.segSortToggle selectedSegmentIndex] == 0) {
        gaevCell.itemSeqNumberLbl.text = [NSString stringWithFormat:@"%i", indexPath.row + 1];//removed colon
    } else {
        gaevCell.itemSeqNumberLbl.text = @"";
    }
    
    if ( [self.currentSelGroupSet containsObject:group] ) {
        //    if ([self.lastSetGroup.name isEqualToString:group.name]) {
        gaevCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        gaevCell.accessoryType = UITableViewCellAccessoryNone;
    }
    gaevCell.itemNameLabel.text = group.name;
    
    int totalCount = [group.tasks count];
    int activeCount = 0;
    for (Task *taskObj in group.tasks) {
        if ([taskObj.done intValue] == 0)
            activeCount +=1;
    }
    gaevCell.itemTasksCountLabel.text = [NSString stringWithFormat:@"(%i/%i)",activeCount, totalCount];

    // Ensure delegate is set so that call backs can be sent
    gaevCell.delegate   = self;
    gaevCell.cellObject = group; // Cell Data Obj to reuse later
    
    // After updating a cell, the cell selection was not removed, but this API is called, so trying to disable selection
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"ItemDetailsCell";
    /* Old way
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     if (cell == nil) {
     cell = [[UITableViewCell alloc]
     initWithStyle:UITableViewCellStyleDefault
     reuseIdentifier:CellIdentifier];
     }
     */
    ItemDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /* This is not working with xib. learn later and try again
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (ItemDetailsCell *) [nib objectAtIndex:0];
     } */
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        Group *group = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        // When delete is done from Add/Update UI and if that is the selected group we need to reset the selection
        // Check if we are in Add/updateUI mode and item deleted is the one last selected, clear the select then & return
        if ( self.groupOperationMode == ReadOnlySingleSelection && [self.lastSelGroupSet containsObject:group] ) {
            
            // reset the lastSelectedGroupName to nill and the grop selected also to nil
            self.lastSelectedGroupName = nil;
            self.groupSelected = nil;
            
            [self finishSelection]; // Finally call finish to return back
        }
        
        // Delete the managed object.
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:group];
        
        NSError *error;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


// Here starts the complicated handling of UI based on which way user is looking at the groups.
/* if it is ordered than show the label and sequence without the index bar
 * If it is A-Z show the index bar but index number cannot be shown
 *
 */
/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
 // Method 1: Use the index value but that is not good as we using name as the group and for names staring with same letter we will get duplicates and the label or index number will not work, so add a transient property as in
 http://stackoverflow.com/questions/1741093/how-to-use-the-first-character-as-a-section-name/1741131#1741131
 
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return  [sectionInfo indexTitle];;
}*/

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([self.segSortToggle selectedSegmentIndex] == 0) {
        return  1; // No grouping single section
    }
    else if ([self.segSortToggle selectedSegmentIndex] == 1) { // As per the count based on our grouping
        return [[self.fetchedResultsController sections] count];
    }
    else
        return 1;
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if ([self.segSortToggle selectedSegmentIndex] == 0) {
        return  nil;
    }
    
    // Do this only if sorting order is based on name i.e. alphabetical
    
    // Method 1:
    NSLog(@"Test2: %@", [self.fetchedResultsController sectionIndexTitles]);
    return [self.fetchedResultsController sectionIndexTitles];

    // Method 2: (THIS IS TO BE GLOBALIZED http://benedictcohen.co.uk/blog/archives/230
    NSMutableArray *sectionedArray = [[NSMutableArray alloc]init];
    for(char c ='A' ; c <= 'Z' ;  c++)
    {
        [sectionedArray addObject:[NSString stringWithFormat:@"%c",c]];
    }
    return sectionedArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {

    if ([self.segSortToggle selectedSegmentIndex] == 0) {
        return  0;
    }
    
    // Do this only if sorting order is based on name i.e. alphabetical
    
    // Method 1:
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
    
    // Method 2:
    NSInteger count = 0;
    for(NSString *character in [self.fetchedResultsController sectionIndexTitles])
    {
        if([character isEqualToString:title])
        {
            return count;
        }
        count ++;
    }

    NSLog(@"SectionForSectionIndexTitle: %@ Index:%i ReturnCount:%i", title, index, count);
    return count;
}
*/

-(bool) isCurrentLangEnglish {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language caseInsensitiveCompare: ENGLISH_LOCALE] == NSOrderedSame)
        return YES;
    else
        return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if ( [self isCurrentLangEnglish]) {
//        
//    }
//    else {
         return [[self.fetchedResultsController sections] count];
//    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
     if ( [self isCurrentLangEnglish] && [self.segSortToggle selectedSegmentIndex] == 1) {
         NSMutableArray *sectionedArray = [[NSMutableArray alloc]init];
         for(char c ='A' ; c <= 'Z' ;  c++)
         {
             [sectionedArray addObject:[NSString stringWithFormat:@"%c",c]];
         }
         return sectionedArray;
     }
     else {
         return [self.fetchedResultsController sectionIndexTitles];
     }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {

    if ( [self isCurrentLangEnglish] && [self.segSortToggle selectedSegmentIndex] == 1) {
        NSInteger count = 0;
        for(NSString *character in [self.fetchedResultsController sectionIndexTitles])
        {
            if([character isEqualToString:title])
            {
                return count;
            }
            count ++;
        }
        return count;
    }
    else {
        return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}

- (void) resetSortOrder {
    
    
}
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    /*[self resetSortOrder];
     NSInteger items = [[_fetchedResultsController fetchedObjects] count];
     
     // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
     for (NSInteger i=0; i< items; i++) {
     NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
     Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
     NSNumber *newPosition = [NSNumber numberWithInteger:i+1];
     
     
     //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
     curObj.sort_order = newPosition;
     }
     
     Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
     movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row + 1];
     NSError *error;
     if (![_fetchedResultsController.managedObjectContext save:&error]) {
     NSLog(@"Could not save context: %@", error);
     }
     */
    
    NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Group *fromGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Group *toGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    DLog(@"itemMoved: %@ to: %@ FsortOrder: %d toSO: %d FromIdx: %d to: %d", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row);
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];
        
        
        //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
    Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];
    
    // Save of this operation will be done on click of Edit Button as Done
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    
    // if the UI is sorted in alphabetical order, sorting should not be allowed
    if (self.segSortToggle.selectedSegmentIndex != 0) {
        return NO;
    }
    
    switch (self.groupOperationMode)
    {
        case ReadOnlySingleSelection:
        case ReadOnlyMultipleSelection:
        {
            return NO;
            
            break;
        }
        case ReadWriteSingleSelection:
            // No selected group info needed when being launched from tab bar. Only from tab bar we allow editing to prevent handling several cases when using this controller being launched from other views.
            return YES;
            break;
            
        default:
            return YES;
            break;
    }
    
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    //    NSLog(@"NSFetchType: %d", type);
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self enableAddGroupName];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];

    // To update the row indexes after delete, doing a reload after every update.
    [self.tableView reloadData];
    
}

#pragma mark - Custom Editing Options in this controller
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Disable the keyboard for editing cell if required
    ItemDetailsCell *cell = (ItemDetailsCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ( self.cellUnderEditing && cell != self.cellUnderEditing) {
        [self.cellUnderEditing hideKeyboard ];
        //         [self.cellUnderEditing setSelected:NO];
    }
    
    
    Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // if we are in add/update the groups mode, no need to perform selection/deselection
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection:
        {
            UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
            [cell setSelected:NO animated:YES];
            
            // Reset Previous Selected Index if any
            if (self.lastSelectedGroupName && ![self.lastSelectedGroupName isEqual:indexPath]) {
                UITableViewCell * lastCell = [self.tableView  cellForRowAtIndexPath:self.lastSelectedGroupName];
                [lastCell setSelected:NO animated:YES];
                lastCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            if (cell.accessoryType == UITableViewCellAccessoryNone) {
                // Set current cell as checked
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                // remember current IndexPath now and the corresponding group
                self.lastSelectedGroupName = indexPath;
                self.groupSelected = group;
            }
            else if (cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
                cell.accessoryType = UITableViewCellAccessoryNone;
                
                [self enableAddGroupName];
            }
            else {
                DLog(@"Some error");
                [self enableAddGroupName];
            }
            
            [self finishSelection];
            break;
        }
        case ReadWriteSingleSelection:
        {
            //        NSLog(@"LastSelcted: %@", self.lastSelectedGroupName);
            //        NSLog(@"CurrenSeled: %@", indexPath);
            if ([self.lastSelectedGroupName isEqual:indexPath]) {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                [self enableAddGroupName];
            } else {
                
                [self enableEditGroupName: indexPath];
                // not selecting group as it is not needed in this case
            }
            break;
        }
        case ReadOnlyMultipleSelection:
        {
            Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
            UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
            [cell setSelected:NO animated:YES];
            
            if ([self.currentSelGroupSet containsObject:group]) {
                
                [self.currentSelGroupSet removeObject:group];
                cell.accessoryType = UITableViewCellAccessoryNone;
                
            } else {
                
                [self.currentSelGroupSet addObject:group];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            
            break;
        }
            
        default:
            break;
    }
}


#pragma mark - Result controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Group"
                                   inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // to allow easy selection on add/update task sorting by name or sorting by sort order
    NSString *sortOrderToUse = nil;
    NSString *groupOn_secNameKeyPath = nil;
    NSSortDescriptor *sortDescriptor = nil;
    switch ([self.segSortToggle selectedSegmentIndex]) {
        case 0: {
            sortOrderToUse = @"sort_order";
            groupOn_secNameKeyPath = nil;
            sortDescriptor = [[NSSortDescriptor alloc]
             initWithKey:sortOrderToUse
                          ascending:YES];
             

            break;
        }
        case 1: {
            sortOrderToUse = @"name";
            groupOn_secNameKeyPath = @"uppercaseFirstLetterOfName";
            sortDescriptor = [[NSSortDescriptor alloc] // Use a sort descriptor to compare only when we do group A-Z
                              initWithKey:sortOrderToUse
                              ascending:YES
                              selector:@selector(localizedCaseInsensitiveCompare:)]; // Case insensitive sorting
        }
        default:
            break;
    }
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]  initWithFetchRequest:fetchRequest
                                                                                                 managedObjectContext:APP_DELEGATE_MGDOBJCNTXT
                                                                                                   sectionNameKeyPath:groupOn_secNameKeyPath
                                                                                                            cacheName:nil];
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    DLog(@"Error Fetching Group %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Without this autoupdate will not work.
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

#pragma mark - ItemDetailsDelegate API
- (void) notifyCellEditOnFor:(NSObject *)cellObject Cell:(ItemDetailsCell *) cell {
    self.cellUnderEditing = cell;
}

-(void) notifyEditBtnClick:(NSObject *)cellObject {
    if (cellObject != nil) {
        Group *group = (Group *)cellObject;
        self.tmpSelGroup = group; // Tempvariable to hold and pass value during segue
        [self performSegueWithIdentifier:@"ShowTabAllTasksViewController" sender:self];
    }
}
-(void) notifyEditFinish:(NSObject *)cellObject NewValue:(NSString *)newValue {
    // Save the value
    Group *grp = (Group *) cellObject;
    
    if (grp) {
        NSString *groupName = [AppUtilities trim:newValue];
        if ( groupName && [groupName length] > 0 && [self ifGroupAlreadyExist:groupName] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                            message:NSLocalizedString(@"Group Already Exist!", @"Group Already Exist!")
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                                  otherButtonTitles:nil];
            [alert show];
        }
       else {
            grp.name = groupName;
            [AppUtilities saveContext];
        }
    }
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowTabAllTasksViewController"]) {
        
        TabAllTasksSortViewController *controller = (TabAllTasksSortViewController *)[segue destinationViewController];
        
        controller.lastSelGroupTogetDetails = self.tmpSelGroup;
        controller.operationMode = FilterGroup;
        
        self.groupNameText.text= @""; //Bug fix clear before going
    }
}

#pragma TextField Delegate Handling
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if ( textField == self.groupNameText) {
        
        NSString *groupName = [AppUtilities trim:textField.text];
        
        if (groupName.length == 0) {
            [textField resignFirstResponder];
        }
        else {
            if ([ self ifGroupAlreadyExist:groupName ]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                                message:NSLocalizedString(@"Group Already Exist!", @"Group Already Exist!")
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") 
                                                      otherButtonTitles:nil];
                [alert show];
                self.groupNameText.text= @"";
            }
            else {
                switch (self.groupOperationMode) {
                    case ReadOnlySingleSelection:
                    {
                        // Edit is not allowed so no need to update and direclty go for add.
                        Group *grp = [AppUtilities addGroup:textField.text SortOrder:([self.fetchedResultsController.fetchedObjects count] + 1)];
                        
                        // Focus to the newly added group
                        [self focusSelectionToGroup:grp];
                        
                        self.groupNameText.text= @"";
                        break;
                    }
                    case ReadWriteSingleSelection:
                    {
                        // Edit of existing is allowed hence see if need to update
                        if (self.lastSelectedGroupName == nil) {
                            Group *grp = [AppUtilities addGroup:textField.text SortOrder:([self.fetchedResultsController.fetchedObjects count] + 1)];
                            
                            // Focus to the newly added group
                            [self focusSelectionToGroup:grp];
                            
                            self.groupNameText.text= @"";
                        } else {
                            self.groupSelected.name = groupName;
                        }
                        break;
                    }
                    case ReadOnlyMultipleSelection: {
                        break;
                    }
                    default:
                        break;
                }
                self.groupNameText.text= @"";
                [textField resignFirstResponder];
            }
        }
    }
    
    return YES;
}

- (void) fetchData {
    
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    [self.tableView reloadData];
}

-(void) saveUpdatedGroup: (Group *) existingGroup {
    NSError *error = nil;
    if (![existingGroup.managedObjectContext save:&error]) {
        DLog(@"Error Saving Group %@, %@", error, [error userInfo]);
        
        abort();
    }
}



-(BOOL) ifGroupAlreadyExist: (NSString *)groupName {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Group" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(name == %@)", groupName]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT                        countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void) finishSelection {
    [self.delegate tabGroupsViewControllerDidFinish:self SelectedGroup:self.groupSelected];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
    
//    if ( self.groupSelected != nil ) // While handling delete of a single group during add/update there is a pissibility of selected group being nil henche adding this check.
//        [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma Save Operation
- (IBAction)done:(id)sender {
    
    [self.delegate tabGroupsViewControllerDidFinish:self SelectedGroup:self.groupSelected];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
//    [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}

// Adding the ability to toggle sorting on this UI
- (void) forceReloadThisUI
{
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsController = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}

- (IBAction)segSortToggle_ValueChanged:(id)sender {
        [self forceReloadThisUI];
}


#pragma mark - Support for Drag & Drop Reordering
// 1. select the tableview in the storyboard and use the custom class
// 2. Implement its delegate
// 3. Copied the code from my move api to this move API. 
// This method is called when starting the re-ording process. You insert a blank row object into your
// data source and return the object you want to save for later. This method is only called once.
- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Index: row:%i section:%i", indexPath.row, indexPath.section);
//    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:indexPath.section];
    
    
    NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
    
//    [_fetchedResultsController replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    
    /*
    NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:indexPath.section];
    id object = [section objectAtIndex:indexPath.row];
    [section replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
     */
    return curObj;
}


// This method is called when the selected row is dragged to a new position. You simply update your
// data source to reflect that the rows have switched places. This can be called multiple times
// during the reordering process.
- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    if (self.segSortToggle.selectedSegmentIndex != 0) {
        return; // Do not allow moving records when the sorting order selected is on name or A-Z as it is allowed only when sorted with order.
    }
    NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Group *fromGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Group *toGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    DLog(@"itemMoved: %@ to: %@ FsortOrder: %d toSO: %d FromIdx: %d to: %d", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row);
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];
        
        
        //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
    Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];
    /*
    NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:fromIndexPath.section];
    id object = [section objectAtIndex:fromIndexPath.row];
    [section removeObjectAtIndex:fromIndexPath.row];
    
    NSMutableArray *newSection = (NSMutableArray *)[data objectAtIndex:toIndexPath.section];
    [newSection insertObject:object atIndex:toIndexPath.row];
     */
    
    // BUG FIX: After moving or rearranign the manage UI was not loading, hence setting its global flag to reload. ALso saving the context before exit so that the reload occurs.
    [AppSharedData getInstance].dynamicViewData.reloadUI = YES;
}



// This method is called when the selected row is released to its new position. The object is the same
// object you returned in saveObjectAndInsertBlankRowAtIndexPath:. Simply update the data source so the
// object is in its new position. You should do any saving/cleanup here.
- (void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath; {
        NSLog(@"Index: row:%i section:%i", indexPath.row, indexPath.section);
    /*
    NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:indexPath.section];
    [section replaceObjectAtIndex:indexPath.row withObject:object];
    // do any additional cleanup here
     */
}


#pragma mark - pull down to refresh
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (self.groupOperationMode == ReadWriteSingleSelection) {
        if (scrollView.contentOffset.y <= -30) {
            [self enableAddGroupName];
            [self.groupNameText becomeFirstResponder];
        }
        if (scrollView.contentOffset.y > 30 ) {
            self.groupNameText.text= @"";
            [self.groupNameText resignFirstResponder];
        }
    }
}

@end
