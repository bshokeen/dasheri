//
//  SubtaskCell.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/24.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Subtask.h"


@protocol SubtaskCellDelegate;

@interface SubtaskCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
@property (weak, nonatomic) IBOutlet UITextView *dataTxt;
@property (weak, nonatomic) IBOutlet UITextField *hiddenTxt;
@property (weak, nonatomic) IBOutlet UIButton *deleteSubtaskBtn;

@property (strong, nonatomic) Subtask *subtask;
@property (nonatomic) CGFloat fSize;

// TO inform operations back to the caller/delegate
@property (weak, nonatomic) id <SubtaskCellDelegate> delegate;

-(void) resetInitialText;
-(void) initializeWith:(Subtask *)subtask Delegate:(id)delegate FontSize:(CGFloat) fSize;
-(void) toggleStatus;
-(void) hideKeyboard;

- (IBAction)statusBtnClicked:(id)sender;

- (IBAction)deleteBtnClicked:(id)sender;

@end



@protocol SubtaskCellDelegate <NSObject>

// Notify the caller, cell is under editing mode
-(void) notifyCellEditOnFor: (Subtask *) subtask Cell:(SubtaskCell *) cell;

// Notify the caller, cell is double tapped and edit mode is finished
-(void) notifyEditFinish: (Subtask *) subtask NewValue: (NSString*)newValue;


@end