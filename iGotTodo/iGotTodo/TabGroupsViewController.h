//
//  TabGroupsViewController.h
//  iGotTodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Task.h"
#import "Group.h"
#import "ItemDetailsCell.h"
#import "BVReorderTableView.h"

@protocol TabGroupsViewControllerDelegate;

// Single Or MultiSelect is same select behavior other than that one need to allow multiple selection.
typedef enum { ReadOnlySingleSelection=1, ReadOnlyMultipleSelection=2, ReadWriteSingleSelection=3 } GroupOperationMode;


@interface TabGroupsViewController : UITableViewController
<NSFetchedResultsControllerDelegate, UITextFieldDelegate, ReorderTableViewDelegate, ItemDetailsCellDelegate>

//@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSSet                  *lastSelGroupSet;
@property (nonatomic, strong) NSMutableSet           *currentSelGroupSet;
@property(weak, nonatomic) id <TabGroupsViewControllerDelegate> delegate;
@property (nonatomic) GroupOperationMode groupOperationMode;


@property (weak, nonatomic) IBOutlet UIView          *headerView;
@property (weak, nonatomic) IBOutlet UITextField     *groupNameText;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBtn;
// Moved this control to the top so that when the view is scrolled down use can still change teh modes. Don't know why i did not do tha earlier, may be had an + button earlier there
@property (weak, nonatomic) IBOutlet UISegmentedControl *segSortToggle;


- (IBAction)done:(id)sender;
- (IBAction)segSortToggle_ValueChanged:(id)sender;


- (void) initializeWithMode: (GroupOperationMode) selOperationMode LastSelGroupSet: (NSSet*) lastSelGroupSet Context:(NSManagedObjectContext *) managedContext Delegate: (id<TabGroupsViewControllerDelegate>) delegate;

@end

@protocol TabGroupsViewControllerDelegate <NSObject>

-(void) tabGroupsViewControllerDidFinish:(id) controller SelectedGroup:(Group *)group;
@end
