//
//  ReminderViewController.h
//  realtodo
//
//  Created by Balbir Shokeen on 9/5/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDTO.h"
#import "SelectCustomDaysViewController.h"

@protocol ReminderViewControllerDelegate;


@interface ReminderViewController : UITableViewController <SelectCustomDaysViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *dateQuickOptionSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *timeQuickOptionSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *monthQuickOptionSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *yearQuickOptionSegment;


@property (weak, nonatomic) IBOutlet UISegmentedControl *remindOnChoiceSegment;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;

@property (weak, nonatomic) IBOutlet UILabel *displayDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *displayCustomDaysLabel;

@property (strong, nonatomic) id <ReminderViewControllerDelegate> delegate;

- (void) initWithArgumentsRemindOn: (NSDate *) remindOn RemindRepeatInterval: (NSCalendarUnit) remindRepeatInterval;

- (IBAction)remindOnChoiceSegmentValueChanged:(id)sender;
- (IBAction)dateQuickOptionSegment_ValueChanged:(id)sender;
- (IBAction)timeQuickOption_ValueChanged:(id)sender;
- (IBAction)monthQuickOptionSegment_ValueChanged:(id)sender;
- (IBAction)yearQuickOptionSegment_ValueChanged:(id)sender;

- (IBAction)bottomSaveBtnClick:(id)sender;

@end

@protocol ReminderViewControllerDelegate <NSObject>

- (void)RemindViewControllerDidFinish: (NSDate *)remindOn RepeatInterval:(NSCalendarUnit) nscuRemindRepeatInterval FormatedLabelText: (NSString *) formatedLabelText ;

@end