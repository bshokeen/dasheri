//
//  CoreDataOperations.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/08/05.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//

#import "CoreDataOperations.h"
#import "AppSharedData.h"
#import "AppDelegate.h"

@implementation CoreDataOperations

+ (int) countGroups {

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"Group" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT countForFetchRequest:fetchRequest error:&error];
    
    return count;
}

+ (int) countTasks {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT countForFetchRequest:fetchRequest error:&error];
    
    return count;
}

+ (Tag *) addTag: (NSString *)tagName SortOrder:(int) sortOrder {
    
    Tag *tag  = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
                                                    inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
   
    tag.name = tagName;
    
    NSError *error = nil;
    if (![tag.managedObjectContext save:&error]) {
        DLog(@"Error Saving Tag %@, %@", error, [error userInfo]);
        
        abort();
    }
    
    return tag;
}

+ (Group *) addGroup: (NSString *)groupName SortOrder:(int) sortOrder {
    Group *newGroup = [NSEntityDescription insertNewObjectForEntityForName:@"Group"
                                                    inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    newGroup.name = groupName;
    newGroup.sort_order = [NSNumber numberWithInt: sortOrder];
    
    NSError *error = nil;
    if (![newGroup.managedObjectContext save:&error]) {
        DLog(@"Error Saving Group %@, %@", error, [error userInfo]);
        
        abort();
    }
    
    return newGroup;
}

+ (Task *) addTask:(NSString *) name Gain:(int) gain DueOn:(NSDate *)dueOn RemindOn:remindOn Note:(NSString *)note Flag:(int) flag Group:(Group *)group EstimCountIdx: (int) estimCountIdx EstimUnitIdx: (int) estimUnitIdx Tags: (NSMutableSet *) selTags RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval {
    Task *newTaskManagedObject = (Task *)[NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];
    
    [newTaskManagedObject setName:name];
    [newTaskManagedObject setGain:[NSNumber numberWithInt:gain]];
    [newTaskManagedObject setDone:[NSNumber numberWithInt:0]];
    [newTaskManagedObject setCreatedOn:[NSDate date]];
    // Not updating modified time as it will be automatically handled using categories on save, updating the time.
    [newTaskManagedObject setDueOn:dueOn];
    [newTaskManagedObject setGroup:group];
    [newTaskManagedObject setProjectName:group.name];
    [newTaskManagedObject setNote:note];
    [newTaskManagedObject setFlag:[NSNumber numberWithInt:flag]];
    [newTaskManagedObject setEffort_time:[NSNumber numberWithInt:estimCountIdx]];
    [newTaskManagedObject setEffort_time_unit:[NSNumber numberWithInt:estimUnitIdx]];
    [newTaskManagedObject setTagS:selTags];
    
    // Convert Selected CustomDays array to string (to store for the task)
    NSString *customDaysString = [[AppSharedData getInstance].reminderCustomDaysArray componentsJoinedByString:@","];
    
    [newTaskManagedObject setRemindOnCustomDays:customDaysString];
    // Reset the seconds part to make the reminder come exactly on the minute set.
    [newTaskManagedObject setRemind_on: [AppUtilities getDateWithSecondsResetForDate:remindOn]];
    [newTaskManagedObject setRemind_repeat_interval:[NSNumber numberWithInt:nscuRemindRepeatInterval]];
    
    // PUt a temporary check for debugging
    if (nscuRemindRepeatInterval == NSDayCalendarUnit || nscuRemindRepeatInterval == NSWeekCalendarUnit || nscuRemindRepeatInterval == NSMonthCalendarUnit || nscuRemindRepeatInterval == NSYearCalendarUnit || nscuRemindRepeatInterval == NSEraCalendarUnit || nscuRemindRepeatInterval == 0 ) {
        
    }
    else {
        ULog(@"Invalid Remind Repeat Interval: %d", nscuRemindRepeatInterval);
        DLog(@"ERROR - Invalid Repeat Interval: %d", nscuRemindRepeatInterval);
    }
    
    // Add Sort Order to each task in a way that each group has its own counter starting from 1. Here we just getting max sort order value of existing tasks and increment by 1
    int taskMaxSortOrder = [AppUtilities getMaxSortOrder: newTaskManagedObject.group];
    [newTaskManagedObject setSort_order: [NSNumber numberWithInt: taskMaxSortOrder + 1]];
    
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    // Schedule the Alarm if applicable
    if (newTaskManagedObject.remind_on) {
        
        NSString *taskObjectID = [[[newTaskManagedObject objectID] URIRepresentation] absoluteString];
        
        [AppUtilities scheduleAlarm:newTaskManagedObject.group.name AlarmDate:newTaskManagedObject.remind_on  TaskName:newTaskManagedObject.name TaskObjectID:taskObjectID RemindInterval:nscuRemindRepeatInterval];
    }
    
    return newTaskManagedObject;
}


+ (Subtask *) addSubtaskForTask:(Task*)task Data: (NSString *)data Status:(int)status SortOrder:(int) sortOrder Type:(int) type {
    Subtask *newSubtask = [NSEntityDescription insertNewObjectForEntityForName:@"Subtask"
                                                    inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    newSubtask.task       = task;
    newSubtask.data       = data;
    newSubtask.status     = [NSNumber numberWithInt:status];
    newSubtask.type       = [NSNumber numberWithInt:type];
    newSubtask.sort_order = [NSNumber numberWithInt: sortOrder];
    newSubtask.created_on = [NSDate date];
    
    NSError *error = nil;
    if (![newSubtask.managedObjectContext save:&error]) {
        DLog(@"Error Saving Subtask %@, %@", error, [error userInfo]);
        
        abort();
    }
    
    return newSubtask;
}


#pragma mark - Delete Operations
+(NSArray *) getItemsGreaterThanSortOrder: (int) sortOrder EntityName:(NSString *) entityName  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order > %i ", sortOrder];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    return fetchResults;
}


+(BOOL) deleteItemByEntityName:(NSString *)entityName Item:(NSManagedObject *)item  {
    
    int sortOrder = -1;
    
    if ( [entityName isEqualToString:@"Subtask"] ) {
        Subtask *subtask = (Subtask *) item;
        sortOrder = [subtask.sort_order intValue];
    }
    else {
        NSLog(@"Throw Exception");
        
    }
    
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    [context deleteObject:item];
    
    // Before Deleting update the sorting order of all the items beyond the sorting order of item being deleted. This prevent duplicate sort order and hence bad tableview data
    
    NSArray *fetchedResults = nil;
    fetchedResults = [CoreDataOperations getItemsGreaterThanSortOrder:sortOrder EntityName:entityName ];
    
    // Save Context
    [CoreDataOperations saveContext];
    
    // Saving context after doing the sorting order fix was resulting in crash when first itme of a group is deleted as the item was unavailable
    // could have queried the results after saving but then required task.group name
    // so retrieving the groups first, save context of delete operation, then perform the update on sorting order and finally save this also.
    for (int i=0; i < [fetchedResults count]; i++) {
        
        [[fetchedResults objectAtIndex:i] setSort_order: [NSNumber numberWithInt:sortOrder + i]];
    }
    // Save Context
    [CoreDataOperations saveContext];
    
    return YES;
}


/*** Extract Data Operations ***/
// Sumation API - NOTE THEIRE IS A COLON AFTER SUM
// NSNumber *fooSum = [Foo aggregateOperation:@"sum:" onAttribute:@"bar" withPredicate:nil inManagedObjectContext:context];

// Included the ability to specify the entity name as well.
+(NSNumber *)aggregateOperation:(NSString *)function onEntity:(NSString *) entityName onAttribute:(NSString *)attributeName withPredicate:(NSPredicate *)predicate inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSExpression *ex = [NSExpression expressionForFunction:function
                                                 arguments:[NSArray arrayWithObject:[NSExpression expressionForKeyPath:attributeName]]];
    
    NSExpressionDescription *ed = [[NSExpressionDescription alloc] init];
    [ed setName:@"result"];
    [ed setExpression:ex];
    [ed setExpressionResultType:NSInteger64AttributeType];
    
    NSArray *properties = [NSArray arrayWithObject:ed];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setPropertiesToFetch:properties];
    [request setResultType:NSDictionaryResultType];
    
    if (predicate != nil)
        [request setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    
    
    NSArray *results = [context executeFetchRequest:request error:nil];
    NSDictionary *resultsDictionary = [results objectAtIndex:0];
    NSNumber *resultValue = [resultsDictionary objectForKey:@"result"];
    
    return resultValue;
    
}


// Find the maxDueOn on a specified day
+(NSDate *) getMaxDueOnOnDay:(NSDate *) datePartOnly {
    
    NSDate *maxDueOn = nil;
    
    if (datePartOnly != nil) {
        // Generate nextDayStart to prepare between query and fetch this day record only.
        NSDate *nextDayStart     = [AppUtilities getStartOfNextDayDate:datePartOnly];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
        [fetchRequest setEntity:entity];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(dueOn >= %@ && dueOn < %@ && dueOn==max(dueOn))", datePartOnly, nextDayStart];
        [fetchRequest setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
        
        if ( fetchResults != nil && [fetchResults count] > 0) {
            Task *task = (Task *)[fetchResults objectAtIndex:0];
            maxDueOn = task.dueOn;
        }
    }
    
    return maxDueOn;
}

// Get a list of tasks sorted by sort_order for sortorder including the input range
+ (NSArray *) taskBetweenSortOrder:(int)sortOrderA AndSortOrder:(int)sortOrderB ForGroup:(Group *) group {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order >= %i && sort_order <= %i and group == %@", sortOrderA, sortOrderB, group];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    return fetchResults;
}


+ (NSArray *) getTaskGreaterThanSortOrder: (int) sortOrderVal Group:(Group *) group {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order > %i and group == %@", sortOrderVal, group];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    return fetchResults;
}

+(Group *) getGroupBySortID:(int)sortOrder {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Group" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(sort_order == %i)", sortOrder];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    Group *val =[fetchResults objectAtIndex:0];
//    if (val == nil) {
//        NSLog(@"TEST: NIL");
//    }
//    else {
//        NSLog(@"TEST: %@", val.name);
//    }
    return val;
}

// I ahve written this generic function also but using it complicates the reading so left it for learning from it in future
+ (BOOL) updateAllSortOrder: (NSString *) entityName {
    
    BOOL fixRequired = NO;
    
    if ([entityName isEqualToString:@"Group"] || [entityName isEqualToString:@"Tag"] ) { // supported to fix
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
        
        NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
        
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        // Execute the fetch
        NSError *error = nil;
        NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
        
        int sortOrderCounter    = TASK_SORT_ORDER_START_IDX; // 1
        
        
        for (NSObject *item in fetchResults) {
            if ([item isKindOfClass:[Group class]]) {
                Group *grp = (Group *) item;
                DLog(@"Sort_Order:%i NewSortOrder:%i Group:%@", [grp.sort_order intValue], sortOrderCounter, grp.name);
                
                if ([grp.sort_order intValue] != sortOrderCounter) { // Updating only when required to avoid writing back all records
                    fixRequired = YES;
                    grp.sort_order = [NSNumber numberWithInt:sortOrderCounter];
                }
            }
            else if ([item isKindOfClass:[Tag class]]) {
                Group *tag = (Group *) item;
                DLog(@"Sort_Order:%i NewSortOrder:%i Tag:%@", [tag.sort_order intValue], sortOrderCounter, tag.name);
                
                if ([tag.sort_order intValue] != sortOrderCounter) { // Updating only when required to avoid writing back all records
                    fixRequired = YES;
                    tag.sort_order = [NSNumber numberWithInt:sortOrderCounter];
                }
            }
            
            sortOrderCounter++;
        }
    }
    
    return fixRequired;
}

// Loop through all the Tags in its current sort_order sequence and update the items where the sort order is not as expected
+ (BOOL) updateAllTagsSortOrder {
    
    BOOL fixRequired = NO;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Tag" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    int sortOrderCounter    = TASK_SORT_ORDER_START_IDX; // 1
    
    for (Tag *item in fetchResults) {
        DLog(@"Sort_Order:%i NewSortOrder:%i Tag  :%@", [item.sort_order intValue], sortOrderCounter, item.name);
        
        if ([item.sort_order intValue] != sortOrderCounter) { // Updating only when required to avoid writing back all records
            fixRequired = YES;
            item.sort_order = [NSNumber numberWithInt:sortOrderCounter];
        }
        
        sortOrderCounter++;
    }
    
    return fixRequired;
}
// Loop through all the groups in its current sort_order sequence and update the items where the sort order is not as expected
+ (BOOL) updateAllGroupsSortOrder {
    
    BOOL fixRequired = NO;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Group" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    int sortOrderCounter    = TASK_SORT_ORDER_START_IDX; // 1
    
    for (Group *item in fetchResults) {
        DLog(@"Sort_Order:%i NewSortOrder:%i Group:%@", [item.sort_order intValue], sortOrderCounter, item.name);
        
        if ([item.sort_order intValue] != sortOrderCounter) { // Updating only when required to avoid writing back all records
            fixRequired = YES;
            item.sort_order = [NSNumber numberWithInt:sortOrderCounter];
        }
        
        sortOrderCounter++;
    }
    
    return fixRequired;
}


// Insert basic records into the Group & Task Tables with some default meaningful values
+ (void) insertDefaultData {
    
    Group *grp1 = [CoreDataOperations addGroup:NSLocalizedString(@"Family", @"Family")    SortOrder:1];
    Group *grp2 = [CoreDataOperations addGroup:NSLocalizedString(@"Ideas", @"Ideas")     SortOrder:2];
    Group *grp3 = [CoreDataOperations addGroup:NSLocalizedString(@"Goals", @"Goals")     SortOrder:3];
    Group *grp6 = [CoreDataOperations addGroup:NSLocalizedString(@"Work", @"Work")      SortOrder:4];
    Group *grp4 = [CoreDataOperations addGroup:NSLocalizedString(@"Shopping", @"Shopping")  SortOrder:5];
    Group *grp5 = [CoreDataOperations addGroup:NSLocalizedString(@"Study", @"Study")     SortOrder:6];
    
    Tag *tag1 = [CoreDataOperations addTag:NSLocalizedString(@"Important", @"Important")     SortOrder:1];
    Tag *tag2 = [CoreDataOperations addTag:NSLocalizedString(@"Urgent", @"Urgent")        SortOrder:2];
    Tag *tag3 = [CoreDataOperations addTag:NSLocalizedString(@"Not Important", @"Not Important") SortOrder:3];
    Tag *tag4 = [CoreDataOperations addTag:NSLocalizedString(@"Not Urgent", @"Not Urgent")    SortOrder:4];
    NSMutableSet *tagSet1_Imp    = [[NSMutableSet alloc] initWithObjects:tag1, nil];
    NSMutableSet *tagSet2_Urg    = [[NSMutableSet alloc] initWithObjects:tag2, nil];
    NSMutableSet *tagSet3_Urg_NI = [[NSMutableSet alloc] initWithObjects:tag2, tag3, nil];
    NSMutableSet *tagSet4_Imp_NU = [[NSMutableSet alloc] initWithObjects:tag1, tag4, nil];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    NSDate *today=[NSDate date];
    NSDate *tomorrow = [today dateByAddingTimeInterval:(1*24*60*60)];
    NSDate *weeklater =  [today dateByAddingTimeInterval:(7*1*24*60*60)];
    components.month = 1;
    NSDate *monthlater = [gregorian dateByAddingComponents:components toDate:today options:0];
    components.year = 1;
    NSDate *YearLater =  [gregorian dateByAddingComponents:components toDate:today options:0];
    
    
    [CoreDataOperations addTask:NSLocalizedString(@"Recharge Electricity", @"Recharge Electricity") Gain:2 DueOn:tomorrow RemindOn:nil Note:nil Flag:0 Group:grp1 EstimCountIdx:0 EstimUnitIdx:0 Tags:tagSet2_Urg RemindInterval:0];
    
    [CoreDataOperations addTask:NSLocalizedString(@"Social Website For Kids", @"Social Website For Kids") Gain:2 DueOn:monthlater RemindOn:nil Note:nil Flag:0 Group:grp2 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];
    
    [CoreDataOperations addTask:NSLocalizedString(@"Quit Smoking", @"Quit Smoking") Gain:0 DueOn:YearLater RemindOn:nil Note:nil Flag:0 Group:grp3 EstimCountIdx:0 EstimUnitIdx:0 Tags:tagSet1_Imp RemindInterval:0];
    
    [CoreDataOperations addTask:NSLocalizedString(@"Take Course On Leadership", @"Take Course On Leadership") Gain:1 DueOn:monthlater RemindOn:nil Note:nil Flag:0 Group:grp3 EstimCountIdx:0 EstimUnitIdx:0 Tags:tagSet4_Imp_NU RemindInterval:0];
    [CoreDataOperations addTask:NSLocalizedString(@"Read A Book Every Month", @"Read A Book Every Month") Gain:1 DueOn:monthlater RemindOn:nil Note:NSLocalizedString(@"Great By Choice", @"Great By Choice") Flag:0 Group:grp4 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];
    
    [CoreDataOperations addTask:NSLocalizedString(@"Buy Perfume", @"Buy Perfume") Gain:2 DueOn:today RemindOn:nil Note:nil Flag:0 Group:grp4 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];
    [CoreDataOperations addTask:NSLocalizedString(@"Buy Candles", @"Buy Candles") Gain:2 DueOn:today RemindOn:nil Note:nil Flag:0 Group:grp4 EstimCountIdx:0 EstimUnitIdx:0 Tags:tagSet3_Urg_NI RemindInterval:0];

    [CoreDataOperations addTask:NSLocalizedString(@"Corporate Governance", @"Corporate Governance") Gain:2 DueOn:weeklater RemindOn:nil Note:nil Flag:0 Group:grp5 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];
    
    [CoreDataOperations addTask:NSLocalizedString(@"Review Accounts", @"Review Accounts") Gain:1 DueOn:today RemindOn:nil Note:nil Flag:0 Group:grp6 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];
    [CoreDataOperations addTask:NSLocalizedString(@"Interview Candidates", @"Interview Candidates") Gain:1 DueOn:today RemindOn:nil Note:nil Flag:0 Group:grp6 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];
    [CoreDataOperations addTask:NSLocalizedString(@"Send Invoice To Xyz Company", @"Send Invoice To Xyz Company") Gain:3 DueOn:tomorrow RemindOn:nil Note:nil Flag:0 Group:grp6 EstimCountIdx:0 EstimUnitIdx:0 Tags:nil RemindInterval:0];

    // TODO: Add dummy data for subtasks and Subtasks globalised/locazatoin strings
    // Add reminder on xyz and interview candidate to take snapshot of act on
    
}

// UPGRADE APIS

// Get the list of tasks ordered by group, task.sortorder. Keep incrementing the sort order starting from 1 for each group.
+ (BOOL)updateAllTasksSortOrder {
    
    BOOL fixRequried = NO;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"group" ascending:YES];
    NSSortDescriptor *orderByDescriptor_2 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, orderByDescriptor_2, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    NSString *lastGroupName = @" ";
    int sortOrderCounter    = TASK_SORT_ORDER_START_IDX; // 1
    
    for (Task *taskItem in fetchResults) {
        NSString *curGrpName = taskItem.group.name;
        if (lastGroupName != curGrpName) {
            sortOrderCounter = TASK_SORT_ORDER_START_IDX; // 1
        }
        
        DLog(@"Sort_Order:%i NewSortOrder:%i Group:%@ Task:%@", [taskItem.sort_order intValue], sortOrderCounter, taskItem.group.name, taskItem.name);
        
        if ([taskItem.sort_order intValue] != sortOrderCounter) { // Updating only when required to avoid writing back all records
            fixRequried = YES;
            taskItem.sort_order = [NSNumber numberWithInt:sortOrderCounter];
        }
        
        sortOrderCounter++;
        lastGroupName = curGrpName;
    }
    
    // Save all the updates done so far.
    [AppUtilities saveContext];
    
    return fixRequried;
}

+(void) splitNoteToSubtasksForTask:(Task *) task {
    if (task && task.note) {
        NSArray *subtaskS = [task.note componentsSeparatedByString:@"\u2022"];

        for (int i=0; i< [subtaskS count]; i++) {
            NSString *data = [AppUtilities trim:[subtaskS objectAtIndex:i]];
            if ( data && data.length > 0 ) {
                [CoreDataOperations addSubtaskForTask:task Data:data Status:0 SortOrder:i Type:0];
//                NSLog(@"Inserting Note:%i val:%@ Task:%@", i, data, task.name);
            }
        }
        task.note = nil; // clear the old notes
    }
}

+(BOOL) runOnceOnDeviceUpgradeNotesToSubtasks {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    int sortOrderCounter    = TASK_SORT_ORDER_START_IDX; // 1
    
    for (Task *item in fetchResults) {
        
        if (item.note)  { // Updating only when required to avoid writing back all records
            
            [CoreDataOperations splitNoteToSubtasksForTask:(Task*) item];
        }
        
        sortOrderCounter++;
        DLog(@"Task Subtasks: %i Name: %@", [item.subtaskS count] , item.name);
    }

    // Save all the updates done so far.
    [AppUtilities saveContext];
    
    return YES;
}

/* Common API for Group and Tag
 *
 * To find the sum of a certain number of terms of an arithmetic sequence: Sn = n(a1 + an)/2
 * where Sn is the sum of n terms (nth partial sum),
 * a1 is the first term,  an is the nth term.
 */
+(BOOL) isSortOrderFragmented: (NSString *) entityName {
    
    BOOL isSortOrderFragmented = NO;
    
    NSString *useEntityName = nil;
    if ( [entityName isEqualToString:@"Group"] ) {
        useEntityName = @"Group";
    }
    else if ( [entityName isEqualToString:@"Tag"] ) {
        useEntityName = @"Tag";
    }
    else {
        // retain nil and cannot do anyting
        isSortOrderFragmented = NO;
    }
    
    if (entityName) {
        NSNumber *sumAllSortOrders =   [CoreDataOperations aggregateOperation:@"sum:" onEntity:useEntityName onAttribute:@"sort_order" withPredicate:nil inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
        
        NSNumber *countAllSortOrders = [CoreDataOperations aggregateOperation:@"count:" onEntity:useEntityName onAttribute:@"sort_order" withPredicate:nil inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
        
        int an = [countAllSortOrders intValue];
        int a1 = 1;
        int calculated = (an * (a1 + an))/2; //an = n as it is sequential series
        
        if ( calculated != [sumAllSortOrders intValue]) {
            isSortOrderFragmented = YES;
        }
        else {
            isSortOrderFragmented = NO;
        }
    }
   
    return isSortOrderFragmented;
}

// Data Fixes & Upgrade API
+(void) fixSortOrder {

    /** Check if sort order is not correct.
     *      See for Groups First & Then Tasks
     *  Loop through the groups and validate
     *  While looping you can validate for Tasks as well
     */
    
    BOOL isSortOrderFragmentedForGroup = [CoreDataOperations isSortOrderFragmented:@"Group"];
    BOOL isSortOrderFragmentedForTag   = [CoreDataOperations isSortOrderFragmented:@"Tag"];
    
    BOOL grpFixed = NO;
    BOOL tagFixed = NO;
    BOOL taskFixed= NO;
    
    if (isSortOrderFragmentedForGroup) {
        [Flurry logEvent:@"ERROR: Sort Order Gap: GROUP - DETECTED"];
        grpFixed = [CoreDataOperations updateAllGroupsSortOrder];
    
        [AppUtilities saveContext]; // save so that we get the correct sort order when fixing tasks
    }
    
    if (isSortOrderFragmentedForTag) {
        [Flurry logEvent:@"ERROR: Sort Order Gap: TAG   - DETECTED"];
        tagFixed = [CoreDataOperations updateAllTagsSortOrder];
        
        [AppUtilities saveContext]; // save so that we get the correct sort order when fixing tasks
    }
    
    if (grpFixed || tagFixed ) {
        // No way to check if tasks have a gap so just going and doing if gap found in group or tag
        taskFixed= [CoreDataOperations updateAllTasksSortOrder];

        [AppUtilities saveContext];
    }
    
    //LOGGING
    if ( grpFixed )
        [Flurry logEvent:@"ERROR: Sort Order Gap: GROUP - FIXED"];
    if ( tagFixed )
        [Flurry logEvent:@"ERROR: Sort Order Gap: TAG   - FIXED"];
    if ( taskFixed )
        [Flurry logEvent:@"ERROR: Sort Order Gap: TASK  - FIXED"];
    
    if (grpFixed || tagFixed || taskFixed )
        NSLog(@"RunOnLaunch: Fixed Sort Order: [ GROUP:%@ TAG:%@ TASK:%@ ]", grpFixed?@"Yes" :@"No", tagFixed?@"Yes":@"No", taskFixed?@"Yes": @"No");
}


#pragma mark - SaveContext
+ (void) saveContext {
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    NSError *error;
    if (![context save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

@end
