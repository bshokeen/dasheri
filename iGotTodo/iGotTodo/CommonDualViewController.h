//
//  CommonDualViewController.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/08.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UpdateTaskViewController.h"
#import "AddTaskViewController.h"
#import "UITableSectionHeaderView.h"
#import "SectionHeaderCell.h"
#import "SectionHeaderView.h"
#import "TabFilterViewController.h"
#import "Task.h"
#import "Group.h"
#import "AppSharedData.h"
#import "AppUtilities.h"
#import "TaskViewCell.h"
#import "DragHelper.h"
#import "GroupCollViewCell.h"
#import "TaskDetailView.h"
#import "ToolbarCell.h"
#import "ToolbarItem.h"

// TODO: Rename, give better name. Maximum number for which we will track open/closed section names.
#define MAX_TRACKED_SEGMENTS        7


// Track which data mode is active right now to take appropriate action. Mode imply button clicked under collectionview
typedef enum { ModeShowGroups=0, ModeShowGain=1, ModeShowDue=2, ModeShowTags=3, ModeShowTaskDetail=4 } TypeModeShow;
// To set the state of the cell. We reset images using these and set a few values in the tag while configuring cell. Normal, Special Delete, Add (not used) are valid tags. FocusOn is state which is changing in between operations
typedef enum { TypeOfCellIsNormal=0, TypeOfCellIsSpecialDelete=1, TypeOfCellIsSpecialAdd=2, TypeOfCellIsSpecialFocusOn=3} TypeCellCollnView;
// Due on types supported
typedef enum { DueOnToday=0, DueOnTomorrow=1, DueOnWeekend=2, DueOnNextWeek=3, DueOnNextMonth=4 } TypeDueOn;
// Gain Types
typedef enum { GainHigh=0, GainMedium=1, GainNormal=2, GainLow=3 } TypeGain;

@interface CommonDualViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UpdateTaskViewControllerDelegate, NSFetchedResultsControllerDelegate, SectionHeaderCellDelegate, SectionHeaderViewDelegate, UITableSectionHeaderViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, DragHelperDelegate> //, TaskViewCellDelegate not implementing the going to details of task/subtask in this case

//@property (nonatomic, strong) NSManagedObjectContext        *mgdObjContext;

/** This is the Source collection */
@property (nonatomic, strong) IBOutlet UITableView* leftTable;

/** This is the Destination collection */
@property (nonatomic, strong) IBOutlet UICollectionView* rightCollection;
// Added to show details of a task
@property (weak, nonatomic) IBOutlet TaskDetailView *taskDetailView;

// This object is actually populated by the subclasses
// Core data fetched results controller for the left table
@property (nonatomic, strong) NSFetchedResultsController *leftTableFetchedResultsControllerObj;
// Core data fetched results controller for the right collections
@property (nonatomic, strong) NSFetchedResultsController *rightColnFetchedResultsControllerObj;
// Property to holde the help class, required for supporting drag n drop
@property (nonatomic, strong) DragHelper* helper;

// Idx which is the key to record open/closed groups
//TODO: How do i ensure that whenever a selection changes these variables are updated by caller or user of this controller and also how to ensure the must have methods are overriden in the subclasses
@property (nonatomic) int selSegmentIdx;


@property (nonatomic, strong) NSString *thisInterfaceEntityName;

// All these properties are for the cell comprising of the below button bar in the collection view
// Outlets are defined to get the location/point and action to act on click
@property (nonatomic) TypeModeShow selDataModeForCollnView; // local variable to identify which button is clicked for collection table


@property (weak, nonatomic) IBOutlet UICollectionView *rightToolbarCollnView;

- (IBAction)btn_GroupClick:(id)sender; //To switch between views
- (IBAction)btn_GainClick:(id)sender;
- (IBAction)btn_DueClick:(id)sender;
- (IBAction)btn_TagClick:(id)sender;
- (IBAction)btn_TaskDetailClick:(id)sender;


@property (weak, nonatomic) IBOutlet UIImageView *selBtnIndicatorImg;
@property (weak, nonatomic) IBOutlet UIButton *btn_Group;
@property (weak, nonatomic) IBOutlet UIButton *btn_Gain;
@property (weak, nonatomic) IBOutlet UIButton *btn_Due;
@property (weak, nonatomic) IBOutlet UIButton *btn_Tags;
@property (weak, nonatomic) IBOutlet UIButton *btn_TaskDetails;


- (void) forceReloadThisUI;
- (void) fetchData_leftTable;
- (void) fetchData_rightColn;
- (NSFetchedResultsController *) leftTableFetchedResultsController;
- (NSFetchedResultsController *) rightColnFetchedResultsController;
- (NSString *) computeSectionNameFromRawName: (NSInteger) section;


@end
