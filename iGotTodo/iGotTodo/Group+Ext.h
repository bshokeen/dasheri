//
//  Group+Ext.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/02/18.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "Group.h"

@interface Group (Ext)

@property (nonatomic, retain) NSString *primitiveUppercaseFirstLetterOfName;

@end
