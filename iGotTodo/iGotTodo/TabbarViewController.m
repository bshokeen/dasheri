//
//  TabbarViewController.m
//  iGotTodo
//
//  Created by Me on 15/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabbarViewController.h"

@interface TabbarViewController ()

@end

@implementation TabbarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.delegate = self; // Making itself get the edit mode completion events. It was very confusing to me.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
       return YES;
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// Save the order of tabs by saving the restorationID of the viewcontrollers
-(void) tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {

    NSUInteger count = self.viewControllers.count;
    NSMutableArray *tabOrderArray = [[NSMutableArray alloc] initWithCapacity:count];
    
    for (UIViewController *viewController in viewControllers) {
        // Could have used a tag but using restoration ID as that is anyway needed on my tabs for state preserve and restore.
        NSString *controlrName = [viewController restorationIdentifier];
        NSAssert(controlrName, @"Restoration ID not specified on TabItem ViewController Type:%@", [[viewController class] description]);
        
        [tabOrderArray addObject:controlrName];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tabOrderArray forKey:SAVED_TAB_ORDER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



@end
