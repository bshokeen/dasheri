//
//  Group.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/02/18.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Task;

@interface Group : NSManagedObject

@property (nonatomic, retain) NSNumber * color;
@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSDate * deleted_on;
@property (nonatomic, retain) NSNumber * dirty;
@property (nonatomic, retain) NSString * group_id;
@property (nonatomic, retain) NSNumber * icon_id;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSString * uppercaseFirstLetterOfName;
@property (nonatomic, retain) NSSet *tasks;
@end

@interface Group (CoreDataGeneratedAccessors)

- (void)addTasksObject:(Task *)value;
- (void)removeTasksObject:(Task *)value;
- (void)addTasks:(NSSet *)values;
- (void)removeTasks:(NSSet *)values;

@end
