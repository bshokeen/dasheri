//
//  DueDateViewController.h
//  realtodo
//
//  Created by Me on 17/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDTO.h"

@protocol DueDateViewControllerDelegate;

@interface DueDateViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextView *dataTextView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *enableDateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *enableDateSwitch;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;

@property (weak, nonatomic) IBOutlet UISegmentedControl *dueOnChoiceSegment;

@property (nonatomic, strong) CommonDTO *commonDTOObj;


@property(weak, nonatomic) id <DueDateViewControllerDelegate> delegate;

- (IBAction)save:(id)sender;
- (IBAction)resetBtnClick:(id)sender;
- (IBAction)bottomSaveBtnClick:(id)sender;


@end

@protocol DueDateViewControllerDelegate <NSObject>

-(void) dueDateViewControllerDidCancel: (id *) controller;
-(void) dueDateViewControllerDidFinish:(CommonDTO *) commonDTO FormatedDate: (NSString *)FormatedDate;

@end
