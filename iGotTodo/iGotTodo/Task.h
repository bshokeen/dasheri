//
//  Task.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/25.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category, Group, Subtask, Tag;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSNumber * actual_time;
@property (nonatomic, retain) NSNumber * actual_time_unit;
@property (nonatomic, retain) NSNumber * alert_notification;
@property (nonatomic, retain) NSDate * alert_notification_on;
@property (nonatomic, retain) NSNumber * child_count;
@property (nonatomic, retain) NSDate * completed_on;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSDate * deleted_on;
@property (nonatomic, retain) NSNumber * dirty;
@property (nonatomic, retain) NSNumber * done;
@property (nonatomic, retain) NSString * doneSectionName;
@property (nonatomic, retain) NSDate * dueOn;
@property (nonatomic, retain) NSString * dueOnSectionName;
@property (nonatomic, retain) NSNumber * effort_time;
@property (nonatomic, retain) NSNumber * effort_time_unit;
@property (nonatomic, retain) NSString * extra1;
@property (nonatomic, retain) NSNumber * flag;
@property (nonatomic, retain) NSString * flagSectionName;
@property (nonatomic, retain) NSNumber * gain;
@property (nonatomic, retain) NSString * gainSectionName;
@property (nonatomic, retain) NSString * group_id;
@property (nonatomic, retain) NSString * icon_name;
@property (nonatomic, retain) NSString * location_alert;
@property (nonatomic, retain) NSString * location_id;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * parent_task_id;
@property (nonatomic, retain) NSNumber * priority;
@property (nonatomic, retain) NSNumber * progress;
@property (nonatomic, retain) NSString * progressSectionName;
@property (nonatomic, retain) NSString * projectName;
@property (nonatomic, retain) NSDate * remind_on;
@property (nonatomic, retain) NSNumber * remind_repeat_interval;
@property (nonatomic, retain) NSString * remind_via;
@property (nonatomic, retain) NSString * remindOnCustomDays;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * statusSectionName;
@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * task_id;
@property (nonatomic, retain) NSNumber * task_type;
@property (nonatomic, retain) NSString * dateSectionName;
@property (nonatomic, retain) Category *category;
@property (nonatomic, retain) Group *group;
@property (nonatomic, retain) NSSet *subtaskS;
@property (nonatomic, retain) NSSet *tagS;
@end

@interface Task (CoreDataGeneratedAccessors)

- (void)addSubtaskSObject:(Subtask *)value;
- (void)removeSubtaskSObject:(Subtask *)value;
- (void)addSubtaskS:(NSSet *)values;
- (void)removeSubtaskS:(NSSet *)values;

- (void)addTagSObject:(Tag *)value;
- (void)removeTagSObject:(Tag *)value;
- (void)addTagS:(NSSet *)values;
- (void)removeTagS:(NSSet *)values;

@end
