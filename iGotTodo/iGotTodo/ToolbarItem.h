//
//  ToolbarItem.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/22.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

/** This class is object model for a toolbar item */

#import <Foundation/Foundation.h>

@interface ToolbarItem : NSObject

@property (nonatomic) int  index;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *iconImg;
@property (nonatomic) int  count;

- (instancetype)initWithIndex:(int)idx Title:(NSString*)titleLbl IconImg:(NSString*)iconImage Count:(int)cnt;

@end
