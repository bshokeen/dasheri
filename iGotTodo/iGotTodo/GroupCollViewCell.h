//
//  GroupCollViewCell.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/02/26.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupCollViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *countBtn;
@property (weak, nonatomic) IBOutlet UIButton *iconImg2;

-(void) resetInitialText;
@end
