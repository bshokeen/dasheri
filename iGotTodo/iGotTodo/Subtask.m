//
//  Subtask.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/25.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "Subtask.h"
#import "Task.h"


@implementation Subtask

@dynamic created_on;
@dynamic data;
@dynamic name;
@dynamic sort_order;
@dynamic status;
@dynamic type;
@dynamic task;

@end
