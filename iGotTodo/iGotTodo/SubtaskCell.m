//
//  SubtaskCell.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/24.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "SubtaskCell.h"
#import "AppUtilities.h"
#import "CoreDataOperations.h"
@interface SubtaskCell ()

@end

@implementation SubtaskCell




- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) resetInitialText {
//    self.statusBtn
    self.dataTxt.text = @"";
}

// ENded up taking long route and passing the font size from the caller
-(void) initializeWith:(Subtask *)subtask Delegate:(id) delegate FontSize:(CGFloat) fSize{

    self.delegate = delegate;
    self.subtask = subtask;
    self.fSize   = fSize;
    
    if ([self.subtask.status intValue] == 1) {
        self.dataTxt.attributedText = [AppUtilities getAtrbutedWith_FontStriked:self.subtask.data FontSize:self.fSize];
        
        [self.statusBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOn"]] forState:UIControlStateSelected];
        [self.statusBtn setSelected:YES];
        
    }
    else {
        
        self.dataTxt.attributedText = [AppUtilities getAtrbutedWith_FontNotStriked:self.subtask.data FontSize:self.fSize]; // use smaller font in this view
        
        [self.statusBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOff"]] forState:UIControlStateNormal];
        
        [self.statusBtn setSelected:NO];

    }

}

-(void) toggleStatus {
    if ([self.subtask.status intValue] == 1) {
        self.subtask.status = [NSNumber numberWithInt:0];
        self.dataTxt.attributedText = [[NSMutableAttributedString alloc] initWithString: self.subtask.data];
        
        [self.statusBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOff"]] forState:UIControlStateNormal];
        [self.statusBtn setSelected:NO];
        
        // Setting values on teh .text was not removing the attributes
    } else {
        self.subtask.status = [NSNumber numberWithInt:1];
        self.dataTxt.attributedText = [AppUtilities getAtrbutedWith_FontStriked:self.subtask.data FontSize:self.fSize];
        
        [self.statusBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOn"]] forState:UIControlStateSelected];
        [self.statusBtn setSelected:YES];
    }
    [AppUtilities saveContext];
}

// To allow some to call and hide the keyboard
-(void) hideKeyboard {
    [self.hiddenTxt resignFirstResponder];
    [self.hiddenTxt setHidden:YES];
    [self.dataTxt setHidden:NO];
    [self.deleteSubtaskBtn setHidden:NO];
}

/* Detect double tap on the cell
 * Switch over to text mode
 * If value is changed send a notification to parent
 * Validate the change and save
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if(((UITouch *)[touches anyObject]).tapCount == 2)
    {
        self.hiddenTxt.text = self.dataTxt.text;
        [self.dataTxt setHidden:YES];
        [self.hiddenTxt setHidden:NO];
        [self.deleteSubtaskBtn setHidden:YES];
        [self.hiddenTxt becomeFirstResponder];
        
        // Notify the call that edit is on and when edit is of, it can ask this cell to disbale the keyboard
        [self.delegate notifyCellEditOnFor:self.subtask Cell:self];
    }
    else {
        self.hiddenTxt.text = @""; // set the hidden field to blank and no swapping of data is done here
        [self.hiddenTxt setHidden:YES];
        [self.dataTxt setHidden:NO];
        [self.deleteSubtaskBtn setHidden:NO];
        [self.hiddenTxt resignFirstResponder];
    }
    [super touchesEnded:touches withEvent:event];
}
// the txtfield delegate to cell object to recieve this event
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    NSString *newValue = [AppUtilities trim:textField.text];
    NSString *oldValue = self.dataTxt.text;
    
    if ([newValue length] > 0 && ![newValue isEqualToString:oldValue]) { //notify only if value is different and new value len >0
        self.subtask.data = newValue;
        [AppUtilities saveContext];
        
        [self.delegate notifyEditFinish:self.subtask NewValue:textField.text];
    }
    
    if ( textField == self.hiddenTxt) {
        [self.hiddenTxt resignFirstResponder];
        [self.hiddenTxt setHidden:YES];
        [self.dataTxt setHidden:NO];
        [self.deleteSubtaskBtn setHidden:NO];
    }
    
    return YES;
}



- (void)textFieldDidEndEditing:(UITextField *)textField;
{
    [textField resignFirstResponder];
}

- (IBAction)statusBtnClicked:(id)sender {
    [self toggleStatus];
}

- (IBAction)deleteBtnClicked:(id)sender {
    [CoreDataOperations deleteItemByEntityName:@"Subtask" Item:self.subtask];
}
@end
