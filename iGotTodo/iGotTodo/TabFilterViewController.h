//
//  FilterCriteriaViewController.h
//  realtodo
//
//  Created by Balbir Shokeen on 9/3/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FilterItemDetailsViewController.h"

// Single Or MultiSelect is same select behavior other than that one need to allow multiple selection.
typedef enum { View=1, Tab=2, ModelView=3 } FilterLaunchedFrom; //Added model view as clicking on apply was not taking away the view when launched form landscape mode.



@interface TabFilterViewController : UITableViewController 
                                            <FilterItemDetailsViewControllerDelegate>

@property (nonatomic) FilterLaunchedFrom filterLaunchedFrom;

@property (weak, nonatomic) IBOutlet UIButton *orderBy_btn;
@property (weak, nonatomic) IBOutlet UILabel *selectedCollapseOn;
@property (weak, nonatomic) IBOutlet UILabel *selectedDisplayType;
@property (weak, nonatomic) IBOutlet UILabel *selectedOrderBy;
@property (weak, nonatomic) IBOutlet UILabel *selectedTags;
@property (weak, nonatomic) IBOutlet UILabel *selectedGroups;
@property (weak, nonatomic) IBOutlet UISwitch *recentItemsOnlySwitch;

@property (nonatomic, weak) IBOutlet UIView *accessoryView;


- (IBAction)recentItemsBtnClick:(id)sender;

- (IBAction)setOrderDirection:(id)sender;
- (IBAction)rightNavItemResetBtnClick:(id)sender;
- (IBAction)bottomResetBtnClick:(id)sender;
- (IBAction)bottomSaveBtnClick:(id)sender;

- (void) setOrderBtnImage;

@end
