//
//  SelectCustomDaysViewController.m
//  realtodo
//
//  Created by Balbir Shokeen on 9/27/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "SelectCustomDaysViewController.h"
#import "AppSharedData.h"

@interface SelectCustomDaysViewController ()

@end

@implementation SelectCustomDaysViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.sectionHeaderHeight = 30;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    // LOCALIZATION SKIPPED - I think i am not using it now, old stray code
    NSArray *customDaysArray = [NSArray arrayWithObjects:@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", nil];
    
    cell.textLabel.text = [customDaysArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.textLabel.textColor = [UIColor lightGrayColor];
    
    if ([[AppSharedData getInstance].reminderCustomDaysArray indexOfObject:[NSString stringWithFormat:@"%d", indexPath.row +1]] < 7) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    for (int i=0; i < [[AppSharedData getInstance].reminderCustomDaysArray count]; i++) {
        
        
        
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    
    if ([[AppSharedData getInstance].reminderCustomDaysArray indexOfObject:[NSString stringWithFormat:@"%d", indexPath.row +1]] < 7) {
        [[AppSharedData getInstance].reminderCustomDaysArray removeObject:[NSString stringWithFormat:@"%d", (indexPath.row)+1]];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // These lines were commented to let the code compile but whenever we resume this feature we would need to pass these values to caller of this controller
//        [AppSharedData getInstance].remindInterval = REMIND_INTERVAL_CUSTOMDAYS;
//        [AppSharedData getInstance].remindOption = REMINDON_CUSTOMDAYS;
        [[AppSharedData getInstance].reminderCustomDaysArray addObject:[NSString stringWithFormat:@"%d", (indexPath.row)+1]];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

@end
