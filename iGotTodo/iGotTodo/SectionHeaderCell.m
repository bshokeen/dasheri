//
//  TaskGroupHeaderViewCell.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/11/09.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//

#import "SectionHeaderCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AppUtilities.h"

@implementation SectionHeaderCell

@synthesize collapseImg=_collapseImg, sectionIndexLbl = _sectionIndexLbl, sectionNameLbl = _sectionNameLbl, sectionCountBtn = _sectionCountBtn, sectionIsOpen=_sectionIsOpen, sectionIdx = _sectionIdx, sectionItemsCount = _sectionItemsCount, groupManagedObjectID = _groupManagedObjectID;

-(void) updateSectionViewData:(BOOL)isOpen Index:(NSInteger)sectionIndex Name:(NSString *)sectionName Count:(NSInteger)sectionItemsCount GrpMgdObjID:(NSString *)groupManagedObjectID Tag:(NSInteger) tagNumber Delegate:(id<SectionHeaderCellDelegate>)delegate {
    
    // Attributes
    self.sectionIsOpen = isOpen;
    self.sectionIdx = sectionIndex;
    self.sectionItemsCount = sectionItemsCount;
    self.groupManagedObjectID = groupManagedObjectID;
    self.tag = tagNumber;
    self.delegate = delegate; // To invoke the delegate methods and notify section open/close
    
    // Populate Values
    self.selected = self.sectionIsOpen;
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    self.selectedBackgroundView.backgroundColor = [UIColor colorWithHue:0.612 saturation:.04 brightness:.96 alpha:1.0];

    if (self.sectionIsOpen) {
        UIImage *img = [UIImage imageNamed:[AppUtilities getImageName:@"sectionOpen"]];
        [self.collapseImg setImage:img];
    }
    else {
        UIImage *img = [UIImage imageNamed:[AppUtilities getImageName:@"sectionClosed"]];
        [self.collapseImg setImage:img];
    }
    
    self.sectionIndexLbl.text = [[NSString stringWithFormat:@"%d", (self.sectionIdx + 1)] stringByAppendingString:@": "]; // Increasing section index by 1
    self.sectionNameLbl.text = [sectionName uppercaseString];
    [self.sectionCountBtn setTitle:[NSString stringWithFormat:@"%d", self.sectionItemsCount] forState:UIControlStateNormal];
    
    // Handle the line separators
    if (self.sectionIdx == 0) {
        self.groupTopLineLbl.hidden = YES;
    }
    else {
        self.groupTopLineLbl.hidden = NO;
    }
    
    if (self.sectionIsOpen) {
        self.groupBottomLineLbl.hidden = NO;
    }
    else {
        self.groupBottomLineLbl.hidden = YES;
    }
    
    // Initialization code
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOpen:)];
    [self addGestureRecognizer:tapGesture];
    
    // Set Gradience: it was not automatically expanding with orientation, so set up the highlight color and tint colors
//    [self setGradience];
}

+ (Class)layerClass {
    
    return [CAGradientLayer class];
}

- (CAGradientLayer *) getGradientLayer {
    return nil;
}

-(void) setGradience {
    // Set the colors for the gradient layer.
    static NSMutableArray *colors = nil;
    if (colors == nil) {
        colors = [[NSMutableArray alloc]initWithCapacity:3];
        UIColor *color = nil;
        
        color = [UIColor colorWithHue:0.56 saturation:0.10 brightness: 1 alpha:1.0];
        [colors addObject:(id)[color CGColor]];
        color = [UIColor colorWithHue:0.59 saturation:0.01 brightness: 1 alpha:1.0];        [colors addObject:(id)[color CGColor]];
        color = [UIColor colorWithHue:0.59 saturation:0.01 brightness: 1 alpha:1.0];        [colors addObject:(id)[color CGColor]];

        /*
         color = [UIColor colorWithHue:0.58 saturation:0.1 brightness: 1 alpha:1.0];
         [colors addObject:(id)[color CGColor]];
         color = [UIColor colorWithHue:0.60 saturation:0.30 brightness: 0.75 alpha:1.0];
         [colors addObject:(id)[color CGColor]];
         color = [UIColor colorWithHue:0.62 saturation:0.80 brightness: 0.75 alpha:1.0];
         [colors addObject:(id)[color CGColor]];
         */
    }
    

    
  CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
//    gradient.frame = CGRectMake(0,0,1024, 1024); //To get full screen tint color
    [gradient setColors:colors];
    [gradient setColors:[NSArray arrayWithObjects:(id)[[UIColor whiteColor]CGColor], (id)[[UIColor redColor]CGColor], nil]];
    [gradient setLocations:[NSArray arrayWithObjects: [NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.10], [NSNumber numberWithFloat:1.0], nil]];
    
//    [self.layer addSublayer:gradient];
//        [self.sectionCountBtn.layer addSublayer:gradient];
    [self.contentView.layer insertSublayer:gradient atIndex:0];
    
    
    UIBezierPath *linePath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0,self.bounds.size.width, 1)];
    
    //shape layer for the line
    CAShapeLayer *line = [CAShapeLayer layer];
    line.path = [linePath CGPath];
    line.fillColor = [[UIColor blackColor] CGColor];
    line.frame = CGRectMake(0, 1, self.contentView.frame.size.width,1);
    
    [self.contentView.layer addSublayer:line];
}

// Added to get a tap across the cell/section header
-(void) toggleOpen:(id) sender {
    [self toggleOpenWithUserAction:YES];
}

-(void) toggleOpenWithUserAction:(BOOL)userAction {
    //Toggle the section Image control state
    self.sectionIsOpen = !self.sectionIsOpen;
    
    // If this was a user action, send the delegate the appropriate message
    if (userAction && [self.delegate respondsToSelector:@selector(uitableSectionHeaderView:Section:Operation:)]) {
        if (self.sectionIsOpen) {
            [self.delegate uitableSectionHeaderView:self Section:self.sectionIdx Operation: SECTION_OPEN];
        }
        else {
            [self.delegate uitableSectionHeaderView:self Section:self.sectionIdx Operation: SECTION_CLOSE];
        }
    }
//    NSLog(@"Self: bounds: %f", self.bounds.size.height);
}

-(void) updateCountOnDelete:(int)recordsDeleted {
    self.sectionItemsCount = self.sectionItemsCount - recordsDeleted ;
    self.sectionCountBtn.titleLabel.text = [NSString stringWithFormat:@"%d", self.sectionItemsCount];
    DLog(@"updateCountOnDelete: Section: %@ Idx:%i ItemCount:%i", _sectionNameLbl.text, self.sectionIdx, self.sectionItemsCount);
}
@end

