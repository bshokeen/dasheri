//
//  DualSectionViewController.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/08.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "DualSectionViewController.h"
    #import "QuartzCore/QuartzCore.h"

@interface DualSectionViewController ()


@property BOOL searchON;
@property (nonatomic, weak)   AppSharedData  *appSharedData;
@end

@implementation DualSectionViewController


@synthesize uiSearchBar = _uiSearchBar, totalRowsLabel = _totalRowsLabel, totalRowsBtn = _totalRowsBtn;
@synthesize searchON = _searchON, appSharedData = _appSharedData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.thisInterfaceEntityName = @"DualSectionViewController";

    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: DualSection"];
    
    // Do any additional setup after loading the view.
    self.appSharedData = [AppSharedData getInstance];
    
    //    self.uiSearchBar.text=@"" ;
    self.uiSearchBar.delegate = self;
    self.uiSearchBar.showsCancelButton = NO;
    self.leftTable.tableHeaderView = self.uiSearchBar;
    self.leftTable.tableHeaderView = nil;// in this view hiding it as it was looking not so clean
    
    // Add the ability to touble tap and reset the filter
    UITapGestureRecognizer* tapNavTitleDoubleRecon = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(navigationBarTitleDoubleTap:)];
    
    tapNavTitleDoubleRecon.numberOfTapsRequired = 2;
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] setUserInteractionEnabled:YES];
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] addGestureRecognizer:tapNavTitleDoubleRecon];
    
    // Adding ability to perform swipe on task cell
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(oneFingerSwipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
    
    // Add border color
    self.leftTable.layer.borderWidth = 0.25;
    self.leftTable.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    // Additional configuration of dual section to
    self.helper.isDragBySwipeOutEnabled = YES; // only for this view as it makes it easy to use in this mode.
    
    DLog(@"Dynamic View is Loaded...");
}

- (void)oneFingerSwipeLeft:(UITapGestureRecognizer *)recognizer {
    //    ULog(@"swipe left"); // IT show a message when swiped on section header, so disabled
}

- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    /* This was working code but disabled as it was causing confusion. Swipe Out, Swipe to subtask etc. make it difficult to use here.
    // Insert your own code to handle swipe right
    // Get the point of swipe, find the indexpath and from that index path find the actual task. Task is updated and gets refreshed in tableview via the nsfetchresultcontroller changes
    
    CGPoint location = [recognizer locationInView:self.leftTable];
    NSIndexPath *swipedIndexPath = [self.leftTable indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [self.leftTable cellForRowAtIndexPath:swipedIndexPath];
    TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
    
    [AppUtilities markUnmarkTaskComplete: taskViewCell.task ];*/
}

- (void)navigationBarTitleSingleTap:(UIGestureRecognizer*)recognizer {
    
    // [self performSegueWithIdentifier:@"ShowFilterSettingsViewController" sender:self];
}

-(void) navigationBarTitleDoubleTap:(UIGestureRecognizer*)recognizer {
    
    if ( [[AppSharedData getInstance].dynamicViewData checkValuesSetToDefaults] )
    {
        [[AppSharedData getInstance].dynamicViewData resetToDefaultValues];
        self.selSegmentIdx = self.appSharedData.dynamicViewData.collapsibleOnIdx; // Reset the current segment as well othewise we would see the old entries in section name.
        [self forceReloadThisUI];
        
        [self processFilterIndications];
    }
}


-(void) processFilterIndications {
    BOOL filterApplied = [[AppSharedData getInstance].dynamicViewData checkValuesSetToDefaults];
    
    if (filterApplied) {
        
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithHue:HUE saturation:SATURATION brightness:BRIGHTNESS alpha:ALPHA]];
    }
    else {
        self.navigationController.navigationBar.tintColor = nil;
    }
    
    // Enable Disable Dragging to ensure dragging is supported only when data is not filtered,
    [self dragDisable:filterApplied]; // if filter is on disable dragging
}

// Enable disable dragging if filter is on/off. Dragging is supported only when data is not filtered and shown in the order of sort_order field
-(void) dragDisable:(BOOL) disabled {
    
    if (disabled) {
        self.helper.isSrcRearrangeable = NO;
    }
    else {
        self.helper.isSrcRearrangeable = YES;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    
    // Moved this setting to view will appear to ensure it is available before the UI comes up.
    // As dynamic view itself don't have the UISegment Control. Get the selectedsegment information on view appear
    
    self.selSegmentIdx = self.appSharedData.dynamicViewData.collapsibleOnIdx;
    
    // Apply filter indicators if required
    [self processFilterIndications];
    
    // Localized the Cancel Button as for this app custom language it was appearing in english
    [[UIButton appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    
    //Not working well.
    //    [self.tableView setContentOffset:CGPointMake(0, 44)]; // Hide search bar first
    
    // Ensure you call this to let the data be shown on first load
    [super viewWillAppear:animated];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    if (self.appSharedData.dynamicViewData.reloadUI) {
        self.appSharedData.dynamicViewData.reloadUI = NO;
        
        [self forceReloadThisUI];
        
        //       not required now self.appSharedData.dynamicViewData.collapsibleOnIdxPrevSelected = self.appSharedData.dynamicViewData.collapsibleOnIdx;
    }
    
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration{
    if(self.interfaceOrientation == UIDeviceOrientationPortrait || self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown){
        NSLog(@"UNHANDLED");
    } else  if(self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight){
        
        /* Present next run loop. Prevents "unbalanced VC display" warnings. */
//        double delayInSeconds = 0.1;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self performSegueWithIdentifier: @"ShowRearrangeViewController" sender: self];
//        });
    }
}
- (void) updateTotalRowsLabel {
    int count = [[self leftTableFetchedResultsController].fetchedObjects count];
    /* Older Value before changing to ios7 coloring
     self.totalRowsLabel.text = [@" Total Records         " stringByAppendingFormat:@"%i  ", count];
     */
    NSString *leftPart = NSLocalizedString(@"Total Records", @" Total Records");
    // Reduced the space in Total: & number to make it look together as outline is not there
    self.totalRowsLabel.text = [ @"" stringByAppendingFormat:@"%@:   %i", leftPart, count];
    [self.totalRowsBtn setTitle:[ @"" stringByAppendingFormat:@"%@:  %i", leftPart, count] forState:UIControlStateNormal];
    
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];;
    //    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    //    self.dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
}

- (void)viewDidUnload
{
    [self setTotalRowsView:nil];
    [super viewDidUnload];
    
    [self setUiSearchBar:nil];
    [self setTotalRowsLabel:nil];
    //[self setDateLabel:nil];
    
    DLog(@"Dynamic: View Unloaded");
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
     if (controller == self.leftTableFetchedResultsControllerObj) {
         [super controllerDidChangeContent:controller];
         
         [self updateTotalRowsLabel];
     }
}

#pragma mark Search Support
-(void) searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
}
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    //    ULog(@"BeginEditing");
    
    if (!self.uiSearchBar.showsCancelButton) {
        self.uiSearchBar.showsCancelButton = YES;
    }
}

- (void) searchBarTextDidEndEditing:(UITextField *)textField {
    //    if ( [textField.text length] == 0 ) {
    //        [self.uiSearchBar resignFirstResponder];
    //    }
    //        ULog(@"EndEditing");
}

// Hide the keyboard
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.uiSearchBar.text = @"";
    
    [AppSharedData getInstance].dynamicViewData.searchText = @"";
    
    [searchBar resignFirstResponder];
    
    self.uiSearchBar.showsCancelButton=NO;
    [self forceReloadThisUI];
    //  DLog(@"Search Bar Cancel clicked");
}

-(void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)Tosearch{
    
    NSString *searchText = [searchBar text];
    
    if ( [searchText length] == 0 ) {
        //        [searchBar resignFirstResponder];
        //        [self.view endEditing:YES];
    }
    
    [AppSharedData getInstance].dynamicViewData.searchText = searchText;
    
    [self forceReloadThisUI];
    
    //  DLog(@"Search Bar TEXT DID CHANGE");
}

#pragma mark - Action Implementation
- (IBAction)actOnSwipeGesture:(UISwipeGestureRecognizer *)sender {
    //    ULog(@"Abc"); // Swipe did not work for now, with
}

- (IBAction)leftNavBarItemClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"ShowTabFilterViewController" sender:self];
}

- (IBAction)totalRecordsBtnClick:(id)sender {
}

// Hide the Search Keyboard if appearing when switching over
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.uiSearchBar resignFirstResponder];
}


#pragma mark - DB Operations

- (void) fetchData_leftTable {
    
    [super fetchData_leftTable];
    
    [self updateTotalRowsLabel];
}

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}

#pragma mark - Fetch Result Controller
- (NSFetchedResultsController *)leftTableFetchedResultsController
{
    if (self.leftTableFetchedResultsControllerObj != nil) {
        return self.leftTableFetchedResultsControllerObj;
    }
    
    self.leftTableFetchedResultsControllerObj = [[AppSharedData getInstance].dynamicViewData genFetchReqCtrlFromSelConfig: APP_DELEGATE_MGDOBJCNTXT];
    self.leftTableFetchedResultsControllerObj.delegate = self;
    
    // Memory management.
    DLog(@"TabManageViewController: fetchResultsCtroller: RE-QUERY");
    
    return self.leftTableFetchedResultsControllerObj;
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
}
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if(self.interfaceOrientation == UIDeviceOrientationPortrait){
        
        //        NSLog(@"REARRANGE PORTRAIT-UNHANDLED");
        //         [[NSNotificationCenter defaultCenter] postNotificationName:@"RotateParent"object:nil];
        
        [self goBack];
        
    } else  if(self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight){
        
        //        NSLog(@"REARRANGE LANDS-UNHANDLED");
    }
    
}

//// Bug Fix: If the child views had orientation change this view was not aware. So setting the supported orientation
//- (NSUInteger)supportedInterfaceOrientations{
//    return UIInterfaceOrientationMaskLandscape;
//}

-(void) goBack {
    //    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    
    // Use together else results were not as expected, sequence of the 2 lines is also important
    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
}

// Show or hide the search bar as needed
#pragma mark - Swipe Gesture Recognizers
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    self.leftTable.tableHeaderView = self.uiSearchBar;

//    return;
    if (scrollView.contentOffset.y <= -40) {
        self.leftTable.tableHeaderView = self.uiSearchBar;
    }
    if (scrollView.contentOffset.y > 60 ) { //since scroll is needed in fiter mode, avoid hiding the keyboard unelss done really beyound 60
//        self.leftTable.tableHeaderView = nil; did not work nice so removing this
    }
}
- (IBAction)cancelBtnClick:(id)sender {
     [self goBack];
}

// Allow adding a new task from this screen
- (IBAction)addBtnClick:(id)sender {
    [self performSegueWithIdentifier:@"ShowAddTaskViewController" sender:self];
}

- (IBAction)filterBtnClick:(id)sender {
    [self performSegueWithIdentifier:@"ShowTabFilterViewController" sender:self];
}

@end
