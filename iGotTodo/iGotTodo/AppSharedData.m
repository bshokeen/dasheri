//
//  AppSharedData.m
//  iGotTodo
//
//  Created by Me on 01/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "AppSharedData.h"


@interface AppSharedData ()
// Holds the text to show on UI for a specific reminder interval type
@property (nonatomic, strong) NSMutableArray* remindRepeatIntervalTextLabel;

@end

@implementation AppSharedData

static AppSharedData *sharedDataModel = nil;

//@synthesize lastUsedGroup = _lastUsedGroup;
@synthesize lastSelectedGroup = _lastSelectedGroup;

// To set Reminder
@synthesize reminderCustomDaysArray = _reminderCustomDaysArray;

// For Effort UI
@synthesize effortUnitArray = _effortUnitArray;
@synthesize effortCountArray = _effortCountArray;
@synthesize lastSelEffortCount = _lastSelEffortCount;
@synthesize lastSelEffortUnit = _lastSelEffortUnit;
@synthesize defaultEffortUnitIdx = _defaultEffortUnitIdx;
@synthesize defaultEffortCountIdx = _defaultEffortCountIdx;

@synthesize lastSelGain = _lastSelGain;
@synthesize defaultGainIdx = _defaultGainIdx;


@synthesize remindRepeatIntervalTextLabel = _remindRepeatIntervalTextLabel;

// For AddTask UI
@synthesize lastSelectedTagsOnNewTask = _lastSelectedTags;

// DynamicViewController
@synthesize dynamicViewData = _dynamicViewData;

// Holding the settings for this application
@synthesize appSettings = _appSettings;

// Variables to hold data for additional languages supported by the app
@synthesize addnLangCodesInternalValArr = _addnLangCodesInternalValArr;
@synthesize addnLangSupportedUIValArr = _addnLangSupportedUIValArr;

-(id) init {
    self = [super init];
    
    self.dynamicViewData = [[DynamicViewData alloc]init];
    // Initializing based on the common values which might be needed by user
    _effortCountArray = [[NSMutableArray alloc] initWithObjects:
                         NSLocalizedString(@"1", @"1"),  NSLocalizedString(@"1.5", @"1.5"),
                         NSLocalizedString(@"2", @"2"), NSLocalizedString(@"3", @"3"), NSLocalizedString(@"4", @"4"), NSLocalizedString(@"5", @"5"), NSLocalizedString(@"6", @"6"), NSLocalizedString(@"7", @"7"),
                         NSLocalizedString(@"10", @"10"), NSLocalizedString(@"15", @"15"),  NSLocalizedString(@"25", @"25"), NSLocalizedString(@"30", @"30"), NSLocalizedString(@"45", @"45"),
                         nil];
    
    _effortUnitArray = [[NSMutableArray alloc] initWithObjects:
                        NSLocalizedString(@"Minutes", @"Minutes") , NSLocalizedString(@"Hours", @"Hours"),
                        NSLocalizedString(@"Days", @"Days"), NSLocalizedString(@"Months", @"Months"), NSLocalizedString(@"Years", @"Years"), nil];
    
    
    // Initialize one time the app settings before using
    self.appSettings = [[AppSettings alloc] init];
    // To reterive the data from the plist
    self.defaultEffortUnitIdx  = [self.appSettings defaultEffortUnitIdx];
    self.defaultEffortCountIdx = [self.appSettings defaultEffortCountIdx];
    self.defaultGainIdx        = [self.appSettings defaultGainIdx];

    
    _lastSelGain = self.defaultGainIdx;
    _lastSelEffortCount = self.defaultEffortCountIdx;
    _lastSelEffortUnit = self.defaultEffortUnitIdx;
    
    _reminderCustomDaysArray = [NSMutableArray arrayWithCapacity:7];
    _reminderCustomDaysArray = [NSMutableArray arrayWithObjects: nil];
    
    _remindRepeatIntervalTextLabel = [[NSMutableArray alloc] initWithObjects:
                                      REMIND_INTERVAL_SELECTED_DATE,
                 REMIND_INTERVAL_DAILY,
                 NSLocalizedString(@"Every:", @"Every:"),
                 NSLocalizedString(@"of Every Month At:", @"of Every Month At:"),
                 NSLocalizedString(@"Every Year On:", @"Every Year On:"),
                 NEVER, nil];
    
    // Not localized as not required
    _addnLangCodesInternalValArr = [[NSMutableArray alloc] initWithObjects:@"en", @"hi", @"af", nil];
    _addnLangSupportedUIValArr = [[NSMutableArray alloc] initWithObjects:@"Default", @"Hindi (हिंदी)", @"Afrikaans", @"English Only", nil];
    
    return self;
}

+ (AppSharedData *) getInstance
{
    
    @synchronized(self)
    {
        if (sharedDataModel == nil)
        {
            sharedDataModel = [[AppSharedData alloc] init];
        }
    }
    return sharedDataModel;
}


- (NSString *) uiTextFromCountIdx: (int) countIdx {
    return [self.effortCountArray objectAtIndex:countIdx];
}

- (NSString *) uiTextFromUnitIdx:  (int) unitIdx {
    return [self.effortUnitArray objectAtIndex:unitIdx];
}

//TODO: For ReminderView I can add an index return api for nscalendar unit, here.
- (NSString *) uiLabelFromNSCalendarUnit : (NSCalendarUnit) remindRepeatInterval {
    
    NSString *uiLabel = nil;
    
    if ( remindRepeatInterval == 0)
        uiLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:0];
    
    else if ( remindRepeatInterval == NSDayCalendarUnit)
        uiLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:1];
    
    else if (remindRepeatInterval == NSWeekCalendarUnit)
        uiLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:2];
    
    else if ( remindRepeatInterval == NSMonthCalendarUnit  )
        uiLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:3];
    
    else if ( remindRepeatInterval == NSYearCalendarUnit)
        uiLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:4];
    else {
        uiLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:5]; // This scenario is not in DB as of now.
    }
    
    return uiLabel;
}

- (int) getSegmentIdxForCalenderUnit: (NSCalendarUnit) nscuRepeatInterval {
    int selIdx = 0;
    if ( nscuRepeatInterval == 0)                           selIdx = 0; // Day
    else if (nscuRepeatInterval == NSDayCalendarUnit)       selIdx = 1; // Daily
    else if (nscuRepeatInterval == NSWeekCalendarUnit)      selIdx = 2; // WEEKLY
    else if (nscuRepeatInterval == NSMonthCalendarUnit)     selIdx = 3; // MONTHLY
    else if (nscuRepeatInterval == NSYearCalendarUnit)      selIdx = 4; // YEARLY
    else if (nscuRepeatInterval == NSEraCalendarUnit)       selIdx = 5; // NEVER -  Using Era for NEVER case.
    return selIdx;
}

- (NSCalendarUnit) getCalenderUnitForSegmentIndex: (int) selIdx {
    NSCalendarUnit nscu = 0;
    switch (selIdx) {
        case 0: { nscu = 0;                  break;} // Day
        case 1: { nscu = NSDayCalendarUnit;  break;} // Daily
        case 2: { nscu = NSWeekCalendarUnit; break;} // WEEKLY
        case 3: { nscu = NSMonthCalendarUnit;break;} // MONTHLY
        case 4: { nscu = NSYearCalendarUnit; break;} // YEARLY
        case 5: { nscu = NSEraCalendarUnit;  break;} // NEVER
        default:{                            break;}}
    return nscu;
}

- (NSString *) getRemindOnLabelTextFromSelSegIdx: (int) selIdx RemindDate: (NSDate *) remindOnDate {
    NSString *retLabelText = nil;
    switch (selIdx) {
        case 0: {   // Day
            retLabelText = [NSString stringWithFormat:@"%@", [AppUtilities uiFormatedDate:remindOnDate]];
            break;
        }
        case 1: {   // Daily
            NSString *uiTextLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:1];
            retLabelText = [NSString stringWithFormat:@"%@ %@", uiTextLabel, [AppUtilities uiFormatedTime:remindOnDate]];
            break;
        }
        case 2: {   // WEEKLY
            NSString *uiTextLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:2];
            NSString *dayName = [AppUtilities dayNameFromDate:remindOnDate];
            NSString *timeText= [AppUtilities uiFormatedTime:remindOnDate];
            retLabelText = [NSString stringWithFormat:@"%@ %@ %@", uiTextLabel, dayName, timeText];
            break;
        }
        case 3: {   // MONTHLY
            NSString *uiTextLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:3];
            NSInteger day = [AppUtilities dayFromDate:remindOnDate];
            NSString *timeText= [AppUtilities uiFormatedTime:remindOnDate];
            retLabelText = [NSString stringWithFormat:@"%d %@ %@", day, uiTextLabel, timeText];
            break;
        }
        case 4: {   // YEARLY
            NSString *uiTextLabel = [self.remindRepeatIntervalTextLabel objectAtIndex:4];
            NSInteger day = [AppUtilities dayFromDate:remindOnDate];
            NSString *monthName = [AppUtilities monthNameFromDate:remindOnDate];
            NSString *timeText= [AppUtilities uiFormatedTime:remindOnDate];
            retLabelText = [NSString stringWithFormat:@"%@ %d-%@ %@", uiTextLabel, day, monthName, timeText];
            break;
        }
        case 5: {   // NEVER
            retLabelText = NEVER;
            break;
        }
        default:
            break;
    }
    
    return retLabelText;
}
- (NSString *) getRemindOnLabelTextFromCalendarUnit: (NSCalendarUnit) nscuRepeatInterval RemindDate: (NSDate *) remindOnDate {
    
    int selIdx = [self getSegmentIdxForCalenderUnit:nscuRepeatInterval];
    
    return     [self getRemindOnLabelTextFromSelSegIdx:selIdx RemindDate:remindOnDate];
}


@end
