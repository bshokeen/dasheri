//
//  AlertViewController.m
//  iGotTodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TabAlertViewController.h"
#import "AppDelegate.h" // Included to get the manged object context macro

@interface TabAlertViewController ()

    // Added this flag to help highlight the recently updated/added, which is assumed to be the top item as the table is sorted based on the notification time in descending. if we don't add this flag and simply try to set the first cell selected, in first load of this UI that was not happening, so had to put this flag, wait for the reload of this UI and then based on flag enable selection.
    @property (nonatomic) BOOL selectRecentlyAddedOrUpdatedItem;
    @property (nonatomic) double delayInSeconds;

// This is a temp var, set in the child when gesture is swiped and then used in the segue defined in this class to process data on this item
@property (weak, nonatomic) Task *passToSegueSwipedOnIdxPath;

@end

@implementation TabAlertViewController

@synthesize selectRecentlyAddedOrUpdatedItem = _selectRecentlyAddedOrUpdatedItem;
@synthesize delayInSeconds = _delayInSeconds;

@synthesize fetchedResultsControllerObj = _fetchedResultsControllerObj;
//@synthesize mgdObjContext = _mgdObjContext;

@synthesize passToSegueSwipedOnIdxPath = _passToSegueSwipedOnIdxPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    
    NSLog(@"Force Reloading UI");
    // Load the data on first run
    [self forceReloadThisUI];
    
//    int badgeCount = _fetchedResultsControllerObj.fetchedObjects.count;
    // TODO: Option to cut down a DB call here. Use teh badgeCount from fetchresults
    [AppUtilities updateAlertBadge:2];
    
        NSLog(@"Force Reloading UI - Finished");
   }

- (void) viewDidAppear:(BOOL)animated {
    
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
        [self forceReloadThisUI];
    }
    
    // After view is finished loading, check if the last selected item require higlighting
    if (self.selectRecentlyAddedOrUpdatedItem) {
        // Reset the flag
        self.selectRecentlyAddedOrUpdatedItem = NO;
        
        // Get the first/top item and mark selected
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell setSelected:YES animated:YES]; // Highlight cell
        
        // Reset the selection after delay time
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, self.delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [cell setSelected:NO animated:YES];
            });
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: Alert"];
    
    self.delayInSeconds = 2.0; // initialize the delay interval after which selected item has to be deselected
    
    // Register swipe
    [self registerSwipe];
}

-(void) registerSwipe{
    
    // Adding ability to perform swipe on task cell
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(oneFingerSwipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
}

- (void)oneFingerSwipeLeft:(UITapGestureRecognizer *)recognizer {
    //    ULog(@"swipe left"); // IT show a message when swiped on section header, so disabled
}

- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    /* DISABLING IT AS IT IS NOT SO USEFUL HERE and rather taking user to sublist view
     // Insert your own code to handle swipe right
     // Get the point of swipe, find the indexpath and from that index path find the actual task. Task is updated and gets refreshed in tableview via the nsfetchresultcontroller changes
     
     CGPoint location = [recognizer locationInView:self.leftTable];
     NSIndexPath *swipedIndexPath = [self.leftTable indexPathForRowAtPoint:location];
     UITableViewCell *swipedCell  = [self.leftTable cellForRowAtIndexPath:swipedIndexPath];
     TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
     
     [AppUtilities markUnmarkTaskComplete: taskViewCell.task ];*/
    
     /* Disabled all this aslo as swiping ot go to next view was not convienent and intiuitive, so made an area to tap and do this segue
    CGPoint location = [recognizer locationInView:self.tableView];
    NSIndexPath *swipedIndexPath = [self.tableView indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [self.tableView cellForRowAtIndexPath:swipedIndexPath];
    TaskViewCell *taskViewCell = (TaskViewCell *) swipedCell;
    //Now cache the task identified for passing to segue api
    self.passToSegueSwipedOnIdxPath = taskViewCell.task;
    
//    NSLog(@"Debug: Task:%@ Cell:%@ X:%f Y:%f", taskViewCell.task.name, [taskViewCell description], location.x, location.y );
    if (self.passToSegueSwipedOnIdxPath) { //Bug Fix: if swipe is unable to produce desired task, section closed may be, do nothing)
        [self performSegueWithIdentifier:@"ShowSubtasksViewController" sender:self];
    }*/
}


- (void)viewDidUnload
{
    // Release any properties that are loaded in viewDidLoad or can be recreated lazily.
    self.fetchedResultsControllerObj = nil;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = LANDSCAPE_WIDTH_FACTOR;
    else {
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = 0;
    }
    [self forceReloadThisUI];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return IS_PAD || (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    return YES;
}


#pragma mark Table view data source methods
/*
 The data source methods are handled primarily by the fetch results controller
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
   /* TODO: In some cases alerts where there but no Clear All button, so trying to disable this & see how it works, temporarily
    if ([sectionInfo numberOfObjects] == 0) {
        self.navigationItem.rightBarButtonItem = nil;
    }*/
    return [sectionInfo numberOfObjects];
}

// Customize the appearance of table view cells.

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    // Configure the cell to show the Tasks's details
    TaskViewCell *mvaCell = (TaskViewCell *) cell;
    [mvaCell resetInitialText];
    
    Task *activityAtIndex =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    [AppUtilities configureCell:mvaCell Task:activityAtIndex];
    //TODO: Make is a sinlge api call with congirecell in TaskViewCell itself. Bad code,  but adding to set the delgate for notifications back
    mvaCell.delegate = self;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"TaskViewCell";
    
    TaskViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    /*  // This is not being invoked in my case but still retained as code here.
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (TaskViewCell *) [nib objectAtIndex:0];
     }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma mark Table view editing

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Instead of deleting the task, clear up the alert on it.
        Task *task = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        switch ([task.remind_repeat_interval intValue]) {
            case 0: {
                task.remind_on = nil;
                task.alert_notification = 0;
                task.alert_notification_on = nil;
                break;
            }
            case NSDayCalendarUnit:
            case NSWeekCalendarUnit:
            case NSMonthCalendarUnit:
            case NSYearCalendarUnit: 
            case NSEraCalendarUnit:{
                // In these cases we reseting the notification to prevent them for appearing again. Code is by default excluding them to show on this UI as pending alerts as their date will always be of past. We can do a query of all notifications for this app and query to show those records.
                task.alert_notification = 0;
                task.alert_notification = nil;
                break;
            }
            default:
                DLog(@"AlertVIewController: ERROR - Should not have come here");
                break;
        }
        
        [AppUtilities saveTask:task];
        
        //    int badgeCount = _fetchedResultsControllerObj.fetchedObjects.count;
        // TODO: Option to cut down a DB call here. Use teh badgeCount from fetchresults
        [AppUtilities updateAlertBadge:2];
        
        DLog(@"AlertViewController: commitEditingStyle: Finished Delete");
        
    } 
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return NSLocalizedString(@"Clear", @"Clear");
}

#pragma mark Fetched results controller

/*
 NSFetchedResultsController delegate methods to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    UITableView *tableView = self.tableView;
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        ALog(@"AlertViewController: Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI 
{    
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // TODO: We need to use push notifications instead of local to avoid missing notifications sent on teh lock screen. For now using the remind_on field to do so.
    // TODO: To avoid repeated fetch of recourring intervals fetching only repeat interval = 0. it is a workaround for now. Push notification to fix this.
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(alert_notification == 1) || (remind_on <= %@ and remind_repeat_interval == 0) ", [NSDate date]];
    [fetchRequest setPredicate:filterPredicate];
    
    
    // Create the sort descriptors array.
    NSSortDescriptor *grpSortOrderDescriptor = [[NSSortDescriptor alloc]initWithKey:@"alert_notification_on" ascending:NO];
    
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:
                                grpSortOrderDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Create and initialize the fetch results controller.
    self.fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                           managedObjectContext:APP_DELEGATE_MGDOBJCNTXT 
                                                                             sectionNameKeyPath:nil
                                                                                      cacheName:nil];
    self.fetchedResultsControllerObj.delegate = self;
    
    DLog(@"AlertViewController: getFetchResultsCtroller: RE - QUERY");
    // Memory management.
    return self.fetchedResultsControllerObj;
}  


#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowUpdateTaskViewController"] )
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Task *task = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        UpdateTaskViewController *updateTaskViewController = [segue destinationViewController];
        updateTaskViewController.delegate = self;
        updateTaskViewController.task = task;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddTaskViewController"])
    {
        AddTaskViewController *addTaskViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        
        addTaskViewController.managedObjectContext = APP_DELEGATE_MGDOBJCNTXT;
    }
    else if ([[segue identifier] isEqualToString:@"ShowSubtasksViewController"])  {
        
        if (self.passToSegueSwipedOnIdxPath) { // if swipe is unable to produce desired task, section closed may be, do nothing)
            SubtasksViewController *nlVC = (SubtasksViewController *) [segue destinationViewController];
            nlVC.task = self.passToSegueSwipedOnIdxPath;
        }
        
    }
}

#pragma mark - Add/Update controller delegate
- (void) updateTaskViewControllerDidCancel:(UpdateTaskViewController *)controler {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) updateTaskViewControllerDidFinish:(UpdateTaskViewController *)controller task:(Task *)task WithDelete:(BOOL)isDeleted
{
    if (task) {
        if (isDeleted) {
            [AppUtilities deleteTask:task];
        }
        else {
            [AppUtilities saveTask:task];
        }
    }

//    [self.navigationController popViewControllerAnimated:YES];
    
    [self forceReloadThisUI];
}

-(void) selectTopItemNotifiedRecently {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    // If the UI is not loaded cell could be null and mark it for highlight after loading is done
    if (cell == nil) {
        // UI is not loaded yet and hence mark it to be highlighted after the UI finishes loading.
        self.selectRecentlyAddedOrUpdatedItem = YES;
    }
    else {  // cell is available so highlight it and disable highlight after a time interval
        [cell setSelected:YES animated:YES];
        
        // Deselect after a time interval
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, self.delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [cell setSelected:NO animated:YES];
        });
        
        // This API was successful in doing the highlight directly so no need to do it after the loading.
        self.selectRecentlyAddedOrUpdatedItem = NO;
    }
}

#pragma mark - Action
- (IBAction)clearBtnClick:(id)sender {
    
    [AppUtilities clearAllAlertNotification];
}

#pragma mark - TaskViewCell Delegate
-(void) notifyShowSubtasksClicked:(Task *)task Cell:(TaskViewCell *)cell {
    //Now cache the task identified for passing to segue api
    self.passToSegueSwipedOnIdxPath = task;
    
    //    NSLog(@"Debug: Task:%@ Cell:%@ X:%f Y:%f", taskViewCell.task.name, [taskViewCell description], location.x, location.y );
    if (self.passToSegueSwipedOnIdxPath) { //Bug Fix: if swipe is unable to produce desired task, section closed may be, do nothing)
        [self performSegueWithIdentifier:@"ShowSubtasksViewController" sender:self];
    }
}

@end
