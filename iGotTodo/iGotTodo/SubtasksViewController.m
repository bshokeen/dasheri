//
//  SubtaskViewController.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/24.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "SubtasksViewController.h"
#import "AppUtilities.h"
#import "AppDelegate.h"

#define STD_CELL_HEIGHT 38.0 //reduced table height from 44 to 38, pulled text view to -4, saw the sizing of adding the delta on top of fontbased height to reasonable size

@interface SubtasksViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSString *origTitle; // To allow showing the title of the caller or the original title

@property (nonatomic) int countReturnPress;

// To disable the keyboard editing in a cell, if tap is done on another cell.
@property (nonatomic, strong) SubtaskCell *cellUnderEditing;

//Keyboard show/hide, just ocpied did not attempt inside http://stackoverflow.com/questions/13845426/generic-uitableview-keyboard-resizing-algorithm
@property BOOL keyboardShown;
@property CGFloat keyboardOverlap;

@end

@implementation SubtasksViewController

@synthesize fetchedResultsController=_fetchedResultsController, origTitle = _origTitle, cellUnderEditing = _cellUnderEditing, countReturnPress = _countReturnPress;
@synthesize keyboardOverlap = _keyboardOverlap, keyboardShown= _keyboardShown;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Google Custom
    [AppUtilities logAnalytics:@"View Open: Notes New"];
    
    // Register
    [self registerSwipeRight];
    [self registerSwipeLeft];
    
    // Trying using only the fetchdata call
    // Load the data on first run
//    [self forceReloadThisUI];
    [self fetchData];
    
    [self registerKeyboardShowHide];
    
    // If there aren't any subtasks, activate the keyboard to allow entering right away
    if ( [self.task.subtaskS count] <= 0 )
        [self.addTxt becomeFirstResponder];

}

-(void) registerTapOutside {
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCellKeyboard)];
    [self.leftTable addGestureRecognizer:gestureRecognizer];
}

-(void) hideCellKeyboard {
     [self.cellUnderEditing hideKeyboard ];
}

-(void) registerSwipeRight {
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
    
}

-(void) registerSwipeLeft {
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
}


- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle swipe right
    // Get the point of swipe, find the indexpath and from that index path find the actual task. Task is updated and gets refreshed in tableview via the nsfetchresultcontroller changes
    
    CGPoint location = [recognizer locationInView:self.leftTable];
    NSIndexPath *swipedIndexPath = [self.leftTable indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [self.leftTable cellForRowAtIndexPath:swipedIndexPath];
    SubtaskCell *noteCell = (SubtaskCell *) swipedCell;
    
    [noteCell toggleStatus];
}


// Do the keyboard adjustments
-(void) registerKeyboardShowHide {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    if(self.keyboardShown)
        return;
    
    self.keyboardShown = YES;
    
    // Get the keyboard size
    UIScrollView *tableView;
    if([self.leftTable.superview isKindOfClass:[UIScrollView class]])
        tableView = (UIScrollView *)self.leftTable.superview;
    else
        tableView = self.leftTable;
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [tableView.superview convertRect:[aValue CGRectValue] fromView:nil];
    
    // Get the keyboard's animation details
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    
    // Determine how much overlap exists between tableView and the keyboard
    CGRect tableFrame = tableView.frame;
    CGFloat tableLowerYCoord = tableFrame.origin.y + tableFrame.size.height;
    self.keyboardOverlap = tableLowerYCoord - keyboardRect.origin.y;
    if(self.inputAccessoryView && self.keyboardOverlap>0)
    {
        CGFloat accessoryHeight = self.inputAccessoryView.frame.size.height;
        self.keyboardOverlap -= accessoryHeight;
        
        tableView.contentInset = UIEdgeInsetsMake(0, 0, accessoryHeight, 0);
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, accessoryHeight, 0);
    }
    
    if(self.keyboardOverlap < 0)
        self.keyboardOverlap = 0;
    
    if(self.keyboardOverlap != 0)
    {
        tableFrame.size.height -= self.keyboardOverlap;
        
        NSTimeInterval delay = 0;
        if(keyboardRect.size.height)
        {
            delay = (1 - self.keyboardOverlap/keyboardRect.size.height)*animationDuration;
            animationDuration = animationDuration * self.keyboardOverlap/keyboardRect.size.height;
        }
        
        [UIView animateWithDuration:animationDuration delay:delay
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{ tableView.frame = tableFrame; }
                         completion:^(BOOL finished){ [self tableAnimationEnded:nil finished:nil contextInfo:nil]; }];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    if(!self.keyboardShown)
        return;
    
    self.keyboardShown = NO;
    
    UIScrollView *tableView;
    if([self.leftTable.superview isKindOfClass:[UIScrollView class]])
        tableView = (UIScrollView *)self.leftTable.superview;
    else
        tableView = self.leftTable;
    if(self.inputAccessoryView)
    {
        tableView.contentInset = UIEdgeInsetsZero;
        tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
    
    if(self.keyboardOverlap == 0)
        return;
    
    // Get the size & animation details of the keyboard
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [tableView.superview convertRect:[aValue CGRectValue] fromView:nil];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    
    CGRect tableFrame = tableView.frame;
    tableFrame.size.height += self.keyboardOverlap;
    
    if(keyboardRect.size.height)
        animationDuration = animationDuration * self.keyboardOverlap/keyboardRect.size.height;
    
    [UIView animateWithDuration:animationDuration delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{ tableView.frame = tableFrame; }
                     completion:nil];
}

- (void) tableAnimationEnded:(NSString*)animationID finished:(NSNumber *)finished contextInfo:(void *)context
{
    //    // Scroll to the active cell
    //    if(self.activeCellIndexPath)
    //    {
    //        [self.leftTable scrollToRowAtIndexPath:self.activeCellIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    //        [self.leftTable selectRowAtIndexPath:self.activeCellIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    //    }
}


// Launching add view on swipe down to activate/deactive entry of new note
#pragma mark - Swipe Gesture Recognizers
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.contentOffset.y <= -30) {
        [self.addTxt becomeFirstResponder];
    }
    if (scrollView.contentOffset.y > 30 ) { 
        [self.addTxt setText:@""]; //blank the value to avoid searching
        [self.addTxt resignFirstResponder];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source methods
/*
 The data source methods are handled primarily by the fetch results controller
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

// Customize the appearance of table view cells.

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Subtask *subtask =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    // Configure the cell to show the Tasks's details
    SubtaskCell *noteCell = (SubtaskCell *) cell;
    [noteCell resetInitialText];
    [noteCell initializeWith:subtask Delegate:self FontSize:12]; // had to pass font from caller
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"SubtaskCell";
    
    SubtaskCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    /*  // This is not being invoked in my case but still retained as code here.
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (TaskViewCell *) [nib objectAtIndex:0];
     }*/
    
    // Configure the cell to show the Tasks's details
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Subtask *subtask =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    NSString *txt = subtask.data;
    
    CGSize size = [txt sizeWithFont:[UIFont fontWithName:@"Verdana" size:12] constrainedToSize:CGSizeMake(260, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];

    CGFloat height = size.height + 12;
    if (height < STD_CELL_HEIGHT) {
        height = STD_CELL_HEIGHT;
    }
    else {
        height = height;
    }
        NSLog(@"TESETING: FONT size: %f : new:%f", size.height, height);
    return height;
    /*
    
    Note *note =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    NSString *txt = note.data;
    
    NotesCell *noteCell = [(NotesCell *)[NotesCell alloc] init];
    UIFont *cellFont =     noteCell.dataTxt.font;
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    CGSize size = [txt sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    return size.height + 5;*/
}

// Set the highligth color same as used by the application across
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIView *selectedView = [[UIView alloc]init];
    //    selectedView.backgroundColor = [UIColor colorWithHue:0.612 saturation:.04 brightness:.96 alpha:1.0];
    selectedView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = selectedView;
    cell.textLabel.highlightedTextColor    = [UIColor clearColor];// [AppUtilities getSelectedTextHighlightColor];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma mark Table view editing
// To disable the swipe delet ebutton and use the swipetoleft gesutre configured in the load
/*
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    // return YES;
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        DLog(@"SortViewController: commitEditingStyle: Starting Delete");
        [AppUtilities deleteTask:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}*/


 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
     SubtaskCell *cell = (SubtaskCell *) [self.leftTable cellForRowAtIndexPath:indexPath];
     
     if ( self.cellUnderEditing && cell != self.cellUnderEditing) {
         [self.cellUnderEditing hideKeyboard ];
     }
     
     // If keyboar is there hide it
     [self.addTxt resignFirstResponder];
     
     // Disable cell highlight as we don't have any operation on it.
//     [cell setSelected:NO];
     
// NSLog(@"MasterViewCOntroller: didSelectRowAtIndexPatch: I wanted to see if it is called");
 }



#pragma mark Fetched results controller
//
//-(id) initWithMgdObj: (NSManagedObjectContext *)mgdobjctx {
//    DLog(@"SortViewController: MgdObj from CalendarVC: %@", mgdobjctx);
//    self.managedObjectContext = mgdobjctx;
//
//    int count = [self fetchedResultsController].fetchedObjects.count;
//    DLog(@"SortViewController: %d", count);
//    return self;
//}


/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Subtask" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];

    // Create the sort descriptors array.
    NSString *sortKeyName   = @"created_on";
    BOOL sortOrderAscending = NO;
    NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKeyName ascending:sortOrderAscending];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dueOnDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    // Using array to generated and of predicates
    NSMutableArray *predicates = [[NSMutableArray alloc] init ];
    if (self.task) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"task == %@", self.task]];
    }

    // Compound Predicates as AND Conditions
    if ([predicates count] > 0 ) {
        NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        [fetchRequest setPredicate:p];
    }
    
    // Create and initialize the fetch results controller.
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:APP_DELEGATE_MGDOBJCNTXT
                                                                      sectionNameKeyPath:nil
                                                                               cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    NSLog(@"Test: %i", [[_fetchedResultsController fetchedObjects] count]);
    // Memory management.
    return _fetchedResultsController;
    
}

/*
 NSFetchedResultsController delegate methods to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.leftTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.leftTable;
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.leftTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.leftTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.leftTable endUpdates];
}

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        NSLog(@"SortViewController: Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI
{
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsController = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.leftTable reloadData];
}

#pragma TextField Delegate Handling
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    
    if ( textField == self.addTxt) {
        BOOL inserted = [self insertNewSubtask:textField.text];
        textField.text = @"";
        
        if (!inserted) {
            [textField resignFirstResponder];
        }
    }
    
    return YES;
}

-(BOOL) insertNewSubtask: (NSString *) noteInputValue
{
    if (!noteInputValue) {
        return NO;
    }
    
    noteInputValue = [AppUtilities trim:noteInputValue];
    if (noteInputValue.length > 0) {
            
            [AppUtilities addSubtaskForTask:self.task Data:self.addTxt.text Status:0 SortOrder:0 Type:0];
            [AppUtilities saveContext];
        return YES;
        }
    else {
        return NO;
    }

}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
    if ( [[segue identifier] isEqualToString:@"ShowUpdateTaskViewController"] )
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Task *task = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        UpdateTaskViewController *updateTaskViewController = [segue destinationViewController];
        updateTaskViewController.delegate = self;
        updateTaskViewController.task = task;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddTaskViewController"])
    {
        AddTaskViewController *addTaskViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        
        addTaskViewController.managedObjectContext = APP_DELEGATE_MGDOBJCNTXT;
    }*/
}

#pragma mark - SubtaskEditFinish
-(void) notifyEditFinish:(NSObject *)cellObject NewValue:(NSString *)newValue {
  
}
-(void) notifyCellEditOnFor:(Subtask *)subtask Cell:(SubtaskCell *)subtasksCell {
    self.cellUnderEditing = subtasksCell;
}

- (IBAction)addSubtaskBtnClick:(id)sender {
    
    BOOL inserted = [self insertNewSubtask:self.addTxt.text];
    self.addTxt.text = @"";
    
    if (!inserted)
        [self.addTxt resignFirstResponder];
}

@end
