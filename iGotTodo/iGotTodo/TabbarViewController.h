//
//  TabbarViewController.h
//  iGotTodo
//
//  Created by Me on 15/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>

// Adding the delegate to get the event which saves the order
@interface TabbarViewController : UITabBarController <UITabBarControllerDelegate>

@end
