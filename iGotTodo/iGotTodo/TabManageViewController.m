//
//  DynamicViewController.m
//  iGotTodo
//
//  Created by Me on 12/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabManageViewController.h"
#import "AppDelegate.h" // Included to get the manged object context macro

@interface TabManageViewController ()

@property BOOL searchON;
@property (nonatomic, weak)   AppSharedData  *appSharedData;

//Keyboard show/hide, just ocpied did not attempt inside http://stackoverflow.com/questions/13845426/generic-uitableview-keyboard-resizing-algorithm
@property BOOL keyboardShown;
@property CGFloat keyboardOverlap;


@end



@implementation TabManageViewController


@synthesize uiSearchBar = _uiSearchBar;
@synthesize totalRowsLabel = _totalRowsLabel;
@synthesize totalRowsBtn = _totalRowsBtn;
//@synthesize dateLabel = _dateLabel;

@synthesize searchON = _searchON;
@synthesize appSharedData = _appSharedData;
@synthesize keyboardOverlap = _keyboardOverlap, keyboardShown= _keyboardShown;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.thisInterfaceEntityName = @"TabManageViewController";
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: Manage"];
    
    // Do any additional setup after loading the view.
    self.appSharedData = [AppSharedData getInstance];
    
    //    self.uiSearchBar.text=@"" ;
    self.uiSearchBar.delegate = self;
     self.uiSearchBar.showsCancelButton = NO;
        self.leftTable.tableHeaderView = self.uiSearchBar;
    //Not working well. Search show hide is working on landscape, was somehow able to make it work here with scroll api and setcontent. but prefer to keep the search always on as it making easy to use it
//    [self.tableView setContentOffset:CGPointMake(0, 44)]; // Hide search bar first on load only as tabel having issue in willappear
    
    /* Older Coloring Scheme
self.totalRowsLabel.layer.backgroundColor = [UIColor blueColor].CGColor;
     self.totalRowsLabel.layer.cornerRadius = 10.0;
     self.totalRowsLabel.layer.borderColor = [UIColor blackColor].CGColor;
     self.totalRowsLabel.layer.borderWidth = 0.5;
     */

/* New coloring scheme
    self.totalRowsLabel.textColor = [UIColor blueColor];
    self.totalRowsLabel.layer.backgroundColor = [UIColor clearColor].CGColor;
    self.totalRowsLabel.layer.cornerRadius = 10.0;
    self.totalRowsLabel.layer.borderColor = [UIColor blueColor].CGColor;
    self.totalRowsLabel.layer.borderWidth = 1;
  */
    // Label is Wrapped in View first 
// Remove the border color
    //    self.totalRowsLabel.textColor = [UIColor blueColor];
//    self.totalRowsLabel.layer.borderColor = [UIColor redColor].CGColor;
//    self.totalRowsLabel.layer.borderWidth = 1;
//    self.totalRowsView.layer.backgroundColor = [UIColor clearColor].CGColor;
    /* Disabling outline from iOS7
    self.totalRowsView.layer.cornerRadius = 10.0;
    self.totalRowsView.layer.borderColor = [UIColor blueColor].CGColor;
    self.totalRowsView.layer.borderWidth = 1;
*/


    // Add the ability to touble tap and reset the filter
    UITapGestureRecognizer* tapNavTitleDoubleRecon = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(navigationBarTitleDoubleTap:)];
    
    tapNavTitleDoubleRecon.numberOfTapsRequired = 2;
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] setUserInteractionEnabled:YES];
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] addGestureRecognizer:tapNavTitleDoubleRecon];
    

    
    // Load Drag N Drop Feature
    [self registerDragNDrop];

    // Register table view dynamic size change on Keyboard show/hide
    [self registerKeyboardShowHide];
    
    DLog(@"Dynamic View is Loaded...");
}

#pragma mark Drag N Drop API
-(void) registerDragNDrop {
    //    [self.leftTable registerClass:[UITableViewCell class] forCellReuseIdentifier:tableViewReusableCell];
    
    //    [self.rightCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:tableViewReusableCell];
    
    /* Configure the helper */
    
    self.helper = [[DragHelper alloc] initWithSuperview:self.view
                                                srcView:self.leftTable
                                                dstView:self.dummy // HACK setting source as dest to avoid nil check in Draghelper
                                        SwipeOutEnabled:NO]; // Disable swipe out on main UI to keep it easy for scrolling
    
    self.helper.delegate = self;
    
    /* The source is rearrangeable or exchangeable with the dest but dest is not rearrangeable */
    self.helper.isSrcRearrangeable = YES;
    self.helper.doesSrcRecieveDst = YES;
    self.helper.hideSrcDraggingCell = YES;
    
    self.helper.isDstRearrangeable = NO;
    self.helper.doesDstRecieveSrc = NO;
    self.helper.hideDstDraggingCell = YES;
}

// Enable disable dragging if filter is on/off. Dragging is supported only when data is not filtered and shown in the order of sort_order field
-(void) dragDisable:(BOOL) disabled {

    if (disabled) {
        self.helper.isSrcRearrangeable = NO;
    }
    else {
        self.helper.isSrcRearrangeable = YES;
    }
}



- (void)navigationBarTitleSingleTap:(UIGestureRecognizer*)recognizer {
    
    // [self performSegueWithIdentifier:@"ShowFilterSettingsViewController" sender:self];
}

-(void) navigationBarTitleDoubleTap:(UIGestureRecognizer*)recognizer {
    
    if ( [[AppSharedData getInstance].dynamicViewData checkValuesSetToDefaults] )
    {
        [[AppSharedData getInstance].dynamicViewData resetToDefaultValues];
        self.selSegmentIdx = self.appSharedData.dynamicViewData.collapsibleOnIdx; // Reset the current segment as well othewise we would see the old entries in section name.
        [self forceReloadThisUI];
        
        [self processFilterIndications];
    }
}

-(void) processFilterIndications {
    BOOL filterApplied = [[AppSharedData getInstance].dynamicViewData checkValuesSetToDefaults];
    
    if (filterApplied) {
        
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithHue:HUE saturation:SATURATION brightness:BRIGHTNESS alpha:ALPHA]];
    }
    else {
        self.navigationController.navigationBar.tintColor = nil;
    }
    
    // Enable Disable Dragging to ensure dragging is supported only when data is not filtered,
    [self dragDisable:filterApplied]; // if filter is on disable dragging
}

// Do the keyboard adjustments
-(void) registerKeyboardShowHide {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    if(self.keyboardShown)
        return;
    
    self.keyboardShown = YES;
    
    // Get the keyboard size
    UIScrollView *tableView;
    if([self.leftTable.superview isKindOfClass:[UIScrollView class]])
        tableView = (UIScrollView *)self.leftTable.superview;
    else
        tableView = self.leftTable;
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [tableView.superview convertRect:[aValue CGRectValue] fromView:nil];
    
    // Get the keyboard's animation details
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    
    // Determine how much overlap exists between tableView and the keyboard
    CGRect tableFrame = tableView.frame;
    CGFloat tableLowerYCoord = tableFrame.origin.y + tableFrame.size.height;
    self.keyboardOverlap = tableLowerYCoord - keyboardRect.origin.y;
    if(self.inputAccessoryView && self.keyboardOverlap>0)
    {
        CGFloat accessoryHeight = self.inputAccessoryView.frame.size.height;
        self.keyboardOverlap -= accessoryHeight;
        
        tableView.contentInset = UIEdgeInsetsMake(0, 0, accessoryHeight, 0);
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, accessoryHeight, 0);
    }
    
    if(self.keyboardOverlap < 0)
        self.keyboardOverlap = 0;
    
    if(self.keyboardOverlap != 0)
    {
        tableFrame.size.height -= self.keyboardOverlap;
        
        NSTimeInterval delay = 0;
        if(keyboardRect.size.height)
        {
            delay = (1 - self.keyboardOverlap/keyboardRect.size.height)*animationDuration;
            animationDuration = animationDuration * self.keyboardOverlap/keyboardRect.size.height;
        }
        
        [UIView animateWithDuration:animationDuration delay:delay
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{ tableView.frame = tableFrame; }
                         completion:^(BOOL finished){ [self tableAnimationEnded:nil finished:nil contextInfo:nil]; }];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    if(!self.keyboardShown)
        return;
    
    self.keyboardShown = NO;
    
    UIScrollView *tableView;
    if([self.leftTable.superview isKindOfClass:[UIScrollView class]])
        tableView = (UIScrollView *)self.leftTable.superview;
    else
        tableView = self.leftTable;
    if(self.inputAccessoryView)
    {
        tableView.contentInset = UIEdgeInsetsZero;
        tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
    
    if(self.keyboardOverlap == 0)
        return;
    
    // Get the size & animation details of the keyboard
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [tableView.superview convertRect:[aValue CGRectValue] fromView:nil];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    
    CGRect tableFrame = tableView.frame;
    tableFrame.size.height += self.keyboardOverlap;
    
    if(keyboardRect.size.height)
        animationDuration = animationDuration * self.keyboardOverlap/keyboardRect.size.height;
    
    [UIView animateWithDuration:animationDuration delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{ tableView.frame = tableFrame; }
                     completion:nil];
}

- (void) tableAnimationEnded:(NSString*)animationID finished:(NSNumber *)finished contextInfo:(void *)context
{
//    // Scroll to the active cell
//    if(self.activeCellIndexPath)
//    {
//        [self.leftTable scrollToRowAtIndexPath:self.activeCellIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
//        [self.leftTable selectRowAtIndexPath:self.activeCellIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//    }
}

- (void) viewWillAppear:(BOOL)animated {
    
    // Moved this setting to view will appear to ensure it is available before the UI comes up.
    // As dynamic view itself don't have the UISegment Control. Get the selectedsegment information on view appear
    
    self.selSegmentIdx = self.appSharedData.dynamicViewData.collapsibleOnIdx;
    
    // Apply filter indicators if required
    [self processFilterIndications];
    
    // Localized the Cancel Button as for this app custom language it was appearing in english
    [[UIButton appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    
    //Not working well.
//    [self.tableView setContentOffset:CGPointMake(0, 44)]; // Hide search bar first
    
    // Ensure you call this to let the data be shown on first load
    [super viewWillAppear:animated];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    if (self.appSharedData.dynamicViewData.reloadUI) {
        self.appSharedData.dynamicViewData.reloadUI = NO;
        
        [self forceReloadThisUI];
        
        //       not required now self.appSharedData.dynamicViewData.collapsibleOnIdxPrevSelected = self.appSharedData.dynamicViewData.collapsibleOnIdx;
    }
}




// Using did rotate to ensure we do what we want after rotation of parent view is done, no problem found but protective check.
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if(self.interfaceOrientation == UIDeviceOrientationPortrait || self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown){
        NSLog(@"UNHANDLED");
    } else  if(self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight){
        
        [AppUtilities showDualSectionView: self];
    }
}

- (void) updateTotalRowsLabel {
    int count = [[self fetchedResultsController].fetchedObjects count];
    /* Older Value before changing to ios7 coloring 
      self.totalRowsLabel.text = [@" Total Records         " stringByAppendingFormat:@"%i  ", count];
     */
    NSString *leftPart = NSLocalizedString(@"Total Records", @" Total Records");
    // Reduced the space in Total: & number to make it look together as outline is not there
    self.totalRowsLabel.text = [ @"" stringByAppendingFormat:@"%@:   %i", leftPart, count];    
    [self.totalRowsBtn setTitle:[ @"" stringByAppendingFormat:@"%@:  %i", leftPart, count] forState:UIControlStateNormal];
    
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];;
    //    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    //    self.dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
}

- (void)viewDidUnload
{
    [self setTotalRowsView:nil];
    [super viewDidUnload];
    
    [self setUiSearchBar:nil];
    [self setTotalRowsLabel:nil];
    //[self setDateLabel:nil];
    
    DLog(@"Dynamic: View Unloaded");
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [super controllerDidChangeContent:controller];
    
    [self updateTotalRowsLabel];
}

#pragma mark Search Support
-(void) searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
}
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
//    ULog(@"BeginEditing");
    
    if (!self.uiSearchBar.showsCancelButton) {
        self.uiSearchBar.showsCancelButton = YES;
    }
}

- (void) searchBarTextDidEndEditing:(UITextField *)textField {
    //    if ( [textField.text length] == 0 ) {
    //        [self.uiSearchBar resignFirstResponder];
    //    }
//        ULog(@"EndEditing");
}

// Hide the keyboard
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.uiSearchBar.text = @"";
    
    [AppSharedData getInstance].dynamicViewData.searchText = @"";
    
    [searchBar resignFirstResponder];
    
    self.uiSearchBar.showsCancelButton=NO;
    [self forceReloadThisUI];
    //  DLog(@"Search Bar Cancel clicked");
}

-(void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)Tosearch{
    
    NSString *searchText = [searchBar text];
    
    if ( [searchText length] == 0 ) {
        //        [searchBar resignFirstResponder];
        //        [self.view endEditing:YES];
    }
    
    [AppSharedData getInstance].dynamicViewData.searchText = searchText;

    [self forceReloadThisUI];
    
    //  DLog(@"Search Bar TEXT DID CHANGE");
}

#pragma mark - Action Implementation
- (IBAction)actOnSwipeGesture:(UISwipeGestureRecognizer *)sender {
//    ULog(@"Abc"); // Swipe did not work for now, with 
}

- (IBAction)leftNavBarItemClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"ShowTabFilterViewController" sender:self];
}

- (IBAction)totalRecordsBtnClick:(id)sender {
}

// Hide the Search Keyboard if appearing when switching over
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.uiSearchBar resignFirstResponder];
}

#pragma mark - DB Operations

- (void) fetchData {
    
    [super fetchData];
    [self updateTotalRowsLabel];
}

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}

#pragma mark - Fetch Result Controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    self.fetchedResultsControllerObj = [[AppSharedData getInstance].dynamicViewData genFetchReqCtrlFromSelConfig: APP_DELEGATE_MGDOBJCNTXT];
    self.fetchedResultsControllerObj.delegate = self;
    
    // Memory management.
    DLog(@"TabManageViewController: fetchResultsCtroller: RE-QUERY");
    
    return self.fetchedResultsControllerObj;
}

/*
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{ 
    return YES;
    //(touch.view == self); 
}*/


#pragma mark - Drag n drop exchange and rearrange delegate methods
// Drag within the table to another group
-(void) droppedOnSrcAtIndexPath:(NSIndexPath *)to fromSrcIndexPath:(NSIndexPath *)from {
    
    Task *srctask   = (Task *) [self.fetchedResultsController objectAtIndexPath:from];
    Task *dstTask   = (Task *) [self.fetchedResultsController objectAtIndexPath:to];
    
    [AppUtilities moveDragedTask:srctask ToATask:dstTask];
    
    //    [self forceReloadThisUI]; disabled to avoid reload.
    
}

-(BOOL) droppedOutsideAtPoint:(CGPoint) pointIn fromDstIndexPath:(NSIndexPath*) from{
    
    /* Here we're going to 'clean' the cell before its snapped back. */
    
    //    NSInteger fromIndex = (from.item);
    //    NSMutableDictionary* rightCellData = ([self.leftTableFetchedResultsController objectAtIndex:fromIndex];
    //    [rightCellData setObject:CELL_CLEAN_COLOR forKey:kCellMetaKeyColor];
    
    /* NOTE: We're not updating the cell here - its been hidden so the affects
     won't be visible. Instead we reload the cell in dragFromDstSnappedBackFromIndexPath */
    
    return YES;
}

-(void) dragFromDstSnappedBackFromIndexPath:(NSIndexPath*) path{
    
    /* Reload the newly cleaned cell once its snapped back. */
    
    if(path != nil) { // Bug Fix: it was crashing if we do wild dragging around, so added this nil check
        [self.leftTable reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Undraggable/Unrearrangeable cell implementations

-(BOOL) isCellAtIndexPathDraggable:(NSIndexPath*) index inContainer:(UIView*) container{
    
    if(container == self.leftTable){
        
        /* We only care about cell draggabillity config for the right hand table */
        
        return YES; // Table view is draggable
        
    }
    else {
        return NO; // Collection View is not draggable
    }
}

-(BOOL) isCellInDstAtIndexPathExchangable:(NSIndexPath*) to withCellAtIndexPath:(NSIndexPath*) from{
    
    return NO;// You can decide conditionally, like for few cells allow exchange, not using & overriding just to let know this option is there.
}

-(BOOL) isCellInSrcAtIndexPathExchangable:(NSIndexPath*) to withCellAtIndexPath:(NSIndexPath*) from{
    
    return YES;// You can decide conditionally, like for few cells allow exchange, not using & overriding just to let know this option is there.s there.
}


@end
