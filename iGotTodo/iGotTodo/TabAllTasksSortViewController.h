//
//  TabAllTasksSortViewController.h
//  iGotTodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdateTaskViewController.h"
#import "AddTaskViewController.h"
#import "TaskViewCell.h"
#import "Task.h"
#import <QuartzCore/QuartzCore.h>
#import "Group.h"

// To identify from where this UI is called and do appropriate filtering
typedef enum { FilterGroup=1, FilterTag=2, NoFilter=3 } AllTasksOperationMode;


@interface TabAllTasksSortViewController : UITableViewController 
                                            <UpdateTaskViewControllerDelegate, NSFetchedResultsControllerDelegate, TaskViewCellDelegate>

//@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
// Can be called from groups or tags ui to show a group or tag specific tasks in All Tasks UI.
@property (nonatomic) AllTasksOperationMode operationMode;

@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegment;

// To enable showing all tasks for a selected group. This helps filter tasks of the selected group
@property (nonatomic, strong) Group *lastSelGroupTogetDetails;
// To pass tag from caller & use it to filter tasks fro this tag
@property (nonatomic, strong) Tag   *lastSelTagTogetDetails;

- (NSFetchedResultsController *)fetchedResultsController;

@end


