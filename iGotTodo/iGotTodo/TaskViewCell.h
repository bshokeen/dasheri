//
//  TaskViewCell.h
//  iGotTodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"


@protocol TaskViewCellDelegate;

@interface TaskViewCell : UITableViewCell

@property (nonatomic, weak) Task *task;


@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel1;
@property (nonatomic, weak) IBOutlet UIButton *statusButton1;
@property (nonatomic, weak) IBOutlet UIButton *statusButton2;
@property (nonatomic, weak) IBOutlet UIButton *infoButton1;
@property (nonatomic, weak) IBOutlet UIButton *infoButton2;
@property (nonatomic, weak) IBOutlet UIButton *notesButton;
@property (nonatomic, weak) IBOutlet UIButton *tagsButton;
@property (weak, nonatomic) IBOutlet UILabel *notesLbl;
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;
@property (weak, nonatomic) IBOutlet UILabel *monthLbl;
@property (weak, nonatomic) IBOutlet UILabel *yearLbl;
@property (weak, nonatomic) IBOutlet UIButton *subtaskCntBtn;
@property (weak, nonatomic) IBOutlet UIButton *subtaskBtn;

@property (weak, nonatomic) IBOutlet UIButton *showSubtaskBtn;
// TO inform operations back to the caller/delegate
@property (weak, nonatomic) id <TaskViewCellDelegate> delegate;


- (IBAction)showSubtaskBtnClick:(id)sender;

- (IBAction)doneClick:(id)sender;


-(void) resetInitialText;

@end


@protocol TaskViewCellDelegate <NSObject>


// Notify the caller, cell is under editing mode
-(void) notifyShowSubtasksClicked: (Task *) task Cell:(TaskViewCell *) cell;


@end