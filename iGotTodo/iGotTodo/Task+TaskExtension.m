//
//  Task+TaskExtension.m
//  iGotTodo
//
//  Created by Me on 29/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "Task+TaskExtension.h"

@implementation Task (TaskExtension)

// Custom Code
@dynamic primitiveDueOn, primitiveDueOnSectionName, primitiveProgress, primitiveProgressSectionName;
@dynamic primitiveDone, primitiveDoneSectionName, primitiveFlag, primitiveFlagSectionName;
@dynamic primitiveGain,  primitiveGainSectionName, primitiveStatus, primitiveStatusSectionName; //, primitiveGroupName, primitiveGroupSectionName;
@dynamic primitiveDateSectionName;


#pragma mark Transient properties
- (NSString *)dateSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"dateSectionName"];
    NSString *tmp = [self primitiveDateSectionName];
    [self didAccessValueForKey:@"dateSectionName"];
    
    if (!tmp) {
        /*
         Sections are organized by month and year. Create the section identifier as a string representing the number (year * 1000) + month; this way they will be correctly ordered chronologically regardless of the actual name of the month.
         */
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        if ([self dueOn] == nil)
            return EMPTY;
        
        NSDate *createdOn = [self dueOn];
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:createdOn];
        long longDate =  ((([components year] * 1000) + [components month]) * 100) + [components day];
        tmp = [NSString stringWithFormat:@"%ld",longDate] ;
        
        static NSArray *monthSymbols = nil;
        
        if (!monthSymbols) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setCalendar:[NSCalendar currentCalendar]];
            monthSymbols = [formatter shortMonthSymbols] ;
        }
        
        long numericSection = [tmp integerValue];
        long year = numericSection / 1000 /100;
        long tmpMonth = numericSection - (year * 1000 * 100);
        long month = tmpMonth/100;
        long day = tmpMonth - (month *100);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat=@"EEE";
        NSString * dayString = [[dateFormatter stringFromDate:createdOn] capitalizedString];
        
        tmp = [NSString stringWithFormat:@"%@, %li %@ %li", dayString, day, [monthSymbols objectAtIndex:month-1], year];
        
        [self setPrimitiveDateSectionName:tmp];
    }
    return tmp;
}

#pragma mark Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingDateSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"dueOn"];
}

#pragma mark Transient properties
- (NSString *)dueOnSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"dueOnSectionName"];
    NSString *tmp = [self primitiveDueOnSectionName];
    [self didAccessValueForKey:@"dueOnSectionName"];
    
    if (!tmp) {
        /*
         Sections are organized by month and year. Create the section identifier as a string representing the number (year * 1000) + month; this way they will be correctly ordered chronologically regardless of the actual name of the month.
         */
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        if ([self dueOn] == nil)
            return EMPTY;
        
        // TODO: SIMPLIFY THIS HANDLING. Earlier storing string as 2012xx format and then converting for UI. Now pulled UI code here only but not simplied this yet.
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:[self dueOn]];
        tmp = [NSString stringWithFormat:@"%d", ([components year] * 1000) + [components month]];

        /*
         Section information derives from an event's sectionIdentifier, which is a string representing the number (year * 1000) + month.
         To display the section title, convert the year and month components to a string representation.
         */
        static NSArray *monthSymbols = nil;
        
        if (!monthSymbols) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setCalendar:[NSCalendar currentCalendar]];
            monthSymbols = [formatter monthSymbols] ;
        }
        
        NSInteger numericSection = [tmp integerValue];
        NSInteger year = numericSection / 1000;
        NSInteger month = numericSection - (year * 1000);
        
        tmp = [NSString stringWithFormat:@"%@ %d", [monthSymbols objectAtIndex:month-1], year];
        
        
        [self setPrimitiveDueOnSectionName:tmp];
    }
    return tmp;
}

#pragma mark Time stamp setter
- (void)setDueOn:(NSDate *)newDate {
    
    // If the time stamp changes, the section identifier become invalid.
    [self willChangeValueForKey:@"dueOn"];
    [self setPrimitiveDueOn:newDate];
    [self didChangeValueForKey:@"dueOn"];
    
    [self setPrimitiveDueOnSectionName:nil];
    [self setPrimitiveDateSectionName:nil]; // Bug fix: 2 transiet columns dependent on DueOn, if not adding this line, chaning task data was not updating the UI for this section name
}

#pragma mark Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingDueOnSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"dueOn"];
}


#pragma mark - Progress Transient Handler
- (NSString *)progressSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"progressSectionName"];
    NSString *tmp = [self primitiveProgressSectionName];
    [self didAccessValueForKey:@"progressSectionName"];
    
    if (!tmp) {
        //        NSLog(@"%@", [self progress]);
        
        int progress = (int)([[self progress] floatValue] + 0.5f); // Convert to int as we do it over UI for generating UI Label.
        progress = progress * 5; // A factor of 5 is used in our slider/db storage, hence multiplying by 5
        
        if ( progress <= 25 ) {
            tmp = NSLocalizedString(@"25%", @"25%");
        }
        else if ( progress <= 50 ) {
            tmp = NSLocalizedString(@"50%", @"50%");
        }
        else if ( progress  <= 75 ) {
            tmp = NSLocalizedString(@"75%", @"75%");
        }
        else {
            tmp = NSLocalizedString(@"~100%", @"~100%");
        }
        
        [self setPrimitiveProgressSectionName:tmp];
    }
    return tmp;
}

- (void)setProgress:(NSNumber *) newProgress {
    
    // If the time stamp changes, the section identifier become invalid.
    [self willChangeValueForKey:@"progress"];
    [self setPrimitiveProgress:newProgress];
    [self didChangeValueForKey:@"progress"];
    
    [self setPrimitiveProgressSectionName:nil];
}

+ (NSSet *)keyPathsForValuesAffectingProgressSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"progress"];
}

#pragma mark - Gain Transient Handler
- (NSString *) gainSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"gainSectionName"];
    NSString *tmp = [self primitiveGainSectionName];
    [self didAccessValueForKey:@"gainSectionName"];
    
    if (!tmp) {
        
        switch ([[self gain] intValue]) {
            case 0:
                tmp = NSLocalizedString(@"High", @"High");
                break;
            case 1:
                tmp = NSLocalizedString(@"Medium", @"Medium");
                break;
            case 2:
                tmp = NSLocalizedString(@"Normal", @"Normal");
                break;
            case 3:
                tmp = NSLocalizedString(@"Low", @"Low");
                break;
            default:
                tmp = UNDEFINED;
                break;
        }
        
        [self setPrimitiveGainSectionName:tmp];
    }
    return tmp;
}

- (void)setGain:(NSNumber *) newGain {
    
    // If the gain changes, the section identifier become invalid.
    [self willChangeValueForKey:@"gain"];
    [self setPrimitiveGain:newGain];
    [self didChangeValueForKey:@"gain"];
    
    [self setPrimitiveGainSectionName:nil];
}

+ (NSSet *)keyPathsForValuesAffectingGainSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"gain"];
}


#pragma mark - Done Transient Handler
- (NSString *) doneSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"doneSectionName"];
    NSString *tmp = [self primitiveDoneSectionName];
    [self didAccessValueForKey:@"doneSectionName"];
    
    if (!tmp) {
        
        switch ([[self done] intValue]) {
            case 0:
                tmp = NSLocalizedString(@"Pending", @"Pending");
                break;
            case 1:
                tmp = NSLocalizedString(@"Done", @"Done");
                break;
            default:
                tmp = UNDEFINED;
                break;
        }
        
        [self setPrimitiveDoneSectionName:tmp];
    }
    return tmp;
}

- (void)setDone:(NSNumber *) newDone {
    
    // If the gain changes, the section identifier become invalid.
    [self willChangeValueForKey:@"done"];
    [self setPrimitiveDone:newDone];
    [self didChangeValueForKey:@"done"];
    
    [self setPrimitiveDoneSectionName:nil];
}

+ (NSSet *)keyPathsForValuesAffectingDoneSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"done"];
}


#pragma mark - Flag Transient Handler
- (NSString *) flagSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"flagSectionName"];
    NSString *tmp = [self primitiveFlagSectionName];
    [self didAccessValueForKey:@"flagSectionName"];
    
    if (!tmp) {
        
        switch ([[self flag] intValue]) {
            case 0:
                tmp = NSLocalizedString(@"Flagged!", @"Flagged!");
                break;
            case 1:
                tmp = NSLocalizedString(@"Not Flagged!", @"Not Flagged!");
                break;
            default:
                tmp = UNDEFINED;
                break;
        }
        
        [self setPrimitiveFlagSectionName:tmp];
    }
    return tmp;
}

- (void)setFlag:(NSNumber *) newFlag {
    
    // If the gain changes, the section identifier become invalid.
    [self willChangeValueForKey:@"flag"];
    [self setPrimitiveFlag:newFlag];
    [self didChangeValueForKey:@"flag"];
    
    [self setPrimitiveFlagSectionName:nil];
}

+ (NSSet *)keyPathsForValuesAffectingFlagSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"flag"];
}


#pragma mark - Status Transient Handler
- (NSString *) statusSectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"statusSectionName"];
    NSString *tmp = [self primitiveStatusSectionName];
    [self didAccessValueForKey:@"statusSectionName"];
    
    if (!tmp) {
        
        switch ([[self status] intValue]) {
            case 0:
                tmp = NSLocalizedString( @"Status A", @"Status A");
                break;
            case 1:
                tmp = NSLocalizedString(@"Status B", @"Status B");
                break;
            default:
                tmp = UNDEFINED;
                break;
        }
        
        [self setPrimitiveStatusSectionName:tmp];
    }
    return tmp;
}

- (void)setStatus:(NSNumber *) newStatus {
    
    // If the gain changes, the section identifier become invalid.
    [self willChangeValueForKey:@"status"];
    [self setPrimitiveStatus:newStatus];
    [self didChangeValueForKey:@"status"];
    
    [self setPrimitiveStatusSectionName:nil];
}

+ (NSSet *)keyPathsForValuesAffectingStatusSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"status"];
}

/* To automatically keep the modified_on field upto date adding WillSave and using primitivevalue to avoid recursion on willsave */
- (void)willSave {
    [self setPrimitiveValue:[NSDate date] forKey:@"modified_on"];
    
    [super willSave];
}

@end
