//
//  ToolbarCell.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/03/22.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "ToolbarCell.h"

@implementation ToolbarCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) resetInitialText {
    self.mainImg.image      = nil;
    self.highlightImg.image = nil;
    [self.highlightImg setHidden:YES];
    [self.name         setText:@""];
}

-(void) setHighlighted:(BOOL)highlighted {
    
    [self.highlightImg setHidden:!highlighted];
}

@end
