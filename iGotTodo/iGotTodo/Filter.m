//
//  Filter.m
//  iGotTodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "Filter.h"


@implementation Filter

@dynamic color;
@dynamic created_on;
@dynamic deleted_on;
@dynamic dirty;
@dynamic filter_id;
@dynamic filter_name;
@dynamic icon;
@dynamic sort_order;
@dynamic sync_id;
@dynamic updated_on;

@end
