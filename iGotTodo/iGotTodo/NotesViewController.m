//
//  NotesViewController.m
//  realtodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "NotesViewController.h"
#import "AppUtilities.h"

@interface NotesViewController ()
    @property (nonatomic) BOOL editOn;
    @property (nonatomic, strong) UIBarButtonItem *editDoneButton;
    @property   (nonatomic, strong) NSString *lastCharacterEntered;
    @property (nonatomic) int lastCharacterEnteredLoc;
    @property BOOL bulletModeActive;
@end

@implementation NotesViewController

@synthesize dataTextView = _dataTextView;
@synthesize commonDTOObj = _commonDTOObj;
@synthesize delegate = _delegate;

@synthesize editOn = _editOn;
@synthesize editDoneButton;
@synthesize lastCharacterEntered = _lastCharacterEntered;
@synthesize lastCharacterEnteredLoc = _lastCharacterEnteredLoc;

@synthesize bulletModeActive = _bulletModeActive;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Google Custom
    [AppUtilities logAnalytics:@"View Opened: Notes"];

    
    self.editDoneButton          = [[UIBarButtonItem alloc]
                                                initWithTitle:NSLocalizedString(@"Edit", @"Edit")
                                    style:UIBarButtonItemStyleDone target:self action:@selector(editDoneItemBtnClick:)];
   /*
    UIBarButtonItem *bListButton         = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                            target:self
                                            action:@selector(bulletListItemBtnClick:)];
   
    
    UIBarButtonItem *clearButton          = [[UIBarButtonItem alloc] 
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                            target:self action:@selector(clearItemBtnClick:)];
      */
    
    // Disabled the save button to enable saving by default
//    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.editDoneButton,  nil]; //clearButton,
    
    self.dataTextView.layer.cornerRadius = 10.0;
    self.dataTextView.delegate = self; // Get all the events to this controller.
    
    // Enable going into edit mode by tapping on the notes screen to help
    UITapGestureRecognizer* uiTextViewTap = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(uiTextViewTapped:)];

    // observe keyboard hide and show notifications to resize the text view appropriately
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    //    _editOn = NO;
//    [self toggleEditing];
    uiTextViewTap.numberOfTapsRequired = 1;
    [self.dataTextView setUserInteractionEnabled:YES];
    [self.dataTextView addGestureRecognizer:uiTextViewTap];
}

- (void)viewDidUnload
{
    [self setDataTextView:nil];
    [super viewDidUnload];
    
    [self setDataTextView:nil];
}
- (void) viewWillAppear:(BOOL)animated {
    self.dataTextView.text = (NSString *) [self.commonDTOObj getEditFieldValue];
     [self.dataTextView becomeFirstResponder];
}

// Making the UI Save by default on disappear to remove the hassle of pressing the save button
// So brought the code from done button click to viewwilldisapper so removing the dot symbol before exit.
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // Do not do the trimming else handling the bullet adding will be a problem in edit mode.
    // Skip the default added content if nothing is changed to avoid showing a notes icon if user had not added the content.
    NSString *finalStr = self.dataTextView.text ;
    if ( [finalStr isEqualToString:@"\u2022  "] ) {
        finalStr = @"";
    }
    
    [self.commonDTOObj setEditFieldValue:finalStr];
    
    [self.delegate notesViewControllerDidFinish: self.commonDTOObj];
}

/* OLD METHOD, stopped owrking after ios7
// Added BELOW API for resizing the notes text view automatically based on keyboard

-(void) viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)moveTextViewForKeyboard:(NSNotification*)aNotification up:(BOOL)up {
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.dataTextView.frame;
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    keyboardFrame.size.height -= self.tabBarController.tabBar.frame.size.height;
    newFrame.size.height -= keyboardFrame.size.height * (up?1:-1);
    self.dataTextView.frame = newFrame;
    
    [UIView commitAnimations];
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
//    buttonDone.enabled = true;
    [self moveTextViewForKeyboard:aNotification up:YES];
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
//    buttonDone.enabled = false;
    [self moveTextViewForKeyboard:aNotification up:NO];
}
*/
// Added above API for resizing the notes text view automatically based on keyboard

/*
 Get the size of the keyboard.
 
 Adjust the bottom content inset of your scroll view by the keyboard height.
 
 Scroll the target text field into view.

-(void)keyboardWasHidden:(NSNotification*)aNotification {
    if(!keyboardShown) {
        return;
    }
    
    // Reset the height of the scroll view to its original value
    CGRect viewFrame = [self.textView frame];
    if(orientationAtShown == UIInterfaceOrientationPortrait || orientationAtShown == UIInterfaceOrientationPortraitUpsideDown) {
        viewFrame.size.height += keyboardSize.height;
    } else {
        viewFrame.size.height += keyboardSize.width;
    }
    
    self.textView.frame = viewFrame;
    
    keyboardShown = NO;
}
 */

- (void)keyboardWillShow:(NSNotification *)notification {
    
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [notification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's
    // coordinate system. The bottom of the text view's frame should align with the top
    // of the keyboard's final position.
    //
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.dataTextView.frame = newTextViewFrame;
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.dataTextView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}



#pragma mark - Text view delegate methods
// Code retained to add accessory view to notes later.
- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView {
    
    // you can create the accessory view programmatically (in code), or from the storyboard
    if (self.dataTextView.inputAccessoryView == nil) {
        
        self.dataTextView.inputAccessoryView = self.accessoryView;
    }
    
//    self.navigationItem.rightBarButtonItem = self.doneButton;
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)aTextView {
    
    [aTextView resignFirstResponder];
//    self.navigationItem.rightBarButtonItem = self.editButton;
    
    return YES;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
       return YES;
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range  replacementText:(NSString *)text
{
    BOOL returnWithoutChange = YES;
    int noOfCharsToReplace=0;

    // See if the characters being verified are in sequence or not
    // OR return key is pressed twice
    if ( range.length == 0 // When dictionary corrected word is used it was hindering bulleted list & observe range length set for those cases, so adding thsi check to ensure we don't do anything with those cases
        && (range.location == (self.lastCharacterEnteredLoc+1)  || ([self.lastCharacterEntered isEqualToString:@"\n"] && [text isEqualToString:@"\n"]))) {
        
        // Last Key was * or @ and now user pressed enter OR last key was \n and user pressed * or Bullet mark
        if ( (([self.lastCharacterEntered isEqualToString:@"*"] ||  [self.lastCharacterEntered isEqualToString:@"@"])  && [text isEqualToString:@"\n"])
            || ([self.lastCharacterEntered isEqualToString:@"\n"] && ([text isEqualToString:@"*"] || [text isEqualToString:@"•"])))
        {
            self.bulletModeActive     = YES;                  
            noOfCharsToReplace        = 2;
            // It was eating one extra character so handling it
            if ( [self.lastCharacterEntered isEqualToString:@"\n"] ) {
                noOfCharsToReplace = 1;
            }
            
            NSString *modifiedString  = @"";
            NSString *temp = self.dataTextView.text;

            // With blank note trying bullet listing giving error. Work around below used. As there would not be chars to go back, replacing 0 char without a \nBullet
            if ((range.location - noOfCharsToReplace) == -1) {
                
                noOfCharsToReplace -= 1;
                modifiedString = [temp stringByReplacingCharactersInRange:NSMakeRange(range.location - noOfCharsToReplace, noOfCharsToReplace) withString:@"\u2022  "];
            }
            else {
                modifiedString = [temp stringByReplacingCharactersInRange:NSMakeRange(range.location - noOfCharsToReplace, noOfCharsToReplace) withString:@"\n\u2022  "];
            }
            //DLog(@"NotesView: currData: %@ DataLen:%d Range:%d,%d, GoBackNoOfCar:%d modifiedStr:%@", temp, [temp length], NSRangeFromString(temp).location, NSRangeFromString(temp).length, range.location - noOfCharsToReplace, modifiedString);

            [self.dataTextView setText:modifiedString];

            self.lastCharacterEntered = text;
            self.lastCharacterEnteredLoc = range.location - noOfCharsToReplace + 4;

            self.dataTextView.selectedRange = NSMakeRange(self.lastCharacterEnteredLoc, 0);
            
            returnWithoutChange = NO;
            
            DLog(@"NotesView: Location:%d Length:%d lastCharLoc:%d LastChar:%@ - Bullet Mode Switch: ON, User entered the bullet char before/after \\n", range.location, range.length, self.lastCharacterEnteredLoc, [self.lastCharacterEntered stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
        }
        else if([self.lastCharacterEntered isEqualToString:@"\n"] && [text isEqualToString:@"\n"] && self.bulletModeActive)
        {

            // Enter press in between was deleting notes, so adding this check
            if (self.lastCharacterEnteredLoc != range.location) { // If user has gone to different location while presseing second enter, we should simply let that be a normal \n.
                self.lastCharacterEntered = text;
                self.lastCharacterEnteredLoc = range.location;
                
                DLog(@"NotesView: Location:%d Length:%d lastCharLoc:%d LastChar:%@ - Last & Cur Char not in sequence, capture as is.", range.location, range.length, self.lastCharacterEnteredLoc, [self.lastCharacterEntered stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
            } else {
                self.bulletModeActive = NO;
                NSString *temp = self.dataTextView.text;
                
                // First try to find the bullet char as sometimes newline is not there
                int tempIndex = (int)([temp rangeOfString:@"\u2022" options:NSBackwardsSearch].location);
                if (tempIndex < 0 ) {
                    // if it is not found clear the entire string
                    noOfCharsToReplace    = [temp length];
                }
                else {
                    // if it is found include the last newline as well by adding 1
                    noOfCharsToReplace    = [temp length] - tempIndex + 1;
                }
                // if still no of char to replace is more simply take the overall lenght to replace
                if ( range.location < noOfCharsToReplace)
                    noOfCharsToReplace = range.location;
                
                //            NSLog(@"%i %i %i", range.location-noOfCharsToReplace, noOfCharsToReplace, tempIndex);
                //            if ( [self.dataTextView.text length] )
                self.dataTextView.text = [temp
                                          stringByReplacingCharactersInRange:NSMakeRange (range.location-noOfCharsToReplace, noOfCharsToReplace) withString:@""];
                
                self.lastCharacterEnteredLoc = range.location - noOfCharsToReplace;
                self.lastCharacterEntered = text;
                
                self.dataTextView.selectedRange = NSMakeRange(self.lastCharacterEnteredLoc, 0);
                
                returnWithoutChange = NO;
                DLog(@"NotesView: Location:%d Length:%d Loc-2: %d lastCharLoc:%i LastChar:%@ - Bullet Mode Switched OFF - User Choice, Pressed Return Key Twice.", range.location, range.length, range.location-noOfCharsToReplace, self.lastCharacterEnteredLoc, [self.lastCharacterEntered stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
            }
        }
        
        else if (self.bulletModeActive  && [text isEqualToString:@"\n"]) { // Edit mode is one and current character is new line, switch on Bullet Mode
            NSString *temp = self.dataTextView.text;
            NSString *modifiedString = [temp stringByReplacingCharactersInRange:NSMakeRange(range.location, 0) withString:@"\n\u2022  "]; 
            [self.dataTextView setText:modifiedString];
            
            self.lastCharacterEnteredLoc = range.location + 4;
            self.lastCharacterEntered = text;    
            
            self.dataTextView.selectedRange = NSMakeRange(self.lastCharacterEnteredLoc, 0);
            
            returnWithoutChange = NO;
            DLog(@"NotesView: Location:%d Length:%d lastCharLoc:%i LastChar:%@ - Bullet Mode Switch: ON - Starting Newline & this is not a 2nd newline in sequence.", range.location, range.length, self.lastCharacterEnteredLoc, [self.lastCharacterEntered stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
        }
        else {
            // No Chars to Replace
            noOfCharsToReplace = 0;
            
            self.lastCharacterEntered = text;
            self.lastCharacterEnteredLoc = range.location;
            
            DLog(@"NotesView: Location:%d Length:%d lastCharLoc:%d lastChar:%@ - No Chars to Replace", range.location, range.length, self.lastCharacterEnteredLoc, [self.lastCharacterEntered stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
        }
    } else {
        self.lastCharacterEntered = text;
        self.lastCharacterEnteredLoc = range.location;
        
        DLog(@"NotesView: Location:%d Length:%d lastCharLoc:%d LastChar:%@ - Last & Cur Char not in sequence, capture as is.", range.location, range.length, self.lastCharacterEnteredLoc, [self.lastCharacterEntered stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
    }

    return returnWithoutChange;
}

-(void) toggleEditing {
    if (self.editOn) 
    {
        [self.commonDTOObj setEditFieldValue:self.dataTextView.text];
        
        [self.delegate notesViewControllerDidFinish: self.commonDTOObj];
        [self.navigationController popViewControllerAnimated:YES];  
        
    }
    else 
    {
        self.editDoneButton.title = NSLocalizedString(@"Done", @"Done");
        
        self.dataTextView.dataDetectorTypes = UIDataDetectorTypeAll;
        self.dataTextView.editable = YES;
//        int w=self.dataTextView.frame.size.width;
//        int h=self.dataTextView.frame.size.height;
//        DLog(@"WIdth %i: height: %i", w, h);
        
/* Old method of resizing the notes view textbox. But on iPhone 5 it did not give good results so used a new method of getting size from keyboard etc. considering keyboard of different languages
        CGRect frame = self.dataTextView.frame;
        frame.size.height = 167;//199 or self.dataTextView.contentSize.height;
        self.dataTextView.frame = frame;
*/
        
        self.editOn = YES;
        
        [self.dataTextView becomeFirstResponder];
        
        // if note is blank by default  have the bullet list on
        NSString *curText = self.dataTextView.text;
        if ([curText length] == 0 ) {
            self.dataTextView.text = [NSString stringWithFormat:@"\u2022  "];
            self.bulletModeActive = YES;
            DLog(@"NotesView: Bullet Mode Switch: ON - Starting Fresh.");
        }
        else {
            // Check if bullet is already out there and if so simply NotesView: Switch ON bullet list
            NSString *noteTextStr = self.dataTextView.text;
            NSError *error = NULL;
            NSString *pattern=@"^.*•  $"; // If user modified this format, we do not consider that
            NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                    options:NSRegularExpressionDotMatchesLineSeparators
                                                                                      error:&error];
            NSUInteger numberOfMatches = [regex1 numberOfMatchesInString:noteTextStr options:0 range:NSMakeRange(0, [noteTextStr length])];
            
//            DLog(@"NotesView: Matches: %i {%@} {%@}", numberOfMatches, pattern, noteTextStr);
            if ( numberOfMatches == 1 ) {
                self.bulletModeActive = YES;
                DLog(@"NotesView: Bullet Mode Switch: ON - Was Left On, Resume.");
            }
            else {
                // Else always start with bullet list on
                NSString *trimmed = [curText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                self.dataTextView.text = [trimmed stringByAppendingFormat:@"\n\u2022  "];
                self.bulletModeActive = YES;
                // Find if the last line begins with the bullet character and if so start again with a bullet list - Earlier thought this but found complicated, so simplifed using above method.
                DLog(@"NotesView: Bullet Mode Switch: ON - Start Always with On.");
            }
        }
    }
}

- (IBAction) editDoneItemBtnClick :(id)sender {
    [self toggleEditing];
}

// TO make it easy to start editing adding the ability to edit on tap of the UITextView
- (void)uiTextViewTapped:(UIGestureRecognizer*)recognizer {
    if (!self.editOn) {
        [self toggleEditing];
    }
}

/*
- (IBAction) bulletListItemBtnClick :(id)sender {
    UIActionSheet *sheet;
    sheet = [[UIActionSheet alloc] initWithTitle:@"Modify Selection"
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:@"Delete"
                               otherButtonTitles:@"Bullet List (* * *) ", @"Number List (1 2 3 ...)", nil];
    
    [sheet showFromRect:self.view.bounds inView:self.view animated:YES];
    
    // Show the sheet
    [sheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
        if (buttonIndex == actionSheet.cancelButtonIndex)
        { return; }
        else if ( buttonIndex == actionSheet.destructiveButtonIndex) {
            self.dataTextView.text = nil;
        }
        else if ( buttonIndex == 1) // Bullet List
        {
            DLog(@"Button Index: %d", buttonIndex);
//            NSMutableString * bulletList = [NSMutableString stringWithCapacity:items.count*30];
//            for (NSString * s in items)
//            {
//                [bulletList appendFormat:@"\u2022 %@\n", s];
//            }
//            textView.text = bulletList;

            
        }
        else if ( buttonIndex == 2) // Number List
        {
            DLog(@"Button Index: %d", buttonIndex);
        }
    
    DLog(@"Button %d", buttonIndex);
}
 */

- (IBAction) clearItemBtnClick:(id)sender  {
    
    self.dataTextView.text = @"";
    [self toggleEditing];
}

// Allow copy paste in notes data so that user can send it on email or bring from email...
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if(action == @selector(copy:)) {
        return YES;
    }
    else {
        return [super canPerformAction:action withSender:sender];
    }
}

- (IBAction)axsryBtn_1:(id)sender {
    ULog(@"Hello");
}
@end
