//
//  GroupViewCell.m
//  iGotTodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "ItemDetailsCell.h"
#import "AppUtilities.h"

@implementation ItemDetailsCell

@synthesize itemSeqNumberLbl = _itemSeqNumberLbl;
@synthesize itemNameLabel = _itemNameLabel;
@synthesize itemTasksCountLabel = _itemTasksCountLabel;

@synthesize cellObject = _cellObject;

/* Trying capturing touch on label but it was going to didselect so this solution did not work and i had to create a button on top to capture its click as if clicked on label and icon.
-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self registerGesture]; // Register taps
    }
    return self;
}

-(void) registerGesture {
    
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTap:)];
    [self.itemTasksCountLabel addGestureRecognizer:tapGesture];
}

// Handle Label Tap
- (void)labelTap:(UIGestureRecognizer*)recognizer {
    [self notifyGoToDetailsButton];
}
*/

// Common Operation
-(void) notifyGoToDetailsButton {
    [self.delegate notifyEditBtnClick:self.cellObject];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    // iOS7 onwards the color was very odd so setting up manually
    self.selectedBackgroundView = [AppUtilities getSelectedBackgroundView];
    self.itemSeqNumberLbl.highlightedTextColor    = [AppUtilities getSelectedTextHighlightColor];
    self.itemTasksCountLabel.highlightedTextColor = [AppUtilities getSelectedTextHighlightColor];
    self.itemNameLabel.highlightedTextColor       = [AppUtilities getSelectedTextHighlightColor];

}


// To allow some to call and hide the keyboard
-(void) hideKeyboard {

    [self.itemNameEditTxt resignFirstResponder];
    [self.itemNameEditTxt setHidden:YES];
    [self.itemNameLabel setHidden:NO];
    self.itemNameEditTxt.text = @"";
    
}

/* Detect double tap on the cell
 * Switch over to text mode
 * If value is changed send a notification to parent
 * Validate the change and save
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if(((UITouch *)[touches anyObject]).tapCount == 2)
    {
        [self.itemNameEditTxt setHidden:NO];
        [self.itemNameLabel setHidden:YES];
        self.itemNameEditTxt.text = self.itemNameLabel.text;
        [self.itemNameEditTxt becomeFirstResponder];

        // Notify the call that edit is on and when edit is of, it can ask this cell to disbale the keyboard
        [self.delegate notifyCellEditOnFor:self.cellObject Cell:self];
    }
    else {
        [self.itemNameEditTxt resignFirstResponder];
        [self.itemNameEditTxt setHidden:YES];
        [self.itemNameLabel setHidden:NO];
        self.itemNameEditTxt.text = @""; // No swapping of data is done here
    }
    [super touchesEnded:touches withEvent:event];
}

// the txtfield delegate to cell object to recieve this event
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    NSString *newValue = [AppUtilities trim:textField.text];
    NSString *oldValue = self.itemNameLabel.text;
    
    if ([newValue length] > 0 && ![newValue isEqualToString:oldValue]) { //notify only if value is different and new value len >0
        [self.delegate notifyEditFinish:self.cellObject NewValue:textField.text];
    }
    
    if ( textField == self.itemNameEditTxt) {
        [self.itemNameEditTxt resignFirstResponder];
        [self.itemNameEditTxt setHidden:YES];
        [self.itemNameLabel setHidden:NO];
        self.itemNameEditTxt.text = @"";
    }
    
    return YES;
}

// select all text by default
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    [textField selectAll:self]; //It works but was looking ugly so disabled selection of value change
    
}

// Click the button, send notification to parent, where you can do the edit operation
- (IBAction)editBtnClicked:(id)sender {
        [self notifyGoToDetailsButton];
}

@end
