//
//  Group+Ext.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/02/18.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "Group+Ext.h"

@implementation Group (Ext)

@dynamic primitiveUppercaseFirstLetterOfName;

- (NSString *)uppercaseFirstLetterOfName {
    [self willAccessValueForKey:@"uppercaseFirstLetterOfName"];
    NSString *aString = [[self valueForKey:@"name"] uppercaseString];
    
    // support UTF-16:
    NSString *stringToReturn = [aString substringWithRange:[aString rangeOfComposedCharacterSequenceAtIndex:0]];
    
    // OR no UTF-16 support:
    //NSString *stringToReturn = [aString substringToIndex:1];
    
    [self didAccessValueForKey:@"uppercaseFirstLetterOfName"];
    return stringToReturn;
}



@end
