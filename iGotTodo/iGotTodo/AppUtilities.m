//
//  AppUtilities.m
//  iGotTodo
//
//  Created by Me on 12/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//


#import "AppUtilities.h"
#import "AppDelegate.h"
#import "CoreDataOperations.h"
#import "GAI.h" //Google
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

static int DEFAULT_GAP_NEXTTASKDUE =  1800; // 30 minutes
static int DEFAULT_GAP_STARTOFDAY  = 28800; // 8 HOURS I.E. 8 AM

@implementation AppUtilities

+(NSString *) trim: (NSString *) inputString {
    // FIxed the bug: It was replacing all white spaces earlier. Now using Trim API.
//    return [inputString stringByReplacingOccurrencesOfString:@" " withString:@""];
    return  [inputString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+ (NSString *) handleEmptyString: (NSString *) str {
    NSString *name = str;
    if (name.length == 0 ) {
        name = EMPTY;
    }
    else if ([name isEqualToString: EMPTY]) {
        name = nil;
    }
    return name;
}

+ (BOOL) ifTaskAlreadyExist: (NSManagedObjectContext *) context TaskName: (NSString *)taskObjectID
{
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(ID == %@)", taskObjectID]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

+(BOOL) ifEntityAlreadyExist: (NSString *)entityName AttributeName: (NSString *) attributeName AttributeValue: (NSString *) attributeValue {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:entityName inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(%@ == %@)", attributeName, attributeValue]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT
                        countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}


#pragma mark Date Formatter
+ (NSString *)uiFormatedDate:(NSDate *) date
{
    NSString *returnVal = nil;
    
    if (date == nil) {
        returnVal = NEVER;
    }
    else {
        returnVal = [[AppUtilities lDateFormatter] stringFromDate: date];
    }
    return returnVal;
}

+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate
{
    NSDate *returnVal = nil;
    if (formatedDate == nil || formatedDate.length == 0 || [formatedDate isEqualToString:NEVER] ) {
        returnVal = [NSDate date];
    }
    else {
        returnVal = [[AppUtilities lDateFormatter] dateFromString:formatedDate];
    }
    return returnVal;
}

// TODO: addDay is not being used here???
+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate WithDayAdd: (BOOL) addDay
{
    NSDate *returnVal = nil;
    if (formatedDate == nil || formatedDate.length == 0 || [formatedDate isEqualToString:NEVER] ) {
        returnVal = [NSDate dateWithTimeInterval:3600*24*1 sinceDate:[NSDate date]];
    }
    else {
        returnVal = [[AppUtilities lDateFormatter] dateFromString:formatedDate];
    }
    return returnVal;
}

+ (NSString *) uiFormatedTime: (NSDate *) timeWithDate {
    if (timeWithDate == nil) {
        return NEVER;
    }
    else {
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"h:mm a"];
        
        return [outputFormatter stringFromDate:timeWithDate] ;
    }
}

+ (NSString *) dayNameFromDate: (NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:date];
    return dayName;
}

+ (NSInteger) dayFromDate: (NSDate *) date {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSDayCalendarUnit ) fromDate:date];
 
    return [dateComponents day];
}

+ (NSInteger) monthFromDate: (NSDate *) date {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSMonthCalendarUnit ) fromDate:date];
    
    return [dateComponents month];
}
+ (NSString *) monthNameFromDate: (NSDate *) date {

    NSDateFormatter *df = [[NSDateFormatter alloc] init];    
    [df setDateFormat:@"MMM"];
//    [df setDateFormat:@"yy"];
//    [df setDateFormat:@"dd"];    
    
    return [NSString stringWithFormat:@"%@", [df stringFromDate:[NSDate date]]];
}

+ (NSString*)getReadableTimeWithStartDate:(NSDate*) startDate WithEndDate: (NSDate*)endDate {
    
    NSString *readableTime = nil;
    
    if (startDate != nil && endDate != nil) {
        readableTime = @""; // Initialize with some blank text
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSDateComponents *components = [gregorianCalendar components:unitFlags
                                                            fromDate:startDate
                                                              toDate:endDate
                                        
                                                             options:0];
        
        long year = [components year];
        long month= [components month];
        long day  = [components day];
        long hour = [components hour];
        long min  = [components minute];
        long sec  = [components second];
        
        if ( year > 0 )
            readableTime = [NSString stringWithFormat:@"%li Y  %li M  %li d  %li h  %li m", year, month, day, hour, min];
        else if ( month > 0)
            readableTime = [NSString stringWithFormat:@"%li M  %li d  %li h  %li m", month, day, hour, min];
        else if (day > 0) {
            readableTime = [NSString stringWithFormat:@"%li d  %li h  %li m", day, hour, min];
        }
        else if ( hour > 0 ) {
            readableTime = [NSString stringWithFormat:@"%li h  %li m", hour, min];
        }
        else if ( min > 0) {
            readableTime = [NSString stringWithFormat:@"%li m", min];
        }
        else {
            readableTime = [NSString stringWithFormat:@"%li s", sec];
        }
    }
    
    return readableTime;
}

+ (NSString*)getReadableTime:(double)timeInMilliSecs {
    
    NSDate *curDate = [NSDate date];
    NSDate *newDate    = [NSDate dateWithTimeInterval:timeInMilliSecs sinceDate:curDate];
    
    
    return [AppUtilities getReadableTimeWithStartDate:curDate WithEndDate:newDate];
}



+ (NSDate *) getDatePartOnlyFor:(NSDate *)inputDate {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    // get year, month and day parts
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    
    // create a date object using custom dateComponents against current calendar
    NSDateComponents *todayCmpnt = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:inputDate];
    
    [dateFormatter dateFromString:
     [NSString stringWithFormat:@"%li-%li-%li 00:00:00 +0000",
      (long)[todayCmpnt year],
      (long)[todayCmpnt month],
      (long)[todayCmpnt day]]];
    
    NSDate *today = [calendar dateFromComponents:todayCmpnt];
    
    
    return today;
}

+ (NSDate *) getStartOfMonthDate:(NSDate*)inputDate {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *monthdayComponents =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:inputDate];
    
    NSDate *startOfMonth = [calendar dateFromComponents:monthdayComponents];
    
    
    return startOfMonth;
}

+ (NSDate *) getStartOfNextMonthDate:(NSDate *)inputDate {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *monthdayComponents =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:inputDate];
    
    monthdayComponents.month = monthdayComponents.month + 1; // Difference is we add one to month
    
    NSDate *startofNextMonth = [calendar dateFromComponents:monthdayComponents];
    
    
    return startofNextMonth;
}

+ (NSDate *) getStartOfNextDayDate:(NSDate *)inputDate {
    // Addition of day + 1 was not going to next month, so using this API to get right date
    NSDate *tomorrow = [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:inputDate];
    return tomorrow;
}

+ (NSDate *) getStartOfWeekMondayDate:(NSDate *)inputDate {
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:inputDate];
    NSInteger weekday = [weekdayComponents weekday];
    
    // Calendar day is starting from Sunday, so if you are on Wednessday it would give weekday as 4. But note 4th day is not closed, so we somewhere around 3.x days now but overall this si 4th day, So if i subtract and go back 4 days, it will take me byound Sunday. 4 days are not completed and going back for more than 3.x means we go beyound Sunday. Hence I am not doing 4-1 but 4-2 i.e Wednessay - 2 days
    NSDate *weekStart = [NSDate dateWithTimeInterval:-(weekday-2)*24*60*60 sinceDate:inputDate];
    
    
    return weekStart;
}

+(NSDate *)getStartOfWeekDate:(NSDate *)inputDate {
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:inputDate];
    NSInteger weekday = [weekdayComponents weekday];
    
    // Calendar day is starting from Sunday, so if you are on Monday it would give weekday as 2. To get end of the week 7 - 2 = 5, add 5 days in todays date. It gives the date for Saturday. But as I am searching data with time 00:00:00 I am adding 1 extra day to get the next date. As I don't want to start my week from Sunday rather Monday, so adding 1 more to shift from Sunday to Monday. Hence 7 - weekday + 2.
    NSDate *weekStart = [NSDate dateWithTimeInterval:-(weekday)*24*60*60 sinceDate:inputDate];
    
    
    return weekStart;
}

+(NSDate *)getStartOfNextWeekDate:(NSDate *)inputDate {
    NSDate *week  = [AppUtilities getStartOfWeekDate:inputDate];
    
    week = [NSDate dateWithTimeInterval:9*24*60*60 sinceDate:week]; // adding 9 days to get the week start on Monday
    
    return week;
}
+(NSDate *) getStartOfWeekEndDate:(NSDate *)inputDate {
    NSDate *week  = [AppUtilities getStartOfWeekDate:inputDate];
    
    week = [NSDate dateWithTimeInterval:7*24*60*60 sinceDate:week]; // (adding 7 days as day of week was starting on Saturday)
    
    return week;
}

+ (NSDate *) getRecentDate:(NSDate*)inputDate {
    // Date 6 hours earlier than the input date (current date)
    NSDate *recentDate = [NSDate dateWithTimeInterval:-6*60*60 sinceDate:inputDate];
    return recentDate;
}

+ (NSDate *) getNextTimeSlot:(NSDate *)inputDate {
    
    // Default new date is curr
    NSDate *nextDueOn = nil;
    
    if (inputDate != nil) {
        // Get todays date without time factor
        NSDate *datePartOnly = [AppUtilities getDatePartOnlyFor:inputDate];
        
        NSDate *lastTaskDueAt = [CoreDataOperations getMaxDueOnOnDay:datePartOnly];
        
        if (lastTaskDueAt == nil) { // we don't have any task with a date, so use default start time
            nextDueOn  = [NSDate dateWithTimeInterval:DEFAULT_GAP_STARTOFDAY sinceDate:datePartOnly];
        }
        else {
            nextDueOn  = [NSDate dateWithTimeInterval:DEFAULT_GAP_NEXTTASKDUE sinceDate:lastTaskDueAt];
            
            int inDay = [AppUtilities dayFromDate:inputDate];
            int newDay= [AppUtilities dayFromDate:nextDueOn];
            if (newDay > inDay) { // check new date is not beyond current date, if so, start it in the morning to let you prioritise later
                nextDueOn  = [NSDate dateWithTimeInterval:DEFAULT_GAP_STARTOFDAY sinceDate:lastTaskDueAt];
            }
        }
    }

    return nextDueOn;
}

/*
 -(NSDate *) getDateFromString: (NSString *)dateString {
 NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
 dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
 //    NSDateFormatter *dformatter = [self dateFormatter];
 NSDate* date = [dateFormatter dateFromString:dateString];
 
 DLog(@"Date %@", [date description]);
 return date;
 }*/

+ (NSString *) mFormatedDateYearOnly: (NSDate *) date {
    NSString *formatedDate = @"";
    if (date) {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit fromDate:date];
        
        NSInteger year = [dateComponents year];
        formatedDate =  [NSString stringWithFormat:@"%i", year ];
    }
    
    return formatedDate;
}
+ (NSString *) mFormatedDateMonthName: (NSDate *) date {
    NSString *formatedDate = @"";
    if (date) {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponents = [calendar components:NSMonthCalendarUnit fromDate:date];
        NSInteger monthNumber = [dateComponents month];
        
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[[df monthSymbols] objectAtIndex:(monthNumber-1)] substringToIndex:3];
        
        formatedDate = monthName;
    }
    
    return formatedDate;
}

+ (NSString *) mFormatedDateDay: (NSDate *) date {
    NSString *formatedDate = @"";
    if (date) {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponents = [calendar components:NSDayCalendarUnit fromDate:date];
        NSInteger day = [dateComponents day];
        formatedDate =  [NSString stringWithFormat:@"%i", day ];
    }
    return formatedDate;
}


+ (NSString *) getFormattedDateOnly: (NSDate *) date {
    
    NSString *formatedDate = NEVER;
    if (date) {
        static NSDateFormatter *formatter = nil;
        if (formatter == nil) {
            formatter = [[NSDateFormatter alloc] init];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
        }
        
        NSString *dateText = [formatter stringFromDate:date];
        if ([dateText length] == 0 )
            dateText = NEVER;

        formatedDate = dateText;
    }
    return formatedDate;
}

+ (NSDateFormatter *)lDateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle]; //NSTimeZoneNameStyleShortGeneric, earlier it was with seconds but warning, now removed warning but no seconds information, will see in long run if it is unexpected.
    }
    
    return dateFormatter;
}

+(NSDate *) ifEmptyAddDay: (NSDate *) date {
    NSDate *tmpDate = date;
    if (tmpDate == nil) {
        tmpDate = [NSDate dateWithTimeInterval:3600*24*1 sinceDate:[NSDate date]];
    }
    return tmpDate;
}

+(NSString *) getImageName: (NSString *) imageTag {
    
    NSString *imgName = nil;
    
    if ([imageTag isEqualToString:@"starOn"]) {
        imgName = @"RTTaskStarIcon.png";
    }
    else if ([imageTag isEqualToString:@"starOff"]) {
        imgName = @"RTTaskStarDeselected.png";
    }
    else if ([imageTag isEqualToString:@"checkOn"]) {
        imgName = @"RTTaskCompleted.png";
    }
    else if ([imageTag isEqualToString:@"checkOff"]) {
        imgName = @"RTTaskPending.png";
    }
    else if ([imageTag isEqualToString:@"alarmOn"]) {
        imgName = @"RTTaskAlarmIcon-23.png";
    }
    else if ([imageTag isEqualToString:@"notesOn"]) {
        imgName = @"RT_Button_StickyIcon_27x27.png";
    }
    else if ([imageTag isEqualToString:@"tagSOn"]) {
        //imgName = @"RT_Tab_Icon_Views_23.23.png";
        imgName = @"RT_Icon_Tag.png";
    }
    else if ([imageTag isEqualToString:@"none"]) {
        imgName = @"";
    }
    else if ([imageTag isEqualToString:@"sectionOpen"]) {
        imgName = @"RTTaskSectionCarat-open.png";
    }
    else if ([imageTag isEqualToString:@"sectionClosed"]) {
        imgName = @"RTTaskSectionCarat.png";
    }
    else if ([imageTag isEqualToString:@"OrderDOWN"]) {
        imgName = @"RT_Icon_Btn_DownArrow_Blue.png";
    }
    else if ([imageTag isEqualToString:@"OrderUP"]) {
        imgName = @"RT_Icon_Btn_UPArrow_Blue.png";
    }
    else if ([imageTag isEqualToString:@"highGain"]) {
        imgName = @"RTTaskIconRed.png";
    }
    else if ([imageTag isEqualToString:@"mediumGain"]) {
        imgName = @"RTTaskIconYellow.png";
    }
    else if ([imageTag isEqualToString:@"normalGain"]) {
        imgName = @"RTTaskIconGreen.png";
    }
    else if ([imageTag isEqualToString:@"lowGain"]) {
        imgName = @"RTTaskIconBlue.png";
    }
    else if ([imageTag isEqualToString:@"Btn_Delete"]) {
        imgName = @"Btn_Delete.png";
    }
    else if ([imageTag isEqualToString:@"Btn_Delete_Open"]) {
        imgName = @"Btn_Delete_Open";
    }
    else if ([imageTag isEqualToString:@"Btn_Calendar"]) {
        imgName = @"Btn_Calendar_Gray_128";
    }
    else if ([imageTag isEqualToString:@"Btn_Calendar_Open"]) {
        imgName = @"Btn_Calendar_Green_128";
    }
    else if ([imageTag isEqualToString:@"Folder"]) {
        imgName = @"Folder";
    }
    else if ([imageTag isEqualToString:@"Btn_Folder_Open"]) {
        imgName = @"Btn_Folder_Open";
    }
    else if ([imageTag isEqualToString:@"Group"]) {
        imgName = @"Btn_Group_128";
    }
    else if ([imageTag isEqualToString:@"Tag"]) {
        imgName = @"Btn_Tag_128";
    }
    else if ([imageTag isEqualToString:@"Due"]) {
        imgName = @"Btn_Calendar_BlackBlue_128";
    }
    else if ([imageTag isEqualToString:@"Gain"]) {
        imgName = @"Btn_Gain_128";
    }
    else if ([imageTag isEqualToString:@"Detail"]) {
        imgName = @"Btn_Detail_128";
    }
    else if ([imageTag isEqualToString:@"Tab_Due"]) {
        imgName = @"Tab_Due";
    }
    else if ([imageTag isEqualToString:@"Tab_Tags"]) {
        imgName = @"Tab_Tags";
    }
    else if ([imageTag isEqualToString:@"Schedule"]) {
        imgName = @"Btn_Schedule";
    }
    else if ([imageTag isEqualToString:@"Delete"]) {
        imgName = @"BT_Delete";
    }
    else if ( [imageTag isEqualToString:@"Subtask"]) {
        imgName = @"AddSubtasks";
    }
    else {
        imgName = @"Btn_Help";
    }
    
    return imgName;
}

+(UIImage *) getGainImage:(NSUInteger) gainIndex {
    UIImage *retImg = nil;
    
    switch (gainIndex) {
            
        case 0:
            retImg = [UIImage imageNamed:[AppUtilities getImageName:@"highGain"]];
            break;
        case 1:
            retImg = [UIImage imageNamed:[AppUtilities getImageName:@"mediumGain"]];
            break;
        case 2:
            
            retImg = [UIImage imageNamed:[AppUtilities getImageName:@"normalGain"]];
            break;
        case 3:
            retImg = [UIImage imageNamed:[AppUtilities getImageName:@"lowGain"]];
            break;
        default:
            retImg = [UIImage imageNamed:[AppUtilities getImageName:@"normalGain"]];
            break;
    }
    return retImg;
}

+(UIView *) getSelectedBackgroundView {
    UIView *selectedView = [[UIView alloc]init];
//    selectedView.backgroundColor = [UIColor colorWithHue:0.612 saturation:.04 brightness:.96 alpha:1.0];
    selectedView.backgroundColor = [UIColor colorWithHue:0.557 saturation:1.0 brightness:1 alpha:1.0];
    return selectedView;
}

+(UIColor *) getSelectedTextHighlightColor {
    return [UIColor whiteColor];
}

// StrikesThrough the input string
+(NSAttributedString *) getAtrbutedString_Significant: (NSString *) inputStr Begin:(int)begin End:(int) end {
    
    // DO nothing to avoid crash if the text/label is not populated
    if (inputStr == nil)
        return nil;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString: inputStr];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Verdana" size:10] range:(NSRange){begin,end}];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSRange){begin,end}];
    return attributeString;
}

// StrikesThrough the input string
+(NSAttributedString *) getAtrbutedString_Striked: (NSString *) inputStr {
    
    // DO nothing to avoid crash if the text/label is not populated
    if (inputStr == nil)
        return nil;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString: inputStr];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    return attributeString;
}

// StrikesThrough the input string
+(NSAttributedString *) getAtrbutedWith_FontStriked: (NSString *) inputStr FontSize:(CGFloat) fSize {
    
    // DO nothing to avoid crash if the text/label is not populated
    if (inputStr == nil)
        return nil;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString: inputStr];
     [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Verdana" size:fSize] range:(NSRange){0,[attributeString length]}];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    return attributeString;
}
+(NSAttributedString *) getAtrbutedWith_FontNotStriked: (NSString *) inputStr FontSize:(CGFloat) fSize{
    
    // DO nothing to avoid crash if the text/label is not populated
    if (inputStr == nil)
        return nil;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString: inputStr];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Verdana" size:fSize] range:(NSRange){0,[attributeString length]}];

    return attributeString;
}


/** This API is added to add schedule an alarm **/
+ (void) scheduleAlarm: (NSString *) groupName AlarmDate: (NSDate *) alarmDate TaskName: (NSString *) taskName TaskObjectID: (NSString *)taskObjectID RemindInterval: (NSCalendarUnit) nscuRemindRepeatInterval {
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    // Get the current date
    NSDate *pickerDate = alarmDate;
    
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:pickerDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:pickerDate];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
	[dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    if (nscuRemindRepeatInterval != NSEraCalendarUnit ) { // ERA is internally used to indicate NEVER/No Reminder
        localNotif.repeatInterval = nscuRemindRepeatInterval;        
    }
    
    NSString *bodyText = @"";
    if (groupName == nil) {
        bodyText = [bodyText stringByAppendingString:EMPTY];
        bodyText = [bodyText stringByAppendingString:@" : "];
        bodyText = [bodyText stringByAppendingString:taskName];
    }
    else {
        bodyText = [bodyText stringByAppendingString: groupName];
        bodyText = [bodyText stringByAppendingString:@": "];
        bodyText = [bodyText stringByAppendingString:taskName];
    }
    
	// Notification details
    localNotif.alertBody = bodyText;
    
	// Set the action button
    localNotif.alertAction = NSLocalizedString(@"View", @"View");
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    
	// Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:taskObjectID  forKey:@"TaskObjectID"];
    localNotif.userInfo = infoDict;
    
	// Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    
    DLog(@"Added Schedule Body: %@, Alert: %@ itemDate: %@ repeatInterval: %d", bodyText, [pickerDate description], [itemDate description], nscuRemindRepeatInterval);
}

+ (BOOL) removeNotificationForTaskObjectID: (NSString *) modifiedTaskObjectID {
    
    BOOL removedAny = NO;

    UIApplication *app = [UIApplication sharedApplication];
    NSArray *notificationArray = [app scheduledLocalNotifications];
    
    for (UILocalNotification *item in notificationArray) {
        
        NSDictionary *infoDict = (NSDictionary *) item.userInfo;
        NSString *taskObjectID = [infoDict objectForKey:@"TaskObjectID"];
        
        if (taskObjectID.length == 0) {
            DLog(@"Error, unable to find the task Object ID for this reminder");
        }
        else if ( [taskObjectID isEqualToString: modifiedTaskObjectID] ) {
            [app cancelLocalNotification:item];
            DLog(@"Removed Notification on taskObjectID: %@ taskName: %@", taskObjectID, item.alertBody);
            removedAny = YES;
            break;
        }
    }
    
    return removedAny;
}

/* First delete the notification from schedular and then update the task */
+ (void) clearTaskAlertNotifications: (Task *) task {
    // DELETE The notification if any
    // Schedule the Alarm if applicable
    NSString *taskObjectID = [[[task objectID] URIRepresentation] absoluteString];
    [AppUtilities removeNotificationForTaskObjectID: taskObjectID ];
    
    task.alert_notification = 0;
    task.alert_notification_on = nil;
    task.remind_on = nil;
    task.remind_repeat_interval = [NSNumber numberWithInt:NSEraCalendarUnit];
}

+ (Group *) fetchGroupBySortOrder: (NSString *) sortOrder ManagedContext: (NSManagedObjectContext *) context {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Group" inManagedObjectContext:context]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(sort_order == %@)", sortOrder]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    DLog(@"FetchGroup: SortOrder: %@    Error: %@, %@", sortOrder, error, error.userInfo);
    if ([fetchResults count] > 0) {
        DLog(@"FetchGroup: Count: %i", [fetchResults count]);
        return [fetchResults objectAtIndex:0];
        
    }
    else {
        DLog(@"FetchGroup: Count: %i", [fetchResults count]);
        return nil;
    }
}

/*
 * Delete tasks older than x days
 */
+ (int) deleteCompletedTasks: (int) olderThan {
    
    DLog(@"Auto Delete completed task called");
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    
    
    NSDate *completedBeforeThisDate = [NSDate dateWithTimeIntervalSinceNow:-24*60*60*olderThan];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(done == 1 && completed_on < %@)", completedBeforeThisDate]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    DLog(@"Records Fetched for deletion are: %d", [fetchResults count]);
    
    int i = 0;
    for (i= 0; i < [fetchResults count]; i++) {
        Task *task = [fetchResults objectAtIndex:i];

        DLog(@"AppUtilities: Deleting Older Task: Name: %@ Created on: %@ Modified On: %@ Completed On Date:%@", task.name, [task.createdOn description], [task.modified_on description], [task.completed_on description]);

        [AppUtilities deleteTask:task];
    }
    
    return i;
}

/**
 * User click clear on ALert UI to reset the flag for all alert notification on every qualified task 
 */
+ (void) clearAllAlertNotification {
    
    DLog(@"Clear All Alert Notification, Badge and Applicationbadge");
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(alert_notification == 1) || (remind_on <= %@)", [NSDate date]]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    for (int i=0; i < [fetchResults count]; i++) {
        Task *task = [fetchResults objectAtIndex:i];
        task.alert_notification = 0;
        task.alert_notification_on = nil;
        task.remind_on = nil;
        task.remind_repeat_interval = [NSNumber numberWithInt:NSEraCalendarUnit];
    }
    
    [AppUtilities saveContext];
    
    [AppUtilities updateAlertBadge: 0];
}

+ (void) updateAlertBadge: (int) mode {
    
    switch (mode) {
        case 0: // Clear Badge
        {
            [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:nil];   
            
            // Reset the application badge number also
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            break;
        }
        case 1: // Increment Badge
        {
            int badgeCount = [[[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] badgeValue] intValue];
            badgeCount += 1;
            [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:[NSString stringWithFormat:@"%d",badgeCount]];
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;

            break;
        }
        case 2: // Update Badge from DB Count
        {
            int badgeCount = [AppUtilities countAlertNotificationItems];
            if ( badgeCount == 0 ) {
                [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:nil];   
                
                // Reset the application badge number also
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            }
            else {
                [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:[NSString stringWithFormat:@"%d",badgeCount]];
                [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
                
            }
            break;
        }
        default:
            break;
    }
}

+ (int) countAlertNotificationItems
{
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
   
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    // TODO: To avoid repeated fetch of recourring intervals fetching only repeat interval = 0. it is a workaround for now. Push notification to fix this.
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(alert_notification == 1) || (remind_on <= %@ && remind_repeat_interval == 0)", [NSDate date]]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    return count;
}


+(NSManagedObjectContext *)createManagedObjectContext{
    
    NSManagedObjectContext *context = nil;
    
    NSPersistentStoreCoordinator *coordinator = [APP_DELEGATE persistentStoreCoordinator];
    if (coordinator != nil) {
        context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setPersistentStoreCoordinator:coordinator];
    }
    return context;
}

+ (void) saveContext {
    
    [CoreDataOperations saveContext];
}

+ (BOOL) deleteTask: (Task *) task {
    // Delete the notification for this task before delete
    [AppUtilities clearTaskAlertNotifications: task];
    
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    [context deleteObject:task];
    
    // Before Deleting update the sorting order of all the task beyond the sorting order of task being deleted. This prevent duplicate sort order and hence bad tableview data
    int delTaskSortOrder = [task.sort_order intValue];
    NSArray *fetchedResults = [CoreDataOperations getTaskGreaterThanSortOrder:delTaskSortOrder Group:task.group];
    
    // Save Context
    [AppUtilities saveContext];
    
    // Saving context after doing the sorting order fix was resulting in crash when first itme of a group is deleted as the item was unavailable
    // could have queried the results after saving but then required task.group name
    // so retrieving the groups first, save context of delete operation, then perform the update on sorting order and finally save this also.
    for (int i=0; i < [fetchedResults count]; i++) {
        Task *t = (Task *)[fetchedResults objectAtIndex:i];
        t.sort_order = [NSNumber numberWithInt:delTaskSortOrder + i];
    }
    // Save Context
    [AppUtilities saveContext]; // TODO: If we delete a row in empty section all entries outside is deleted, bcz of the loop above and this context saving. Need to be fixed.
    
    // Google Custom
    [AppUtilities logAnalytics:@"Task: Deleted"];
    
    return YES;
}

+ (BOOL) saveTask: (Task *) task {
    
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    ;
    
    // PUt a temporary check for debugging
    NSCalendarUnit nscuRemindRepeatInterval = [task.remind_repeat_interval intValue];
    if (nscuRemindRepeatInterval == NSDayCalendarUnit || nscuRemindRepeatInterval == NSWeekCalendarUnit || nscuRemindRepeatInterval == NSMonthCalendarUnit || nscuRemindRepeatInterval == NSYearCalendarUnit || nscuRemindRepeatInterval == NSEraCalendarUnit || nscuRemindRepeatInterval == 0 ) {
        
    }
    else {
        ULog(@"Invalid Remind Repeat Interval: %d", nscuRemindRepeatInterval);
        DLog(@"ERROR - Invalid Repeat Interval: %d", nscuRemindRepeatInterval);
    }

    // Remove the alert if task is completed task
    if ( [task.done intValue] == 1 )
        [AppUtilities clearTaskAlertNotifications: task];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    // Google Custom
    [AppUtilities logAnalytics:@"Task: Saved"];
    
    return YES;
}

+ (Task *) addTask:(NSString *) name Gain:(int) gain DueOn:(NSDate *)dueOn RemindOn:remindOn Note:(NSString *)note Flag:(int) flag Group:(Group *)group EstimCountIdx: (int) estimCountIdx EstimUnitIdx: (int) estimUnitIdx Tags: (NSMutableSet *) selTags RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval {
   
    Task *newTask = [CoreDataOperations addTask:name Gain:gain DueOn:dueOn RemindOn:remindOn Note:note Flag:flag Group:group EstimCountIdx:estimCountIdx EstimUnitIdx:estimUnitIdx Tags:selTags RemindInterval:nscuRemindRepeatInterval];
    
    // Google Custom
    [AppUtilities logAnalytics:@"Task: Added"];
    
    return newTask;
}
+ (Tag *) addTag:(NSString *)tagName SortOrder:(int)sortOrder {
    Tag *tag = [CoreDataOperations addTag:tagName SortOrder:sortOrder];

    // Google Custom
    [AppUtilities logAnalytics:@"Tag: Added"];
    
    return tag;
}

+ (Group *) addGroup: (NSString *)groupName SortOrder:(int) sortOrder {
    Group* grp = [CoreDataOperations addGroup:groupName SortOrder:sortOrder];
    
    // Google Custom
    [AppUtilities logAnalytics:@"Group: Added"];
    
    return grp;
}
+ (Subtask *) addSubtaskForTask:(Task*)task Data:(NSString *)data Status:(int)status SortOrder:(int) sortOrder Type:(int) type {
    Subtask *subtask = [CoreDataOperations addSubtaskForTask:(Task*)task Data:data Status:status SortOrder:sortOrder Type:type];
    
    // Google Custom
    [AppUtilities logAnalytics:@"Subtask: Added"];

    
    return subtask;
}

+ (NSArray *) taskBetweenSortOrder:(int)sortOrderA AndSortOrder:(int)sortOrderB ForGroup:(Group *)group {
   return [CoreDataOperations taskBetweenSortOrder:sortOrderA AndSortOrder:sortOrderB ForGroup:group];
}

+ (NSDate *) getDateWithSecondsResetForDate: (NSDate *) date {
    
    if (date == nil)
        return date;
    
    NSDate              *   dateWithoutSeconds = date;
    NSDateComponents    *   startSeconds = [[NSCalendar currentCalendar] components: NSSecondCalendarUnit fromDate: dateWithoutSeconds];
    [startSeconds setSecond: -[startSeconds second]];
    dateWithoutSeconds = [[NSCalendar currentCalendar] dateByAddingComponents: startSeconds toDate: dateWithoutSeconds options: 0];
    
    return dateWithoutSeconds;
    
}

+ (NSString *)getSelectedTagsLabelText: (NSSet *) tags {
    NSString *tagLabelText = nil;
    
    // Move more code to here, after you done with updates in subsequent views rather using FINISH
    NSMutableArray *tagNamesArray = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for (Tag *tag in tags) {
        [tagNamesArray addObject:tag.name];
    }
    
    tagLabelText  = [tagNamesArray componentsJoinedByString:@", "];
    
    if (tagLabelText == nil || [tagLabelText length] == 0) {
        tagLabelText = NOT_SET;
    }
    
    return tagLabelText;
}

+ (NSArray *) getTaskGreaterThanSortOrder: (int) sortOrderVal Group:(Group *) group {
    return  [CoreDataOperations getTaskGreaterThanSortOrder:sortOrderVal Group:group];
}

// Customize the appearance of table view cells.
+ (void)configureCell:(TaskViewCell *)mvaCell Task: (Task*) task
{
    
    // debug code.
//    [[mvaCell titleLabel] setText:[task.name stringByAppendingFormat:@" %@ %d", task.group.name, [task.sort_order intValue]]];
    [[mvaCell titleLabel] setText:task.name];
    
    NSString *dueOnText = [AppUtilities getFormattedDateOnly:task.dueOn];
    [mvaCell.subtitleLabel setText: dueOnText];
    
    // Retain the old date label above and new labels added below
    /* HIDE these labels for now
    mvaCell.dayLbl.text = [AppUtilities mFormatedDateDay:activityAtIndex.dueOn];
    mvaCell.monthLbl.text = [AppUtilities mFormatedDateMonthName:activityAtIndex.dueOn];
    mvaCell.yearLbl.text = [AppUtilities mFormatedDateYearOnly:activityAtIndex.dueOn];

    // Trim the notes to make it look simpler
    NSString *note        = [AppUtilities cleanUpNotes:task.note];
    
     // Append dates to avoid use of additional spacing somewhere
    if (dueOnText && note) {
        NSString *str = [dueOnText stringByAppendingFormat:@" \t|| %@",note];
        NSAttributedString *atrStr = [AppUtilities getAtrbutedString_Significant:str Begin:0 End:([dueOnText length])];
        mvaCell.notesLbl.attributedText = atrStr;
        //        mvaCell.notesLbl.text = str;
    }
    else if (dueOnText) {
        mvaCell.notesLbl.text = dueOnText;
    }
    else {
        mvaCell.notesLbl.text = note;
    }
      */
    
    int progressAsInt =(int)([task.progress floatValue] + 0.5f);
    progressAsInt = progressAsInt*5;
    NSString *newText =[[NSString alloc] initWithFormat:@"%d",progressAsInt];
    [[mvaCell infoLabel1] setText: [newText stringByAppendingString:@"%"]];
    
    //    DLog(@"Gain: %@", activityAtIndex.gain);
    switch ([task.gain integerValue]) {
        case 0:
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"highGain"]] forState:UIControlStateNormal];
            //             mvaCell.statusButton2.backgroundColor = [UIColor redColor];
            break;
        case 1:
            //            mvaCell.statusButton2.backgroundColor = [UIColor blueColor];
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"mediumGain"]] forState:UIControlStateNormal];
            break;
        case 2:
            //            mvaCell.statusButton2.backgroundColor = [UIColor grayColor];
            //            mvaCell.statusButton2.titleLabel.text = @"B";//.backgroundColor = [UIColor redColor];
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"normalGain"]] forState:UIControlStateNormal];
            break;
        case 3:
            //            mvaCell.statusButton2.backgroundColor = [UIColor greenColor];
            //            cell.statusButton2.backgroundColor = [UIColor blueColor];
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"lowGain"]] forState:UIControlStateNormal];
            break;
        default:
            mvaCell.statusButton2.backgroundColor = [UIColor redColor];
            break;
    }
    
    switch ([task.done intValue]) {
        case 0: {
            [mvaCell.statusButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOff"]] forState:UIControlStateNormal];
            [mvaCell.statusButton1 setSelected:NO];
            
//            CGFloat oldFontSize = mvaCell.titleLabel.font.pointSize;
//            mvaCell.titleLabel.font = [mvaCell.titleLabel.font fontWithSize:oldFontSize - 2];
            // Make the title, duedate and the progress font colors, to show it is still pending.
//            [mvaCell titleLabel].font = [UIFont boldSystemFontOfSize:16];
//            [mvaCell titleLabel].textColor = [UIColor blackColor];

            break;}
        case 1: {
            [mvaCell.statusButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOn"]] forState:UIControlStateSelected];
            [mvaCell.statusButton1 setSelected:YES];
            
            [mvaCell titleLabel].attributedText    = [AppUtilities getAtrbutedString_Striked:mvaCell.titleLabel.text];
            [mvaCell subtitleLabel].attributedText = [AppUtilities getAtrbutedString_Striked:mvaCell.subtitleLabel.text];
            [mvaCell infoLabel1].attributedText    = [AppUtilities getAtrbutedString_Striked:mvaCell.infoLabel1.text];
            
            /* HIDE these labels for now
            [mvaCell dayLbl].attributedText   = [AppUtilities getAtrbutedString:mvaCell.dayLbl.text];
            [mvaCell monthLbl].attributedText = [AppUtilities getAtrbutedString:mvaCell.monthLbl.text];
            [mvaCell yearLbl].attributedText  = [AppUtilities getAtrbutedString:mvaCell.yearLbl.text]; 
             [mvaCell notesLbl].attributedText = [AppUtilities getAtrbutedString_Striked:mvaCell.notesLbl.text];
             */
            break;}
        default:
            break;
    }
    

    if ( task.remind_on != nil ) {
        [mvaCell.infoButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"alarmOn"]] forState:UIControlStateSelected];
        [mvaCell.infoButton1 setSelected:YES];
    }
    else {
        [mvaCell.infoButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.infoButton1 setSelected:NO];
    }
    
    if ( [task.flag intValue] == 1 ) {
        [mvaCell.infoButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOn"]] forState:UIControlStateSelected];
        [mvaCell.infoButton2 setSelected:YES];
    }
    else {
        [mvaCell.infoButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.infoButton2 setSelected:NO];
    }
    
    if ( task.note == nil || task.note.length == 0 ) {
        [mvaCell.notesButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.notesButton setSelected:NO];
    }
    else {
        [mvaCell.notesButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"notesOn"]] forState:UIControlStateSelected];
        [mvaCell.notesButton setSelected:YES];
    }
    
    // TODO: See why setting hte text label did not work
    if (task.tagS == nil || [task.tagS count] == 0 ) {
        mvaCell.tagsButton.titleLabel.text = @"";
        [mvaCell.tagsButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        
        [mvaCell.tagsButton setSelected:NO];
    }
    else {
        mvaCell.tagsButton.titleLabel.text = @"T";
        [mvaCell.tagsButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"tagSOn"]] forState:UIControlStateNormal];
        [mvaCell.tagsButton setSelected:YES];
    }
    
    //    [cell.statusButton addTarget:self action:@selector(changeStatus:) forControlEvents:UIControlEventTouchDown];
    // Set Self as delegate to allow calling the button operations to parent
    mvaCell.task = task;
    
    // iOS7 onwards the color was very odd so setting up manually
    mvaCell.selectedBackgroundView = [AppUtilities getSelectedBackgroundView];
    mvaCell.titleLabel.highlightedTextColor     = [AppUtilities getSelectedTextHighlightColor];
    mvaCell.subtitleLabel.highlightedTextColor  = [AppUtilities getSelectedTextHighlightColor];
    mvaCell.infoLabel1.highlightedTextColor     = [AppUtilities getSelectedTextHighlightColor];

    // Added subtask ability so connecting its icon and count btn
    int activeSTCnt = 0;
    for (Subtask *item in task.subtaskS) {
        if ([item.status intValue] == 0) {
            activeSTCnt++;
        }
    }
    if ( task.subtaskS != nil && activeSTCnt > 0) {
        [mvaCell.subtaskBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"Subtask"]] forState:UIControlStateSelected];
        [mvaCell.subtaskBtn setSelected:YES];
        

        [mvaCell.subtaskCntBtn  setTitle: [NSString stringWithFormat:@"%i", activeSTCnt] forState:UIControlStateDisabled];// Bug Fix: if we don't set for disabled state on doubleclick on toolbar button, counts were jumbled up
        // SETSELECTED NOT REQUIRED FOR COUNT ELSE IT COMES HIGHLIGHTED
    }
    else {
        [mvaCell.subtaskBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.subtaskBtn setSelected:NO];

        [mvaCell.subtaskCntBtn  setTitle:@"" forState:UIControlStateDisabled];// Bug Fix: if we don't set for disabled state on doubleclick on toolbar button, counts were jumbled up
    }

}

+(NSString *) cleanUpNotes: (NSString *) note {
    // Clean up temporarily for better vieweing
    if ([note length] > 0) {
        note = [note stringByReplacingOccurrencesOfString:@"\u2022  " withString:@""];
        note = [note stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
    }
    
    return note;
}

+(int) markUnmarkTaskComplete:(Task*)task {
    
    int status = -1;
    if (task) {
        switch ([task.done intValue]) {
            case 0:
            {
                task.done =  [NSNumber numberWithInt:1];
                task.completed_on = [NSDate date];
                
                // When marking task as done remove the alarm & reminder data
                [AppUtilities clearTaskAlertNotifications:task];
                //                self.task.completed_on = [NSDate dateWithTimeIntervalSinceNow:-24*60*60*40]; // Row added to force an older date to test delete older tasks
                status = 1; // MARKED
                break;
            }
            case 1:
            {
                task.done =  [NSNumber numberWithInt:0];
                task.completed_on = nil;
                status = 0; // UNMARKED
                break;
            }
            default:
                break;
        }
    }
    else {
        status = -1;
    }
    return status;
}

+ (NSString *) cleanDictSetPrint: (NSMutableDictionary *) localDictWithSet {
    //    return [localDictWithSet description];
    NSString *formatedStr = @"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t{";
    
    NSString *aakey;
    for (aakey in localDictWithSet) {
        
        NSString *key = [NSString stringWithFormat:@"%@", aakey];
        formatedStr = [formatedStr stringByAppendingString:key];
        formatedStr = [formatedStr stringByAppendingString:@" = ["];
        
        NSMutableSet *myset = [localDictWithSet objectForKey:aakey];
        for (NSNumber *aItem in [myset allObjects]) {
            formatedStr = [formatedStr stringByAppendingString:[ aItem stringValue] ];
            formatedStr = [formatedStr stringByAppendingString:@", "];
        }
        
        formatedStr = [formatedStr stringByAppendingString:@"]  "];
    }
    
    formatedStr = [formatedStr stringByAppendingString:@"}"];
    
    return formatedStr;
}

+ (NSPredicate *) generatePredicate {
    // default predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
    BOOL _excludeConditionFinishedTasks = YES;
    
    if ( [[AppSharedData getInstance].appSettings showCompletedTask] == 1) {
        _excludeConditionFinishedTasks = NO;
    }
    
    if (_excludeConditionFinishedTasks) {
        predicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
    }
    else {
        predicate = [NSPredicate predicateWithFormat:@"(done == 0 || done == 1)"];
    }
    
    return predicate;
}

//TASK_SORT_ORDER_START_IDX: IT IS RELATED HERE BUT NOT USED
// Added this API to get the max sort order value from existing tasks under the group of current task, we do so by taking the top record in desc order
+ (int) getMaxSortOrder: (Group *) group
{
    int retVal = 0;
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];

  /*
   fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order==max(sort_order)"];
    fetchRequest.sortDescriptors = [NSArray array];
*/
    NSSortDescriptor *orderByDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    [fetchRequest setFetchLimit:1];
    
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"group == %@", group]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchRequest !=nil && [fetchResults count] > 0) {
        Task *task = [fetchResults objectAtIndex:0];
        retVal = [task.sort_order intValue];
    }
    else {
        retVal = retVal; //DEFAULT VALUE
    }

//    DLog(@"getMaxSortOrder: Max Value: %i", retVal );
    return retVal;
}



// Reodering of tasks, important API
// Reusable common API
+ (void)updatedSortOrderForTasksInGroup:(Group *)group  sortOrderA:(int)sortOrderA sortOrderB:(int)sortOrderB moveDirection:(NSInteger)moveDirection {
    
   /* There was a case where we need to update the == case, so enaled it again.
    if (sortOrderA == sortOrderB) { // It is the last item, nothing to do. SortOrder starts with 1.
        NSLog(@"updatedSortOrderForTasksInGroup: FsortOrder: %d toSO: %d GroupName: %@. A==B, it might be last item, so nothing to do.", sortOrderA, sortOrderB, group.name);
        return;
    }*/
    
    if ( sortOrderA > sortOrderB) {
        NSLog(@"updatedSortOrderForTasksInGroup: FsortOrder: %d toSO: %d GroupName: %@. SortOrderA > sortOrderB, nothing to update.", sortOrderA, sortOrderB, group.name);
        return;
    }

    NSLog(@"updatedSortOrderForTasksInGroup: FsortOrder: %d toSO: %d GroupName: %@. STARTED", sortOrderA, sortOrderB, group.name);

    // Search items which can be updated in a sequential flow, inclusive of start and end sort order
    NSArray *fetchedResults = [AppUtilities taskBetweenSortOrder:sortOrderA AndSortOrder:sortOrderB ForGroup:group];
    for (int i=0; i < [fetchedResults count]; i++)         {
        Task *item = (Task *) [fetchedResults objectAtIndex:i];
        NSNumber *newPosition = [NSNumber numberWithInt: sortOrderA + i + moveDirection];
        NSLog(@"ItemMoved: InSeqItem CurSortOrder:%i NewSortOrder:%d Name:%@ Group:%@",  [item.sort_order intValue], [newPosition intValue], item.name, item.group.name);
        
        item.sort_order =  newPosition;
    }
}

+ (BOOL) moveTaskOutOfGroup:(Group *)srcGroup AtIndex:(int) srcTaskSortorder {
   
    NSLog(@"moveTaskOutOfGroup: %@ AtIndex:%d", srcGroup.name, srcTaskSortorder);
    
    if (srcGroup == nil  ) {
        return NO; // Incomplete information sent
    }
    
    // Find out the direction we moving the objects. Use the direction to update the sort_order of the impacted objects
    NSInteger moveDirection = -1; // UP => +1 MOVE DOWN => -1, 0 => NOT SET - WE MANUALLY SET TO -1 in this operation
    
    // Find end point till where sort order to be updated
    int maxSortOrder = [AppUtilities getMaxSortOrder:srcGroup];
    
    // Start from sort order ahead of current task sort order till the max value of a sort order i.e. till end of data in this group
    int sortOrderA = srcTaskSortorder + 1; // Start Sort Order (inclusive of this value)
    int sortOrderB = maxSortOrder;         // End   Sort Order (inclusive of this value)

    [self updatedSortOrderForTasksInGroup:srcGroup sortOrderA:sortOrderA sortOrderB:sortOrderB moveDirection:moveDirection];
    
    return YES;
}

// It is same as moveTaskOUT but retained to make easy to understand
+(BOOL) moveTaskINGroup:(Group *)dstGroup AtIndex:(int) dstIndex {
    NSLog(@"moveTaskINGroup: %@ AtIndex:%d", dstGroup.name, dstIndex);
    
    if (dstGroup == nil  ) {
        return NO; // Incomplete information sent
    }
    
    // Direction of move is like move item in between from bottom i.e. INSERT = MOVE UP
    NSInteger moveDirection = 1; // UP => +1 MOVE DOWN => -1, 0 => NOT SET
    
     // Find end point till where sort order to be updated
    int maxSortOrder = [AppUtilities getMaxSortOrder:dstGroup];
    
    int sortOrderA = dstIndex;      // Start Sort Order (inclusive of this value)
    int sortOrderB = maxSortOrder;  // End   Sort Order (inclusive of this value)
    
    // Update sort order in destination cells
    [self updatedSortOrderForTasksInGroup:dstGroup sortOrderA:sortOrderA sortOrderB:sortOrderB moveDirection:moveDirection];

    return YES;
}

/*
 * Query to test this api behaviour: SELECT  b.zname, a.zsort_order, a.zname, a.* FROM ztask a, zgroup b where a.zgroup = b.z_pk order by b.z_pk
 */
// MOVE TASK FROM TABLE TO COLLECTION GROUP
+ (void) moveDragedTask:(Task *)srcTask ToAGroup:(Group *)dstGroup{
    if (srcTask == nil || dstGroup == nil) {
        NSLog(@"CommonDualSectionVC: ERROR STATE!!! - Source Task or Destination group cannot be found");
        return; // do nothing as not possible to do the movement.
    }
    if ( srcTask.group == dstGroup)
        return; // nothing to do return as task is already in same group
    
    Group *srcGroup = srcTask.group;
    
    int srcTaskSortOrder = [srcTask.sort_order intValue];
    
    // Update the sort order in source group
    [AppUtilities moveTaskOutOfGroup:srcGroup AtIndex:srcTaskSortOrder];
    
    // Perform the group name swapping now
    srcTask.group = dstGroup;
    // Update the task sort order to be the last in the destination group (i.e. current max + 1)
    int dstTaskSortOrder = [AppUtilities getMaxSortOrder:dstGroup];
    srcTask.sort_order = [NSNumber numberWithInt:dstTaskSortOrder + 1];
    
    
    [AppUtilities saveContext];
}

/*
+(void) moveWithinGroupSrcTask:(Task*)srcTask ToATask:(Task*) dstTask MoveDirection:(int)moveDirection{
    // Prepare sortOrder from lower to higher to prepare right query extracting all items impacted. Excluding the item which is impacted bcz for it, would be a value jump
    // Either the top item when moving item down or the last item when moving up
    // Before doing this hold the value of the final destination item to use later as this item is getting updated in a sequential update
    int toSortOrderBak = [dstTask.sort_order intValue];
    
    int sortOrderA = 0;
    int sortOrderB = 0;
    if ( moveDirection == 1 ) { // MOVING UP
        sortOrderA = [srcTask.sort_order intValue];
        sortOrderB = [dstTask.sort_order intValue] - 1; // excluding the item itself
    }
    else {
        sortOrderA = [srcTask.sort_order intValue] + 1; // excluding the item itself
        sortOrderB = [dstTask.sort_order intValue];
    }
    DLog(@"ItemMoved: FsortOrder: %d toSO: %d FromIdx: %d to: %d Direction:%i SortOrderRange(%i:%i) %@ to: %@ ", [fromTask.sort_order intValue], [toTask.sort_order intValue], fromIndexPath.row, toIndexPath.row, moveDirection, sortOrderA, sortOrderB, fromTask.name, toTask.name);
    
    
    // Search items which can be updated in a sequential flow
    NSArray *fetchedResults = [AppUtilities taskBetweenSortOrder:sortOrderA AndSortOrder:sortOrderB ForGroup:fromTask.group];
    for (int i=0; i < [fetchedResults count]; i++)         {
        Task *item = (Task *) [fetchedResults objectAtIndex:i];
        NSNumber *newPosition = [NSNumber numberWithInt: sortOrderA + i + moveDirection];
        DLog(@"ItemMoved: InSeqItem CurSortOrder:%i NewSortOrder:%d Name:%@",  [item.sort_order intValue], [newPosition intValue], item.name);
        
        item.sort_order =  newPosition;
    }
    
    // Finally modify the item which is actually moved using the value to the destination item
    DLog(@"ItemMoved: MovedItem CurSortOrder:%i NewSortOrder:%d Name:%@", [fromTask.sort_order intValue], toSortOrderBak, fromTask.name);
    fromTask.sort_order = [NSNumber numberWithInt: toSortOrderBak];
    
    //    [self forceReloadThisUI];
    //  [tableView reloadData]; // It continued giving duplicate rows when scrolling more than 5 or more records or moving down ful and moving back up. TO prevent, refershing the load.
    
    // Saving the context at the end when rearranging is finally completed. doing it here was leading to some rows getting deleted but not getting inserted.
    //    [AppUtilities saveContext];

}
*/
/* MOVE A TASK FROM GROUP A TO GROUP B
 * Logic
 *      1. Move the task out of source group i.e. update sort order of all tasks below the moving out task
 *      2. INSERT a task in destination  group i.e. update sort order of all tasks starting from the dest task sort order
 *      3. update the source task to take position of dest task in dest group.
 */
+ (void) moveDragedTask:(Task *)srcTask ToATask:(Task *)dstTask{
//    DLog(@"moveDragTask: FGroup:%@ TGroup:%@ FIdx:%d TIdx:%d FTask:%@ ToTask%@ ", srcTask.group.name, dstTask.group.name, srcTask.name, dstTask.name);
    
    if (srcTask == nil || dstTask == nil) {
        NSLog(@"CommonDualSectionVC: ERROR STATE!!! - Source Task or Destination group cannot be found");
        return; // do nothing as not possible to do the movement.
    }
    if ( srcTask == dstTask)
        return; // nothing to do return as task is already in same group

    int srcTaskSortOrder = [srcTask.sort_order intValue];
    srcTask.sort_order = [NSNumber numberWithInt:-100]; // setting this to a value not in the sequence so that when we query the records to update the dest group, we don't include this task.
    
    /****** DEAL WITH SRC TASK GROUP */
    [AppUtilities moveTaskOutOfGroup:srcTask.group AtIndex:srcTaskSortOrder];
    [AppUtilities saveContext]; // Save multiple times to avoid a conflict if updated in the end as required index is not available then.
    
    NSNumber *tmpDstTaskSortOrder = dstTask.sort_order; // Backup
    
    
    /****** DEAL WITH DEST GROUP */
    if (srcTask.group == dstTask.group) { // Pulled the code directly here to ensure we use right sort order. If it is in same group we should not process those records which are below the task which is moved out, to avoid it being modified once again. this code can be reworked to make it simple.
        
        // Direction of move is like move item in between from bottom i.e. INSERT = MOVE UP
        NSInteger moveDirection = 1; // UP => +1 MOVE DOWN => -1, 0 => NOT SET
        
        int sortOrderA = [dstTask.sort_order intValue];      // Start Sort Order (inclusive of this value)
        int sortOrderB = srcTaskSortOrder;  // End   Sort Order (inclusive of this value)
        
        // Update sort order in destination cells
        [self updatedSortOrderForTasksInGroup:dstTask.group sortOrderA:sortOrderA sortOrderB:sortOrderB moveDirection:moveDirection];
        
        // Disabled this good common code to above one to fix bug as mentioned above.
//        [AppUtilities moveTaskINGroup:dstTask.group AtIndex:[srcTask.sort_order intValue]];
        [AppUtilities saveContext]; // Save multiple times to avoid a conflict if updated in the end as required index is not available then.
        
    }
    else {
        [AppUtilities moveTaskINGroup:dstTask.group AtIndex:[dstTask.sort_order intValue]];
        [AppUtilities saveContext]; // Save multiple times to avoid a conflict if updated in the end as required index is not available then.
    }
    /* DEAL WITH SRC TASK */
    srcTask.group      = dstTask.group;
    srcTask.sort_order = tmpDstTaskSortOrder;     // Update the task sort order to be the last in the destination group
    
    [AppUtilities saveContext]; // Save multiple times to avoid a conflict if updated in the end as required index is not available then.
}


/*** Common Reusable API ***/

// Generic API to show dual section view
+(void) showDualSectionView:(id) caller {
    /* We can now show the dual section even if filtered. the only thing we need to do is to avoid the drag if filter is on. */
//    BOOL filterApplied = [[AppSharedData getInstance].dynamicViewData checkValuesSetToDefaults];
    
    // Due to some error/crash if the groups are filtered and drag and drop performing, restrictig showing up of this view if filter is on.
//    if (!filterApplied) {
        /* Present next run loop. Prevents "unbalanced VC display" warnings. */
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //            [self performSegueWithIdentifier: @"ShowRearrangeViewController" sender: self]; // Removing this old view with a new view but retaining for some time
            [caller performSegueWithIdentifier: @"ShowDualSectionViewController" sender: self];
        });
//    }
}

// SECTION FOR RUN ONCE OPERATIONS

// Single API to be invoked from outside to run all runonce operations in required order
+ (void) runOnceOperations {
    // Upgrade Data Operations
    [AppUtilities runOnceOnDeviceFixSortOrderOfAllTasks];
     
     // Insert Default Data if not
     [AppUtilities runOnceAcrossDevice_InsertDefaultData];
    
    // Fix data
    [AppUtilities runOnceOperations_EachLaunch];
    
    // Upgrade for notes
    [AppUtilities runOnceOnDeviceUpdgradeNotesToSubtasks];
}

// Run this API once to fix the data before 2.1 version of this app. Arrange Sort Order for all existing tasks. If we don't do there were cases when data is reorganized
+ (void) runOnceOnDeviceFixSortOrderOfAllTasks {
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName   = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersionName = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersionName = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    NSString *version = [NSString stringWithFormat:@"%@, Version %@ (%@)",
                         appDisplayName, majorVersionName, minorVersionName];
    
    float majorVersion = [majorVersionName floatValue];
    float minorVersion = [minorVersionName floatValue];
    
    DLog(@"Version Info: %@ %f %f", version, majorVersion, minorVersion );
    
    // Check Version First and if it may require upgrade then check if it is not upgraded and perform the upgrades
    if (majorVersion <= 3.0 || (majorVersion == 3.0 && minorVersion <= 1)) {
     
        if ( [[AppSharedData getInstance].appSettings runOnceOnDeviceFixSortOrderOfAllTasks] == 0 ) {
            
            // First: Upgrade the sort order from older no clear order to well ordered
            [CoreDataOperations updateAllTasksSortOrder];
            
            // Second: to be developed as required
            
            // Save the setting, indicating it has been run once now
            [[AppSharedData getInstance].appSettings saveSettingForRunOnceOnDeviceFixSortOrderOfAllTasks: 1];
        }
    }
}

// Insert Default Data if there are no records existing and not run once already
+ (void) runOnceAcrossDevice_InsertDefaultData {
    // Check if the default groups have been created or not
    
    if ( [[AppSharedData getInstance].appSettings runOnceAcrossDeviceInsertedDefaultData] == 0) {
        
        if ( [CoreDataOperations countGroups] == 0 && [CoreDataOperations countTasks] == 0 ) {
            // Ensure no new records are added yet
            [CoreDataOperations insertDefaultData];
        }
    }
    
    //Save the setting, indicating it has been run once now
    [[AppSharedData getInstance].appSettings saveSettingForRunOnceAcrossDeviceInsertedDefaultData: 1];
}

// Run on each launch of app
+ (void) runOnceOperations_EachLaunch {
    
    // Drag and drop was creating bugs, not found yet, so as a workaround attempt to fix sort order when we start
    [CoreDataOperations fixSortOrder];
}

+ (void) runOnceOnDeviceUpdgradeNotesToSubtasks {
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName   = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersionName = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersionName = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    NSString *version = [NSString stringWithFormat:@"%@, Version %@ (%@)",
                         appDisplayName, majorVersionName, minorVersionName];
    
    float majorVersion = [majorVersionName floatValue];
    float minorVersion = [minorVersionName floatValue];
    
    DLog(@"Version Info: %@ %f %f", version, majorVersion, minorVersion );
    
    // Check Version First and if it may require upgrade then check if it is not upgraded and perform the upgrades
    if (majorVersion < 4.2) {
     
        // Safety Check
        NSNumber *countAllSubtasks = [CoreDataOperations aggregateOperation:@"count:" onEntity:@"Subtask" onAttribute:@"sort_order" withPredicate:nil inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
        
        if ( [[AppSharedData getInstance].appSettings runOnceOnDeviceUpgradeNotesToSubtasks] == 0 && [countAllSubtasks intValue] <= 0) {
            
         
            
            // First: Upgrade Notes to subtasks
            [CoreDataOperations runOnceOnDeviceUpgradeNotesToSubtasks];
            
            // Second: to be developed as required
            
            // Save the setting, indicating it has been run once now
            [[AppSharedData getInstance].appSettings saveSettingForRunOnceOnDeviceUpgradeNotesToSubtasks: 1];
        }
    }
}

// Google
+ (void) logAnalytics:(NSString *) screenName {
    
    // Google Tracker - Manual record and push
    [APP_DELEGATE.tracker set:kGAIScreenName value:screenName];
    [APP_DELEGATE.tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    //FLURRY:
    [Flurry logEvent:screenName];
}

@end
