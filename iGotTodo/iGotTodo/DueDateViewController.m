//
//  DueDateViewController.m
//  realtodo
//
//  Created by Me on 17/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "DueDateViewController.h"
#import "AppUtilities.h"
#import "CommonDTO.h"

@interface DueDateViewController ()
//@property (nonatomic, readonly, getter=isEditingDate) BOOL editingDate;

@property (nonatomic, weak) NSDate *tmpDate;
@property (nonatomic) NSIndexPath *lastIndexPath;
@property (nonatomic) BOOL enableDate;

@end

@implementation DueDateViewController
@synthesize datePicker;
@synthesize enableDateLabel = _remindOnLabel;
@synthesize enableDateSwitch = _remindOnSwitch;
@synthesize resetBtn = _resetBtn;
@synthesize dataTextView;
@synthesize delegate;
//@synthesize editingDate=_editingDate;
@synthesize commonDTOObj = _editedObject;
@synthesize tmpDate = _tmpDate;
@synthesize lastIndexPath = _lastIndexPath;
@synthesize enableDate = _enableDate;
@synthesize dueOnChoiceSegment = _dueOnChoiceSegment;

//BOOL hasDeterminedWhetherEditingDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
        
    // Google Custom
    [AppUtilities logAnalytics:@"View Opened: DueDate"];
    
    // Set the title to the user-visible name of the field.
    self.title = [self.commonDTOObj getEditFieldName];
    
    [self.datePicker addTarget:self action:@selector(dateChanged)
              forControlEvents:UIControlEventValueChanged];
    
    UIFont *Boldfont = [UIFont boldSystemFontOfSize:12.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:Boldfont forKey:UITextAttributeFont];
    [self.dueOnChoiceSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    [self.dueOnChoiceSegment addTarget:self action:@selector(dueOnChoiceSegment_ValueChanged:)
                      forControlEvents:UIControlEventValueChanged];
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.dueOnChoiceSegment setTitle:NSLocalizedString(@"Today", @"Today") forSegmentAtIndex:0];
    [self.dueOnChoiceSegment setTitle:NSLocalizedString(@"Day+", @"Day+") forSegmentAtIndex:1];
    [self.dueOnChoiceSegment setTitle:NSLocalizedString(@"Week+", @"Week+") forSegmentAtIndex:2];
    [self.dueOnChoiceSegment setTitle:NSLocalizedString(@"Month+", @"Month+") forSegmentAtIndex:3];
    [self.dueOnChoiceSegment setTitle:NSLocalizedString(@"Year+", @"Year+") forSegmentAtIndex:4];
    [self.dueOnChoiceSegment setTitle:NEVER forSegmentAtIndex:5];
    
}

- (NSDate *) increaseMonthYearByOne: (NSString *) MonthYear {
    
    // This method is to increase the Month or Year by 1 when clicking on Month+ or Year+
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    if ([MonthYear isEqualToString:NSLocalizedString(@"Month", @"Month")])
        components.month = 1;
    
    else
        components.year = 1;
    
    
    NSDate *datePickerDate = self.datePicker.date;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *nextMonth = [gregorian dateByAddingComponents:components toDate:datePickerDate options:0];

    return nextMonth;
}

- (IBAction) dueOnChoiceSegment_ValueChanged:(id)sender {
    
    _enableDate = YES;
    datePicker.enabled = TRUE; // If date picker is disabled using the NEVER option, enable it before selecting any date
    int selIdx = [sender selectedSegmentIndex];
    NSDate *datePickerDate = self.datePicker.date;
    
    switch (selIdx) {
        case 0: {
            //            self.datePicker.date = [AppUtilities getDateWithSecondsResetForDate:[AppUtilities uiUNFormatedDate:nil WithDayAdd:YES]];
            datePickerDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]]; // it is easy to understand if it is today.
            break;
        }
        case 1: {
            datePickerDate = [datePickerDate dateByAddingTimeInterval:(1*24*60*60)];
            break;
        }
        case 2:{
            datePickerDate = [datePickerDate dateByAddingTimeInterval:(7*24*60*60)];
            break;
        }
        case 3:{
            datePickerDate = [self increaseMonthYearByOne: NSLocalizedString(@"Month", @"Month")];
            break;
        }
        case 4: {
            datePickerDate = [self increaseMonthYearByOne: NSLocalizedString(@"Year", @"Year")];
            //            [self performSegueWithIdentifier:@"ShowCustomDaysViewController" sender:self];
            break;
        }
        case 5: {   // NEVER
            datePickerDate = [AppUtilities getDateWithSecondsResetForDate:[NSDate date]];
            datePicker.enabled = FALSE;
            _enableDate = NO;
            // self.selRemindOn = nil; // Don't nullify here but when you save from this UI. For better UI experience retain the old date as use navigates to different option.
            break;
        }
        default:
            break;
    }
    
    // Finally set it over the selected date variable
    self.datePicker.date = datePickerDate;
    [self dateChanged];
}

- (void) dateChanged {
    
    if (!_enableDate) {
        _enableDate = YES;
    }
    self.enableDateLabel.text = [NSString stringWithFormat:@"%@", [AppUtilities uiFormatedDate:self.datePicker.date]];
    
    if (!datePicker.enabled) {
        self.enableDateLabel.text = NEVER;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //    NSString *tmpKey = self.editedFieldKey;
    
    // Configure the user interface according to state.
    
    if ([self.commonDTOObj editFieldType] == dueOn || self.commonDTOObj.editFieldType == remindOn) {
        self.dataTextView.hidden = YES;
        self.datePicker.hidden = NO;
        self.enableDateLabel.hidden = NO;
        self.enableDateSwitch.hidden = NO;
        self.resetBtn.hidden = NO;
        
        self.datePicker.date = (NSDate *) [self.commonDTOObj getEditFieldValue];
        
        _enableDate = YES;
        [self dateChanged];
    }
    else {
        
        self.dataTextView.hidden = NO;
        self.datePicker.hidden = YES;
        self.enableDateLabel.hidden = YES;
        self.enableDateSwitch.hidden = YES;
        self.resetBtn.hidden = YES;
        
        self.dataTextView.text = (NSString *) [self.commonDTOObj getEditFieldValue];
        
        //        self.dataTextView.placeholder = self.title;
        [self.dataTextView becomeFirstResponder];
    }
}



- (void)viewDidUnload
{
    [self setDataTextView:nil];
    [self setDatePicker:nil];
    [self setEnableDateSwitch:nil];
    [self setEnableDateLabel:nil];
    [self setResetBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (IBAction)save:(id)sender {
    //    // Set the action name for the undo operation.
    //    NSUndoManager * undoManager = [[self.editedObject managedObjectContext] undoManager];
    //    [undoManager setActionName:[NSString stringWithFormat:@"%@", self.editedFieldName]];
    
    //    DLog(@"CommonObj Note: %@, DueOn:%@, Name:%@ RemindOn:%@", self.commonDTOObj.note, [self.commonDTOObj.dueOn description], self.commonDTOObj.name, self.commonDTOObj.remindOn);
    // Pass current value to the edited object, then pop.
    if (self.commonDTOObj.editFieldType == dueOn || self.commonDTOObj.editFieldType == remindOn) {
        
        //        self.commonDTOObj.editedFieldType
        
        if (self.commonDTOObj.editFieldType == dueOn || self.commonDTOObj.editFieldType == remindOn) {
            
            if (_enableDate) {
                [self.commonDTOObj setEditFieldValue: self.datePicker.date];
                
            } else {
                [AppUtilities removeNotificationForTaskObjectID: self.commonDTOObj.taskObjectID];
                [self.commonDTOObj setEditFieldValue: nil];
                
            }
        }
        else {
            [self.commonDTOObj setEditFieldValue:self.datePicker.date];
        }
    }
    else {
        [self.commonDTOObj setEditFieldValue:self.dataTextView.text];
    }
    
    //    DLog(@"Saving as Note:%@, DueOn:%@, Name:%@, RemindOn: %@", self.editedObject.note, [self.editedObject.dueOn description], self.editedObject.name, self.editedObject.remindOn);
    
    [self.delegate dueDateViewControllerDidFinish: self.commonDTOObj FormatedDate:self.enableDateLabel.text];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)resetBtnClick:(id)sender {
    self.datePicker.date = [NSDate date];
}

- (IBAction)bottomSaveBtnClick:(id)sender {
    [self save:sender];
}


@end
