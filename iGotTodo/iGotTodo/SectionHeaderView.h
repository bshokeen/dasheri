//
//  SectionHeaderView.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/11/16.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>


#define SECTION_OPEN  0
#define SECTION_CLOSE 1
#define SECTION_ADD_ITEM 2


@protocol SectionHeaderViewDelegate;

@interface SectionHeaderView : UIView
// Controls
@property (weak, nonatomic) IBOutlet UIImageView *groupTopLineLbl;
@property (weak, nonatomic) IBOutlet UIImageView *collapseImg;
@property (weak, nonatomic) IBOutlet UILabel *sectionIndexLbl;
@property (weak, nonatomic) IBOutlet UILabel *sectionNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *sectionCountBtn;
@property (weak, nonatomic) IBOutlet UIImageView *groupBottomLineLbl;

// Attributes
@property  BOOL sectionIsOpen;
@property (nonatomic, assign) NSInteger sectionIdx;
@property (nonatomic, assign) NSInteger sectionItemsCount;
@property (nonatomic, strong) NSString *groupManagedObjectID;

// Delegates
@property (nonatomic, weak) id <SectionHeaderViewDelegate> delegate;

// Methods
-(void) updateSectionViewData:(BOOL) isOpen Index:(NSInteger) sectionIndex Name:(NSString*)sectionName Count:(NSInteger) sectionItemsCount GrpMgdObjID:(NSString*)groupManagedObjectID Tag:(NSInteger) tagNumber Delegate:(id<SectionHeaderViewDelegate>) delegate;

-(void)toggleOpenWithUserAction:(BOOL)userAction;

-(void) updateCountOnDelete: (int) recordsDeleted;

// Action/Operations

- (IBAction)test:(id)sender;

@end

/*
 Protocol to be adopted by the section header's delegate; the section header tells its delegate when the section should be opened and closed.
 */
@protocol SectionHeaderViewDelegate <NSObject>

//@optional
-(void)uitableSectionHeaderView:(SectionHeaderView*)sectionHeaderView Section:(NSInteger) section Operation:(int) sectionOperation;

@end