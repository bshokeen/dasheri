//
//  GroupCollViewCell.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2014/02/26.
//  Copyright (c) 2014 dasherisoft. All rights reserved.
//

#import "GroupCollViewCell.h"
#import "CommonDualViewController.h" // imported for one of the type to use here

@implementation GroupCollViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) resetInitialText {
    
    [self iconImg].image = nil;
    [[self iconImg2] setImage:nil forState:UIControlStateNormal];
    
    [[self name] setText:@""];
    [[self countBtn] setTitle:@"" forState:UIControlStateDisabled];
    [[self countBtn] setTitle:@"" forState:UIControlStateNormal];
    
    [self setTag:TypeOfCellIsNormal]; // We using tags in identifying cells so resetting that. TypeOfCellIsNormal
}

@end
