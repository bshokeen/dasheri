//
//  AddOnLangViewController.m
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/08/15.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//  Use an array to show data on table view, this is populated on start from common appshared. these key are localized, user selection is stored in plist file.

#import "AddOnLangViewController.h"

@interface AddOnLangViewController ()

@property(nonatomic, retain) NSMutableArray *addOnLangOptions;

@end

@implementation AddOnLangViewController

@synthesize addOnLangOptions = _addOnLangOptions;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    
    self.addOnLangOptions = [[AppSharedData getInstance] addnLangSupportedUIValArr];
}

/*
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if ( indexPath.row ==  [[AppSharedData getInstance].appSettings addOnLanguageUserSelection]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [self.addOnLangOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AddOnLangItemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14]];
    cell.textLabel.text = [self.addOnLangOptions objectAtIndex:indexPath.row];
    
    if ( indexPath.row == [[AppSharedData getInstance].appSettings addOnLanguageUserSelection] ) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
            cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (  indexPath.row == [[AppSharedData getInstance].appSettings addOnLanguageUserSelection] ) {
        return; // No change needed.
    }
    
    // Set up the locale language if user has setup their own language
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *locale = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    
    DLog(@"AddOnLangViewController - Old Language Setting: Lang:%@ Locale:%@ All:%@", language, locale, [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);

    
    // Based on selection override the default application language setting.
    switch (indexPath.row) {
        case 0: {  //Reset back to normal
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
            break;
        }
        case 1: { // Set to Hindi Language
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:@"hi"]  forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
        case 2: { // Set to Afrikaans Language
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:@"af"]  forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
        case 3: { // Force English
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:@"en"]  forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
        default:
            break;
    }

    // Get the updated values
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    locale = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    DLog(@"AddOnLangViewController - Save Language Setting: Lang:%@ Locale:%@ All:%@", language, locale, [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);

    // Save user selection permanently
    [[AppSharedData getInstance].appSettings saveSettingForAddOnLanguageUserSelection:indexPath.row];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    // Inform the user to kill & restart the application
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"Settings", @"Settings")
                          message:NSLocalizedString(@"Language Changed.\nKill & start the app again.\nPress home button, press twice again, press & hold on running app icons, kill running app and start again.", @"Language Changed. Stop, kill current instance using home button and restart the app.")
                          delegate:nil cancelButtonTitle:OK
                          otherButtonTitles:nil, nil];
    [alert show];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
