//
//  AddOnLangViewController.h
//  iGotTodo
//
//  Created by Balbir Shokeen on 2013/08/15.
//  Copyright (c) 2013 dasherisoft. All rights reserved.
//
// This class was added to support the UI allowing user to select and override the default iOS language

#import <UIKit/UIKit.h>
#import "AppSharedData.h"

@interface AddOnLangViewController : UITableViewController

@end
