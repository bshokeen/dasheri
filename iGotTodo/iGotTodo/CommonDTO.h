//
//  CommonDTO.h
//  iGotTodo
//
//  Created by Me on 18/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "Group.h"


typedef enum {name, note, dueOn, remindOn, group} EditFieldType;


@interface CommonDTO : NSObject

// Name of the field which is being edited on this object. Later on I can make this an array or dictionary.
@property (nonatomic) EditFieldType editFieldType;

@property (strong, nonatomic) NSString *editFieldName;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *note;
@property (strong, nonatomic) NSDate *dueOn;
@property (strong, nonatomic) NSDate *remindOn;
//@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) NSString *taskObjectID;
@property (strong, nonatomic) Group *group;
//- (void) populate:(Task *) task;

//// Attributes to be shared with EffortViewController
//@property (nonatomic) int effortCountIndx;
//@property (nonatomic) int effortUnitIdx;

-(id) initWithValue: (NSString *)name Note:(NSString *)note DueOn:(NSDate *)dueOn RemindOn:(NSDate *)remindOn Group:(Group *)group TaskObjectID:(NSString *)taskObjectID;

- (void) setEditFieldValue:(id) value;
- (NSString *) getEditFieldName;
- (id) getEditFieldValue;

@end
