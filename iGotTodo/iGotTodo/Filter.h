//
//  Filter.h
//  iGotTodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Filter : NSManagedObject

@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSDate * deleted_on;
@property (nonatomic, retain) NSNumber * dirty;
@property (nonatomic, retain) NSString * filter_id;
@property (nonatomic, retain) NSString * filter_name;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSString * sync_id;
@property (nonatomic, retain) NSDate * updated_on;

@end
