//
//  GroupViewViewController.m
//  iGotTodo
//
//  Created by Me on 18/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabViewsViewController.h"
#import "AppDelegate.h" // Included to get the manged object context macro

@implementation TabViewsViewController

@synthesize groupSegment = _segment;


#pragma mark View lifecycle

- (void)viewDidLoad
{
    self.thisInterfaceEntityName = @"TabManageViewController";
    
    // Google Custom
    [AppUtilities logAnalytics:@"Tab Access: Views"];
    
    // UISegment has a bug from apple - unable to show localized values so manually setting them here with localized strings.
    [self.groupSegment setTitle:NSLocalizedString(@"Group", @"Group") forSegmentAtIndex:0];
    [self.groupSegment setTitle:NSLocalizedString(@"Due", @"Due") forSegmentAtIndex:1];
    [self.groupSegment setTitle:NSLocalizedString(@"Gain", @"Gain") forSegmentAtIndex:2];
    [self.groupSegment setTitle:NSLocalizedString(@"%", @"%") forSegmentAtIndex:3];
    [self.groupSegment setTitle:NSLocalizedString(@"Status", @"Status") forSegmentAtIndex:4];

    [self.groupSegment addTarget:self
                                  action:@selector(didChangeGroupSegmentControl:)
                        forControlEvents:UIControlEventValueChanged];
    
//    self.navigationItem.leftBarButtonItem = self.editButtonItem; // No longer required due to dynamic drag n droop
    
    [super viewDidLoad];   
    
    // To save the space for extra segments not having an edit button. Will use some other smaller image rather
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

- (void)didChangeGroupSegmentControl:(UISegmentedControl *)control {
    
    self.selSegmentIdx = self.groupSegment.selectedSegmentIndex;
    
    [self forceReloadThisUI];
}

- (void)viewDidUnload
{

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
 
    [self setGroupSegment:nil];   
}

- (void) viewDidAppear:(BOOL)animated {
    
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
        [self forceReloadThisUI];
    }
}

// Using did rotate to ensure we do what we want after rotation of parent view is done, no problem found but protective check.
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if(self.interfaceOrientation == UIDeviceOrientationPortrait || self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown){
        NSLog(@"UNHANDLED");
    } else  if(self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight){
        
        [AppUtilities showDualSectionView: self];
    }
}

#pragma mark Fetched results controller

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}


#pragma mark Fetched results controller

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    NSArray *sortDescriptors = nil;
    NSString *sectionNameKeyPath = nil;
    NSPredicate *filterPredicate = [AppUtilities generatePredicate];
    switch( self.groupSegment.selectedSegmentIndex) {
        case 0:
        { 
            // Create the sort descriptors array.
            NSSortDescriptor *grpSortOrderDescriptor = [[NSSortDescriptor alloc]initWithKey:@"group.sort_order" ascending:YES];

//            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];

            
            sortDescriptors = [[NSArray alloc] initWithObjects: grpSortOrderDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"group.sort_order";
            
            break;
        }
        case 1:
        { 
            // Create the sort descriptors array.
            NSSortDescriptor *gainDescriptor = [[NSSortDescriptor alloc]initWithKey:@"dueOn" ascending:YES];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];
            
            sortDescriptors = [[NSArray alloc] initWithObjects:gainDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"dueOnSectionName";
            
            break;
        }
        case 2:
        { 
            // Create the sort descriptors array.
            NSSortDescriptor *gainDescriptor = [[NSSortDescriptor alloc]initWithKey:@"gain" ascending:YES];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];

            sortDescriptors = [[NSArray alloc] initWithObjects:gainDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"gainSectionName";
            
            break;
        }
        case 3:
        {
            // Create the sort descriptors array.
            NSSortDescriptor *doneDescriptor = [[NSSortDescriptor alloc]initWithKey:@"progress" ascending:NO];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];

            sortDescriptors = [[NSArray alloc] initWithObjects:doneDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"progressSectionName";
            break;
        }
            break;
        case 4:
        {
            // Create the sort descriptors array.
            NSSortDescriptor *doneDescriptor = [[NSSortDescriptor alloc]initWithKey:@"done" ascending:NO];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            sortDescriptors = [[NSArray alloc] initWithObjects:doneDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"doneSectionName";
            break;
        }
        default:
            break;
    }

    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Create and initialize the fetch results controller.
    self.fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                    managedObjectContext:APP_DELEGATE_MGDOBJCNTXT 
                                                                      sectionNameKeyPath:sectionNameKeyPath
                                                                               cacheName:nil];
    self.fetchedResultsControllerObj.delegate = self;
    
    DLog(@"GroupViewController: Group:%@ getFetchResultsCtroller: RE-QUERY", sectionNameKeyPath);
    
    // Memory management.
    return self.fetchedResultsControllerObj;
}  

#pragma mark - Test Ability to rearrange items
-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
        
        // Google Custom
        [AppUtilities logAnalytics:@"Tab Access: Views: Editing On"];

    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![self.fetchedResultsControllerObj.managedObjectContext save:&error]) {
            DLog(@"Could not save context: %@", error);
        }
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPathOLD:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // If no change return
    if ( (fromIndexPath.row == toIndexPath.row) && (fromIndexPath.section == toIndexPath.section ) ) {
        return;
    }
    
    // Google Custom
    [AppUtilities logAnalytics:@"Operation Move: Tried"];
    
    NSInteger moveDirection = 1; // UP => +1 MOVE DOWN => -1
    
    // Retrieve the lower & higher row indexes for looping
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    // Get the Tasks which are moving for reference and logging
    Task *fromGroup = (Task *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Task *toGroup   = (Task *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    NSLog(@"itemMoved: %@ to: %@ FsortOrder: %d toSO: %d FromIdx: %d to: %d Direction:%i", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row, moveDirection);
    
    // Store the index of the toIndexPath Item as it will be overwritten in the loop
    //        fromGroup.sort_order = toGroup.sort_order;
    NSLog(@"CurrentObject:%@ Ctr:%i FromRowIdx:%i CurSortOrder:%i NewSortOrder:%d", fromGroup.name, 0, fromIndexPath.row, [fromGroup.sort_order intValue], (toIndexPath.row  + 1));
    fromGroup.sort_order = [NSNumber numberWithInt: (toIndexPath.row  + 1)];
    
    if ( moveDirection == 1 ) { // Exclude the higher index from loop, increase sort order for others
        // Update sort order for remaining items
        for (int i = lowerIndexPath.row ; i < higherIndexPath.row; i++) {
            
            NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
            Task *curObj = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:curIndexPath];
            
            NSNumber *newPosition = [NSNumber numberWithInteger:i + 1 + moveDirection];
            
            
            NSLog(@"CurrentObject:%@ Ctr:%i FromRowIdx:%i CurSortOrder:%i NewSortOrder:%d", curObj.name, i, curIndexPath.row, [curObj.sort_order intValue], [newPosition intValue]);
            curObj.sort_order = newPosition;
            
        }
    }
    else { // Exclude the lower index from loop, reduce sort order for others
        
        // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
        for (int i = lowerIndexPath.row + 1; i <= higherIndexPath.row; i++) {
            
            NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
            Task *curObj = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:curIndexPath];
            
            NSNumber *newPosition = [NSNumber numberWithInteger:i + 1 + moveDirection];
            
            NSLog(@"CurrentObject:%@ Ctr:%i FromRowIdx:%i CurSortOrder:%i NewSortOrder:%d", curObj.name, i, curIndexPath.row, [curObj.sort_order intValue], [newPosition intValue]);
            curObj.sort_order = newPosition;
            
        }
    }
    
    // Saving the context at the end when rearranging is finally completed. doing it here was leading to some rows getting deleted but not getting inserted.
    //    [AppUtilities saveContext];
}

// Override to support rearranging the table view. Here we update the sorting order for all the impacted records.
// Initial code updated only the visible items which are visible on UI. Used the tableview indexes. But when we have completed tasks in between they end up having duplicate values. So changed to consider extracting all items included completed tasks and then updating sort_order of all items even if they are not visible on the UI.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // If no change return
    if ( (fromIndexPath.row == toIndexPath.row) && (fromIndexPath.section == toIndexPath.section ) ) {
        return;
    }
    
    // Get the Tasks which are moving for reference and logging
    Task *fromGroup = (Task *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Task *toGroup   = (Task *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];

    // Find out the direction we moving the objects. Use the direction to update the sort_order of the impacted objects
    NSInteger moveDirection = 1; // UP => +1 MOVE DOWN => -1, 0 => NOT SET
    
    // Retrieve the lower & higher row indexes. We will move from top to bottom updating the items impacted, consider excluding the item actually moving
    NSIndexPath *lowerIndexPath  = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1; // MOVE ITEM DOWN
        
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Task *a = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:lowerIndexPath];
    Task *b = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:higherIndexPath];

    // Prepare sortOrder from lower to higher to prepare right query extracting all items impacted. Excluding the item which is impacted bcz for it, would be a value jump
    // Either the top item when moving item down or the last item when moving up
    // Before doing this hold the value of the final destination item to use later as this item is getting updated in a sequential update
    int toSortOrderBak = [toGroup.sort_order intValue];
    
    int sortOrderA = 0;
    int sortOrderB = 0;
    if ( moveDirection == 1 ) { // MOVING UP
        sortOrderA = [a.sort_order intValue];
        sortOrderB = [b.sort_order intValue] - 1; // excluding the item itself
    }
    else {
        sortOrderA = [a.sort_order intValue] + 1; // excluding the item itself
        sortOrderB = [b.sort_order intValue];
    }
    DLog(@"ItemMoved: FsortOrder: %d toSO: %d FromIdx: %d to: %d Direction:%i SortOrderRange(%i:%i) %@ to: %@ ", [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row, moveDirection, sortOrderA, sortOrderB, fromGroup.name, toGroup.name);
    
    
    // Search items which can be updated in a sequential flow
    NSArray *fetchedResults = [AppUtilities taskBetweenSortOrder:sortOrderA AndSortOrder:sortOrderB ForGroup:fromGroup.group];
    for (int i=0; i < [fetchedResults count]; i++)         {
        Task *item = (Task *) [fetchedResults objectAtIndex:i];
        NSNumber *newPosition = [NSNumber numberWithInt: sortOrderA + i + moveDirection];
        DLog(@"ItemMoved: InSeqItem CurSortOrder:%i NewSortOrder:%d Name:%@",  [item.sort_order intValue], [newPosition intValue], item.name);
        
        item.sort_order =  newPosition;
    }
    
    // Finally modify the item which is actually moved using the value to the destination item
    DLog(@"ItemMoved: MovedItem CurSortOrder:%i NewSortOrder:%d Name:%@", [fromGroup.sort_order intValue], toSortOrderBak, fromGroup.name);
    fromGroup.sort_order = [NSNumber numberWithInt: toSortOrderBak];
    
//    [self forceReloadThisUI];
  //  [tableView reloadData]; // It continued giving duplicate rows when scrolling more than 5 or more records or moving down ful and moving back up. TO prevent, refershing the load.
    
    // Saving the context at the end when rearranging is finally completed. doing it here was leading to some rows getting deleted but not getting inserted.
//    [AppUtilities saveContext];
    
    DLog(@"ItemMoved: Updated Sort Order for impacted items, including completed tasks even if they are visible or not.");
}

/*
 * To prevent movement of records outside the section
 */
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
//    This implementation will prevent re-ordering outside of the original section like Phil's answer, but it will also snap the record to the first or last row of the section, depending on where the drag went, instead of where it started.
//    if (sourceIndexPath.section != proposedDestinationIndexPath.section) {
//        NSInteger row = 0;
//        if (sourceIndexPath.section < proposedDestinationIndexPath.section) {
//            row = [tableView numberOfRowsInSection:sourceIndexPath.section] - 1;
//        }
//        return [NSIndexPath indexPathForRow:row inSection:sourceIndexPath.section];     
//    }
//    
//    return proposedDestinationIndexPath;

// HERE WE PREVENT DRAG N DROP FROM A GROUP/SECTION TO ANOTHER
    if( sourceIndexPath.section != proposedDestinationIndexPath.section )
    {
        return sourceIndexPath;
    }
    else
    {
        return proposedDestinationIndexPath;
    }

}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Allow rearranging only for first index as we have only 1 field in the database to store sort order. So it cannot be for each group presently.
    if ( self.groupSegment.selectedSegmentIndex == 0 ) {
        // Return NO if you do not want the item to be re-orderable.
        return YES;
    }
    else {
        return NO;
    }
}


// ACTIONS
- (IBAction)editDoneBtnClick:(id)sender {
    
    [AppUtilities saveContext]; // Saving context once we click the edit/done button, intentionally. Saving after each reorg we were having some records getting deleted unintentionally.
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Support for Drag & Drop Reordering
// 1. select the tableview in the storyboard and use the custom class
// 2. Implement its delegate
// 3. Copied the code from my move api to this move API.
// This method is called when starting the re-ording process. You insert a blank row object into your
// data source and return the object you want to save for later. This method is only called once.
- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Index: row:%i section:%i", indexPath.row, indexPath.section);
    //    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:indexPath.section];
    
    
    NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    Task *curObj = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:curIndexPath];
    
    //    [_fetchedResultsController replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    
    /*
     NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:indexPath.section];
     id object = [section objectAtIndex:indexPath.row];
     [section replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
     */
    return curObj;
}

- (void)moveRowAtIndexPath1:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    // If no change return
    if ( (fromIndexPath.row == toIndexPath.row) && (fromIndexPath.section == toIndexPath.section ) ) {
        return;
    }
    
    // Get the Tasks which are moving for reference and logging
    Task *srctask = (Task *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Task *dstTask   = (Task *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    
    [AppUtilities moveDragedTask:srctask ToATask:dstTask];
    
    [self forceReloadThisUI];
}

// This method is called when the selected row is dragged to a new position. You simply update your
// data source to reflect that the rows have switched places. This can be called multiple times
// during the reordering process.
- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    // If no change return
    if ( (fromIndexPath.row == toIndexPath.row) && (fromIndexPath.section == toIndexPath.section ) ) {
        return;
    }
    
    // Get the Tasks which are moving for reference and logging
    Task *fromTask = (Task *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Task *toTask   = (Task *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    
    // Find out the direction we moving the objects. Use the direction to update the sort_order of the impacted objects
    NSInteger moveDirection = 1; // UP => +1 MOVE DOWN => -1, 0 => NOT SET
    
    // Retrieve the lower & higher row indexes. We will move from top to bottom updating the items impacted, consider excluding the item actually moving
    NSIndexPath *lowerIndexPath  = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1; // MOVE ITEM DOWN
        
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Task *a = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:lowerIndexPath];
    Task *b = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:higherIndexPath];
    
    // Prepare sortOrder from lower to higher to prepare right query extracting all items impacted. Excluding the item which is impacted bcz for it, would be a value jump
    // Either the top item when moving item down or the last item when moving up
    // Before doing this hold the value of the final destination item to use later as this item is getting updated in a sequential update
    int toSortOrderBak = [toTask.sort_order intValue];
    
    int sortOrderA = 0;
    int sortOrderB = 0;
    if ( moveDirection == 1 ) { // MOVING UP
        sortOrderA = [a.sort_order intValue];
        sortOrderB = [b.sort_order intValue] - 1; // excluding the item itself
    }
    else {
        sortOrderA = [a.sort_order intValue] + 1; // excluding the item itself
        sortOrderB = [b.sort_order intValue];
    }
    DLog(@"ItemMoved: FsortOrder: %d toSO: %d FromIdx: %d to: %d Direction:%i SortOrderRange(%i:%i) %@ to: %@ ", [fromTask.sort_order intValue], [toTask.sort_order intValue], fromIndexPath.row, toIndexPath.row, moveDirection, sortOrderA, sortOrderB, fromTask.name, toTask.name);
    
    
    // Search items which can be updated in a sequential flow
    NSArray *fetchedResults = [AppUtilities taskBetweenSortOrder:sortOrderA AndSortOrder:sortOrderB ForGroup:fromTask.group];
    for (int i=0; i < [fetchedResults count]; i++)         {
        Task *item = (Task *) [fetchedResults objectAtIndex:i];
        NSNumber *newPosition = [NSNumber numberWithInt: sortOrderA + i + moveDirection];
        DLog(@"ItemMoved: InSeqItem CurSortOrder:%i NewSortOrder:%d Name:%@",  [item.sort_order intValue], [newPosition intValue], item.name);
        
        item.sort_order =  newPosition;
    }
    
    // Finally modify the item which is actually moved using the value to the destination item
    DLog(@"ItemMoved: MovedItem CurSortOrder:%i NewSortOrder:%d Name:%@", [fromTask.sort_order intValue], toSortOrderBak, fromTask.name);
    fromTask.sort_order = [NSNumber numberWithInt: toSortOrderBak];
    
    //    [self forceReloadThisUI];
    //  [tableView reloadData]; // It continued giving duplicate rows when scrolling more than 5 or more records or moving down ful and moving back up. TO prevent, refershing the load.
    
    // Saving the context at the end when rearranging is finally completed. doing it here was leading to some rows getting deleted but not getting inserted.
    //    [AppUtilities saveContext];
    
    DLog(@"ItemMoved: Updated Sort Order for impacted items, including completed tasks even if they are visible or not.");
}



// This method is called when the selected row is released to its new position. The object is the same
// object you returned in saveObjectAndInsertBlankRowAtIndexPath:. Simply update the data source so the
// object is in its new position. You should do any saving/cleanup here.
- (void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath; {
    NSLog(@"Index: row:%i section:%i", indexPath.row, indexPath.section);
    /*
     NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:indexPath.section];
     [section replaceObjectAtIndex:indexPath.row withObject:object];
     // do any additional cleanup here
     */
    
    /*
    // Get the Tasks which are moving for reference and logging
    Task *srctask = (Task *)object;
    Task *dstTask   = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    [AppUtilities moveDragedTask:srctask ToATask:dstTask];
    
    [self forceReloadThisUI];
     */
}

// Save & Restore state api implementation

-(void) encodeRestorableStateWithCoder:(NSCoder *)coder {
    
    [super encodeRestorableStateWithCoder:coder];
    
    [coder encodeInt:self.groupSegment.selectedSegmentIndex   forKey:@"TabViewsOnVC_groupSegment"];
}

- (void) decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    int oldIdx = [coder decodeIntForKey:@"TabViewsOnVC_groupSegment"];
    [self.groupSegment setSelectedSegmentIndex:oldIdx];
}

@end
