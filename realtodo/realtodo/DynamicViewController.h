//
//  DynamicViewController.h
//  realtodo
//
//  Created by Me on 12/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "CommonViewController.h"

@interface DynamicViewController : CommonViewController
< UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *uiSearchBar;
@property (weak, nonatomic) IBOutlet UILabel *totalRowsLabel;
//@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

- (IBAction)leftNavBarItemClicked:(id)sender;

@end
