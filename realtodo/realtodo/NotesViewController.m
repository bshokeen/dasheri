//
//  NotesViewController.m
//  realtodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "NotesViewController.h"

@interface NotesViewController ()
    @property (nonatomic) BOOL editOn;
    @property (nonatomic, strong) UIBarButtonItem *editDoneButton;
    @property   (nonatomic, strong) NSString *lastCharacterEntered;
    @property (nonatomic) int lastCharacterEnteredLoc;
    @property BOOL bulletModeActive;
@end

@implementation NotesViewController

@synthesize dataTextView = _dataTextView;
@synthesize commonDTOObj = _commonDTOObj;
@synthesize delegate = _delegate;

@synthesize editOn = _editOn;
@synthesize editDoneButton;
@synthesize lastCharacterEntered = _lastCharacterEntered;
@synthesize lastCharacterEnteredLoc = _lastCharacterEnteredLoc;
@synthesize bulletModeActive = _bulletModeActive;

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.editDoneButton          = [[UIBarButtonItem alloc] 
                                                initWithTitle:@"Edit" style:UIBarButtonItemStyleDone target:self action:@selector(editDoneItemBtnClick:)];
   /*
    UIBarButtonItem *bListButton         = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                            target:self
                                            action:@selector(bulletListItemBtnClick:)];
   
    
    UIBarButtonItem *clearButton          = [[UIBarButtonItem alloc] 
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                            target:self action:@selector(clearItemBtnClick:)];
      */
    
    self.navigationItem.rightBarButtonItems =
    [NSArray arrayWithObjects:self.editDoneButton,  nil]; //clearButton,
    
    self.dataTextView.layer.cornerRadius = 10.0;
    self.dataTextView.delegate = self; // Get all the events to this controller.
    
    // TO make it easy to start editing adding the ability to edit on tap of the UITextView
    UITapGestureRecognizer* uiTextViewTap = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(uiTextViewTapped:)];
//    _editOn = NO;
//    [self toggleEditing];
    uiTextViewTap.numberOfTapsRequired = 1;
    [self.dataTextView setUserInteractionEnabled:YES];
    [self.dataTextView addGestureRecognizer:uiTextViewTap];
}

- (void)viewDidUnload
{
    [self setDataTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [self setDataTextView:nil];
}
- (void) viewWillAppear:(BOOL)animated {
    self.dataTextView.text = (NSString *) [self.commonDTOObj getEditFieldValue];
     [self.dataTextView becomeFirstResponder];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
       return YES;
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//TODO: This has some more bugs to be fixed.
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range  replacementText:(NSString *)text
{
    BOOL returnWithoutChange = YES;
    int noOfCharsToReplace=0;

    // See if the characters being verified are in sequence or not
    if ( range.location == (self.lastCharacterEnteredLoc+1) ) {
        
        if ( ([self.lastCharacterEntered isEqualToString:@"*"] ||  [self.lastCharacterEntered isEqualToString:@"@"])  && [text isEqualToString:@"\n"]  )
        {
            self.bulletModeActive = YES;
            
            noOfCharsToReplace = 2;
            
            NSString *temp = self.dataTextView.text;
            // With blank note trying bullet listing giving error. Work around below used.
            if (range.location - noOfCharsToReplace == -1) noOfCharsToReplace-=1;
//            NSLog(@"TextView: Data: %@ Lenght:%d Range:%d,%d, cal:%d", temp, [temp length], NSRangeFromString(temp).location, NSRangeFromString(temp).length, range.location - noOfCharsToReplace);

            NSString *modifiedString = [temp stringByReplacingCharactersInRange:NSMakeRange(range.location - noOfCharsToReplace, noOfCharsToReplace) withString:@"\n\u2022  "]; 
            [self.dataTextView setText:modifiedString];

            self.lastCharacterEntered = text;
            self.lastCharacterEnteredLoc = range.location - noOfCharsToReplace + 4;

            self.dataTextView.selectedRange = NSMakeRange(self.lastCharacterEnteredLoc, 0);
            
            returnWithoutChange = NO;
            
//            NSLog(@"NotesView: Location: %d Length:%d lastCharLoc: %d", range.location, range.length, self.lastCharacterEnteredLoc);
        }
        else if([self.lastCharacterEntered isEqualToString:@"\n"] && [text isEqualToString:@"\n"] && self.bulletModeActive )
        {
            self.bulletModeActive = NO;
            noOfCharsToReplace = 4;
            
            NSString *temp = self.dataTextView.text;
        
            self.dataTextView.text = [temp
                                      stringByReplacingCharactersInRange:NSMakeRange (range.location-noOfCharsToReplace, noOfCharsToReplace) withString:@""];

            self.lastCharacterEnteredLoc = range.location - noOfCharsToReplace;
            self.lastCharacterEntered = text;
            
            self.dataTextView.selectedRange = NSMakeRange(self.lastCharacterEnteredLoc, 0);

            returnWithoutChange = NO;
//             NSLog(@"NotesView: Location: %d Length:%d Loc-2: %d", range.location, range.length, range.location-noOfCharsToReplace);
            
            
        }
        else if ( self.bulletModeActive  && [text isEqualToString:@"\n"]) {
            NSString *temp = self.dataTextView.text;
            NSString *modifiedString = [temp stringByReplacingCharactersInRange:NSMakeRange(range.location, 0) withString:@"\n\u2022  "]; 
            [self.dataTextView setText:modifiedString];
            
            self.lastCharacterEnteredLoc = range.location + 4;
            self.lastCharacterEntered = text;    
            
            self.dataTextView.selectedRange = NSMakeRange(self.lastCharacterEnteredLoc, 0);
            
            returnWithoutChange = NO;
//             NSLog(@"NotesView: Location: %d Length:%d lastCharLoc: %d", range.location, range.length, self.lastCharacterEnteredLoc);
        }
        else {
            // No Chars to Replace
            noOfCharsToReplace = 0;
            
            self.lastCharacterEntered = text;
            self.lastCharacterEnteredLoc = range.location;
            
//            NSLog(@"NotesView: Location: %d Length:%d lastCharLoc: %d", range.location, range.length, self.lastCharacterEnteredLoc);
        }
    } else {
        self.lastCharacterEntered = text;
        self.lastCharacterEnteredLoc = range.location;
        
//        NSLog(@"NotesView: Location: %d Length:%d lastCharLoc: %d", range.location, range.length, self.lastCharacterEnteredLoc);
    }

    return returnWithoutChange;
}

-(void) toggleEditing {
    if (self.editOn) 
    {
        [self.commonDTOObj setEditFieldValue:self.dataTextView.text];
        
        [self.delegate notesViewControllerDidFinish: self.commonDTOObj];
        [self.navigationController popViewControllerAnimated:YES];  
        
    }
    else 
    {
        self.editDoneButton.title = @"Done";
        
        self.dataTextView.dataDetectorTypes = UIDataDetectorTypeAll;
        self.dataTextView.editable = YES;
        int w=self.dataTextView.frame.size.width;
        int h=self.dataTextView.frame.size.height;
        DLog(@"WIdth %i: height: %i", w, h);
        CGRect frame = self.dataTextView.frame;
        frame.size.height = 167;//199 or self.dataTextView.contentSize.height;
        self.dataTextView.frame = frame;

        
        self.editOn = YES;
        
        [self.dataTextView becomeFirstResponder];
        
        // TODO: Show the height of the notes view to full screen on start, reduce by half when we start editing.
        //        self.tableView.rowHeight = self.tableView.rowHeight/2 ;
    }
}

- (IBAction) editDoneItemBtnClick :(id)sender {
    [self toggleEditing];
}

// TO make it easy to start editing adding the ability to edit on tap of the UITextView
- (void)uiTextViewTapped:(UIGestureRecognizer*)recognizer {
    if (!self.editOn) {
        [self toggleEditing];
    }
}

/*
- (IBAction) bulletListItemBtnClick :(id)sender {
    UIActionSheet *sheet;
    sheet = [[UIActionSheet alloc] initWithTitle:@"Modify Selection"
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:@"Delete"
                               otherButtonTitles:@"Bullet List (* * *) ", @"Number List (1 2 3 ...)", nil];
    
    [sheet showFromRect:self.view.bounds inView:self.view animated:YES];
    
    // Show the sheet
    [sheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
        if (buttonIndex == actionSheet.cancelButtonIndex)
        { return; }
        else if ( buttonIndex == actionSheet.destructiveButtonIndex) {
            self.dataTextView.text = nil;
        }
        else if ( buttonIndex == 1) // Bullet List
        {
            NSLog(@"Button Index: %d", buttonIndex);
//            NSMutableString * bulletList = [NSMutableString stringWithCapacity:items.count*30];
//            for (NSString * s in items)
//            {
//                [bulletList appendFormat:@"\u2022 %@\n", s];
//            }
//            textView.text = bulletList;

            
        }
        else if ( buttonIndex == 2) // Number List
        {
            NSLog(@"Button Index: %d", buttonIndex);
        }
    
    NSLog(@"Button %d", buttonIndex);
}
 */

- (IBAction) clearItemBtnClick:(id)sender  {
    
    self.dataTextView.text = @"";
    [self toggleEditing];
}

@end
