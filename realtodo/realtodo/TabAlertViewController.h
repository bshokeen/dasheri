//
//  TabAlertViewController.h
//  realtodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "UpdateTaskViewController.h"
#import "AddTaskViewController.h"
#import "TaskViewCell.h"
#import "Task.h"
#import "AppUtilities.h"

@interface TabAlertViewController : UITableViewController
                                    <UpdateTaskViewControllerDelegate, NSFetchedResultsControllerDelegate>


@property (nonatomic, strong) NSManagedObjectContext        *mgdObjContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerObj;


- (IBAction)clearBtnClick:(id)sender;

@end
