//
//  Constants.m
//  realtodo
//
//  Created by Me on 04/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "AppConstants.h"


NSString * const NEVER = @"Never";
NSString * const EMPTY = @"Empty";

NSString * const PREFS_MY_CONSTANT = @"prefs_my_constant";

NSString * const CONFIG_KEY_COLLAPSIBLE_ON     = @"collapsibleOn";
NSString * const CONFIG_KEY_DISPLAYTYPE        = @"displayType";
NSString * const CONFIG_KEY_ORDER_BY           = @"orderBy";
NSString * const CONFIG_KEY_ORDER_BY_DIRECTION = @"orderByDirection";
NSString * const CONFIG_KEY_EXCLUDE_COMPLETED  = @"completed";

NSString * const COLLAPSIBLEON_GROUP           = @"group.sort_order";
NSString * const COLLAPSIBLEON_DATE            = @"dueOn";
NSString * const COLLAPSIBLEON_GAIN            = @"gain";
NSString * const COLLAPSIBLEON_DONE            = @"done";
NSString * const COLLAPSIBLEON_PERCENTAGE      = @"percentage";
NSString * const COLLAPSIBLEON_FLAG            = @"flag";

NSString * const DISPLAYTYPE_ALL               = @"All";
NSString * const DISPLAYTYPE_TODAY             = @"Today";
NSString * const DISPLAYTYPE_WEEK              = @"Week";
NSString * const DISPLAYTYPE_OVERDUE           = @"Overdue";
NSString * const DISPLAYTYPE_NODUE             = @"No due";
NSString * const DISPLAYTYPE_STAR              = @"Star";

// NSString * const ORDERBY_DUEON_ASC             = @"dueOn";
// NSString * const ORDERBY_DUEON_DESC            = @"dueOn DESC";
// NSString * const ORDERBY_GAIN_ASC              = @"gain ASC";
// NSString * const ORDERBY_GAIN_DESC             = @"gain DESC";
// NSString * const ORDERBY_PERCENTAGE_ASC        = @"percentage ASC";
// NSString * const ORDERBY_PERCENTAGE_DESC       = @"percentage DESC";
// NSString * const ORDERBY_DONE_ASC              = @"done ASC";
// NSString * const ORDERBY_DONE_DESC             = @"done DESC";

NSString * const ORDERBY_DUEON                 = @"dueOn";
NSString * const ORDERBY_GAIN                  = @"gain";
NSString * const ORDERBY_PERCENTAGE            = @"percentage";
NSString * const ORDERBY_DONE                  = @"done";

NSString * const ORDER_BY_DIRECTION_ASC        = @"YES";
NSString * const ORDER_BY_DIRECTION_DESC       = @"NO";

NSString * const EXCLUDE_COMPLETED             = @"(done == 0)";

NSString * const SOURCE_UI_ADDUI             = @"FilterUI";
NSString * const SOURCE_UI_FILTERUI          = @"AddUI";

// For Remind Options
NSString * const REMINDON_SELECTED_DATE  = @"selectedDate";
NSString * const REMINDON_DAILY          = @"daily";
NSString * const REMINDON_WEEKDAYS       = @"weekDays";
NSString * const REMINDON_MONTHLY        = @"monthly";
NSString * const REMINDON_CUSTOMDAYS     = @"customDays";
NSString * const REMINDON_NEVER          = @"never";

NSString * const REMIND_INTERVAL_SELECTED_DATE = @"";
NSString * const REMIND_INTERVAL_DAILY         = @"Every Day at";
NSString * const REMIND_INTERVAL_WEEKDAYS      = @"Week Days at";
NSString * const REMIND_INTERVAL_MONTHLY       = @"Every Month at";
NSString * const REMIND_INTERVAL_CUSTOMDAYS    = @"Custom Days at";
NSString * const REMIND_INTERVAL_NEVER         = @"Never";

