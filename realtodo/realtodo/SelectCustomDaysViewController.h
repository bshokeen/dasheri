//
//  SelectCustomDaysViewController.h
//  realtodo
//
//  Created by Balbir Shokeen on 9/27/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectCustomDaysViewControllerDelegate;


@interface SelectCustomDaysViewController : UITableViewController

@property (strong, nonatomic) id <SelectCustomDaysViewControllerDelegate> delegate;


@end


@protocol SelectCustomDaysViewControllerDelegate <NSObject>

- (void)customDaysTVControllerDidFinish:(SelectCustomDaysViewController *)controller;

@end