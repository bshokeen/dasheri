//
//  Group.m
//  realtodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "Group.h"
#import "Task.h"


@implementation Group

@dynamic color;
@dynamic created_on;
@dynamic deleted_on;
@dynamic dirty;
@dynamic group_id;
@dynamic icon_id;
@dynamic modified_on;
@dynamic name;
@dynamic sort_order;
@dynamic tasks;

@end
