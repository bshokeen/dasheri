//
//  CodeDumpBackup.h
//  realtodo
//
//  Created by Me on 12/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CodeDumpBackup : NSObject
+(UIImage *)makeRoundCornerImage:(UIImage*)img :(int) cornerWidth :(int) cornerHeight;

@end
