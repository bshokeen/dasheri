//
//  UpdateTaskViewController.h
//  realtodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "Group.h"
#import "DueDateViewController.h"
#import "TabGroupsViewController.h"
#import "AppUtilities.h"
#import "EffortViewController.h"
#import "TabTagsViewController.h"
#import "ReminderViewController.h"
#import "NotesViewController.h"


@protocol UpdateTaskViewControllerDelegate;


@interface UpdateTaskViewController : UITableViewController
                                    <UISplitViewControllerDelegate, UITextFieldDelegate, MoreTextViewControllerDelegate, 
                                        TabGroupsViewControllerDelegate, EffortViewControllerDelegate, ReminderViewControllerDelegate, 
                                        NotesViewControllerDelegate>

@property (weak, nonatomic) id <UpdateTaskViewControllerDelegate> delegate;
@property (strong, nonatomic) Task *task;


@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gainOptions;
@property (weak, nonatomic) IBOutlet UILabel *dueOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *progressValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *remindOnLabel;
@property (weak, nonatomic) IBOutlet UIButton *flagBtn;
@property (weak, nonatomic) IBOutlet UITableView *updateTaskTableView;
@property (weak, nonatomic) IBOutlet UILabel *estimatCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *actualCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *actualUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagSlabel;
@property (weak, nonatomic) IBOutlet UILabel *contextLabel;
@property (weak, nonatomic) IBOutlet UIButton *gainImage;


- (IBAction)done:(id)sender;
- (IBAction)sliderValueChanged:(id)sender;

- (IBAction)doneBtnClicked:(id)sender;
- (IBAction)flagBtnClicked:(id)sender;

@end


@protocol UpdateTaskViewControllerDelegate <NSObject>

- (void) updateActivityViewControllerDidCancel: (UpdateTaskViewController *) controler;
- (void) updateActivityViewControllerDidFinish: (UpdateTaskViewController *) controller task:(Task *)task WithDelete: (BOOL) isDeleted;

@end