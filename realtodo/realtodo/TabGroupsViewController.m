//
//  GroupEditViewController.m
//  realtodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabGroupsViewController.h"
#import "Group.h"
#import "AppUtilities.h"
#import "AppSharedData.h"

@interface TabGroupsViewController ()
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Group *groupSelected;
@property (nonatomic, strong) NSIndexPath *lastSelectedGroupName;
@property BOOL canEditSelection;

@end

@implementation TabGroupsViewController

@synthesize headerView = _headerView;
@synthesize groupNameText = _groupNameText;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;

@synthesize lastSelGroupSet = _lastSelGroupSet;
@synthesize currentSelGroupSet = _currentSelGroupSet;
@synthesize doneBtn = _doneBtn;
@synthesize groupSelected = _groupSelected;
@synthesize lastSelectedGroupName = _lastSelectedGroupIndexPath;
@synthesize groupOperationMode = _groupOperationMode;
@synthesize canEditSelection = _canEditSelection;

@synthesize delegate = _delegate;


#pragma Enable Edit/Add Mode
-(void) enableEditGroupName : (NSIndexPath *) indexPath {
    if (indexPath) {
        Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        self.groupNameText.text = group.name;
        
        self.lastSelectedGroupName = indexPath;
        self.groupSelected = group;
        
        self.groupNameText.placeholder = @"Enter New Group Name...";// Select/Deselect Item to Edit/Add...";
        
    }else {
        
        [self enableAddGroupName];
    }
}

- (void) enableAddGroupName {
    
    self.groupNameText.text = @"";
    self.groupNameText.placeholder = @"Enter New Group Name...";
    self.groupSelected = nil;
    self.lastSelectedGroupName = nil;
}

#pragma mark - InitWith
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) initializeWithMode:(GroupOperationMode)selOperationMode LastSelGroupSet:(NSSet *)lastSelGroupSet Context:(NSManagedObjectContext *)managedContext Delegate:(id<TabGroupsViewControllerDelegate>)delegate {
    
    self.groupOperationMode = selOperationMode;
    
    self.managedObjectContext = managedContext;
    self.delegate = delegate;
    
    switch (selOperationMode)
    {
        case ReadOnlySingleSelection:
        case ReadOnlyMultipleSelection:
        {
            self.lastSelGroupSet = lastSelGroupSet;
            self.currentSelGroupSet = [[NSMutableSet alloc] init];
            
            for (Group *group in self.lastSelGroupSet)
                [self.currentSelGroupSet addObject:group];
            
            break;
        }
        case ReadWriteSingleSelection:
            // No selected group info needed when being launched from tab bar. Only from tab bar we allow editing to prevent handling several cases when using this controller being launched from other views.
            break;
        default:
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self fetchData];
    [self.groupNameText setDelegate:self];
    
   /* Commenting the default group creation on click, not a prefered method.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *preloadGroups = [defaults objectForKey:@"preloadGroups"];
    
    if ([preloadGroups isEqualToString:@"loadedGroupsAlready"]) {
    }
    
    else {
        [defaults setObject:@"loadedGroupsAlready" forKey:@"preloadGroups"];
        
        NSMutableArray *preGroupsArray = [[AppSharedData getInstance] preCreateGroups];
        
        for (int i=0; i<[preGroupsArray count]; i++) {
            [self createNewGroup: [preGroupsArray objectAtIndex:i]];
        }
    }*/
    
    
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection:
        case ReadWriteSingleSelection:
        {// Enable Edit Button to allow rearrange group and get it ready to allow add groups.
            [self enableAddGroupName];
            self.navigationItem.rightBarButtonItem = self.editButtonItem;
            
            [self.groupNameText addTarget:self
                                   action:@selector(startEditing:)
                         forControlEvents:UIControlEventEditingDidBegin];
            break;
        }
        case ReadOnlyMultipleSelection:
            // No Special handling needed for this mode as their we do not allow add/move/edit in this case.
            self.groupNameText.enabled = NO;
            break;
        default:
            break;
    }
}

- (void) startEditing: (UITextField *)textField
{
    if (self.tableView.indexPathForSelectedRow == nil) {
        self.lastSelectedGroupName = nil;
        self.groupSelected = nil;
    }
}

- (void)viewDidUnload
{
    [self setHeaderView:nil];
    [self setGroupNameText:nil];
    [self setDoneBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![_fetchedResultsController.managedObjectContext save:&error]) {
            DLog(@"GroupAddEditViewController: Could not save context: %@", error);
        }
    }
}


- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    //    self.task.group = self.groupSelected;
    
    // During Disapper save the global variable to pass values for caller to use and share again. This is to be done only when we have multiple select one.
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection: // Selected Item is informed using delegate so not required.
            break;
        case ReadOnlyMultipleSelection:
        {
            [AppSharedData getInstance].dynamicViewData.groupSet = self.currentSelGroupSet;
            break;
        }
        case ReadWriteSingleSelection:
            // Navigation is not expected to set some valus any where as this is the main starting screen.
            // Change on the group for its name is yet to be saved, if edited. Not doing it here as it has to be saved in the delegate which will be invoked from here.
            // later i saw it was not updated and hence putting up  this change and retaining older comments documented above
            if (self.groupSelected != nil) [self saveUpdatedGroup:self.groupSelected];
            break;
        default:
            break;
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ItemDetailsCell *gaevCell = (ItemDetailsCell *) cell;
    
    Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ( [self.currentSelGroupSet containsObject:group] ) {
        //    if ([self.lastSetGroup.name isEqualToString:group.name]) {
        gaevCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        gaevCell.accessoryType = UITableViewCellAccessoryNone;
    }
    gaevCell.itemNameLabel.text = group.name;
    
    int totalCount = [group.tasks count];
    int activeCount = 0;
    for (Task *taskObj in group.tasks) {
        NSLog(@"%@", taskObj.done);
        if ([taskObj.done intValue] == 0)
            activeCount +=1;
    }
    gaevCell.itemTasksCountLabel.text = [NSString stringWithFormat:@"(%i/%i)",activeCount, totalCount];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"ItemDetailsCell";
    /* Old way
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     if (cell == nil) {
     cell = [[UITableViewCell alloc]
     initWithStyle:UITableViewCellStyleDefault
     reuseIdentifier:CellIdentifier];
     }
     */
    ItemDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /* This is not working with xib. learn later and try again
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (ItemDetailsCell *) [nib objectAtIndex:0];
     } */
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the managed object.
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



- (void) resetSortOrder {
    
    
}
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    /*[self resetSortOrder];
     NSInteger items = [[_fetchedResultsController fetchedObjects] count];
     
     // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
     for (NSInteger i=0; i< items; i++) {
     NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
     Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
     NSNumber *newPosition = [NSNumber numberWithInteger:i+1];
     
     
     //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
     curObj.sort_order = newPosition;
     }
     
     Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
     movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row + 1];
     NSError *error;
     if (![_fetchedResultsController.managedObjectContext save:&error]) {
     NSLog(@"Could not save context: %@", error);
     }
     */
    
    NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Group *fromGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Group *toGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    DLog(@"itemMoved: %@ to: %@ FsortOrder: %d toSO: %d FromIdx: %d to: %d", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row);
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];
        
        
        //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
    Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];
    
    // Save of this operation will be done on click of Edit Button as Done
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    
    switch (self.groupOperationMode)
    {
        case ReadOnlySingleSelection:
        case ReadOnlyMultipleSelection:
        {
            return NO;
            
            break;
        }
        case ReadWriteSingleSelection:
            // No selected group info needed when being launched from tab bar. Only from tab bar we allow editing to prevent handling several cases when using this controller being launched from other views.
            return YES;
            break;
            
        default:
            return YES;
            break;
    }
    
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    //    NSLog(@"NSFetchType: %d", type);
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self enableAddGroupName];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
    
}

#pragma mark - Custom Editing Options in this controller
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // if we are in add/update the groups mode, no need to perform selection/deselection
    switch (self.groupOperationMode) {
        case ReadOnlySingleSelection:
        {
            UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
            [cell setSelected:NO animated:YES];
            
            // Reset Previous Selected Index if any
            if (self.lastSelectedGroupName && ![self.lastSelectedGroupName isEqual:indexPath]) {
                UITableViewCell * lastCell = [self.tableView  cellForRowAtIndexPath:self.lastSelectedGroupName];
                [lastCell setSelected:NO animated:YES];
                lastCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            if (cell.accessoryType == UITableViewCellAccessoryNone) {
                // Set current cell as checked
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                // remember current IndexPath now and the corresponding group
                self.lastSelectedGroupName = indexPath;
                self.groupSelected = group;
            }
            else if (cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
                cell.accessoryType = UITableViewCellAccessoryNone;
                
                [self enableAddGroupName];
            }
            else {
                DLog(@"Some error");
                [self enableAddGroupName];
            }
            
            [self finishSelection];
            break;
        }
        case ReadWriteSingleSelection:
        {
            //        NSLog(@"LastSelcted: %@", self.lastSelectedGroupName);
            //        NSLog(@"CurrenSeled: %@", indexPath);
            if ([self.lastSelectedGroupName isEqual:indexPath]) {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                [self enableAddGroupName];
            } else {
                
                [self enableEditGroupName: indexPath];
                // not selecting group as it is not needed in this case
            }
            break;
        }
        case ReadOnlyMultipleSelection:
        {
            Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
            UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
            [cell setSelected:NO animated:YES];
            
            if ([self.currentSelGroupSet containsObject:group]) {
                
                [self.currentSelGroupSet removeObject:group];
                cell.accessoryType = UITableViewCellAccessoryNone;
                
            } else {
                
                [self.currentSelGroupSet addObject:group];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            
            break;
        }
            
        default:
            break;
    }
}


#pragma mark - Result controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Group"
                                   inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"sort_order"
                                        ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]  initWithFetchRequest:fetchRequest
                                                                                                 managedObjectContext:self.managedObjectContext
                                                                                                   sectionNameKeyPath:nil
                                                                                                            cacheName:nil];
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    DLog(@"Error Fetching Group %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Without this autoupdate will not work.
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

#pragma TextField Delegate Handling
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if ( textField == self.groupNameText) {
        
        NSString *groupName = [AppUtilities trim:textField.text];
        
        if (groupName.length == 0) {
            [textField resignFirstResponder];
        }
        else {
            if ([ self ifGroupAlreadyExist:groupName ]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Group Already Exist."
                                                               delegate:self cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                [alert show];
                self.groupNameText.text= @"";
            }
            else {
                switch (self.groupOperationMode) {
                    case ReadOnlySingleSelection:
                    {
                        // Edit is not allowed so no need to update and direclty go for add.
                        [self createNewGroup:textField.text];
                        self.groupNameText.text= @"";
                        break;
                    }
                    case ReadWriteSingleSelection:
                    {
                        // Edit of existing is allowed hence see if need to update
                        if (self.lastSelectedGroupName == nil) {
                            [self createNewGroup:textField.text];
                            self.groupNameText.text= @"";
                        } else {
                            self.groupSelected.name = groupName;
                        }
                        break;
                    }
                    case ReadOnlyMultipleSelection: {
                        break;
                    }
                    default:
                        break;
                }
                self.groupNameText.text= @"";
                [textField resignFirstResponder];
            }
        }
    }
    
    return YES;
}

- (void) fetchData {
    
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    [self.tableView reloadData];
}

-(void) saveUpdatedGroup: (Group *) existingGroup {
    NSError *error = nil;
    if (![existingGroup.managedObjectContext save:&error]) {
        DLog(@"Error Saving Group %@, %@", error, [error userInfo]);
        
        abort();
    }
}

-(void) createNewGroup: (NSString *)groupName {
    // Planning to add a new context object, so referring to fetrscntrl managed context specifically
    
    Group *newGroup = [NSEntityDescription insertNewObjectForEntityForName:@"Group"
                                                    inManagedObjectContext:self.fetchedResultsController.managedObjectContext];
    newGroup.name = groupName;
    newGroup.sort_order = [NSNumber numberWithInt:[self.fetchedResultsController.fetchedObjects count] + 1 ];
    
    NSError *error = nil;
    if (![newGroup.managedObjectContext save:&error]) {
        DLog(@"Error Saving Group %@, %@", error, [error userInfo]);
        
        abort();
    }
}

-(BOOL) ifGroupAlreadyExist: (NSString *)groupName {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Group" inManagedObjectContext:self.managedObjectContext]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(name == %@)", groupName]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [self.managedObjectContext
                        countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void) finishSelection {
    [self.delegate groupEditViewControllerDidFinish:self SelectedGroup:self.groupSelected];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
    [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma Save Operation
- (IBAction)done:(id)sender {
    
    [self.delegate groupEditViewControllerDidFinish:self SelectedGroup:self.groupSelected];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
    [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
