//
//  CommonViewController.m
//  realtodo
//
//  Created by Me on 31/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController ()

// <SegmentID, <SectionID, SectionName>>
@property (nonatomic, strong) NSMutableDictionary *colpsedSectionNameDict; 
@property (nonatomic, strong) NSMutableDictionary *segmentSectionsLoaded; 

@property (nonatomic) int direction;
@property (nonatomic) int deletedOrInsertedSection;

@end

@implementation CommonViewController

@synthesize mgdObjContext = _mgdObjContext, fetchedResultsControllerObj = _fetchedResultsControllerObj;
@synthesize selSegmentIdx = _selSegmentIdx;

@synthesize colpsedSectionNameDict = _colpsedSectionNameDict, segmentSectionsLoaded = _segmentSectionsLoaded, direction = _direction, deletedOrInsertedSection = _deletedOrInsertedSection;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.colpsedSectionNameDict        = [[NSMutableDictionary alloc] init ];
    self.segmentSectionsLoaded         = [[NSMutableDictionary alloc] init ];    
    
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT;

//    // Load the data on first run
//    [self fetchData];
}

- (void) viewWillAppear:(BOOL)animated {
    
    // Load the data on first run
    [self forceReloadThisUI];
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    self.fetchedResultsControllerObj = nil;
    
}


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = LANDSCAPE_WIDTH_FACTOR;
    else {
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = 0;
    }
    [self forceReloadThisUI];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return IS_PAD || (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    return YES;
}


#pragma mark - Section Name API

-(BOOL) isSectionUserCollapsed: (NSInteger)sectionNumber  {

    BOOL isSectionUserCollapsed = NO;
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
    
//    NSString *computedSectionName = [self getManagedObjID:sectionNumber];// [NSString stringWithFormat:@"%i", sectionNumber] ; //[self computeSectionNameFromRawName:sectionNumber];

//    NSString *grpSectionNo =[NSString stringWithFormat:@"%i", sectionNumber];    
    NSNumber *grpSectionNo = [NSNumber numberWithInt:sectionNumber];

    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:key];
    if ([valueSet containsObject:grpSectionNo] )
        isSectionUserCollapsed = YES;
    else {
        isSectionUserCollapsed = NO;
    }
    
    DLog(@"isSectionUserCollapsed:   Section:%i  %@, Segment:%@  ComputedName:%@ colpsedSectionNameDict:%@", sectionNumber, isSectionUserCollapsed? @"YES": @"NO", key,  grpSectionNo, [AppUtilities cleanDictSetPrint: _colpsedSectionNameDict]);
    return isSectionUserCollapsed;
}

- (NSString *) getManagedObjID: (NSInteger) section {
    
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
    
    NSString *objectIDStr = [[[task.group objectID] URIRepresentation] absoluteString];
    if ( objectIDStr == nil || [objectIDStr length] == 0 ) {
        objectIDStr = EMPTY;
    }
    
    return objectIDStr;
}

- (NSString *) computeSectionNameFromRawName: (NSInteger) section 
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self fetchedResultsController] sections] objectAtIndex:section];
    
    
    // TODO: Code Clean up and bettername
    NSString *sectionName = nil;
    
    // NSString *groupName = task.group.name; //sectionInfo.name
    NSString *dbQryGrpName = sectionInfo.name;
    
    switch ( self.selSegmentIdx ) {
        case 0:
        {
            /*
             sectionName = dbQryGrpName;//[sectionInfo name];   
             
             // AS I was not able to get the right sorting for group name, getting it sorted by sort_order and finding the group name selectively.
             Group *grp = [AppUtilities fetchGroupBySortOrder:groupName  ManagedContext:self.mgdObjContext];
             groupName = grp.name;
             //            DLog(@"Section Name%@ %@", sectionName, groupName);
             
             // Display the project name as section headings.
             sectionName =  ([groupName length] == 0) ? @"Empty" : groupName;
             */
            // TODO: Check if the above one is faster or there is another faster method to do this
            NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
            Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
            
            sectionName = task.group.name;
            sectionName =  ([sectionName length] == 0) ? @"Empty" : sectionName;
            
            break;
        }
        case 1: {
            sectionName = dbQryGrpName;
            break;
        }
        case 2:
            sectionName = dbQryGrpName;
            break;
        case 3: 
            sectionName = dbQryGrpName;
            break;
        case 4:
            sectionName = dbQryGrpName;
            break;
        default:
            sectionName = dbQryGrpName;
            break;
    }
    
    DLog(@"ComputeName:              Section:%i, SectionInfoName: %@ newSectionName: %@: END", section, sectionInfo.name, sectionName);
    
    return sectionName;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    int count = [[[self fetchedResultsController] sections] count];
    DLog(@"numberOfSectionsInTableView: %i", count);
    return count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DLog(@"noOfRowsInsection: START  Section:%i", section);   
    
    BOOL val = [self isSectionUserCollapsed:section];
    if (val) {
        DLog(@"noOfRowsInsection: END    Section:%i isUserCollapsed rows:0", section);
        return 0;
    }
    
    // Get the value from the array maintained per groupSegment
    if ([self isSectionCollapsedAsFirstTimeLoad: section]) {
        // mark these sections so that they don't appear as first load again
        [self markSectionLoadedCollapsedOnFirstLoad:section];
        
        // Also put it in the closed section names to get the section as collapsed
//        NSString *sectionName = [self getManagedObjID:section];//[self computeSectionNameFromRawName:section];
        NSString *sectionName = [NSString stringWithFormat:@"%i", section];
        [self addClosedSectionName:sectionName SectionNumber:section];
        
        DLog(@"noOfRowsInsection: END    Section:%i isSectionCollapsedAsFirstTimeLoad rows:0", section);        
        return 0;
    }
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
        DLog(@"noOfRowsInSection: END    Section:%i Returned Actual Row Count (%i).", section, [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
    
}

- (void) markSectionLoadedCollapsedOnFirstLoad: (NSInteger) section {
    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
//    NSString *item = [NSString stringWithFormat:@"%i", section];
    NSNumber *item = [NSNumber numberWithInt:section];
    
    NSMutableOrderedSet *value = [self.segmentSectionsLoaded objectForKey:key];
    if (value == nil) {
        value = [[NSMutableOrderedSet alloc] init];
        [value addObject:item];
        
        [self.segmentSectionsLoaded setObject:value forKey:key];
    }
    else {
        [value addObject:item];
    }
    DLog("MarkSectionLoadedCollapsedOnFirstLoad: %@", [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
}
- (BOOL) isSectionCollapsedAsFirstTimeLoad: (NSInteger) sectionNumber {
   /* 
    int selCollapsibleOnIdx = self.selSegmentIdx;
    
    BOOL valueAtIdex = [[self.isSegmentSectionsFirstLoadArr objectAtIndex:selCollapsibleOnIdx] boolValue];
    
    //    DLog(@"Check if SelectedCollonFirstLoad: %@", self.isSectionFirstLoadArray);
    */
    
    BOOL isSectionFirstLoad = NO;

    NSNumber *key = [NSNumber numberWithInteger: self.selSegmentIdx];
//    NSString *item = [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *item = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *value = [self.segmentSectionsLoaded objectForKey:key];
    
    if (value !=nil && [value containsObject:item])
        isSectionFirstLoad = NO;
    else {
        isSectionFirstLoad = YES;
    }
    
    DLog(@"isSectionCollapsedAtFirstTimeLoad: ForSection:%@ %@ selSegmentIdx:%@ segmentSectionsLoaded:%@", item, isSectionFirstLoad ? @"Yes" : @"NO", key,  [AppUtilities cleanDictSetPrint: self.segmentSectionsLoaded]);
   
    return isSectionFirstLoad;    
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    DLog(@"View Header In Section:%i   Start", section);
    NSString *sectionName = [self computeSectionNameFromRawName:section];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    //  Check if this section name is there in the filter dicitionary. if it is there it imply we need to have this section in closed state.
    BOOL selectState = YES; // TODO: Change this name select name to be more meanigful - groupBtnStateCollapsed
    if ([self isSectionUserCollapsed:section]){
        selectState = NO;
    }
    
    NSString *grpManagedObjectID = [self getManagedObjID:section];
    UITableSectionHeaderView *ghsv = [[UITableSectionHeaderView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.tableView.bounds.size.width, self.tableView.bounds.size.height) title:sectionName section:section sectionItemCount: sectionItemCount selectState: selectState HeaderIndex: (section +1) groupMngedObjID:grpManagedObjectID delegate:self ];
    
    // To identify a group section for updates later
    ghsv.tag = SECTION_NUMBER_STARTFROM + section;
    
    
    DLog(@"View Header In Section:%i   End", section);
    return ghsv;
}


// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    // Configure the cell to show the Tasks's details
    TaskViewCell *mvaCell = (TaskViewCell *) cell;
    [mvaCell resetInitialText];
    
    Task *activityAtIndex =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    [AppUtilities configureCell:mvaCell Task:activityAtIndex];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TaskViewCell";
    
    TaskViewCell *cell = (TaskViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /*     // Tried using TaskViewCell.xib, but found issue of setting segues, initializatin so leave it unless gain more knowledge
    if (cell == nil) {
        NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (TaskViewCell *) [nib objectAtIndex:0];
    }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        DLog(@"Dynamic: commitEditingStyle: Starting Delete");
        [AppUtilities deleteTask:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
        
        // After deleting the record update teh section count value. Added tags for this purpose and finding the section to upate values
        UITableSectionHeaderView *view =  (UITableSectionHeaderView *) [self.tableView viewWithTag:(9000 + indexPath.section)];
        //  DLog(@"Descrit: %@", [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)]);
        
        if ( [view isKindOfClass:[ UITableSectionHeaderView class]] ) {
            [view updateCountOnDelete:1]; // update name for rows deleted arg            
        }
        else {
            DLog(@"Error, unable to find the GroupHeaderSectionView, need to debug.");
        }
        //        id x = self.tableView;
        //        id y = [self.tableView viewWithTag:indexPath.section];
        //        id z = [self.tableView viewWithTag:0];
        //        id a = [self.tableView viewWithTag:3];
        
        
        
        
        // TODO: I NEED TO UPDATE THE COUNT ON THE SECTIONS HERE.
        //        // Delete the managed object.
        //        NSManagedObjectContext *context = [[self fetchedResultsController] managedObjectContext];
        //        [context deleteObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
        //        
        //        NSError *error;
        //        if (![context save:&error]) {
        //            /*
        //             Replace this implementation with code to handle the error appropriately.
        //             
        //             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
        //             */
        //            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //            abort();
        //        }
        //        
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
    DLog(@"Dynamic: commitEditingStyle: FINISHED Operation");
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.   
    [self.tableView beginUpdates];
    DLog(@"Table End Update");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    
    DLog(@"Dynamic:didChangeObject: Type: %i, atIndexSection: %i atIndexRow: %i, newIndexSection: %i newIndexRow: %i", type, indexPath.section, indexPath.row,newIndexPath.section, newIndexPath.row);
    //    DTLog(@"Debug: %@, %@, %@", indexPath, newIndexPath, anObject);
    UITableView *tableView = self.tableView;
    switch(type) {
        // Had to put checks for section being in closed state else it was giving error as rows before and after update don't match. NoOfRows will continue return 0 even after add/update/delete and fail. To prevent it doing these checks. Additionally saving on same context was impacting tableview on other controllers and one section open in this view might be closed on other. To handle them had to put these checks. 
        case NSFetchedResultsChangeInsert:
            if (![self isSectionUserCollapsed:newIndexPath.section]) {
                [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
            
        case NSFetchedResultsChangeDelete:
            if (![self isSectionUserCollapsed:indexPath.section]) {                
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
        case NSFetchedResultsChangeUpdate:
            if ( ![self isSectionUserCollapsed:indexPath.section] ) {
                [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];                
            }
            break;
            
        case NSFetchedResultsChangeMove: {
            if (indexPath.section == newIndexPath.section) {
                // even though there is a move somehow the old and new section numbers were same, don't know why. BUGGY
                [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
                
                self.direction=0;
                self.deletedOrInsertedSection = -1;
            }
            if (! [self isSectionUserCollapsed:indexPath.section] ) {
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            if ( ![self isSectionUserCollapsed:newIndexPath.section] ) {
                [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];                
            }

            break;
        }
    }
    
    DLog(@"Dynamic: didChangeObject: Finished Update.");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{    
    DLog(@"Dynamic:didChangeSection: Type: %i, atIndexSection: %i ", type, sectionIndex);
    switch(type) {
        case NSFetchedResultsChangeInsert: {
            // MOVED LATER
            // To prevent getting an error because of it being marked as collapsed and item count returning as 0, adding it as if it is already collapsed
//            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
            
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            self.direction = 1;
            self.deletedOrInsertedSection = sectionIndex;
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            self.direction = -1;
            self.deletedOrInsertedSection = sectionIndex;
            break;
        }
    }

    DLog(@"Dynamic: didChangeSection: Section Did change: Finished Update");
}

/* Added this api to update the cache storing the closed sections. When a section is inserted or deleted the section numbers are
 * no longer valid and should be readjusted (BETTER USE ARRAY TO ADD/DELETE ITEMS, LATER)
 * Since we need to start from top or bottom using the direction for insert/delete operation
 * Moved the marking API here as wanted it to be run just before begining updates ... forget why?
 * calling this API before the updates and sometimes when objectchanged and somehow the new and old section was coming same and creating problem
 */

-(void) reloadUserCollapsedListOnSectionDelete: (int) sectionIndex Direction:(int) direction 
{    
    DLog(@"ReloadUserCollapsedListOnSectionDelete: Start collapsedSectionNameDict: %@", [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
    if (sectionIndex != -1 ) {
        int sectionCount = [self.tableView numberOfSections];
        if (direction == -1 ) {
            for (int i = sectionIndex; i < sectionCount; i++) {
                NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
                //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
                //            NSString *subKeySectionInx = [NSString stringWithFormat:@"%i", i];
                //            NSString *newSubKeySectionIdx = [NSString stringWithFormat:@"%i", i + direction];        
                NSNumber *subKeySectionInx = [NSNumber numberWithInt:i];
                NSNumber *newSubKeySectionIdx = [NSNumber numberWithInt:(i + direction)];
                
                NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
                if (valueSet && [valueSet containsObject:subKeySectionInx]) {
                    [valueSet removeObject:subKeySectionInx];
                    [valueSet addObject:newSubKeySectionIdx];
                }
            }     
        }
        else if (direction == 1) {
            for (int i = sectionCount; i >= sectionIndex; i--) {
                NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
                //    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
                //            NSString *subKeySectionInx = [NSString stringWithFormat:@"%i", i];
                //            NSString *newSubKeySectionIdx = [NSString stringWithFormat:@"%i", i + direction];
                NSNumber *subKeySectionInx = [NSNumber numberWithInt:i];
                NSNumber *newSubKeySectionIdx = [NSNumber numberWithInt:(i + direction)];
                
                NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
                if (valueSet && [valueSet containsObject:subKeySectionInx]) {
                    [valueSet removeObject:subKeySectionInx];
                    [valueSet addObject:newSubKeySectionIdx];
                }
            }
            [self markSectionLoadedCollapsedOnFirstLoad:sectionIndex];
        }      
    }
         
    DLog(@"ReloadUserCollapsedListOnSectionDelete: Start collapsedSectionNameDict: %@", [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    DLog(@"controllerDidChangeContent: START"); 
    
    [self reloadUserCollapsedListOnSectionDelete:self.deletedOrInsertedSection Direction:self.direction];
    
    self.direction=0;
    self.deletedOrInsertedSection = -1;
    
    @try {
        // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
        [self.tableView endUpdates];
    }
    
    @catch (NSException *exception) {
        DLog(@"main: Caught %@: %@", [exception name], [exception reason]);
        // Tried few things for recovery but did not find something useful.
//        [self.tableView reloadData];
//        [super setEditing:YES animated:YES]; 
//        [self.tableView setEditing:YES animated:YES];
        [self forceReloadThisUI];
    }
}


#pragma mark - DB Operations

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI 
{    
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowUpdateActivityViewController"] ) 
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Task *task = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        UpdateTaskViewController *updateActivityViewController = [segue destinationViewController];
        updateActivityViewController.delegate = self;
        updateActivityViewController.task = task;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddActivityViewController"]) 
    {
        
        AddTaskViewController *addTaskViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];

        addTaskViewController.managedObjectContext = self.mgdObjContext;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowFilterSettingsViewController"]) 
    {
        [segue destinationViewController];
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowFilterSettingsViewController"]) 
    {
        [segue destinationViewController];
    }
}


#pragma Section Open Close API Handling on FilterDictionary

// Rename this to markSectionAsClosed
- (void) addClosedSectionName: (NSString *) sectionName SectionNumber: (NSInteger) sectionNumber {
    
    NSNumber *keySegmentNo = [NSNumber numberWithInt:self.selSegmentIdx];
//    NSString *subKeySectionName = sectionName; //[NSString stringWithFormat:@"%i", sectionNumber];
//    NSString *subKeySectionNo = [NSString stringWithFormat:@"%i", sectionNumber];
//    NSString *subValueSectionNo= [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *subKeySectionNo  = [NSNumber numberWithInt:sectionNumber];
//    NSNumber *subValueSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:keySegmentNo];
    if (valueSet )
        //        [ dictValue setObject:sectionNumberStr forKey:sectionName];
        [valueSet addObject: subKeySectionNo];        
    else {
        valueSet = [[NSMutableOrderedSet alloc] init ];
        //        [dictValue setObject:sectionNumberStr forKey:sectionName];
        [valueSet addObject: subKeySectionNo];        
        [self.colpsedSectionNameDict setObject:valueSet forKey:keySegmentNo];
    }
    DLog(@"AddClosedSecName:         Section:%@ keySegmentNo: %@ colpsedSectionNameDict:%@", subKeySectionNo, keySegmentNo,  [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}

#pragma Section Open Close API Handling on FilterDictionary
- (void) removeOpenSectionName: (NSString *) sectionName  Section: (NSInteger) sectionNumber{
    
    NSNumber *groupSegmentIndex = [NSNumber numberWithInteger: self.selSegmentIdx];
//    NSString *grpSectionNo = [NSString stringWithFormat:@"%i", sectionNumber];
    NSNumber *grpSectionNo = [NSNumber numberWithInt:sectionNumber];
    
    //    NSString *groupSectionNameOpened = sectionName;
    
    NSMutableOrderedSet *valueSet = [self.colpsedSectionNameDict objectForKey:groupSegmentIndex];
    if (valueSet )
//        [ dictValue removeObjectForKey: [NSString stringWithFormat:@"%i", sectionNumber]];
        [ valueSet removeObject: grpSectionNo];
    else {
        DLog(@"Unable to find the object to remove for key: %@", grpSectionNo);
    }
    DLog(@"RemoveOpenSecName: Key:%@ Item:%@ colpsedSectionNameDict:%@", groupSegmentIndex, grpSectionNo, [AppUtilities cleanDictSetPrint: self.colpsedSectionNameDict]);
}


#pragma mark - GroupHeaderSectionView Delegate API

- (void) uitableSectionHeaderView:(UITableSectionHeaderView *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {    
    switch (sectionOperation) {
        case SECTION_OPEN: {
            [self removeOpenSectionName:sectionHeaderView.titleLabel.text Section:sectionHeaderView.section];
//            [self removeOpenSectionName:sectionHeaderView.groupManagedObjectID Section:sectionHeaderView.section];

            break;
        }
        case SECTION_CLOSE: {
            [self addClosedSectionName:sectionHeaderView.titleLabel.text SectionNumber:sectionHeaderView.section];
//            [self addClosedSectionName:sectionHeaderView.groupManagedObjectID SectionNumber:sectionHeaderView.section];            
            break;
        }
        default:
            break;
    }
    
    [self forceReloadThisUI];
}

#pragma mark - Add/Update controller delegate
- (void) updateActivityViewControllerDidCancel:(UpdateTaskViewController *)controler {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) updateActivityViewControllerDidFinish:(UpdateTaskViewController *)controller task:(Task *)task WithDelete:(BOOL)isDeleted
{
    if (task) {
        if (isDeleted) {
            [AppUtilities deleteTask:task];
        }
        else {
            [AppUtilities saveTask:task];
        }
    }
    //    [self dismissModalViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    //    [self.tableView reloadData];
    
    [self forceReloadThisUI];
}

-(NSFetchedResultsController *) fetchedResultsController {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return  nil;
}

@end
