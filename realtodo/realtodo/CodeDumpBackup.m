//
//  CodeDumpBackup.m
//  realtodo
//
//  Created by Me on 12/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "CodeDumpBackup.h"

@implementation CodeDumpBackup

// Balbir SHokeen

/*
 // Gradient Button
 
 // Draw a custom gradient
 CAGradientLayer *btnGradient = [CAGradientLayer layer];
 btnGradient.frame = self.doneBtn.bounds;
 btnGradient.colors = [NSArray arrayWithObjects:
 (id)[[UIColor colorWithRed:102.0f / 255.0f green:102.0f / 255.0f blue:102.0f / 255.0f alpha:1.0f] CGColor],
 (id)[[UIColor colorWithRed:51.0f / 255.0f green:51.0f / 255.0f blue:51.0f / 255.0f alpha:1.0f] CGColor],
 nil];
 [self.doneBtn.layer insertSublayer:btnGradient atIndex:0];
 */

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

+(UIImage *)makeRoundCornerImage : (UIImage*) img : (int) cornerWidth : (int) cornerHeight
{
	UIImage * newImage = nil;
    
	if( nil != img)
	{
		int w = img.size.width;
		int h = img.size.height;
        
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
        
		CGContextBeginPath(context);
		CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
		addRoundedRectToPath(context, rect, cornerWidth, cornerHeight);
		CGContextClosePath(context);
		CGContextClip(context);
        
		CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
        
		CGImageRef imageMasked = CGBitmapContextCreateImage(context);
		CGContextRelease(context);
		CGColorSpaceRelease(colorSpace);
       
		newImage = [UIImage imageWithCGImage:imageMasked];
		CGImageRelease(imageMasked);
        
	}
    
    return newImage;
}

// My Extra Lines
// ##################################
// 

// This line was updated from all #s to some text.
// 

// ##################################
// Code to Create a file
// ##################################
/*
// Create .csv file and save in Documents Directory.
//create instance of NSFileManager
NSFileManager *fileManager = [NSFileManager defaultManager];

//create an array and store result of our search for the documents directory in it
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

//create NSString object, that holds our exact path to the documents directory
NSString *documentsDirectory = [paths objectAtIndex:0];
NSLog(@"Document Dir: %@",documentsDirectory);

NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", @"ExportUserData"]]; //add our file to the path
[fileManager createFileAtPath:fullPath contents:[fetchedData dataUsingEncoding:NSUTF8StringEncoding] attributes:nil]; //finally save the path (file)
*/

// ##################################
//Test Checkin
// ##################################
// Reference checking last group or row
// ##################################

/* 
 
 // Check if it is first time load and if so show the group as closed.
 // Get the value from the array maintained per groupSegment
if ([self isSectionCollapsedAsFirstTimeLoad: section]) {
    selectState = NO;
    
    
    // When the last section is printed, we are reseting the first load. This is one of best single point to do so.
    DLog(@"viewForHeaderInSection: Section:%i TotalCount-1: %i TVSection-1:%i", section, [[[self fetchedResultsController] sections] count]-1, [tableView numberOfSections] -1);
    //        if( section == [[[self fetchedResultsController] sections] count]-1){
    if( section == [tableView numberOfSections] -1 ){
        
        [self unmarkSelSectionForCollapsedOnFirstLoad];
    }
}
*/
// ##################################
// Color Related Backup Code
// ##################################
// DEFAULT APPLE COLOR
/*
 color = [UIColor colorWithRed:0.22f green:0.33f blue:0.53f alpha:1.0f];
 [colors addObject:(id)[color CGColor]];  
 color = [UIColor colorWithRed:0.22f green:0.33f blue:0.53f alpha:1.0f];
 [colors addObject:(id)[color CGColor]];  
 color = [UIColor colorWithRed:0.22f green:0.33f blue:0.53f alpha:1.0f];
 [colors addObject:(id)[color CGColor]];  
 
 
 
 color = [UIColor colorWithRed:0.82 green:0.84 blue:0.87 alpha:1.0];
 [colors addObject:(id)[color CGColor]];
 color = [UIColor colorWithRed:0.41 green:0.41 blue:0.59 alpha:1.0];
 [colors addObject:(id)[color CGColor]];
 color = [UIColor colorWithRed:0.41 green:0.41 blue:0.59 alpha:1.0];
 [colors addObject:(id)[color CGColor]];
 
 color = [UIColor colorWithRed:0.121653f green:0.558395f blue:0.837748f alpha:1.0];            
 color = [UIColor colorWithRed:118/255 green:141/255 blue:176/255 alpha:1.0];
 [colors addObject:(id)[UIColor colorWithHue:0.6 saturation:0.33 brightness: 0.65 alpha:1.0]];
 RGB(118, 141, 176).
 */

// ##################################
// How to check if the delegate responds to a method
// ##################################
/*
// If this was a user action, send the delegate the appropriate message
if (userAction) {
    if (self.disclosureButton.selected) {
        if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
            [self.delegate sectionHeaderView:self Section:self.section Operation: SECTION_OPEN];
        }
    }
    else {
        if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
            [self.delegate sectionHeaderView:self Section:self.section Operation: SECTION_CLOSE];
        }
    }
}
*/

// ##################################
// Ability to Tap on title bar
// ##################################

/*    
 // Ability to tap on the tile bar item.
 UITapGestureRecognizer* tapNavTitleSingleRecon = [[UITapGestureRecognizer alloc]
 initWithTarget:self action:@selector(navigationBarTitleSingleTap:)];
 
 tapNavTitleSingleRecon.numberOfTapsRequired = 1;
 [[self.navigationController.navigationBar.subviews objectAtIndex:1] setUserInteractionEnabled:YES];
 [[self.navigationController.navigationBar.subviews objectAtIndex:1] addGestureRecognizer:tapNavTitleSingleRecon];
 */


// ##################################
// Scroll to tableview index manually
// ##################################
/*
if (index != 0) {
    // i is the index of the cell you want to scroll to
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
}
*/
// ##################################
// Starting controller from button click - not treid yet
// ##################################
/*
 -(IBAction)addCustomer
{
	AddCustomerController *addcustomerController=[[AddCustomerController alloc] initWithNibName:@"AddCustomerController" bundle:nil];
	addcustomerController.delegate=self;
	addcustomerController.cust=(Customer *)[NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:managedObjectContext_];
	UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:addcustomerController];
	[self.navigationController  presentModalViewController:navController animated:YES];
	[addcustomerController release];
	[navController release];
}*/

// ##################################
// Date Diff
// ##################################
// NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow:-24*60*60];
/*
 http://www.daveoncode.com/2010/11/15/cocoa-objective-c-iphone-dates-objects/
 USEFULL Date operations link
 
 // create a custom dateComponents object
 NSDateComponents *customComponents = [[NSDateComponents alloc] init];
 [customComponents setYear:2015];
 [customComponents setMonth:7];
 [customComponents setDay:23];
 
 // create a date object using custom dateComponents against current calendar
 NSDate *customDate = [[NSCalendar currentCalendar] dateFromComponents:customComponents];
 
 
 // get year, month and day parts
 NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
 
 then we can use retrieved date parts in this way:
 1
 
 NSLog(@"year: %i - month: %i - day: %i", [components year], [components month], [components day]);
 
 
NSDate *date1 = [NSDate dateWithString:@"2010-01-01 00:00:00 +0000"];
NSDate *date2 = [NSDate dateWithString:@"2010-02-03 00:00:00 +0000"];

NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];

int numberOfDays = secondsBetween / 86400;

NSLog(@"There are %d days in between the two dates.", numberOfDays);
 
 
 SDateComponents *components;
 NSInteger days;
 
 components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit 
 fromDate: startDate toDate: endDate options: 0];
 days = [components day];

 
 Add or subtract date parts from/to a date:
 
 To add or subtract date parts from/to a date, we have to create a dateComponents object configuring it with the desired gap to fill and then use calendar’s method dateByAddingComponents:toDate:options:. For example if we want to add 2 years and half to a date we can do this
 
 // create a dateComponents with 2 years and 6 months
 NSDateComponents *newComponents = [[NSDateComponents alloc] init];
 [newComponents setYear:2];
 [newComponents setMonth:6];
 
 // get a new date by adding components
 NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:newComponents toDate:date options:0];
 
 Get difference between two dates:
 
 To get the difference between 2 dates we have to rely on NSCalendar once again and use its components:fromDate:toDate method, by specifying the date parts we are interested in and 2 different dates. Values returned from components can be negative if toDate is earlier than fromDate. The following example retrieve days difference between 2 dates:

 NSDateComponents *componentsDiff = [calendar components:NSDayCalendarUnit fromDate:dateA toDate:dateB options:0];
 
 
 // get my current time zone
 NSLog(@"my time zone: %@ - abbreviation: %@", [[NSTimeZone systemTimeZone] name], [[NSTimeZone systemTimeZone] abbreviation]);
 // get my current calendar
 NSLog(@"my calendar: %@", [[NSCalendar currentCalendar] calendarIdentifier]);
 // get my current locale
 NSLog(@"my locale: %@", [[NSLocale currentLocale] localeIdentifier]);
 
 // create an american locale
 NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
 
 // assign custom locale to the formatter
 [formatter setLocale:usLocale];
 
 // print formatted date
 NSLog(@"formatted date: %@", [formatter stringFromDate:currentDate]);
 
 // set a GMT time zone
 [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
 
 // print formatted date
 NSLog(@"formatted date: %@", [formatter stringFromDate:currentDate]);
 
*/
// ##################################
// Quartz Layers Sample
// ##################################
/*
self.view.layer.backgroundColor = [UIColor orangeColor].CGColor;
self.view.layer.cornerRadius = 80.0;
self.view.layer.frame = CGRectInset(self.view.layer.frame, 20, 20);
*/

// ##################################
// Find/Search or Query from core data table 
// ##################################
/*
// get the names to parse in sorted order

NSArray *employeeIDs = [[listOfIDsAsString componentsSeparatedByString:@"\n"]
                        
                        sortedArrayUsingSelector: @selector(compare:)];



// create the fetch request to get all Employees matching the IDs
NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
[fetchRequest setEntity:
 
 [NSEntityDescription entityForName:@"Group" inManagedObjectContext:self.managedObjectContext]];
[fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(name == %@)", groupName]];

// make sure the results are sorted as well
[fetchRequest setSortDescriptors: [NSArray arrayWithObject:
                                   [[[NSSortDescriptor alloc] initWithKey: @"sort_order"
                                                                ascending:YES] autorelease]]];
// Execute the fetch
NSError *error = nil;
NSArray *employeesMatchingNames = [self.managedObjectContext
                                   executeFetchRequest:fetchRequest error:&error];

*/

// ##################################
// Inputbox style Alert or message box
// ##################################

/*
- (void) addTag {
    
    UIAlertView *newTagAlert = [[UIAlertView alloc] initWithTitle:@"New tag" message:@"Insert new tag name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    
    newTagAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [newTagAlert show];
    
}
*/

// ##################################
// Set Scroll to Row for Tableview
// ##################################

//[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];


// ##################################
// NSMutableArray creation, add objects and handling BOOL and convertin Number to bool
// ##################################
/*
NSNumber* yesObj = [NSNumber numberWithBool:YES];
NSMutableArray* arr = [[NSMutableArray alloc] initWithObjects:
                       yesObj, yesObj, yesObj, yesObj, nil];
NSLog(@"%d", [[arr objectAtIndex:1] boolValue]);

// Convert Back
BOOL b = [[array objectAtIndex:i] boolValue]
 
 
 
 // First Load so set it to YES
 self.isSectionFirstLoadArray = [[NSMutableArray alloc] initWithCapacity:[self.groupSegment numberOfSegments]];
 
 for (int i=0; i < [self.groupSegment numberOfSegments]; i++) {
 [self.isSectionFirstLoadArray addObject:[NSNumber numberWithBool:YES]];
 }
*/

// ##################################
// Int value to Number or integer store in db
// ##################################
//  [self.task setDone: [NSNumber numberWithInt:0]];

// ##################################
// How to check if methods are supported.
// ##################################
/*
-(void) toggleOpen:(id) sender {
    [self toggleOpenWithUserAction:YES];
}

- (void) toggleOpenWithUserAction:(BOOL)userAction {
    
    //    NSLog(@"Selected: %@", self.disclosureButton.selected);
    //Toggle the disclosure button state.
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    // If this was a user action, send the delegate the appropriate message
    if (userAction) {
        if (self.disclosureButton.selected) {
            if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                [self.delegate sectionHeaderView:self sectionOpened:self.section];
            }
        }
        else {
            if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                [self.delegate sectionHeaderView:self sectionClosed:self.section];
            }
        }
    }
}
*/


// ##################################
// Adding ability to play default sounds and vibrate.
// ##################################
//#import <AudioToolbox/AudioToolbox.h>
//   AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
//AudioServicesPlaySystemSound(1005);

// ##################################
// Button Rounded and useful API
// ##################################

//    CALayer * layer = [yourUIButton layer];
//    [layer setMasksToBounds:YES];
//    [layer setCornerRadius:0.0]; //note that when radius is 0, the border is a rectangle
//    [layer setBorderWidth:1.0];
//    [layer setBorderColor:[[UIColor grayColor] CGColor]];

//mvaCell.infoButton1.layer.cornerRadius = 10;
//mvaCell.infoButton1.clipsToBounds = YES;
//mvaCell.infoButton2.layer.cornerRadius = 10;
//mvaCell.infoButton2.clipsToBounds = YES;
//[mvaCell.infoButton2.layer setBorderWidth:1.0];
//[mvaCell.infoButton2.layer setBorderColor:[[UIColor grayColor]CGColor]];

// ##################################
// Action Sheet Example
// ##################################
/*
 @interface TempViewController : UIViewController<UIActionSheetDelegate>
- (IBAction)btnClick:(id)sender {
    //   
    UIActionSheet *sheet;
    sheet = [[UIActionSheet alloc] initWithTitle:@"Select Account to Delete"
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:@"Delete All Accounts"
                               otherButtonTitles:@"Checking", @"Savings", @"Money Market", nil];
    
 [sheet showFromRect:self.view.bounds inView:self.view animated:YES];
 
 // Show the sheet
 [sheet showInView:self.view];
 
 }
 - (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
 {
 //    if (buttonIndex == actionSheet.cancelButtonIndex) { return; }
 
 NSLog(@"Button %d", buttonIndex);
 }
*/

// ##################################
// NSMutableDicitionary (need to do alloc/init]
// ##################################
/*
@property (nonatomic, strong
           ) NSMutableDictionary *dic;
@synthesize dic =_dic;
self.dic = [[NSMutableDictionary alloc] init ];
[self.dic setObject:@"a" forKey:@"key1"];
NSLog(@"Count %d", [self.dic count]);
*/

// ##################################
// Date Handling - converting for display on UI and back to date
// ##################################
/*
- (IBAction)btnClick:(id)sender {
    
    NSDate *today = [NSDate date];
    NSLog(@"Today: %@", today);
    
    NSString *formatedD = [self uiFormatedDate:today];
    NSLog(@"Formated: %@", formatedD);
    
    NSDate *unfromated = [[self lDateFormter] dateFromString:formatedD];
    NSLog(@"Unfromated: %@", unfromated);
    
}

- (NSString *)uiFormatedDate:(NSDate *) date
{
    return  [[self lDateFormter] stringFromDate: date];
}
- (NSDate *) uiDateUNFormated: (NSString *) formatedDate
{
    return [[self lDateFormter] dateFromString:formatedDate];
}
- (NSDateFormatter *)lDateFormter
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSTimeZoneNameStyleShortGeneric];
    }
    
    return dateFormatter;
}*/
// ##################################
// Convert NSNumber or int to NSString
// ##################################
//[NSString stringWithFormat:@"%d", [month intValue]];

// ##################################
// Search Controller with ...
// ##################################
/* NOT USED AS NOT USING THE SEARCHBARDISPLAY CONTROLLER
 #pragma mark -
 #pragma mark UISearchDisplayController Delegate Methods
 - (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
 {
 [self filterContentForSearchText:searchString scope:
 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
 
 // Return YES to cause the search result table view to be reloaded.
 return YES;
 }
 
 
 
 - (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
 {
 [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
 
 // Return YES to cause the search result table view to be reloaded.
 return YES;
 }
 */

/* Hiding as not sure where it is sused
 // Added to support Search Bar
 -(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 [self.uiSearchBar resignFirstResponder];
 return indexPath;
 }
 */
//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    if ( [searchText length] > 0 ) {
//        self.appSharedData.dynamicViewData.searchText = searchText;
//    }
//    else {
//        self.appSharedData.dynamicViewData.searchText = searchText;
//    }
//    
//    self.appSharedData.dynamicViewData.searchText = searchText;
//    [self forceReloadThisUI];
//    
////	/*
////	 Update the filtered array based on the search text and scope.
////	 */
////	
////	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
////	
////	/*
////	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
////	 */
////	for (Product *product in listContent)
////	{
////		if ([scope isEqualToString:@"All"] || [product.type isEqualToString:scope])
////		{
////			NSComparisonResult result = [product.name compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
////            if (result == NSOrderedSame)
////			{
////				[self.filteredListContent addObject:product];
////            }
////		}
////	}
////}
//
//    
//}

// ##################################
// How to show hidden mac folders
// ##################################

//defaults write com.apple.finder AppleShowAllFiles -bool true

// ##################################
// Convert Date Description to NSDate
// ##################################
//Cancel all local notifications with this code:
//
//[[UIApplication sharedApplication] cancelAllLocalNotifications];
//Cancel one local notification with this line of code:
//
//[[UIApplication sharedApplication] cancelLocalNotification:theNotification];
//where theNotification is a UILocalNotification object, so in order to cancel a specific notification you need to hold on to it's UILocalNotification.

// ##################################
// Convert Date Description to NSDate
// ##################################

//NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
//dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
//NSDate* date = [dateFormatter dateFromString:self.remindOnLabel.text];
//[self.task setRemind_on:date];


// ##################################
// MasterViewController
// ##################################

//- (void) detailViewControllerDidFinish:(DetailViewController *)controller name:(NSString *)name endDate:(NSDate *)endDate 
//{
// 
//    if ( [name length] ) {
//        
//        [self.taskDataController updateTask:name date: endDate];
//
//        [self.tableView reloadData];
//    }
//    
//    [self dismissModalViewControllerAnimated:YES];
//}

// AddActivityFinish
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                              withRowAnimation:UITableViewRowAnimationFade];
//        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

//- (void)awakeFromNib
//{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//        self.clearsSelectionOnViewWillAppear = NO;
//        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
//    }
//    [super awakeFromNib];
//}
//

//ViewDidLoad()
//TODO: get the table view populated during load

// Do any additional setup after loading the view, typically from a nib.
/*
 self.navigationItem.leftBarButtonItem = self.editButtonItem;
 
 UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
 self.navigationItem.rightBarButtonItem = addButton;
 self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
 */

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//}
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
//    } else {
//        return YES;
//    }
//}
/*
 - (void)insertNewObject:(id)sender
 {
 if (!_objects) {
 _objects = [[NSMutableArray alloc] init];
 }
 [_objects insertObject:[NSDate date] atIndex:0];
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
 [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 }*/
/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier] isEqualToString:@"showDetail"]) {
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        NSDate *object = [_objects objectAtIndex:indexPath.row];
//        [[segue destinationViewController] setDetailItem:object];
//    }
//}

// ##################################
//  TOGGLE SWITCH BUTTON
// ##################################
//
//if(toggleSwitch.on){
//    18	    [toggleSwitch setOn:NO animated:YES];
//    19	  }
//20	  else{
//    21	    [toggleSwitch setOn:YES animated:YES];
//    22	 
//    23	  }
//24	 

// ##################################
//  Number to Int and Integer to Number 
// ##################################
// [self.task setGain: [NSNumber numberWithInt:[self.gainOptions selectedSegmentIndex]]];
//       if ( [theTask.done integerValue] == 1) 

// ##################################
//  Current Date & format 
// ##################################
//NSDate *currDate = [NSDate date];
//NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//[dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
//NSString *dateString = [dateFormatter stringFromDate:currDate];
//NSLog(@"%@",dateString);

//static NSDateFormatter *formatter = nil;
//if (formatter == nil) {
//    formatter = [[NSDateFormatter alloc] init];
//    
//    [ formatter setDateStyle:NSDateFormatterMediumStyle];
//    [formatter setTimeStyle:NSTimeZoneNameStyleShortGeneric];
//    
//}
//    self.DueDateLabel.text = [formatter stringFromDate: theTask.dueOn];
//    
//}

// ##################################
//  Create a new Class
// ##################################
//   Task *aTask = [[Task alloc] init];

// ##################################
//  Segue to another view
// ##################################

//        AddActivityViewController *addActivityViewController = [segue destinationViewController];
//        addActivityViewController.delegate = self;

//        [segue destinationViewController];


// ##################################
//  String Check
// ##################################
/*
if ([sectionName length] == 0 ) {
    ALog(@"len = 0 %@", sectionName);
}

else if ( sectionName == nil ) {
    
    ALog(@"Nil %@", sectionName);
}
else if (sectionName == @""){
    ALog(@"Empty%@", sectionName);
    
}
else
{      ALog(@"Value:%@", sectionName);   
}

*/
// ##################################
//  Property attributes description
// ##################################
/*
Setter Semantics

These attributes specify the semantics of a set accessor. They are mutually exclusive.

strong

Specifies that there is a strong (owning) relationship to the destination object.
weak

Specifies that there is a weak (non-owning) relationship to the destination object.

If the destination object is deallocated, the property value is automatically set to nil.

(Weak properties are not supported on OS X v10.6 and iOS 4; use assign instead.)
copy

Specifies that a copy of the object should be used for assignment.

The previous value is sent a release message.

The copy is made by invoking the copy method. This attribute is valid only for object types, which must implement the NSCopying  protocol.
assign

Specifies that the setter uses simple assignment. This attribute is the default.

You use this attribute for scalar types such as NSInteger and CGRect.
retain

Specifies that retain should be invoked on the object upon assignment.

The previous value is sent a release message.

In OS X v10.6 and later, you can use the __attribute__ keyword to specify that a Core Foundation property should be treated like an Objective-C object for memory management:

@property(retain) __attribute__((NSObject)) CFDictionaryRef myDictionary;

Atomicity

You can use this attribute to specify that accessor methods are not atomic. (There is no keyword to denote atomic.)

nonatomic

Specifies that accessors are nonatomic. By default, accessors are atomic. 

Properties are atomic by default so that synthesized accessors provide robust access to properties in a multithreaded environment—that is, the value returned from the getter or set via the setter is always fully retrieved or set regardless of what other threads are executing concurrently.

If you specify strong, copy, or retain and do not specify nonatomic, then in a reference-counted environment, a synthesized get accessor for an object property uses a lock and retains and autoreleases the returned value—the implementation will be similar to the following:

[_internal lock]; // lock using an object-level lock

id result = [[value retain] autorelease];

[_internal unlock];

return result;

If you specify nonatomic, a synthesized accessor for an object property simply returns the value directly.
*/

// ##################################
// Primary Key for Core Data NSManagedObject ID
// ##################################
/* As Barry Wark said, remember always that Core Data is not an orm. Pure SQL details are not exposed to the user and every row is just an object. By the way, sometime you should need to access the "primary key", for example when you need to sync the coredata db with external sql databases (in my case I needed it in a callback function to change the state of an object after INSERT it with success in the remote db). In this case, you can use:
 
 objectId=[[[myCoredataObject objectID] URIRepresentation] absoluteString]
 
 that will return a string like: x-coredata://76BA122F-0BF5-4D9D-AE3F-BD321271B004/Object/p521 that is the unique id used by coredata to identify that object.
 
 If you want to get back an object with that unique id:
 
 NSManagedObject *managedObject= [managedObjectContext objectWithID:[persistentStoreCoordinator managedObjectIDForURIRepresentation:[NSURL URLWithString:objectId]]];
 
 NB: Remember that if the receiver has not yet been saved in the CoreData Context, the object ID is a temporary value that will change when the object is saved.
 */

@end
