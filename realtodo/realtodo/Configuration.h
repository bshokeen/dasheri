//
//  Configuration.h
//  realtodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Configuration : NSManagedObject

@property (nonatomic, retain) NSString * extra;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * value;

@end
