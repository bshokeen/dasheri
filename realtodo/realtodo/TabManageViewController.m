//
//  DynamicViewController.m
//  realtodo
//
//  Created by Me on 12/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabManageViewController.h"
#import "AppConstants.h"

@interface TabManageViewController ()

@property BOOL searchON;
@property (nonatomic, weak)   AppSharedData  *appSharedData;

@end



@implementation TabManageViewController


@synthesize uiSearchBar = _uiSearchBar;
@synthesize totalRowsLabel = _totalRowsLabel;
//@synthesize dateLabel = _dateLabel;

@synthesize searchON = _searchON;
@synthesize appSharedData = _appSharedData;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.appSharedData = [AppSharedData getInstance];
    
    //    self.uiSearchBar.text=@"" ;
    self.uiSearchBar.delegate = self;
    
    self.totalRowsLabel.layer.backgroundColor = [UIColor blueColor].CGColor;
    self.totalRowsLabel.layer.cornerRadius = 10.0;
    self.totalRowsLabel.layer.borderColor = [UIColor blackColor].CGColor;
    self.totalRowsLabel.layer.borderWidth = 0.5;
    
    DLog(@"Dynamic View is Loaded...");
}

- (void)navigationBarTitleSingleTap:(UIGestureRecognizer*)recognizer {
    
    // [self performSegueWithIdentifier:@"ShowFilterSettingsViewController" sender:self];
}
- (void) viewWillAppear:(BOOL)animated {
    
    // Moved this setting to view will appear to ensure it is available before the UI comes up.
    // As dynamic view itself don't have the UISegment Control. Get the selectedsegment information on view appear
    
    self.selSegmentIdx = self.appSharedData.dynamicViewData.collapsibleOnIdx;
    
    BOOL filterApplied = [[AppSharedData getInstance].dynamicViewData checkForDefaults];
    
    if (filterApplied) {
        
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithHue:HUE saturation:SATURATION brightness:BRIGHTNESS alpha:ALPHA]];
    }
    else {
        self.navigationController.navigationBar.tintColor = nil;
    }
    
    // Ensure you call this to let the data be shown on first load
    [super viewWillAppear:animated];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    if (self.appSharedData.dynamicViewData.reloadUI) {
        self.appSharedData.dynamicViewData.reloadUI = NO;
        
        [self forceReloadThisUI];
        
        //       not required now self.appSharedData.dynamicViewData.collapsibleOnIdxPrevSelected = self.appSharedData.dynamicViewData.collapsibleOnIdx;
    }
    
    
}

- (void) updateTotalRowsLabel {
    int count = [[self fetchedResultsController].fetchedObjects count];
    self.totalRowsLabel.text = [@" Total Records         " stringByAppendingFormat:@"%i  ", count];
    
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];;
    //    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    //    self.dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setUiSearchBar:nil];
    [self setTotalRowsLabel:nil];
    //[self setDateLabel:nil];
    
    DLog(@"Dynamic: View Unloaded");
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [super controllerDidChangeContent:controller];
    
    [self updateTotalRowsLabel];
}

#pragma mark Search Support
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    //    ULog(@"Yes");
}

- (void) searchBarTextDidEndEditing:(UITextField *)textField {
    //    if ( [textField.text length] == 0 ) {
    //        [self.uiSearchBar resignFirstResponder];
    //    }
}

// Hide the keyboard
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.uiSearchBar.text = @"";
    
    [AppSharedData getInstance].dynamicViewData.searchText = @"";
    
    [searchBar resignFirstResponder];
    
    [self forceReloadThisUI];
    //  DLog(@"Search Bar Cancel clicked");
}

-(void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)Tosearch{
    
    NSString *searchText = [searchBar text];
    
    if ( [searchText length] == 0 ) {
        //        [searchBar resignFirstResponder];
        //        [self.view endEditing:YES];
    }
    
    [AppSharedData getInstance].dynamicViewData.searchText = searchText;
    [self forceReloadThisUI];
    
    //  DLog(@"Search Bar TEXT DID CHANGE");
}

#pragma mark - Action Implementation
- (IBAction)leftNavBarItemClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"ShowFilterSettingsViewController" sender:self];
}

// Hide the Search Keyboard if appearing when switching over
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.uiSearchBar resignFirstResponder];
}


#pragma mark - DB Operations

- (void) fetchData {
    
    [super fetchData];
    [self updateTotalRowsLabel];
}

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}

#pragma mark - Fetch Result Controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    self.fetchedResultsControllerObj = [[AppSharedData getInstance].dynamicViewData genFetchReqCtrlFromSelConfig: self.mgdObjContext];
    self.fetchedResultsControllerObj.delegate = self;
    
    // Memory management.
    DLog(@"Dynamic: fetchResultsCtroller: RE-QUERY");
    
    return self.fetchedResultsControllerObj;
}

/*
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{ 
    return YES;
    //(touch.view == self); 
}*/

@end
