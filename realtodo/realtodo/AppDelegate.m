//
//  AppDelegate.m
//  realtodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AppUtilities.h"
#import "TabTagsViewController.h"
#import "TabGroupsViewController.h"


@interface AppDelegate ()


@end


@implementation AppDelegate

@synthesize window = _window;
@synthesize tabBarController = _uiTabBarController;

@synthesize managedObjectContext = __managedObjectContext, managedObjectModel = __managedObjectModel, persistentStoreCoordinator = __persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];

    TabAllTasksSortViewController *sortViewController;
    TabViewsViewController *groupViewController;
    TabGroupsViewController *groupEditViewController;
    TabDueOnViewController *filterGroupViewController;
    TabManageViewController *dynamicViewController;
    TabAlertViewController *alertViewController;
    
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        splitViewController.delegate = (id)navigationController.topViewController;
        
        UINavigationController *masterNavigationController = [splitViewController.viewControllers objectAtIndex:0];
        TabGroupsViewController *controller = (TabGroupsViewController *)masterNavigationController.topViewController;
        
        UINavigationController *detailNavCtrl = [splitViewController.viewControllers objectAtIndex:1];
        TabGroupsViewController *controller1 = (TabGroupsViewController *)detailNavCtrl.topViewController;
        
        NSManagedObjectContext *context = [self managedObjectContext];
        if (!context) {
            ALog(@"Unable to generate the context: %@", context);
        }
        controller.managedObjectContext = context;
        controller1.managedObjectContext = context;
        
    }
    else {
        self.tabBarController = (UITabBarController *) self.window.rootViewController;
        
        UINavigationController *dynamicViewCtrlNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:0];
        dynamicViewController = (TabManageViewController *) [[dynamicViewCtrlNavigCntrl viewControllers] objectAtIndex:0];
        
        
        UINavigationController *filterGroupViewCtrlrNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:1];
        filterGroupViewController = (TabDueOnViewController *) [[filterGroupViewCtrlrNavigCntrl viewControllers] objectAtIndex:0];
        
        UINavigationController *groupViewCtrlrNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:2];
        groupViewController = (TabViewsViewController *) [[groupViewCtrlrNavigCntrl viewControllers] objectAtIndex:0];
        
        
        UINavigationController *alertViewCtrlNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:ALERT_TAB_IDX];
        alertViewController = (TabAlertViewController *) [[alertViewCtrlNavigCntrl viewControllers] objectAtIndex:0];
        
        UINavigationController *groupEditViewCtrlNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:4];
        groupEditViewController = (TabGroupsViewController *) [[groupEditViewCtrlNavigCntrl viewControllers] objectAtIndex:0];
        
        UINavigationController *sortViewCtrlNavigCntrl = (UINavigationController *) [self.tabBarController.viewControllers objectAtIndex:6];
        sortViewController = (TabAllTasksSortViewController *) [[sortViewCtrlNavigCntrl viewControllers] objectAtIndex:0];
        
        /*
         DLog(@"first: %@", self.window.rootViewController);
         DLog(@"Second: %@", [uiTabBarController.viewControllers objectAtIndex:2]);
         DLog(@"Third: %@", [sortViewCtrlNavigCntrl.viewControllers objectAtIndex:0]);
         */
        
        
        NSManagedObjectContext *context = [self managedObjectContext];
        
        if (!context) {
            ALog(@"Unable to generate the context: %@", context);
        }
        
        groupViewController.mgdObjContext = context;
        sortViewController.managedObjectContext = context;
        filterGroupViewController.mgdObjContext = context;

        // Allow running in Add/Edit Mode directly from tab controller
        [groupEditViewController initializeWithMode:ReadWriteSingleSelection LastSelGroupSet:nil Context:context Delegate:nil];
        
        dynamicViewController.mgdObjContext = context;
        alertViewController.mgdObjContext = context;
    
        
        DLog(@"List all the notifications at startup");
        // Get list of local notifications
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        
        for (UILocalNotification *item in notificationArray) {
            DLog(@"Notif Body: %@, fire: %@ repeat:%d", item.alertBody, [item.fireDate description], item.repeatInterval);
        }
        
        // Handle launching from a notification
        UILocalNotification *notif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        
        // If not nil, the application was based on an incoming notifiation
        if (notif)
        {
            DLog(@"Notification initiated app startup");
            
            // Access the payload content
            DLog(@"Notification payload: %@", [notif.userInfo objectForKey:@"payload"]);

            [self.tabBarController setSelectedIndex:ALERT_TAB_IDX]; // Start the app with a specific view open.
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Task Reminder"
                                                            message:notif.alertBody
                                                           delegate:self cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            
            // As alert has fired now handle the reset of the alert
            [self resetReminder:notif];
        }
                
        // On start of the application the alert badge count to be updated from the database.
        [AppUtilities updateAlertBadge:2];
        
//        [[UIApplication sharedApplication] cancelAllLocalNotifications];         DLog(@"CanceledLocalNotification");
    }
    
    return YES;
}

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    // Handle the notificaton when the app is running
    DLog(@"Recieved Notification %@",notif);
    
    DLog(@"Show the list of notifications...");
    // Get list of local notifications
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *item in notificationArray) {
        DLog(@"Notif Body: %@, fire: %@ repeat:%d", item.alertBody, [item.fireDate description], item.repeatInterval);
    }
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    AudioServicesPlaySystemSound(1005);
    
    // Access the payload content
    //	NSLog(@"Notification payload: %@", [notification.userInfo objectForKey:@"payload"]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Task Reminder"
                                                    message:notif.alertBody
                                                   delegate:self cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
    
    // As alert has fired now handle the reset of the alert
    [self resetReminder:notif];
        
	DLog(@"Incoming notification in running app");
}

-(void) resetReminder: (UILocalNotification *) notif {
    
    NSDictionary *infoDict = (NSDictionary *) notif.userInfo;
    NSString *taskObjectID = [infoDict objectForKey:@"TaskObjectID"];
    
    if (taskObjectID.length == 0) {
        DLog(@"Error, unable to find the task Object ID for this reminder");
    }
    else {
        NSManagedObject *managedObjectTask= [[self managedObjectContext] objectWithID:[[self persistentStoreCoordinator] managedObjectIDForURIRepresentation:[NSURL URLWithString:taskObjectID]]];
        
        Task *task = (Task *) managedObjectTask;
        
        // TODO: As some of the tasks were deleted by the time alert was notified we got an exception. Ideally it should not happen and all deleted tasks to delete their notifications. 
//        to let this issue keep breaking the code and get addressed properly, not putting a check and putting a TODO
        
        if ([task.remind_repeat_interval intValue] == 0) {
            task.remind_on = nil;   
            task.remind_repeat_interval = [NSNumber numberWithInt: NSEraCalendarUnit];
        }
        
        // Added 2 additional attributes to track if the notificaiton is ON and the time of notification
        task.alert_notification = [NSNumber numberWithInt: 1];
        task.alert_notification_on = [NSDate date];
        
        // Not required so far but additionally added to save immediately
        [AppUtilities saveTask:task];
        
        // Increment the alertBadge
        [AppUtilities updateAlertBadge:1];
        
        [self.tabBarController setSelectedIndex:ALERT_TAB_IDX]; // Start the app with a specific view open.
    }
}



- (void)application:(UIApplication *)app
didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    DLog(@"Error in registration. Error: %@", err);
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"realToDo.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:path])
    {
        NSMutableDictionary *savedDefaultValues = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        
        if ([[savedDefaultValues objectForKey:@"AutoDeleteCompletedTask"] intValue] == 1) {
            [AppUtilities deleteCompletedTasks:DAYS_60];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error;
    if (__managedObjectContext != nil) {
        if ([__managedObjectContext hasChanges] && ![__managedObjectContext save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            ALog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc]
                                  //initWithConcurrencyType:NSMainQueueConcurrencyType];
                                  init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"realtodomodel" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    DLog(@"Path: %@", modelURL);
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"realtodo.sqlite"];
    
    // To all lightweight migration adding these options. Passing it as options for Persistent Store Cordinator. Read the apple guide.
    //    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                       [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:optionsDictionary error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
@end
