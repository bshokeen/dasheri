//
//  Tag.h
//  realtodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Task;

@interface Tag : NSManagedObject

@property (nonatomic, retain) NSNumber * color;
@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSDate * deleted_on;
@property (nonatomic, retain) NSNumber * dirty;
@property (nonatomic, retain) NSNumber * icon_id;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSString * tag_id;
@property (nonatomic, retain) NSSet *taskS;
@end

@interface Tag (CoreDataGeneratedAccessors)

- (void)addTaskSObject:(Task *)value;
- (void)removeTaskSObject:(Task *)value;
- (void)addTaskS:(NSSet *)values;
- (void)removeTaskS:(NSSet *)values;

@end
