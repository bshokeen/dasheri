//
//  Category.m
//  realtodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "Category.h"
#import "Task.h"


@implementation Category

@dynamic category_id;
@dynamic color;
@dynamic created_on;
@dynamic deleted_on;
@dynamic dirty;
@dynamic icon_id;
@dynamic modified_on;
@dynamic name;
@dynamic sort_order;
@dynamic taskS;

@end
