//
//  FilterGroupViewController.h
//  realtodo
//
//  Created by Me on 08/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "GroupViewController.h"
#import "CommonViewController.h"

@interface FilterGroupViewController : CommonViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *filterSegmentControl;

@end
