//
//  TabDueOnViewController.h
//  realtodo
//
//  Created by Me on 08/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabViewsViewController.h"
#import "CommonViewController.h"


@interface TabDueOnViewController : CommonViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *filterSegmentControl;

@end
