//
//  Constants.h
//  realtodo
//
//  Created by Me on 04/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_PAD                     ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)

#define APP_DELEGATE               ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define APP_DELEGATE_MGDOBJCNTXT   ([(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext])

#define DEFAULT_ROW_HEIGHT   78
#define HEADER_HEIGHT        30

// To change header color (if Filter is applied)
#define HUE 1.0
#define SATURATION 0.78
#define BRIGHTNESS 0.68
#define ALPHA 1.0

#define LANDSCAPE_WIDTH_FACTOR 160 // 160 was old value, after autoresizing set in code it is now set to 0. TODO: Try using autoresizing and a UIView instead of using from code.
#define SECTION_NUMBER_STARTFROM 9000

// TO help identify the controls in same API
#define TAG_EFFORT_COUNT_PICKERVIEW 10001
#define TAG_EFFORT_UNIT_PICKERVIEW 10002

// Delete Task
#define DAYS_60  60
#define DAYS_30  30

// No of days for reminder
#define REMINDER_DAYS 17

// Index of Alert Tab for managing the badge count
#define ALERT_TAB_IDX  3

// TODO: Use Test Flight Simulator Logging
#if DEBUG == 0

#define DLog(inputFrmt, ...) NSLog((@"" inputFrmt), ##__VA_ARGS__)
#define ULog(inputFrmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:inputFrmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }

#define DLog1(inputFrmt, ...) NSLog((@""))
#define DDDDLog(inputFrmt, ...) NSLog((@"%s: " inputFrmt  ),__PRETTY_FUNCTION__, ##__VA_ARGS__)
#define DDLog(inputFrmt, ...) NSLog((inputFrmt @"- \t\t\t  %s: [Line %d] " ), ##__VA_ARGS__, __PRETTY_FUNCTION__, __LINE__)
#define DDDLog(inputFrmt, ...) NSLog((@"%s: [Line %d] - \n\t\t\t" inputFrmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#else
//#define DLog(...) // For now show logs in debug and iphone also ain testing mode
#define DLog(inputFrmt, ...) NSLog((@"" inputFrmt), ##__VA_ARGS__)
#define ULog(inputFrmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:inputFrmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }

#endif
#define ALog(inputFrmt, ...) NSLog((@"%s [Line %d] " inputFrmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);


extern NSString * const NEVER;
extern NSString * const EMPTY;

extern NSString * const PREFS_MY_CONSTANT;

extern NSString * const CONFIG_KEY_COLLAPSIBLE_ON;
extern NSString * const CONFIG_KEY_DISPLAYTYPE;
extern NSString * const CONFIG_KEY_ORDER_BY;
extern NSString * const CONFIG_KEY_ORDER_BY_DIRECTION;
extern NSString * const CONFIG_KEY_EXCLUDE_COMPLETED;

extern NSString * const COLLAPSIBLEON_GROUP;
extern NSString * const COLLAPSIBLEON_DATE;
extern NSString * const COLLAPSIBLEON_GAIN;
extern NSString * const COLLAPSIBLEON_DONE;
extern NSString * const COLLAPSIBLEON_PERCENTAGE;
extern NSString * const COLLAPSIBLEON_FLAG;

extern NSString * const DISPLAYTYPE_ALL;
extern NSString * const DISPLAYTYPE_TODAY;
extern NSString * const DISPLAYTYPE_WEEK;
extern NSString * const DISPLAYTYPE_OVERDUE;
extern NSString * const DISPLAYTYPE_NODUE;
extern NSString * const DISPLAYTYPE_STAR;


extern NSString * const ORDERBY_DUEON;
extern NSString * const ORDERBY_GAIN;
extern NSString * const ORDERBY_PERCENTAGE;
extern NSString * const ORDERBY_DONE;

//extern NSString * const ORDERBY_DUEON_ASC;
//extern NSString * const ORDERBY_DUEON_DESC;
//extern NSString * const ORDERBY_GAIN_ASC;
//extern NSString * const ORDERBY_GAIN_DESC;
//extern NSString * const ORDERBY_PERCENTAGE_ASC;
//extern NSString * const ORDERBY_PERCENTAGE_DESC;
//extern NSString * const ORDERBY_DONE_ASC;
//extern NSString * const ORDERBY_DONE_DESC;

extern NSString * const ORDER_BY_DIRECTION_ASC;
extern NSString * const ORDER_BY_DIRECTION_DESC;

extern NSString * const EXCLUDE_COMPLETED;


extern NSString * const SOURCE_UI_ADDUI;
extern NSString * const SOURCE_UI_FILTERUI;

extern NSString * const REMINDON_SELECTED_DATE;
extern NSString * const REMINDON_DAILY;
extern NSString * const REMINDON_WEEKDAYS;
extern NSString * const REMINDON_MONTHLY;
extern NSString * const REMINDON_CUSTOMDAYS;
extern NSString * const REMINDON_NEVER;

extern NSString * const REMIND_INTERVAL_SELECTED_DATE;
extern NSString * const REMIND_INTERVAL_DAILY;
extern NSString * const REMIND_INTERVAL_WEEKDAYS;
extern NSString * const REMIND_INTERVAL_MONTHLY;
extern NSString * const REMIND_INTERVAL_CUSTOMDAYS;
extern NSString * const REMIND_INTERVAL_NEVER;


