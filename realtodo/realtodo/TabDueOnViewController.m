//
//  FilterGroupViewController.m
//  realtodo
//
//  Created by Me on 08/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabDueOnViewController.h"

@interface TabDueOnViewController ()


@end


@implementation TabDueOnViewController

@synthesize filterSegmentControl = _filterSegmentControl;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{ 
        return YES;
}

- (void)viewDidLoad
{    
    [self.filterSegmentControl addTarget:self
                                  action:@selector(didChangeGroupSegmentControl:)
                        forControlEvents:UIControlEventValueChanged];

    [super viewDidLoad];    
    
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)didChangeGroupSegmentControl:(UISegmentedControl *)control {
    
    self.selSegmentIdx = self.filterSegmentControl.selectedSegmentIndex;
    [self forceReloadThisUI];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setFilterSegmentControl:nil];   
}

- (void) viewDidAppear:(BOOL)animated {
    
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
        [self forceReloadThisUI];
    }
}


#pragma mark Fetched results controller

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:self.mgdObjContext];
    [fetchRequest setEntity:entity];
    
    NSArray *sortDescriptors = nil;
    NSString *sectionNameKeyPath = nil;

    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    // get year, month and day parts
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    
    // create a date object using custom dateComponents against current calendar
    NSDateComponents *todayCmpnt = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
    
    NSPredicate *new1 = [AppUtilities generatePredicate];
    
    switch( self.filterSegmentControl.selectedSegmentIndex) {
        case 0:
        { 
            NSDate *today = [calendar dateFromComponents:todayCmpnt];

            //    NSDateFormatter *dformatter = [self dateFormatter];
            today = [dateFormatter dateFromString:
                     [NSString stringWithFormat:@"%i-%i-%i 00:00:00 +0000", 
                      [todayCmpnt year], 
                      [todayCmpnt month],
                      [todayCmpnt day]]];
           
            // Addition of day + 1 was not going to next month, so using this API to get right date
            NSDate *tomorrow = [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:today];

            NSMutableArray *parr = [NSMutableArray array];

            NSPredicate *filterPredicate1 = [NSPredicate predicateWithFormat:@" (dueOn >= %@ && dueOn < %@)", today, tomorrow];
            NSPredicate *new1 = [AppUtilities generatePredicate];
            
            [parr addObject: filterPredicate1];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
            break;
        }
        case 1:
        { 
            
            
            NSDate *today = [calendar dateFromComponents:todayCmpnt];
            
            today = [dateFormatter dateFromString:
                     [NSString stringWithFormat:@"%i-%i-%i 00:00:00 +0000", 
                      [todayCmpnt year], 
                      [todayCmpnt month],
                      [todayCmpnt day]]];
            

            NSCalendar *gregorian = [[NSCalendar alloc]
                                     initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *weekdayComponents =
            [gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:today];
            NSInteger day = [weekdayComponents day];
            NSInteger weekday = [weekdayComponents weekday];
            
            int shiftWeekByDays = 1;
            // Calendar day is starting from Sunday, so if you are on Monday it would give weekday as 2. To get end of the week 7 - 2 = 5, add 5 days in todays date. It gives the date for Saturday. But as I am searching data with time 00:00:00 I am adding 1 extra day to get the next date. As I don't want to start my week from Sunday rather Monday, so adding 1 more to shift from Sunday to Monday. Hence 7 - weekday + 2.
            NSDate *weekStart = [NSDate dateWithTimeInterval:-(day-weekday-shiftWeekByDays)*24*60*60 sinceDate:today]; 
            NSDate *weekEnd = [NSDate dateWithTimeInterval:(7-weekday+shiftWeekByDays+1)*24*60*60 sinceDate:today];
            
            NSMutableArray *parr = [NSMutableArray array];
            
            NSPredicate *filterPredicate1 = [NSPredicate predicateWithFormat:@" (dueOn >= %@ && dueOn < %@)", weekStart, weekEnd];
            //NSPredicate *new1 = [NSPredicate predicateWithFormat:@"(done == 0)"];
            
            [parr addObject: filterPredicate1];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: Week: StartDate: %@, EndDate: %@", weekStart, weekEnd);
            break;
        }
        case 2:
        { 
            NSDate *today = [calendar dateFromComponents:todayCmpnt];
            
            today = [dateFormatter dateFromString:
                     [NSString stringWithFormat:@"%i-%i-%i 00:00:00 +0000", 
                      [todayCmpnt year], 
                      [todayCmpnt month],
                      [todayCmpnt day]]];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(dueOn <= %@)", today];
            
            NSMutableArray *parr = [NSMutableArray array];
            [parr addObject:filterPredicate];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: OverDue: %@", today);
            break;
        }
        case 3:
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(dueOn == nil)"];
            
            NSMutableArray *parr = [NSMutableArray array];
            [parr addObject:filterPredicate];
            [parr addObject:new1];
            
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
            
            [fetchRequest setPredicate:final];
            
            DLog(@"Filter: No Due Set");
            break;
        }
        default:
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];
            
            break;
        }
    }
    
    // Create the sort descriptors array.
    NSSortDescriptor *grpSortOrderDescriptor = [[NSSortDescriptor alloc]initWithKey:@"group.sort_order" ascending:YES];

    NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
    
    
    sortDescriptors = [[NSArray alloc] initWithObjects:
                       grpSortOrderDescriptor,
                       dueOnDescriptor, nil];
        sectionNameKeyPath = @"group.sort_order";
    
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Create and initialize the fetch results controller.
    self.fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                           managedObjectContext:self.mgdObjContext 
                                                                             sectionNameKeyPath:sectionNameKeyPath
                                                                                      cacheName:nil];
    self.fetchedResultsControllerObj.delegate = self;
    
    DLog(@"FILTERGroupViewController: getFetchResultsCtroller: RE - QUERY");
    // Memory management.
    return self.fetchedResultsControllerObj;
}  


- (NSString *) computeSectionNameFromRawName: (NSInteger) section 
{
    DLog(@"computeSectionName: Start");   
    id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self fetchedResultsController] sections] objectAtIndex:section];
    
    // TODO: Code Clean up and bettername
    NSString *sectionName = nil;
    
    // NSString *groupName = task.group.name; //sectionInfo.name
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:section];
    Task *task =     [[self fetchedResultsController] objectAtIndexPath:newPath];
    
    sectionName = task.group.name;
    sectionName =  ([sectionName length] == 0) ? @"Empty" : sectionName;
    
    DLog(@"ComputeName: Section: %i, SectionInfoName: %@ newSectionName: %@: END", section, sectionInfo.name, sectionName);
    
    return sectionName;
}

@end
