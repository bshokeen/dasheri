//
//  UITableSectionHeaderView.h
//  realtodo
//
//  Created by Me on 21/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppSharedData.h"

#define SECTION_OPEN  0
#define SECTION_CLOSE 1


@protocol UITableSectionHeaderViewDelegate;


@interface UITableSectionHeaderView : UIView

@property (nonatomic, weak) UILabel *headerCountLabel;
@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UILabel *countLabel;
@property (nonatomic, weak) UIButton *disclosureButton;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) NSInteger sectionItemCount;

@property (nonatomic, strong) NSString *groupManagedObjectID;

@property (nonatomic, weak) id <UITableSectionHeaderViewDelegate> delegate;

-(id)initWithFrame:(CGRect)frame title:(NSString*)title section:(NSInteger)sectionNumber sectionItemCount: (NSInteger) sectionItemCount selectState: (BOOL) selectState HeaderIndex:(NSInteger) headerIndex groupMngedObjID: (NSString *) groupManagedObjectID delegate:(id <UITableSectionHeaderViewDelegate>)delegate;
-(void)toggleOpenWithUserAction:(BOOL)userAction;

-(void) updateCountOnDelete: (int) recordsDeleted;

@end

/*
 Protocol to be adopted by the section header's delegate; the section header tells its delegate when the section should be opened and closed.
 */
@protocol UITableSectionHeaderViewDelegate <NSObject>

//@optional
-(void)uitableSectionHeaderView:(UITableSectionHeaderView*)sectionHeaderView Section:(NSInteger) section Operation:(int) sectionOperation;

@end