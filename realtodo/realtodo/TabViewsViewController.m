//
//  GroupViewViewController.m
//  realtodo
//
//  Created by Me on 18/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "TabViewsViewController.h"


@implementation TabViewsViewController

@synthesize groupSegment = _segment;


#pragma mark View lifecycle
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    [self.groupSegment addTarget:self
                                  action:@selector(didChangeGroupSegmentControl:)
                        forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    [super viewDidLoad];   
    
    // To save the space for extra segments not having an edit button. Will use some other smaller image rather
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

- (void)didChangeGroupSegmentControl:(UISegmentedControl *)control {
    
    self.selSegmentIdx = self.groupSegment.selectedSegmentIndex;
    
    [self forceReloadThisUI];
}

- (void)viewDidUnload
{

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
 
    [self setGroupSegment:nil];   
}

- (void) viewDidAppear:(BOOL)animated {
    
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
        [self forceReloadThisUI];
    }
}


#pragma mark Fetched results controller

-(void) forceReloadThisUI {
    [super forceReloadThisUI];
}


#pragma mark Fetched results controller

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:self.mgdObjContext];
    [fetchRequest setEntity:entity];
    
    NSArray *sortDescriptors = nil;
    NSString *sectionNameKeyPath = nil;
    NSPredicate *filterPredicate = [AppUtilities generatePredicate];
    switch( self.groupSegment.selectedSegmentIndex) {
        case 0:
        { 
            // Create the sort descriptors array.
            NSSortDescriptor *grpSortOrderDescriptor = [[NSSortDescriptor alloc]initWithKey:@"group.sort_order" ascending:YES];

//            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];

            
            sortDescriptors = [[NSArray alloc] initWithObjects: grpSortOrderDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"group.sort_order";
            
            break;
        }
        case 1:
        { 
            // Create the sort descriptors array.
            NSSortDescriptor *gainDescriptor = [[NSSortDescriptor alloc]initWithKey:@"dueOn" ascending:YES];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];
            
            sortDescriptors = [[NSArray alloc] initWithObjects:gainDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"dueOnSectionName";
            
            break;
        }
        case 2:
        { 
            // Create the sort descriptors array.
            NSSortDescriptor *gainDescriptor = [[NSSortDescriptor alloc]initWithKey:@"gain" ascending:YES];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];

            sortDescriptors = [[NSArray alloc] initWithObjects:gainDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"gainSectionName";
            
            break;
        }
        case 3:
        {
            // Create the sort descriptors array.
            NSSortDescriptor *doneDescriptor = [[NSSortDescriptor alloc]initWithKey:@"progress" ascending:NO];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetchRequest setPredicate:filterPredicate];

            sortDescriptors = [[NSArray alloc] initWithObjects:doneDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"progressSectionName";
            break;
        }
            break;
        case 4:
        {
            // Create the sort descriptors array.
            NSSortDescriptor *doneDescriptor = [[NSSortDescriptor alloc]initWithKey:@"done" ascending:NO];
            NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dueOn" ascending:NO];
            
            sortDescriptors = [[NSArray alloc] initWithObjects:doneDescriptor, dueOnDescriptor, nil];
            sectionNameKeyPath = @"doneSectionName";
            break;
        }
        default:
            break;
    }

    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Create and initialize the fetch results controller.
    self.fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                    managedObjectContext:self.mgdObjContext 
                                                                      sectionNameKeyPath:sectionNameKeyPath
                                                                               cacheName:nil];
    self.fetchedResultsControllerObj.delegate = self;
    
    DLog(@"GroupViewController: Group:%@ getFetchResultsCtroller: RE-QUERY", sectionNameKeyPath);
    
    // Memory management.
    return self.fetchedResultsControllerObj;
}  

#pragma mark - Test Ability to rearrange items
-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![self.fetchedResultsControllerObj.managedObjectContext save:&error]) {
            DLog(@"Could not save context: %@", error);
        }
    }
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    /*[self resetSortOrder];
     NSInteger items = [[_fetchedResultsController fetchedObjects] count];
     
     // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
     for (NSInteger i=0; i< items; i++) {
     NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
     Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
     NSNumber *newPosition = [NSNumber numberWithInteger:i+1];
     
     
     //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
     curObj.sort_order = newPosition;
     }
     
     Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
     movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row + 1];
     NSError *error;
     if (![_fetchedResultsController.managedObjectContext save:&error]) {
     NSLog(@"Could not save context: %@", error);
     }
     */
    
    if ( (fromIndexPath.row == toIndexPath.row) && (fromIndexPath.section == toIndexPath.section ) ) {
        return;
    }
    
    NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    Task *fromGroup = (Task *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Task *toGroup   = (Task *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    DLog(@"itemMoved: %@ to: %@ FsortOrder: %d toSO: %d FromIdx: %d to: %d", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row);
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Task *curObj = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];
        
        
        //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
    Task *movedObj = (Task *) [self.fetchedResultsControllerObj objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];
        
        // Save of this operation will be done on click of Edit Button as Done
}

/*
 * To prevent movement of records outside the section
 */
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
//    This implementation will prevent re-ordering outside of the original section like Phil's answer, but it will also snap the record to the first or last row of the section, depending on where the drag went, instead of where it started.
//    if (sourceIndexPath.section != proposedDestinationIndexPath.section) {
//        NSInteger row = 0;
//        if (sourceIndexPath.section < proposedDestinationIndexPath.section) {
//            row = [tableView numberOfRowsInSection:sourceIndexPath.section] - 1;
//        }
//        return [NSIndexPath indexPathForRow:row inSection:sourceIndexPath.section];     
//    }
//    
//    return proposedDestinationIndexPath;

    if( sourceIndexPath.section != proposedDestinationIndexPath.section )
    {
        return sourceIndexPath;
    }
    else
    {
        return proposedDestinationIndexPath;
    }

}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Allow rearranging only for first index as we have only 1 field in the database to store sort order. So it cannot be for each group presently.
    if ( self.groupSegment.selectedSegmentIndex == 0 ) {
        // Return NO if you do not want the item to be re-orderable.
        return YES;
    }
    else {
        return NO;
    }
}


- (IBAction)editDoneBtnClick:(id)sender {
    
    [AppUtilities saveContext];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
