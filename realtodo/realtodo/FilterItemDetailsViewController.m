//
//  AFilterItemsViewController.m
//  realtodo
//
//  Created by Balbir Shokeen on 9/5/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "FilterItemDetailsViewController.h"
#import "AppSharedData.h"

@interface FilterItemDetailsViewController ()

@property(nonatomic, retain) NSMutableArray *neededSortArray;

@end

@implementation FilterItemDetailsViewController

@synthesize delegate, SectionTitle, SortItemIdx, neededSortArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationController.navigationBar.tintColor = nil;
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) viewWillAppear:(BOOL)animated {
    
    switch (SortItemIdx) { 
        case 0: {
            neededSortArray = [AppSharedData getInstance].dynamicViewData.collapsibleOnUIValueArr;
            break;
        }
        case 1: {
            neededSortArray = [AppSharedData getInstance].dynamicViewData.displayTypeValArr;
            break;
        }
        case 2: {
            neededSortArray = [AppSharedData getInstance].dynamicViewData.orderByUIValueArr;
            break;
        }
        default:
            break;
        
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return SectionTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [neededSortArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14]];
    cell.textLabel.text = [neededSortArray objectAtIndex:indexPath.row];
    
    switch (SortItemIdx) { 
        case 0: {
            if([AppSharedData getInstance].dynamicViewData.collapsibleOnIdx == indexPath.row)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        }
        case 1: {
            if([AppSharedData getInstance].dynamicViewData.displayTypeIdx == indexPath.row)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        }
        case 2: {
            if([AppSharedData getInstance].dynamicViewData.orderByIdx == indexPath.row)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        }
    }
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (SortItemIdx) { 
        case 0: {
            [AppSharedData getInstance].dynamicViewData.collapsibleOnIdx = indexPath.row;
            break;
        }
        case 1: {
            [AppSharedData getInstance].dynamicViewData.displayTypeIdx = indexPath.row;
            break;
        }
        case 2: {
            [AppSharedData getInstance].dynamicViewData.orderByIdx = indexPath.row;
            break;
        }
            
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
