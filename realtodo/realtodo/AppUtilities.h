//
//  AppUtilities.h
//  realtodo
//
//  Created by Me on 12/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//



#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Task.h"
#import "TaskViewCell.h"

@interface AppUtilities : NSObject

+ (NSString *) trim: (NSString *) inputString;
+ (NSString *) handleEmptyString: (NSString *) inputStr;
+ (BOOL) ifTaskAlreadyExist: (NSManagedObjectContext *) context TaskName: (NSString *)taskObjectID;
+(BOOL) ifEntityAlreadyExist: (NSString *)entityName AttributeName: (NSString *) attributeName AttributeValue: (NSString *) attributeValue;
//+ (BOOL) deleteTask:         (NSManagedObjectContext *) context TaskName: (NSString *)taskObjectID;
//+ (void) resetReminderFor: (Task *) task;


+ (NSString *)uiFormatedDate:(NSDate *) date;
+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate;
+ (NSDateFormatter *)lDateFormatter;
+ (NSDate *) ifEmptyAddDay: (NSDate *) date;
+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate WithDayAdd: (BOOL) addDay;
+ (NSString *) uiFormatedTime: (NSDate *) timeWithDate; // For Remind at a time get FormattedTimeValue showing only the time part in predefined format
+ (NSString *) dayNameFromDate: (NSDate *) date; // Get the name of the day (in your locale)
+ (NSInteger) dayFromDate: (NSDate *) date; // Get the day part from the date.
+ (NSInteger) monthFromDate: (NSDate *) date; // Get the month part from the date.
+ (NSString *) monthNameFromDate: (NSDate *) date;

+(NSString *) getImageName: (NSString *) imageTag;

/** This API is added to add schedule an alarm **/
+ (void) scheduleAlarm: (NSString *) groupName AlarmDate: (NSDate *) alarmDate TaskName: (NSString *) taskName TaskObjectID: (NSString *)taskObjectID RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval;

// Added to allow removal of notification for tasks being deleted
+ (BOOL) removeNotificationForTaskObjectID: (NSString *) modifiedTaskObjectID;
+ (void) deleteConcurrentFutureNotifications: (Task *) task;

// Fetching the group for a sort number to allow displaying grouped data in sorted order but with the name of the group and not the sort number.
+ (Group *) fetchGroupBySortOrder: (NSString *) sortOrder ManagedContext: (NSManagedObjectContext *) context;

+ (BOOL) deleteTask: (Task *) task;
+ (int) deleteCompletedTasks: (int) olderThan;
+ (void) deleteLogic: (Task *) task;

/* clear alert notification flag from tasks
 */
+ (void) clearAllAlertNotification;
/* clear alert badge, increment the alert badge or update badge count from db record count
 */
+ (void) updateAlertBadge: (int) mode;
/* get count of records which has alert notification on
 */
+ (int) countAlertNotificationItems;
    
+ (void) saveContext;
+ (BOOL) saveTask: (Task *) task;

+ (Task *) addTask:(NSString *) name Gain:(int) gain DueOn:(NSDate *)dueOn RemindOn:remindOn Note:(NSString *)note Flag:(int) flag Group:(Group *)group  EstimCountIdx: (int) estimCountIdx EstimUnitIdx: (int) estimUnitIdx Tags: (NSMutableSet *) selTags RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval ;

+ (NSDate *) getDateWithSecondsResetForDate: (NSDate *) date;
// Utility API to be used across different places for getting the text to be used on Tag Label Control from selected tags
+ (NSString *)getSelectedTagsLabelText: (NSSet *) tags;

+ (void)configureCell:(TaskViewCell *)mvaCell Task: (Task*) activityAtIndex;

+ (NSString *) cleanDictSetPrint: (NSMutableDictionary *) localDictWithSet ;

+ (NSPredicate *) generatePredicate ;

@end
