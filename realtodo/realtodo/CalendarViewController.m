//
//  ViewController.m
//  calendar
//
//  Created by iMobile on 10/3/12.
//  Copyright (c) 2012 iMobile. All rights reserved.
//

#import "CalendarViewController.h"
#import "TKCalendarMonthView.h"
#import "AppSharedData.h"
#import "SortViewController.h"

@interface CalendarViewController ()
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerObj;
@end

@implementation CalendarViewController

@synthesize dataArray, dataDictionary, managedObjectContext = _managedObjectContext, fetchedResultsControllerObj=_fetchedResultsControllerObj;


- (void) viewDidLoad{
	[super viewDidLoad];
	[self.monthView selectDate:[NSDate month]];
    
}
- (void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
    }
    
    //NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"dd.MM.yy"];
    //NSDate *d = [dateFormatter dateFromString:@"02.05.11"];
    //[dateFormatter release];
    //[self.monthView selectDate:d];
	
    
	
}

- (NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate{
    NSLog(@"Start date is: %@", startDate);
	[self generateRandomDataForStartDate:startDate endDate:lastDate];
	return dataArray;
}
- (void) calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)date{
	
	// CHANGE THE DATE TO YOUR TIMEZONE
	TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSDate *myTimeZoneDay = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone systemTimeZone]];
	myTimeZoneDay = [NSDate date];
	NSLog(@"Date Selected: %@",myTimeZoneDay);
	
	[self.tableView reloadData];
}
- (void) calendarMonthView:(TKCalendarMonthView*)mv monthDidChange:(NSDate*)d animated:(BOOL)animated{
	[super calendarMonthView:mv monthDidChange:d animated:animated];
	[self.tableView reloadData];
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
	
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSArray *ar = [dataDictionary objectForKey:[self.monthView dateSelected]];
	if(ar == nil) return 0;
	return [ar count];
}
- (UITableViewCell *) tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
	
    
	NSArray *ar = [dataDictionary objectForKey:[self.monthView dateSelected]];
	cell.textLabel.text = [ar objectAtIndex:indexPath.row];
	
    return cell;
	
}


- (void) generateRandomDataForStartDate:(NSDate*)start endDate:(NSDate*)end{
	// this function sets up dataArray & dataDictionary
	// dataArray: has boolean markers for each day to pass to the calendar view (via the delegate function)
	// dataDictionary: has items that are associated with date keys (for tableview)
	
	//start = [NSDate date];
	NSLog(@"Delegate Range: %@ %@ %d",start,end,[start daysBetweenDate:end]);
	[self getAllEvents];
    
	self.dataArray = [NSMutableArray array];
	self.dataDictionary = [NSMutableDictionary dictionary];
    
    NSDate *d = start;
	while(YES){
		
		int r = arc4random();
		if(r % 3==1){
			[self.dataDictionary setObject:[NSArray arrayWithObjects:@"Item one",@"Item two",nil] forKey:d];
			[self.dataArray addObject:[NSNumber numberWithBool:YES]];
			
		}else if(r%4==1){
			[self.dataDictionary setObject:[NSArray arrayWithObjects:@"Item one",nil] forKey:d];
			[self.dataArray addObject:[NSNumber numberWithBool:YES]];
			
		}else
			[self.dataArray addObject:[NSNumber numberWithBool:NO]];
		
		
		TKDateInformation info = [d dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		info.day++;
		d = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		if([d compare:end]==NSOrderedDescending) break;
	}
	
}

-(void) getAllEvents {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext = [appDelegate managedObjectContext];
    _fetchedResultsControllerObj = nil;
    Task *activityAtIndex =     [[self fetchedResultsController12] objectAtIndexPath:0];
    NSLog(@"TaskName: %@", activityAtIndex.name);
    _fetchedResultsControllerObj = nil;
    int count = [self fetchedResultsController12].fetchedObjects.count;
    NSLog(@"ViewController Fetched Objects Count: %d", count);
    
    SortViewController *svc = [[SortViewController alloc] initWithMgdObj:_managedObjectContext];
    count = [svc fetchedResultsController].fetchedObjects.count;
    NSLog(@"ReturnedFromSortVC: %d", count);
}

- (NSFetchedResultsController *)fetchedResultsController12
{
    if (_fetchedResultsControllerObj != nil) {
        return _fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSString *sortKeyName = @"gain";
    //NSString *sortKeyName = nil;
    BOOL sortOrderAscending = NO;
    
    
    // Create the sort descriptors array.
    NSSortDescriptor *dueOnDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKeyName ascending:sortOrderAscending];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dueOnDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *filterPredicate = nil;
    
        //filterPredicate =  [NSPredicate predicateWithFormat:@"(done == 0)"];
        [fetchRequest setPredicate:filterPredicate];
  
    
    // Create and initialize the fetch results controller.
    _fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:_managedObjectContext
                                                                      sectionNameKeyPath:nil
                                                                               cacheName:nil];
    _fetchedResultsControllerObj.delegate = self;
    
    // Memory management.
    return _fetchedResultsControllerObj;
    
}

//- (NSFetchedResultsController *)fetchedResultsController
-(int) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        //return self.fetchedResultsControllerObj;
        [[self fetchedResultsControllerObj].fetchedObjects count];
    }
    
    self.fetchedResultsControllerObj = [[AppSharedData getInstance].dynamicViewData genFetchReqCtrlFromSelConfig: _managedObjectContext];
    //self.fetchedResultsControllerObj.delegate = self;
    
    // Memory management.
    DLog(@"Dynamic: fetchResultsCtroller: RE-QUERY");
    NSLog(@"count: %@", self.fetchedResultsControllerObj);
    //return self.fetchedResultsControllerObj;
    
    
    
    return [[self fetchedResultsControllerObj].fetchedObjects count];
}

- (NSFetchedResultsController *)fetchedResultsController1
//-(int) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        //return self.fetchedResultsControllerObj;
        [[self fetchedResultsControllerObj].fetchedObjects count];
    }
    
    self.fetchedResultsControllerObj = [[AppSharedData getInstance].dynamicViewData genFetchReqCtrlFromSelConfig: _managedObjectContext];
    //self.fetchedResultsControllerObj.delegate = self;
    
    // Memory management.
    DLog(@"Dynamic: fetchResultsCtroller: RE-QUERY");
    NSLog(@"count: %@", self.fetchedResultsControllerObj);
    return self.fetchedResultsControllerObj;
    
    
    
    //return [[self fetchedResultsControllerObj].fetchedObjects count];
}

@end
