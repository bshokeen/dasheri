//
//  AFilterSettingsViewController.h
//  realtodo
//
//  Created by Balbir Shokeen on 9/3/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FilterGroupSelectviewController.h"
#import "FilterItemDetailsViewController.h"


@interface FilterSettingsViewController : UITableViewController <NSFetchedResultsControllerDelegate, FilterItemDetailsViewControllerDelegate, FilterGroupSelectviewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *orderBy_btn;
@property (weak, nonatomic) IBOutlet UILabel *selectedCollapseOn;
@property (weak, nonatomic) IBOutlet UILabel *selectedDisplayType;
@property (weak, nonatomic) IBOutlet UILabel *selectedOrderBy;
@property (weak, nonatomic) IBOutlet UILabel *selectedTags;
@property (weak, nonatomic) IBOutlet UILabel *selectedGroups;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;


- (IBAction)setOrderDirection:(id)sender;


- (void) setOrderBtnImage;

@end
