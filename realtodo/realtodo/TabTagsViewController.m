//
//  TagViewController.m
//  realtodo
//
//  Created by Me on 20/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TabTagsViewController.h"
#import "AppDelegate.h"

@interface TabTagsViewController ()
@property (nonatomic, strong) Tag *tagSelected;
@property (nonatomic, strong) NSIndexPath *lastSelectedTagName;
@end

@implementation TabTagsViewController


@synthesize tagNameText = _tagNameText;

@synthesize task = _task;
@synthesize pickedTags;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize sourceUI = _sourceUI;

@synthesize lastSelectedTagName = _lastSelectedTagIndexPath;
@synthesize tagSelected = _tagSelected;

- (IBAction)rightNavigBtnClick:(id)sender {
    
}

- (void)initWithTask:(Task *) task
{
    //    self = [super init];
    if (task) {
        // Custom initialization
        
        _task = task;
    }
    else {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"You must pass a Task Object else use initWithoutTask: %@", NSStringFromSelector(_cmd)]
                                     userInfo:nil];
        
    }
    //    return self;
}

// TODO: Improve this method of sharing data with TagViewController, why it should be aware of source...
- (void)initWithoutTask: (NSString *) sourceUI
{
    //    self = [super init];
    if (self) {
        // Custom initialization
        _sourceUI = sourceUI;
    }
    //    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /* COmmenting auto creation as this is not the prefered method of adding default tags
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *preloadTags = [defaults objectForKey:@"preloadTags"];
    
    if ([preloadTags isEqualToString:@"loadedTagsAlready"]) {
    }
    
    else {
        [defaults setObject:@"loadedTagsAlready" forKey:@"preloadTags"];
        
        NSMutableArray *preTagsArray = [[AppSharedData getInstance] preCreateTags];
        
        for (int i=0; i<[preTagsArray count]; i++) {
            [self createNewTag: [preTagsArray objectAtIndex:i]];
        }
    }*/
    
    self.pickedTags = [[NSMutableSet alloc] init];
    [self.tagNameText setDelegate:self];
    
    // Retrieve all tags
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
	    DLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Each tag attached to the details is included in the array
    // To allow calling from Filter Screen, i had to use an NSSet. ENsure it is consistently followed.
    NSSet *tags = nil;
    if (self.sourceUI != nil) {
        if ( [self.sourceUI isEqualToString:SOURCE_UI_FILTERUI] ) {
            tags = [AppSharedData getInstance].dynamicViewData.tagSet;
        }
        else if ([self.sourceUI isEqualToString:SOURCE_UI_ADDUI] ) {
            tags = [AppSharedData getInstance].lastSelectedTagsOnNewTask;
        }
        else {
            DLog(@"ERROR!!! - sourceUI is not defined well for TagSelectionUI");
        }
    }
    else {
        tags = self.task.tagS;
    }
    
    
    for (Tag *tag in tags) {
        
        [pickedTags addObject:tag];
        
    }
    
    // setting up add button
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTag)];
    
    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (self.sourceUI != nil) {
        if ( [self.sourceUI isEqualToString:SOURCE_UI_FILTERUI] ) {
            [AppSharedData getInstance].dynamicViewData.tagSet = pickedTags;
        }
        else if ([self.sourceUI isEqualToString:SOURCE_UI_ADDUI] ) {
            [AppSharedData getInstance].lastSelectedTagsOnNewTask = pickedTags;
        }
        else {
            DLog(@"ERROR!!! - sourceUI is not defined well for TagSelectionUI");
        }
    }
    else {
        self.task.tagS = pickedTags;
    }
    
    NSError *error = nil;
    if (![self.fetchedResultsController.managedObjectContext save:&error]) {
        DLog(@"Core data error %@, %@", error, [error userInfo]);
        abort();
    }
    
    //    [self.navigationController popViewControllerAnimated:YES];
}

#pragma Enable Edit/Add Mode
-(void) enableEditTagName : (NSIndexPath *) indexPath {
    if (indexPath) {
        Tag *tag = (Tag *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        self.tagNameText.text = tag.name;
        
        
        self.tagNameText.placeholder = @"Enter New Tag Name...";// Select/Deselect Item to Edit/Add...";
        
    }else {
        
        [self enableAddTagName];
    }
}

- (void) enableAddTagName {
    
    self.tagNameText.text = @"";
    self.tagNameText.placeholder = @"Enter New Tag Name...";
}

#pragma mark - Actions

- (void) addTag {
    
    UIAlertView *newTagAlert = [[UIAlertView alloc] initWithTitle:@"New tag" message:@"Insert new tag name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    
    newTagAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [newTagAlert show];
    
}


#pragma mark - Alert view delegate


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        DLog(@"cancel");
        
    } else {
        
        
        NSString *tagName = [[alertView textFieldAtIndex:0] text];
        
        Tag *tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
                                                 inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
        tag.name = tagName;
        
        NSError *error = nil;
        if (![tag.managedObjectContext save:&error]) {
            DLog(@"Core data error %@, %@", error, [error userInfo]);
            abort();
        }
        
        [self.fetchedResultsController performFetch:&error];
        
        [self.tableView reloadData];
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ItemDetailsCell *tagCell = (ItemDetailsCell *) cell;
    
    Tag *tag = (Tag *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([pickedTags containsObject:tag]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    tagCell.itemNameLabel.text = tag.name;
    
    int totalCount = [tag.taskS count];
    int activeCount = 0;
    for (Task *taskObj in tag.taskS) {
        NSLog(@"%@", taskObj.done);
        if ([taskObj.done intValue] == 0)
            activeCount +=1;
    }
    tagCell.itemTasksCountLabel.text =  [NSString stringWithFormat:@"(%i/%i)",activeCount, totalCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*
     
     static NSString *cellIdentifier = @"TagViewControllerCell";
     
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     if (cell == nil) {
     cell = [[UITableViewCell alloc]
     initWithStyle:UITableViewCellStyleDefault
     reuseIdentifier:CellIdentifier];
     }
     cell.accessoryType = UITableViewCellAccessoryNone;
     
     Tag *tag = (Tag *)[self.fetchedResultsController objectAtIndexPath:indexPath];
     if ([pickedTags containsObject:tag]) {
     cell.accessoryType = UITableViewCellAccessoryCheckmark;
     }
     
     cell.textLabel.text = tag.name;
     */
    static NSString *cellIdentifier = @"ItemDetailsCell";
    
    ItemDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /* Not working right now with xib, learn and try again later.
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (ItemDetailsCell *) [nib objectAtIndex:0];
     }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

#
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the managed object.
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



- (void) resetSortOrder {
    
    
}
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Tag *curObj = (Tag *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];
        
        
        //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
    Tag *movedObj = (Tag *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];
    
    // Save of this operation will be done on click of Edit Button as Done
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self enableAddTagName];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
    
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Tag *tag = (Tag *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (_task || _sourceUI) {
        
        self.tagSelected = nil;
        self.lastSelectedTagName = nil;
        UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:YES];
        
        if ([pickedTags containsObject:tag]) {
            
            [pickedTags removeObject:tag];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        } else {
            
            [pickedTags addObject:tag];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
    }
    
    else {
        if ([self.lastSelectedTagName isEqual:indexPath]) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            self.lastSelectedTagName = nil;
            [self enableAddTagName];
        } else {
            
            self.tagSelected = tag;
            self.lastSelectedTagName = indexPath;
            [self enableEditTagName:indexPath];
        }
        
        
    }
}

#pragma mark - Result controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Tag"
                                   inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"sort_order"
                                        ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]  initWithFetchRequest:fetchRequest
                                                                                                 managedObjectContext:APP_DELEGATE_MGDOBJCNTXT
                                                                                                   sectionNameKeyPath:nil
                                                                                                            cacheName:nil];
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    DLog(@"Core data error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Without this autoupdate will not work.
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload {
    [self setTagNameText:nil];
    self.tagNameText.text= @"";
    self.tagNameText.placeholder = @"Enter New Tag Name...";
    self.tagSelected = nil;
    self.lastSelectedTagName = nil;
    [super viewDidUnload];
}



#pragma TextField Delegate Handling
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if ( textField == self.tagNameText) {
        
        NSString *tagName = [AppUtilities trim:textField.text];
        
        if (tagName.length == 0) {
            [textField resignFirstResponder];
        }
        else {
            if ([self ifTagAlreadyExist:tagName]) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Tag Already Exist."
                                                               delegate:self cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                [alert show];
                self.tagNameText.text= @"";
            }
            else {
                //                if (self.isSelectMode) {
                // Edit is not allowed so no need to update and direclty go for add.
                if (self.lastSelectedTagName == nil) {
                    [self createNewTag:textField.text];
                    self.tagNameText.text= @"";
                }
                else
                    self.tagSelected.name = tagName;
                //                }
                //                else {
                //                    // Edit of existing is allowed hence see if need to update
                //                    if (self.lastSelectedGroupName == nil) {
                //                        [self createNewGroup:textField.text];
                //                        self.groupNameText.text= @"";
                //                    } else {
                //                        self.groupSelected.name = groupName;
                //                    }
                //                }
                self.tagNameText.text= @"";
                self.tagSelected = nil;
                self.lastSelectedTagName = nil;
                [textField resignFirstResponder];
            }
        }
    }
    
    return YES;
}

- (void) fetchData {
    
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    [self.tableView reloadData];
}

-(void) saveUpdatedTag: (Tag *) existingTag {
    NSError *error = nil;
    if (![existingTag.managedObjectContext save:&error]) {
        DLog(@"Error Saving Tag %@, %@", error, [error userInfo]);
        
        abort();
    }
}

-(BOOL) ifTagAlreadyExist: (NSString *)tagName {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.fetchedResultsController.managedObjectContext]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(name == %@)", tagName]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [self.fetchedResultsController.managedObjectContext
                        countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}


-(void) createNewTag: (NSString *)tagName {
    // Planning to add a new context object, so referring to fetrscntrl managed context specifically
    
    Tag *newTag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
                                                inManagedObjectContext:self.fetchedResultsController.managedObjectContext];
    newTag.name = tagName;
    newTag.sort_order = [NSNumber numberWithInt:[self.fetchedResultsController.fetchedObjects count] + 1 ];
    
    NSError *error = nil;
    if (![newTag.managedObjectContext save:&error]) {
        DLog(@"Error Saving Tag %@, %@", error, [error userInfo]);
        
        abort();
    } 
}


@end
