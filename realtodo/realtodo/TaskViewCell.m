//
//  TaskViewCell.m
//  realtodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TaskViewCell.h"

@implementation TaskViewCell

@synthesize titleLabel = _titleLabel;
@synthesize subtitleLabel = _subtitleLabel;
@synthesize statusButton1 = _statusButton1;
@synthesize statusButton2 = _statusButton2;
@synthesize infoLabel1 = _infoLabel1;
@synthesize infoButton1 = _infoButton1;
@synthesize infoButton2 = _infoButton2;
@synthesize notesButton = _notesButton;
@synthesize tagsButton = _tagsButton;
@synthesize task = _task;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        ULog(@"Test");
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) resetInitialText {
    [[self infoButton1] setTitle:@"" forState:UIControlStateNormal];
    [[self infoButton2] setTitle:@"" forState:UIControlStateNormal];
    [[self statusButton1] setTitle:@"" forState:UIControlStateNormal];
    [[self statusButton2] setTitle:@"" forState:UIControlStateNormal];
    [[self notesButton] setTitle:@"" forState:UIControlStateNormal];
    [[self tagsButton] setTitle:@"" forState:UIControlStateNormal];
    
    //TODO: status1 button was not getting the doneClick event called. So added this target to invoke it for respective view controllers. There should be some way to fix it on storyboard, find it out and resolve. As controls are copy pasted it may be sending events to dynamicview only.
    [[self statusButton1] addTarget:self action:@selector(doneClick:) forControlEvents:UIControlEventTouchUpInside];

}

- (IBAction)doneClick:(id)sender {
    
    if (self.task) {
        switch ([self.task.done intValue]) {
            case 0:
            {
                self.task.done =  [NSNumber numberWithInt:1];
                self.task.completed_on = [NSDate date];
//                self.task.completed_on = [NSDate dateWithTimeIntervalSinceNow:-24*60*60*40]; // Row added to force an older date to test delete older tasks
                break;
            }
            case 1:
            {
                self.task.done =  [NSNumber numberWithInt:0];
                self.task.completed_on = nil;
                break;
            }
            default:
                break;
        }
    }
}



@end
