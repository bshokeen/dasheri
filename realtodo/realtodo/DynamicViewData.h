//
//  DynamicViewData.h
//  realtodo
//
//  Created by Me on 12/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Tag.h"

// Used to store the configuration for displaying data
@interface DynamicViewData : NSObject

@property (nonatomic) BOOL reloadUI;

// UI Selected Items Index
@property (nonatomic) int collapsibleOnIdxPrevSelected;
@property (nonatomic) int collapsibleOnIdx;
@property (nonatomic) int orderByIdx;
@property (nonatomic) int displayTypeIdx;
@property (nonatomic) int sortCategoriesIdx;

@property (nonatomic) BOOL defaultValuesChanged; // to check default values got changed (to change header color)
@property (nonatomic) BOOL orderByDirection;
@property (nonatomic) BOOL excludeConditionFinishedTasks;
@property (nonatomic) BOOL autoDeleteFinishedTasks;
@property (nonatomic, strong) NSSet *tagSet;
@property (nonatomic, strong) NSSet *groupSet;

// UI Label Arrays
@property (nonatomic, strong) NSMutableArray* collapsibleOnUIValueArr;
@property (nonatomic, strong) NSMutableArray* orderByUIValueArr;
@property (nonatomic, strong) NSMutableArray* displayTypeValArr;
@property (nonatomic, strong) NSMutableArray* sortCategoriesValArr;

// Reminder Weekdays Array
@property (nonatomic, strong) NSMutableArray* reminderWeekDaysArr;


// Used to store the search tag from DynamicViewController to use when generating the query
@property (nonatomic, strong) NSString *searchText;

// Used to track the shift factor for landscape mode
@property (nonatomic) int landscapeWidthFactor;

- (NSFetchedResultsController *) genFetchReqCtrlFromSelConfig: (NSManagedObjectContext *) mgdObjContext;

- (int) getAllCollapsibleOnCount;
- (void) resetToDefaultValues;
- (BOOL) checkForDefaults;

/*
 // TO prevent it being reused across different screens.
 - (void) resetLastSelectedTagSet;
 */


@end
