//
//  TaskViewCell.h
//  realtodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"


@interface TaskViewCell : UITableViewCell

@property (nonatomic, weak) Task *task;


@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel1;
@property (nonatomic, weak) IBOutlet UIButton *statusButton1;
@property (nonatomic, weak) IBOutlet UIButton *statusButton2;
@property (nonatomic, weak) IBOutlet UIButton *infoButton1;
@property (nonatomic, weak) IBOutlet UIButton *infoButton2;
@property (nonatomic, weak) IBOutlet UIButton *notesButton;
@property (nonatomic, weak) IBOutlet UIButton *tagsButton;


- (IBAction)doneClick:(id)sender;


-(void) resetInitialText;

@end
