//
//  UpdateActivityViewController.m
//  realtodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "UpdateTaskViewController.h"
#import "AppUtilities.h"
#import <QuartzCore/QuartzCore.h>

@interface UpdateTaskViewController ()

@property (strong, nonatomic) UIPopoverController *masterPopoverController;
// To avoid updates on fetchrequestctrl when new group is selected, storing it here and setting during save button click.
@property (nonatomic, strong) Group *selectedGroup;
//@property (nonatomic, strong) NSUndoManager *undoManager;
- (void)updateInterface;

// Attributes to store the selected values are stored here. To avoid processing the UI text labels additionally storing the index values.
@property (nonatomic) int selEstimCountIdx;
@property (nonatomic) int selEstimUnitIdx;
@property (nonatomic) int selActualCountIdx;
@property (nonatomic) int selActualUnitIdx;

// Attributes to store data before that is written over the task object. Maintaining both variables to avoid parsing/processing the UI Label Text.
@property (nonatomic, strong) NSDate *selRemindOn;
@property (nonatomic) NSCalendarUnit selNSCURemindRepeatInterval;
@end

@implementation UpdateTaskViewController
@synthesize progressSlider = _progressSlider;
@synthesize progressValueLabel = _progressValueLabel;
@synthesize remindOnLabel = _remindOnLabel;
@synthesize flagBtn = _flagBtn;
@synthesize updateTaskTableView = _updateTaskTableView;
@synthesize estimatCountLabel = _estimatCountLabel;
@synthesize estimUnitLabel = _estimUnitLabel;
@synthesize actualCountLabel = _actualCountLabel;
@synthesize actualUnitLabel = _actualUnitLabel;
@synthesize tagSlabel = _tagSlabel;
@synthesize contextLabel = _contextLabel;
@synthesize noteLabel = _noteLabel;

@synthesize doneBtn = _doneBtn;
@synthesize task = _task;
@synthesize nameText = _nameText;
@synthesize groupNameLabel = _groupNameLabel;
@synthesize gainOptions = _gainOptions;
@synthesize dueOnLabel = _dueDateLabel;
@synthesize createdDateLabel = _createdDatelabel;
@synthesize selectedGroup= _selectedGroup;
@synthesize gainImage = _gainImage;

//@synthesize detailItem = _detailItem;
//@synthesize detailDescriptionLabel = _detailDescriptionLabel;
@synthesize masterPopoverController = _masterPopoverController;

@synthesize delegate = _delegate;
//@synthesize undoManager =_undoManager;


@synthesize selEstimCountIdx = _selEstimCountIdx;
@synthesize selEstimUnitIdx = _selEstimUnitIdx;
@synthesize selActualCountIdx =_selActualCountIdx;
@synthesize selActualUnitIdx = _selActualUnitIdx;

// Reminder Options
@synthesize selRemindOn = _selRemindOn, selNSCURemindRepeatInterval = _selNSCURemindRepeatInterval;

#pragma mark - Managing my data object item

- (void) setTask:(Task *) newTask
{
    if ( _task != newTask ) {
        _task = newTask;
        
        // Update the view
        [self updateInterface];
    }
}

- (void) updateInterface
{
    // update the user interface for the detail
    Task *theTask  = self.task;
    
    
    if (theTask) {
        self.nameText.text = theTask.name;
        
        self.groupNameLabel.text = [AppUtilities handleEmptyString:theTask.group.name]; //theTask.projectName;
        //        self.groupNameLabel.text = [self handleEmptyString:theTask.projectName];
        self.gainOptions.selectedSegmentIndex = [theTask.gain integerValue];
        
        [self.gainOptions addTarget:self action:@selector(gainChanged) forControlEvents:UIControlEventValueChanged];
        [self setGainButtonImage:[theTask.gain integerValue]]; // set the gain image according to the default gain
        
        if ( [theTask.done integerValue] == 1) {
            [self.doneBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOn"]] forState:UIControlStateSelected];
            [self.doneBtn setSelected:YES];
        }
        
        self.dueOnLabel.text = [AppUtilities uiFormatedDate: theTask.dueOn];
        
        self.createdDateLabel.text = [AppUtilities uiFormatedDate:theTask.createdOn];
        
        self.noteLabel.text = theTask.note;
        [self.progressSlider setValue: [theTask.progress floatValue]];
        
        int progressAsInt =(int)([theTask.progress floatValue] + 0.5f);
        progressAsInt = progressAsInt*5;
        NSString *newText =[[NSString alloc] initWithFormat:@"%d",progressAsInt];
        self.progressValueLabel.text = [newText stringByAppendingString:@"%"];
        
        // Reminder Date preparing for display
        self.remindOnLabel.text = [[AppSharedData getInstance] getRemindOnLabelTextFromCalendarUnit:[theTask.remind_repeat_interval intValue] RemindDate:theTask.remind_on];
        
        if ( [theTask.flag integerValue] == 1)  {
            [self.flagBtn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOn"]] forState:UIControlStateSelected];
            [self.flagBtn setSelected:YES];
        }
        
        self.selEstimCountIdx = [theTask.effort_time intValue];
        self.selEstimUnitIdx = [theTask.effort_time_unit intValue];
        self.selActualCountIdx = [theTask.actual_time intValue];
        self.selActualUnitIdx = [theTask.actual_time_unit intValue];
        self.estimatCountLabel.text = [APP_SHARED_DATA uiTextFromCountIdx:self.selEstimCountIdx];
        self.estimUnitLabel.text = [APP_SHARED_DATA uiTextFromUnitIdx:self.selEstimUnitIdx];
        if ( self.selActualCountIdx > 0 && self.selActualUnitIdx > 0) {
            // Not a good way to detect if the value was not set so assuming this is not set. Similar thing to handle for saving.
            self.actualCountLabel.text = [APP_SHARED_DATA uiTextFromCountIdx:self.selActualCountIdx];
            self.actualUnitLabel.text = [APP_SHARED_DATA uiTextFromUnitIdx:self.selActualUnitIdx];
        }
        //        self.progressValueLabel.layer.cornerRadius = 10.0;
    }
}


- (void) viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBar.tintColor = nil;
    NSSet *tags = self.task.tagS;
    
    self.tagSlabel.text = [AppUtilities getSelectedTagsLabelText:tags];
    
}

- (void) viewDidUnload
{
    self.task = nil;
    
    [self setNameText:nil];
    [self setGainOptions:nil];
    [self setCreatedDateLabel:nil];
    [self setProgressSlider:nil];
    [self setNoteLabel:nil];
    [self setProgressValueLabel:nil];
    [self setRemindOnLabel:nil];
    [self setGroupNameLabel:nil];
    [self setDoneBtn:nil];
    [self setFlagBtn:nil];
    [self setUpdateTaskTableView:nil];
    [self setEstimatCountLabel:nil];
    [self setEstimUnitLabel:nil];
    [self setActualCountLabel:nil];
    [self setActualUnitLabel:nil];
    [self setTagSlabel:nil];
    [self setContextLabel:nil];
    [super viewDidUnload];
}

- (void) gainChanged {
    int selIdx = [self.gainOptions selectedSegmentIndex];
    [self setGainButtonImage:selIdx];
}

-(void) setGainButtonImage:(NSUInteger) gainIndex {
    
    switch (gainIndex) {
            
        case 0:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"highGain"]] forState:UIControlStateNormal];
            break;
        case 1:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"mediumGain"]] forState:UIControlStateNormal];
            break;
        case 2:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"normalGain"]] forState:UIControlStateNormal];
            break;
        case 3:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"lowGain"]] forState:UIControlStateNormal];
            break;
        default:
            _gainImage.backgroundColor = [UIColor redColor];
            break;
    }
    
}


//#pragma mark - Managing the detail item

//- (void)setDetailItem:(id)newDetailItem
//{
//    if (_detailItem != newDetailItem) {
//        _detailItem = newDetailItem;
//
//        // Update the view.
//        [self configureView];
//    }
//
//    if (self.masterPopoverController != nil) {
//        [self.masterPopoverController dismissPopoverAnimated:YES];
//    }
//}

//- (void)configureView
//{
//    // Update the user interface for the detail item.
//
//    if (self.detailItem) {
//        self.detailDescriptionLabel.text = [self.detailItem description];
//    }
//}
//
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self updateInterface];
    //    [self setUpUndoManager];
    
    // To avoid reset of current group when saved without selecting a different group, load it as selected.
    self.selectedGroup = self.task.group;
    
    // To allow going back to prev view without change we don't overwrite task obj variables when updated in child views. So these additional variables are used for saving. Hence setting them up with values on task object.
    self.selRemindOn = self.task.remind_on;
    self.selNSCURemindRepeatInterval = [self.task.remind_repeat_interval intValue];
    
    //    [self.doneBtn setImage:[UIImage imageNamed:@"RTTaskCheckedOff.png"] forState:UIControlStateNormal];
    //    [self.doneBtn setImage:[UIImage imageNamed:@"RTTaskButtonIconCheckmark.png"] forState:UIControlStateSelected];
    
}

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    self.detailDescriptionLabel = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (IBAction)cancel:(id)sender {
    [[self delegate] updateActivityViewControllerDidCancel:self];
}


#pragma mark - Done Buton Click.
- (IBAction)done:(id)sender {
    //TODO: I have to use the data captured from user instead of the hardcoded current date
    [AppUtilities deleteConcurrentFutureNotifications:self.task]; // To delete existing notifications
    [self.task setName:self.nameText.text];
    
    // Update the group value newly selected. It should set by original value if not changed.
    self.task.group = self.selectedGroup;
    [self.task setProjectName: [AppUtilities handleEmptyString:self.groupNameLabel.text]];
    
    [self.task setGain: [NSNumber numberWithInt:[self.gainOptions selectedSegmentIndex]]];
    
    if ([self.dueOnLabel.text isEqualToString:NEVER])
        [self.task setDueOn:nil];
    else
        [self.task setDueOn:[AppUtilities uiUNFormatedDate:self.dueOnLabel.text]];

    // Reset Reminder if there is a change on remind options
    if ( (![self.selRemindOn isEqualToDate:self.task.remind_on]) || (self.selNSCURemindRepeatInterval != [self.task.remind_repeat_interval intValue]) ) {

        // Remove existing notification
        NSString *taskObjectID = [[[self.task objectID] URIRepresentation] absoluteString];
        [AppUtilities removeNotificationForTaskObjectID:taskObjectID];
        
        // Use the latest selected Values
        [self.task setRemind_on:self.selRemindOn];
        [self.task setRemind_repeat_interval:[NSNumber numberWithInt:self.selNSCURemindRepeatInterval]];
        
        // Create the alert if required
        if ( self.task.remind_on != nil )
            [AppUtilities scheduleAlarm:self.groupNameLabel.text AlarmDate:self.task.remind_on  TaskName:self.nameText.text TaskObjectID:taskObjectID RemindInterval:[self.task.remind_repeat_interval intValue]];
    }
    
    
    if ( [self.doneBtn isSelected] )
        [self.task setDone: [NSNumber numberWithInt:1]];
    else
        [self.task setDone: [NSNumber numberWithInt:0]];
    
    
    if ( [self.flagBtn isSelected]) {
        [self.task setFlag: [NSNumber numberWithInt:1]];
    }
    else {
        [self.task setFlag:[NSNumber numberWithInt:0]];
    }
    
    [self.task setModified_on:[NSDate date]];
    
    [self.task setNote:[self.noteLabel text]];
    [self.task setProgress:[NSNumber numberWithFloat:self.progressSlider.value]];
    
    [self.task setEffort_time:[NSNumber numberWithInt:self.selEstimCountIdx]];
    [self.task setEffort_time_unit:[NSNumber numberWithInt:self.selEstimUnitIdx]];
    // As preventing the 0 values does not gain anythng not doing a check
    [self.task setActual_time:[NSNumber numberWithInt:self.selActualCountIdx]];
    [self.task setActual_time_unit:[NSNumber numberWithInt:self.selActualUnitIdx]];
    
    // PUt a temporary check for debugging
    NSCalendarUnit nscuRemindRepeatInterval = [self.task.remind_repeat_interval intValue];
    if (nscuRemindRepeatInterval == NSDayCalendarUnit || nscuRemindRepeatInterval == NSWeekCalendarUnit || nscuRemindRepeatInterval == NSMonthCalendarUnit || nscuRemindRepeatInterval == NSYearCalendarUnit || nscuRemindRepeatInterval == NSEraCalendarUnit || nscuRemindRepeatInterval == 0 ) {
        
    }
    else {
        ULog(@"Invalid Remind Repeat Interval: %d", nscuRemindRepeatInterval);
        DLog(@"ERROR - Invalid Repeat Interval: %d", nscuRemindRepeatInterval);
    }
    
    if (self.nameText.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update Task" message:@"Please enter task name." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    else
        [[self delegate] updateActivityViewControllerDidFinish:self task:self.task WithDelete:NO];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if ( textField == self.nameText) {
        [textField resignFirstResponder];
    }
    
    return YES;
}

/*
 Manage row selection: If a row is selected, create a new editing view controller to edit the property associated with the selected row.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // HIDE keyboard when moving to different views
    [self.nameText resignFirstResponder];
    
    switch (indexPath.section) {
        case 0: 
        {
            switch (indexPath.row) 
            {
                case 0: {
                    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
                    [cell setSelected:NO animated:NO];
                    break;
                }
                case 1:
                    [self performSegueWithIdentifier:@"ShowGroupEditViewController" sender:self];
                    break;
                    
                default: 
                {
                    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
                    [cell setSelected:NO animated:NO];
                    DLog(@"Error, Not expected to come here.");
                break;
                }
            }
            break;
        }
        case 1: {
            // Fixed bug to show moretextview controller for notes also (it was broken due to Reminder view addition
            switch (indexPath.row) 
            {
                case 1:
                    [self performSegueWithIdentifier:@"ShowMoreTextViewController" sender:self];
                    break;
                case 2:
                    [self performSegueWithIdentifier:@"ShowReminderViewController" sender:self];
                    break;
                case 3:
                    [self performSegueWithIdentifier:@"ShowNotesViewController" sender:self];
                    break;
                default:
                {
                    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
                    [cell setSelected:NO animated:NO];
                    DLog(@"Error, Not expected to come here.");
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                break;}
            }
            break;
        }
        case 2: {// Effort
            switch (indexPath.row) {
                case 0:
                    [self performSegueWithIdentifier:@"ShowEffortViewController" sender:self];
                    break;
                case 1:
                    [self performSegueWithIdentifier:@"ShowEffortViewController" sender:self];
                    break;
                default:
                    break;
            }
            break;
        }
        case 3: {
            // Tags and Context
            switch (indexPath.row) {
                case 0: {
                    // As it was doing double navigation, due to this direct segue definition for this cell. I had to disable it here.
                    //                    TagViewController *tagPicker = [[TagViewController alloc] initWithTask:self.task];
                    //                    [self.navigationController pushViewController:tagPicker
                    //                                                         animated:YES];
                    
                    [self performSegueWithIdentifier:@"ShowTagViewController" sender:self];
                    break;
                }
                default:
                    break;
            }
            break;
        }
            
        case 4: { // Created Date
            switch (indexPath.row) {
                case 0:
                    [self performSegueWithIdentifier:@"ShowMoreTextViewController" sender:self];
                    break;
                default:
                    DLog(@"Error, Not expected to come here.");
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                break;}
            break;
            
        }
        case 5: {
            // Delete Button Clicked
            [[self delegate] updateActivityViewControllerDidFinish:self task:self.task WithDelete:YES];
        }
            
        default:
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            break;
    }
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{    // TODO: MAKE CUSTOM API to PERFORM SEGUE, COMMON FOR DIFFERENT VIEWCONTROLLERS
    if ([[segue identifier] isEqualToString:@"ShowReminderViewController"]) {
        
        ReminderViewController *remindCtrl = (ReminderViewController *) [segue destinationViewController];
        [remindCtrl initWithArgumentsRemindOn:self.selRemindOn RemindRepeatInterval:self.selNSCURemindRepeatInterval];
        
        remindCtrl.delegate = self;
    }
    
    else if ([[segue identifier] isEqualToString:@"ShowMoreTextViewController"]) {
        
        DueDateViewController *controller = (DueDateViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        
        
        CommonDTO *commonDTO = [[CommonDTO alloc] initWithValue: self.nameText.text
                                                           Note:self.noteLabel.text
                                                          DueOn:[AppUtilities uiUNFormatedDate:self.dueOnLabel.text WithDayAdd:YES]
                                                       RemindOn:[AppUtilities uiUNFormatedDate:self.remindOnLabel.text WithDayAdd:YES]
                                                          Group:self.task.group
                                                   TaskObjectID:[[[self.task objectID] URIRepresentation] absoluteString]];
        
        switch (indexPath.row) {
            case 0: // It was commented earlier, so i need to check if this id is correct.
                commonDTO.editFieldType = name;
                break;
            case 1:
                commonDTO.editFieldType = dueOn;
                break;
            case 2:
                commonDTO.editFieldType = remindOn;
                break;
            case 3:
                commonDTO.editFieldType = note;
                break;
            default:
                break;
        }
        
        controller.commonDTOObj = commonDTO;
        
        // to get the events, had to set the delegate as self.
        controller.delegate = self;
    }
    else if ([[segue identifier] isEqualToString:@"ShowNotesViewController"]) {
        
        NotesViewController *controller = (NotesViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        
        
        CommonDTO *commonDTO = [[CommonDTO alloc] initWithValue: self.nameText.text
                                                           Note:self.noteLabel.text
                                                          DueOn:nil
                                                       RemindOn:nil
                                                          Group:nil
                                                   TaskObjectID:nil];
        
        switch (indexPath.row) {
            case 0: // It was commented earlier, so i need to check if this id is correct.
                commonDTO.editFieldType = name;
                break;
            case 3:
                commonDTO.editFieldType = note;
                break;
            default:
                break;
        }
        
        controller.commonDTOObj = commonDTO;
        
        // to get the events, had to set the delegate as self.
        controller.delegate = self;
    }
    else if ( [[segue identifier] isEqualToString:@"ShowGroupEditViewController"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        switch (indexPath.row) {
            case 1: {
                TabGroupsViewController *grpEditViewCntlr = (TabGroupsViewController *)[segue destinationViewController];
                
                [grpEditViewCntlr initializeWithMode: ReadOnlySingleSelection
                                     LastSelGroupSet: [[NSMutableSet alloc] initWithObjects:self.selectedGroup, nil]
                                             Context:self.task.managedObjectContext
                                            Delegate:self];
                
                /*
                 // Create a new managed object context for allowing saving a new group. If updates happen to current task it should be not be saved there rather go to parent view of this controller.
                 NSManagedObjectContext *addingContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
                 [addingContext setParentContext:[self.task managedObjectContext]];
                 grpEditViewCntlr.managedObjectContext = addingContext;
                 */
                
                break;}
            default:
                break;
        }
    }
    else if ( [[segue identifier] isEqualToString:@"ShowEffortViewController"] ) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        EffortViewController *evCtrl = (EffortViewController *) [segue destinationViewController];
        evCtrl.delegate = self;
        
        if (indexPath.row == 0)
            [evCtrl initForKeyName:@"effort" WithCountIdx:self.selEstimCountIdx WithUnitIdx:self.selEstimUnitIdx Delegate:self];
        else if (indexPath.row == 1)
            [evCtrl initForKeyName:@"actual" WithCountIdx:self.selActualCountIdx WithUnitIdx:self.selActualUnitIdx Delegate:self];
        
    }
    
    else if ( [[segue identifier] isEqualToString:@"ShowTagViewController"] ) {
        
        TabTagsViewController *tvCtrl = (TabTagsViewController *) [segue destinationViewController];
        [tvCtrl initWithTask: self.task];
        //        TagViewController *tagPicker = [[TagViewController alloc] initWithTask:self.task];
        //        [self.navigationController pushViewController:tagPicker
        //                                             animated:YES];
    }
    
    
}

#pragma mark - ReminderViewControllerDelegate
- (void) RemindViewControllerDidFinish:(NSDate *)remindOn RepeatInterval:(NSCalendarUnit)nscuRemindRepeatInterval FormatedLabelText: (NSString *) formatedLabelText {
    
    self.selNSCURemindRepeatInterval = nscuRemindRepeatInterval;
    self.selRemindOn = remindOn;
    
    self.remindOnLabel.text = formatedLabelText;

}

#pragma mark - MoreTextViewControllerDelegate
- (void) moreTextViewControllerDidCancel:(__autoreleasing id *)controller {
    
}
- (void) moreTextViewControllerDidFinish:(CommonDTO *)commonDTO: (NSString *)FormatedDate
{
    
    if (commonDTO.editFieldType == name )
        self.nameText.text = commonDTO.name;
    
    else if ((commonDTO.editFieldType == dueOn) && (![FormatedDate isEqualToString:NEVER])) {
        self.dueOnLabel.text = [AppUtilities uiFormatedDate:commonDTO.dueOn];
        
        NSDate *remindOn = commonDTO.remindOn;
        if (![self.remindOnLabel.text isEqualToString:NEVER] && [self.dueOnLabel.text isEqualToString:NEVER]) {
            self.dueOnLabel.text = [AppUtilities uiFormatedDate:remindOn];
        }
    }
    
    else if ([FormatedDate isEqualToString:NEVER]) {
        self.dueOnLabel.text = NEVER;
    }
    
    else if (commonDTO.editFieldType == note)
        self.noteLabel.text = commonDTO.note;
    
    else if (commonDTO.editFieldType ) {
        NSDate *remindOn = commonDTO.remindOn;
        // Ensure not null and check if selected remindOn date is greater than the current date
        //        if (remindOn != nil && ([remindOn compare:[NSDate date]] == NSOrderedDescending)) {
        if (remindOn != nil && [remindOn timeIntervalSinceNow] > 0) {
            remindOn = [AppUtilities getDateWithSecondsResetForDate:remindOn];
            self.remindOnLabel.text = [AppUtilities uiFormatedDate:remindOn];
        } else {
            self.remindOnLabel.text = [AppUtilities uiFormatedDate:nil];
        }
        
        if (remindOn != nil && [self.dueOnLabel.text isEqualToString:NEVER]) {
            self.dueOnLabel.text = [AppUtilities uiFormatedDate:remindOn];
        }
    }
    
    else if (commonDTO.editFieldType == group) {
        self.selectedGroup = commonDTO.group;
        self.groupNameLabel.text = commonDTO.group.name;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Notes Controller Operations
-(void) notesViewControllerDidFinish:(CommonDTO *)commonDTO
{
    if (commonDTO.editFieldType == name )
        self.nameText.text = commonDTO.name;
    
    else if (commonDTO.editFieldType == note)
        self.noteLabel.text = commonDTO.note;
       
    [self.navigationController popViewControllerAnimated:YES];   
}

/*
 #pragma mark -
 #pragma mark Undo support
 
 - (void)setUpUndoManager
 {
 /
 If the task's managed object context doesn't already have an undo manager, then create one and set it for the context and self.
 The view controller needs to keep a reference to the undo manager it creates so that it can determine whether to remove the undo manager when editing finishes.
 /
 if (self.task.managedObjectContext.undoManager == nil) {
 
 NSUndoManager *anUndoManager = [[NSUndoManager alloc] init];
 [anUndoManager setLevelsOfUndo:5];
 self.undoManager = anUndoManager;
 
 self.task.managedObjectContext.undoManager = self.undoManager;
 }
 
 // Register as an observer of the book's context's undo manager.
 NSUndoManager *taskUndoManager = self.task.managedObjectContext.undoManager;
 
 NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
 [dnc addObserver:self selector:@selector(undoManagerDidUndo:) name:NSUndoManagerDidUndoChangeNotification object:taskUndoManager];
 [dnc addObserver:self selector:@selector(undoManagerDidRedo:) name:NSUndoManagerDidRedoChangeNotification object:taskUndoManager];
 }
 
 
 - (void)cleanUpUndoManager
 {
 // Remove self as an observer.
 [[NSNotificationCenter defaultCenter] removeObserver:self];
 
 if (self.task.managedObjectContext.undoManager == self.undoManager) {
 self.task.managedObjectContext.undoManager = nil;
 self.undoManager = nil;
 }
 }
 
 
 - (NSUndoManager *)undoManager
 {
 return self.task.managedObjectContext.undoManager;
 }
 
 
 - (void)undoManagerDidUndo:(NSNotification *)notification {
 
 // Redisplay the data.
 [self updateInterface];
 }
 
 
 - (void)undoManagerDidRedo:(NSNotification *)notification {
 
 // Redisplay the data.
 [self updateInterface];
 
 }
 */

#pragma Slider Handling
- (IBAction)sliderValueChanged:(id)sender {
    UISlider *slider = (UISlider *) sender;
    int progressAsInt =(int)(slider.value + 0.5f);
    progressAsInt = progressAsInt*5;
    NSString *newText =[[NSString alloc] initWithFormat:@"%d",progressAsInt];
    self.progressValueLabel.text = [newText stringByAppendingString:@"%"];
    
}

- (IBAction)delete:(id)sender {

    // Delete Button Clicked
    [[self delegate] updateActivityViewControllerDidFinish:self task:self.task WithDelete:YES];
}

- (IBAction)doneBtnClicked:(id)sender {
    
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOff"]] forState:UIControlStateNormal];
        [sender setSelected:NO];
    }else {
        [sender setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOn"]] forState:UIControlStateSelected];
        [sender setSelected:YES];
    }
}

- (IBAction)flagBtnClicked:(id)sender {
    
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOff"]] forState:UIControlStateNormal];
        [sender setSelected:NO];
    }else {
        [sender setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOn"]] forState:UIControlStateSelected];
        [sender setSelected:YES];
    }
}

-(Group *) getExistingGroupObject: (NSString *)groupName {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Group" inManagedObjectContext:self.task.managedObjectContext]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(name == %@)", groupName]];
    
    // Execute the fetch
    NSError *error = nil;
    
    //    NSLog(@"Count %d",count);
    NSArray *group = [self.task.managedObjectContext
                      executeFetchRequest:fetchRequest error:&error];
    
    if ([group count] > 0) {
        Group *tGroup = (Group *) [group objectAtIndex:0];
        return tGroup;
    }
    else {
        return nil;
    }
}


#pragma GroupEditViewControllerDelegate API Implementation
-(void) groupEditViewControllerDidFinish:(id)controller SelectedGroup:(Group *)group {
    /*
     //    [self.task setGroup:group];
     
     //    NSError *error;
     //    NSManagedObjectContext *addingManagedObjectContext = [controller managedObjectContext];
     //    if (![addingManagedObjectContext save:&error]) {
     //        / *
     //         Replace this implementation with code to handle the error appropriately.
     //
     //         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     //         * /
     //        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
     //        abort();
     //    }
     
     //    Group *tGroup = [self getExistingGroupObject:group.name];
     
     //    NSManagedObject *grp = group;
     //    NSManagedObject *task = self.task;
     ////    [[task mutableSetValueForKey:@"group"] addObject:[task managedObjectContext:objectWithID:[grp objectID]]];
     ////
     //    [[task mutableSetValueForKey:@"group"] addObject:[task managedObjectContext:objectWithID:[grp objectID]]];
     
     //    [task managedObjectContext:objectWithID:[grp objectID] forKey:@"group"];
     //    if (![[self.fetchedResultsController managedObjectContext] save:&error]) {
     //        / *
     //         Replace this implementation with code to handle the error appropriately.
     //
     //         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     //         * /
     //        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
     //        abort();
     //    }
     */
    
    
    //    self.selectedGroup = tGroup;
    self.selectedGroup = group;
    self.groupNameLabel.text = [AppUtilities handleEmptyString:group.name];
}

- (void) groupEditViewControllerDidCancel:(__autoreleasing id *)controller {
    //    //no need to save any input when the user taps Cancel, so this method simply dismisses the add scene
    //    [self dismissViewControllerAnimated:YES completion:nil];
    // Don't pass current value to the edited object, just pop.
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Effort Controller Operations

- (void) effortViewControllerDidFinish:(EffortViewController *)controller WithKeyName:(NSString *)keyName WithEffortCount:(int)countIdx WithEffortUnit:(int)unitIdx {
    
    if ([keyName isEqualToString:@"effort"]) {
        self.selEstimCountIdx = countIdx;
        self.selEstimUnitIdx = unitIdx;
        
        self.estimatCountLabel.text = [APP_SHARED_DATA uiTextFromCountIdx:self.selEstimCountIdx];
        self.estimUnitLabel.text  = [APP_SHARED_DATA uiTextFromUnitIdx:self.selEstimUnitIdx];
    }
    else if ( [keyName isEqualToString:@"actual"] ) {
        self.selActualCountIdx = countIdx;
        self.selActualUnitIdx = unitIdx;
        
        self.actualCountLabel.text = [APP_SHARED_DATA uiTextFromCountIdx:self.selActualCountIdx];
        self.actualUnitLabel.text  = [APP_SHARED_DATA uiTextFromUnitIdx:self.selActualUnitIdx];
    }
}

@end
