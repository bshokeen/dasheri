//
//  NotesViewController.h
//  realtodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDTO.h"
#import <QuartzCore/QuartzCore.h>

@protocol NotesViewControllerDelegate;


@interface NotesViewController : UIViewController <UIActionSheetDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *dataTextView;

@property (nonatomic, strong) CommonDTO *commonDTOObj;

@property(weak, nonatomic) id <NotesViewControllerDelegate> delegate;



@end

@protocol NotesViewControllerDelegate <NSObject>

-(void) notesViewControllerDidFinish:(CommonDTO *) commonDTO;

@end