//
//  AddTaskViewController.h
//  realtodo
//
//  Created by Me on 16/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtilities.h"
#import "Task.h"
#import "Group.h"
#import "TabGroupsViewController.h"
#import "DueDateViewController.h"
#import "EffortViewController.h"
#import "TabTagsViewController.h"
#import "ReminderViewController.h"
#import "NotesViewController.h"

// Including <UITextFieldDelegate> to support closing the keyboard on done. Need to implement one of its method.
@interface AddTaskViewController : UITableViewController 
                                    <UITextFieldDelegate, TabGroupsViewControllerDelegate, MoreTextViewControllerDelegate,
                                        EffortViewControllerDelegate, ReminderViewControllerDelegate, NotesViewControllerDelegate>

// Adding it to let it pass to GroupEditViewController for its operations.
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (weak, nonatomic) IBOutlet UITextField *taskNameText;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UIButton *flagBtn;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gainOptions;
@property (weak, nonatomic) IBOutlet UILabel *dueOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *remindOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UIButton *gainImage;

// Adding for tracking effort handling
@property (weak, nonatomic) IBOutlet UILabel *estimUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimatCountLabel;

// Attach tags while creating a new task
@property (weak, nonatomic) IBOutlet UILabel *tagSlabel;
// For future add context
@property (weak, nonatomic) IBOutlet UILabel *contextLabel;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)flagBtnClicked:(id)sender;
@end
