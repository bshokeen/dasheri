//
//  GroupHeaderSectionView.m
//  realtodo
//
//  Created by Me on 21/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "UITableSectionHeaderView.h"
#import <QuartzCore/QuartzCore.h>


@implementation UITableSectionHeaderView

@synthesize titleLabel=_titleLabel, disclosureButton=_disclosureButton, delegate=_delegate, section=_section, countLabel = _countLabel, sectionItemCount = _sectionItemCount, headerCountLabel=_headerCountLabel, groupManagedObjectID = _groupManagedObjectID;

+ (Class)layerClass {
    
    return [CAGradientLayer class];
}

-(id) initWithFrame:(CGRect)frame title:(NSString *)title section:(NSInteger)sectionNumber sectionItemCount:(NSInteger)sectionItemCount selectState:(BOOL)selectState HeaderIndex:(NSInteger)headerIndex groupMngedObjID:(NSString *)groupManagedObjectID delegate:(id<UITableSectionHeaderViewDelegate>)delegate
{

    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOpen:)];
        [self addGestureRecognizer:tapGesture];
        
        _delegate = delegate;
        self.userInteractionEnabled = YES;
  
        // Create and configure the disclosure button
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(3.0, 10.0, 10.0,10.0);
        [button setImage:[UIImage imageNamed:@"RTTaskSectionCarat.png"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"RTTaskSectionCarat-open.png"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        button.selected = selectState;
//        button.layer.borderColor = [UIColor blueColor].CGColor;
//        button.layer.borderWidth = 3.0;       
        _disclosureButton = button;
        
        
        //Create and configure the title index.
        CGRect headerCountLabelFrame = self.bounds;
        headerCountLabelFrame.origin.x += 15.0;
        headerCountLabelFrame.origin.y = 7.0;
        headerCountLabelFrame.size.width = 30.0;
        headerCountLabelFrame.size.height = 15.0;
        CGRectInset(headerCountLabelFrame, 0.0, 5.0);
        UILabel *headerCountLabel = [[UILabel alloc] initWithFrame:headerCountLabelFrame];
        headerCountLabel.text = title;
        headerCountLabel.font = [UIFont boldSystemFontOfSize:14.0];
        headerCountLabel.textColor = [UIColor whiteColor];
        headerCountLabel.backgroundColor = [UIColor clearColor];
        headerCountLabel.textAlignment = UITextAlignmentRight;
        headerCountLabel.text = [[NSString stringWithFormat:@"%d", headerIndex] stringByAppendingString:@": "];
//        headerCountLabel.layer.borderColor = [UIColor blueColor].CGColor;
//        headerCountLabel.layer.borderWidth = 3.0;
        [self addSubview:headerCountLabel];
        _headerCountLabel = headerCountLabel;
    
        
        //Create and configure the title label.
        _section= sectionNumber;
        CGRect titleLabelFrame = self.bounds;
        titleLabelFrame.origin.x += 45.0;
        titleLabelFrame.origin.y = 7.0;
        titleLabelFrame.size.width = 210.0 + [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor ;
//        NSLog(@"Width: %f", titleLabelFrame.size.width);
        titleLabelFrame.size.height = 15.0;
        CGRectInset(titleLabelFrame, 0.0, 5.0);
        UILabel *label = [[UILabel alloc] initWithFrame:titleLabelFrame];
        label.text = title;
        label.font = [UIFont boldSystemFontOfSize:14.0];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
//        label.layer.borderColor = [UIColor purpleColor].CGColor;
//        label.layer.borderWidth = 3.0;
        [self addSubview:label];
        _titleLabel = label;
        
        //Create and configure the count label.
        _sectionItemCount = sectionItemCount;
        CGRect countLabelFrame = self.bounds;
        countLabelFrame.origin.x= 275.0 + [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor ;
        countLabelFrame.origin.y = 5.0;
        countLabelFrame.size.width = 40.0;
        countLabelFrame.size.height = 20.0;
        CGRectInset(countLabelFrame, 0.0, 5.0);
        UILabel *countLabel = [[UILabel alloc] initWithFrame:countLabelFrame];
        countLabel.text = [NSString stringWithFormat:@"%d  ", sectionItemCount];
        countLabel.font = [UIFont boldSystemFontOfSize:14.0];
        countLabel.textColor = [UIColor whiteColor];
        countLabel.backgroundColor = [UIColor clearColor];
        countLabel.textAlignment = UITextAlignmentRight;
        countLabel.layer.backgroundColor = [UIColor blueColor].CGColor;
        countLabel.layer.cornerRadius = 10.0;
        countLabel.layer.borderColor = [UIColor blackColor].CGColor;
        countLabel.layer.borderWidth = 0.5;
        countLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        
//        self.view.layer.backgroundColor = [UIColor orangeColor].CGColor;
//        self.view.layer.cornerRadius = 80.0;
//        self.view.layer.frame = CGRectInset(self.view.layer.frame, 20, 20);
        
        [self addSubview:countLabel];
        _countLabel = countLabel;
        
        // Set the colors for the gradient layer.
        static NSMutableArray *colors = nil;
        if (colors == nil) {
            colors = [[NSMutableArray alloc]initWithCapacity:3];
            UIColor *color = nil;
            
            color = [UIColor colorWithHue:0.58 saturation:0.1 brightness: 1 alpha:1.0];
            [colors addObject:(id)[color CGColor]];
            color = [UIColor colorWithHue:0.60 saturation:0.30 brightness: 0.75 alpha:1.0];
            [colors addObject:(id)[color CGColor]];
            color = [UIColor colorWithHue:0.62 saturation:0.80 brightness: 0.75 alpha:1.0];
            [colors addObject:(id)[color CGColor]];             
        }

        [(CAGradientLayer *)self.layer setColors:colors];
        [(CAGradientLayer *)self.layer setLocations:[NSArray arrayWithObjects: [NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
        
        _groupManagedObjectID = groupManagedObjectID;
    }
    
    return self;
}

-(void) toggleOpen:(id) sender {
    [self toggleOpenWithUserAction:YES];
}

- (void) toggleOpenWithUserAction:(BOOL)userAction {
    
//    NSLog(@"Selected: %@", self.disclosureButton.selected);
    //Toggle the disclosure button state.
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    // If this was a user action, send the delegate the appropriate message
    if (userAction && [self.delegate respondsToSelector:@selector(uitableSectionHeaderView:Section:Operation:)]) {
        if (self.disclosureButton.selected) {            
            [self.delegate uitableSectionHeaderView:self Section:self.section Operation: SECTION_OPEN];
        }
        else {
            [self.delegate uitableSectionHeaderView:self Section:self.section Operation: SECTION_CLOSE];
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
 
}
*/


-(void) updateCountOnDelete: (int) recordsDeleted {

    self.sectionItemCount = self.sectionItemCount - recordsDeleted ;
    self.countLabel.text = [NSString stringWithFormat:@"%d  ", self.sectionItemCount];
    
}

@end
