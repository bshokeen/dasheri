//
//  FilterItemDetailsViewController.h
//  realtodo
//
//  Created by Balbir Shokeen on 9/5/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterItemDetailsViewControllerDelegate;

@interface FilterItemDetailsViewController : UITableViewController

@property (strong, nonatomic) id <FilterItemDetailsViewControllerDelegate> delegate;

@property (nonatomic, retain) NSString *SectionTitle;
@property (nonatomic) NSUInteger SortItemIdx;

@end


@protocol FilterItemDetailsViewControllerDelegate <NSObject>

- (void)filterItemTVControllerDidCancel:(FilterItemDetailsViewController *)controller;

@end