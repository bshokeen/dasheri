//
//  CommonViewController.h
//  realtodo
//
//  Created by Me on 31/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdateTaskViewController.h"
#import "AddTaskViewController.h"
#import "UITableSectionHeaderView.h"
#import "FilterCriteriaViewController.h"
#import "Task.h"
#import "Group.h"
#import "AppSharedData.h"
#import "AppUtilities.h"
#import "TaskViewCell.h"

// TODO: Rename, give better name. Maximum number for which we will track open/closed section names.
#define MAX_TRACKED_SEGMENTS        7


@interface CommonViewController : UITableViewController
                                <UpdateTaskViewControllerDelegate, NSFetchedResultsControllerDelegate, UITableSectionHeaderViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext        *mgdObjContext;

// This object is actually populated by the subclasses
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerObj;

// Idx which is the key to record open/closed groups
//TODO: How do i ensure that whenever a selection changes these variables are updated by caller or user of this controller and also how to ensure the must have methods are overriden in the subclasses
@property (nonatomic) int selSegmentIdx;

- (void) forceReloadThisUI;
- (void) fetchData;
- (NSFetchedResultsController *)fetchedResultsController;
- (NSString *) computeSectionNameFromRawName: (NSInteger) section;

@end
