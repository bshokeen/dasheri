//
//  ItemDetailsCell.h
//  realtodo
//
//  Created by Me on 01/09/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *itemNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *itemTasksCountLabel;

@end
