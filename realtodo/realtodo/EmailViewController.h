//
//  EmailViewController.h
//  realtodo
//
//  Created by Me on 02/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface EmailViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (IBAction)exportTasksToEmail:(id)sender;



@end
