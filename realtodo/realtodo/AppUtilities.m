//
//  AppUtilities.m
//  realtodo
//
//  Created by Me on 12/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//


#import "AppUtilities.h"
#import "AppDelegate.h"

@implementation AppUtilities

+(NSString *) trim: (NSString *) inputString {
    // FIxed the bug: It was replacing all white spaces earlier. Now using Trim API.
//    return [inputString stringByReplacingOccurrencesOfString:@" " withString:@""];
    return  [inputString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+ (NSString *) handleEmptyString: (NSString *) str {
    NSString *name = str;
    if (name.length == 0 ) {
        name = @"Empty";
    }
    else if ([name isEqualToString:@"Empty"]) {
        name = nil;
    }
    return name;
}

+ (BOOL) ifTaskAlreadyExist: (NSManagedObjectContext *) context TaskName: (NSString *)taskObjectID
{
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(ID == %@)", taskObjectID]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

+(BOOL) ifEntityAlreadyExist: (NSString *)entityName AttributeName: (NSString *) attributeName AttributeValue: (NSString *) attributeValue {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:entityName inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(%@ == %@)", attributeName, attributeValue]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT
                        countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}


#pragma mark Date Formatter
+ (NSString *)uiFormatedDate:(NSDate *) date
{
    NSString *returnVal = nil;
    
    if (date == nil) {
        returnVal = NEVER;
    }
    else {
        returnVal = [[AppUtilities lDateFormatter] stringFromDate: date];
    }
    return returnVal;
}

+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate
{
    NSDate *returnVal = nil;
    if (formatedDate == nil || formatedDate.length == 0 || [formatedDate isEqualToString:NEVER] ) {
        returnVal = [NSDate date];
    }
    else {
        returnVal = [[AppUtilities lDateFormatter] dateFromString:formatedDate];
    }
    return returnVal;
}

// TODO: addDay is not being used here???
+ (NSDate *) uiUNFormatedDate: (NSString *) formatedDate WithDayAdd: (BOOL) addDay
{
    NSDate *returnVal = nil;
    if (formatedDate == nil || formatedDate.length == 0 || [formatedDate isEqualToString:NEVER] ) {
        returnVal = [NSDate dateWithTimeInterval:3600*24*1 sinceDate:[NSDate date]];
    }
    else {
        returnVal = [[AppUtilities lDateFormatter] dateFromString:formatedDate];
    }
    return returnVal;
}

+ (NSString *) uiFormatedTime: (NSDate *) timeWithDate {
    if (timeWithDate == nil) {
        return NEVER;
    }
    else {
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"h:mm a"];
        
        return [outputFormatter stringFromDate:timeWithDate] ;
    }
}

+ (NSString *) dayNameFromDate: (NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:date];
    return dayName;
}

+ (NSInteger) dayFromDate: (NSDate *) date {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSDayCalendarUnit ) fromDate:date];
 
    return [dateComponents day];
}

+ (NSInteger) monthFromDate: (NSDate *) date {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSMonthCalendarUnit ) fromDate:date];
    
    return [dateComponents month];
}
+ (NSString *) monthNameFromDate: (NSDate *) date {

    NSDateFormatter *df = [[NSDateFormatter alloc] init];    
    [df setDateFormat:@"MMM"];
//    [df setDateFormat:@"yy"];
//    [df setDateFormat:@"dd"];    
    
    return [NSString stringWithFormat:@"%@", [df stringFromDate:[NSDate date]]];
}

/*
 -(NSDate *) getDateFromString: (NSString *)dateString {
 NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
 dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
 //    NSDateFormatter *dformatter = [self dateFormatter];
 NSDate* date = [dateFormatter dateFromString:dateString];
 
 DLog(@"Date %@", [date description]);
 return date;
 }*/


+ (NSDateFormatter *)lDateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSTimeZoneNameStyleShortGeneric];
    }
    
    return dateFormatter;
}

+(NSDate *) ifEmptyAddDay: (NSDate *) date {
    NSDate *tmpDate = date;
    if (tmpDate == nil) {
        tmpDate = [NSDate dateWithTimeInterval:3600*24*1 sinceDate:[NSDate date]];
    }
    return tmpDate;
}

+(NSString *) getImageName: (NSString *) imageTag {
    
    NSString *imgName = nil;
    
    if ([imageTag isEqualToString:@"starOn"]) {
        imgName = @"RTTaskStarIcon.png";
    }
    else if ([imageTag isEqualToString:@"starOff"]) {
        imgName = @"RTTaskStarDeselected.png";
    }
    else if ([imageTag isEqualToString:@"checkOn"]) {
        imgName = @"RTTaskCompleted.png";
    }
    else if ([imageTag isEqualToString:@"checkOff"]) {
        imgName = @"RTTaskPending.png";
    }
    else if ([imageTag isEqualToString:@"alarmOn"]) {
        imgName = @"RTTaskAlarmIcon-23.png";
    }
    else if ([imageTag isEqualToString:@"notesOn"]) {
        imgName = @"RT_Button_StickyIcon_27x27.png";
    }
    else if ([imageTag isEqualToString:@"tagSOn"]) {
        //imgName = @"RT_Tab_Icon_Views_23.23.png";
        imgName = @"RT_Icon_Tag.png";
    }
    else if ([imageTag isEqualToString:@"none"]) {
        imgName = @"";
    }
    else if ([imageTag isEqualToString:@"sectionOpen"]) {
        imgName = @"RTTaskSectionCarat-open.png";
    }
    else if ([imageTag isEqualToString:@"sectionClosed"]) {
        imgName = @"RTTaskSectionCarat.png";
    }
    else if ([imageTag isEqualToString:@"OrderDOWN"]) {
        imgName = @"RT_Icon_Btn_DownArrow_Blue.png";
    }
    else if ([imageTag isEqualToString:@"OrderUP"]) {
        imgName = @"RT_Icon_Btn_UPArrow_Blue.png";
    }
    else if ([imageTag isEqualToString:@"highGain"]) {
        imgName = @"RTTaskIconRed.png";
    }
    else if ([imageTag isEqualToString:@"mediumGain"]) {
        imgName = @"RTTaskIconYellow.png";
    }
    else if ([imageTag isEqualToString:@"normalGain"]) {
        imgName = @"RTTaskIconGreen.png";
    }
    else if ([imageTag isEqualToString:@"lowGain"]) {
        imgName = @"RTTaskIconBlue.png";
    }
    
    
    return imgName;
}

/** This API is added to add schedule an alarm **/
+ (void) scheduleAlarm: (NSString *) groupName AlarmDate: (NSDate *) alarmDate TaskName: (NSString *) taskName TaskObjectID: (NSString *)taskObjectID RemindInterval: (NSCalendarUnit) nscuRemindRepeatInterval {
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    // Get the current date
    NSDate *pickerDate = alarmDate;
    
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:pickerDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:pickerDate];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
	[dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    if (nscuRemindRepeatInterval != NSEraCalendarUnit ) { // ERA is internally used to indicate NEVER/No Reminder
        localNotif.repeatInterval = nscuRemindRepeatInterval;        
    }
    
    NSString *bodyText = @"";
    if (groupName == nil) {
        bodyText = [bodyText stringByAppendingString:@"Empty : "];
        bodyText = [bodyText stringByAppendingString:taskName];
    }
    else {
        bodyText = [bodyText stringByAppendingString: groupName];
        bodyText = [bodyText stringByAppendingString:@": "];
        bodyText = [bodyText stringByAppendingString:taskName];
    }
    
	// Notification details
    localNotif.alertBody = bodyText;
    
	// Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    
	// Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:taskObjectID  forKey:@"TaskObjectID"];
    localNotif.userInfo = infoDict;
    
	// Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    
    DLog(@"Added Schedule Body: %@, Alert: %@ itemDate: %@ repeatInterval: %d", bodyText, [pickerDate description], [itemDate description], nscuRemindRepeatInterval);
}

+ (BOOL) removeNotificationForTaskObjectID: (NSString *) modifiedTaskObjectID {
    
    BOOL removedAny = NO;
    
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (UILocalNotification *item in notificationArray) {
        
        NSDictionary *infoDict = (NSDictionary *) item.userInfo;
        NSString *taskObjectID = [infoDict objectForKey:@"TaskObjectID"];
        
        if (taskObjectID.length == 0) {
            DLog(@"Error, unable to find the task Object ID for this reminder");
        }
        else if ( [taskObjectID isEqualToString: modifiedTaskObjectID] ) {
            [[UIApplication sharedApplication] cancelLocalNotification:item];
            DLog(@"Removed Notification on taskObjectID: %@ taskName: %@", taskObjectID, item.alertBody);
            removedAny = YES;
            break;
        }
    }
    
    return removedAny;
}

+ (Group *) fetchGroupBySortOrder: (NSString *) sortOrder ManagedContext: (NSManagedObjectContext *) context {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
     [NSEntityDescription entityForName:@"Group" inManagedObjectContext:context]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(sort_order == %@)", sortOrder]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    DLog(@"FetchGroup: SortOrder: %@    Error: %@, %@", sortOrder, error, error.userInfo);
    if ([fetchResults count] > 0) {
        DLog(@"FetchGroup: Count: %i", [fetchResults count]);
        return [fetchResults objectAtIndex:0];
        
    }
    else {
        DLog(@"FetchGroup: Count: %i", [fetchResults count]);
        return nil;
    }
}

/*
 * Delete tasks older than x days
 */
+ (int) deleteCompletedTasks: (int) olderThan {
    
    DLog(@"Auto Delete completed task called");
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    
    
    NSDate *completedBeforeThisDate = [NSDate dateWithTimeIntervalSinceNow:-24*60*60*olderThan];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(done == 1 && completed_on < %@)", completedBeforeThisDate]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    DLog(@"Records Fetched for deletion are: %d", [fetchResults count]);
    
    int i = 0;
    for (i= 0; i < [fetchResults count]; i++) {
        Task *task = [fetchResults objectAtIndex:i];

        DLog(@"AppUtilities: Deleting Older Task: Name: %@ Created on: %@ Modified On: %@ Completed On Date:%@", task.name, [task.createdOn description], [task.modified_on description], [task.completed_on description]);

        [AppUtilities deleteTask:task];
    }
    
    return i;
}

+ (void) deleteConcurrentFutureNotifications: (Task *) task {
    
    NSString *taskObjectID = [[[task objectID] URIRepresentation] absoluteString];
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    // Get the current date
    NSDate *pickerDate = task.remind_on;
    
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                                   fromDate:pickerDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
                                                   fromDate:pickerDate];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
    // Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:[timeComponents second]];
    
    for (int i=0; i< REMINDER_DAYS; i++) {
        
        [AppUtilities removeNotificationForTaskObjectID: [NSString stringWithFormat:@"%@%d%d", taskObjectID, [dateComps day], [dateComps month]] ];
        
        [dateComps setDay:[dateComps day]+1];
        [dateComps setMonth:[dateComponents month]];
        [dateComps setYear:[dateComponents year]];
        
    }
}

+ (void) deleteLogic: (Task *) task {
    // DELETE The notification if any
    // Schedule the Alarm if applicable
    NSString *taskObjectID = [[[task objectID] URIRepresentation] absoluteString];
    [AppUtilities removeNotificationForTaskObjectID: taskObjectID ];
    
    // TODO: When Adding ability to add multiple reminder this needs to be enabled & handled appropriately
    /*
    if (![task.remindInterval isEqualToString:REMINDON_SELECTED_DATE]) {
        [AppUtilities deleteConcurrentFutureNotifications:task];
    }*/
    
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    ;
    [context deleteObject:task];
}

/**
 * User click clear on ALert UI to reset the flag for all alert notification on every qualified task 
 */
+ (void) clearAllAlertNotification {
    
    DLog(@"Clear All Alert Notification, Badge and Applicationbadge");
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(alert_notification == 1) || (remind_on <= %@)", [NSDate date]]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    for (int i=0; i < [fetchResults count]; i++) {
        Task *task = [fetchResults objectAtIndex:i];
        task.alert_notification = 0;
        task.alert_notification_on = nil;
        task.remind_on = nil;
        task.remind_repeat_interval = [NSNumber numberWithInt:NSEraCalendarUnit];
    }
    
    [AppUtilities saveContext];
    
    [AppUtilities updateAlertBadge: 0];
}

+ (void) updateAlertBadge: (int) mode {
    
    switch (mode) {
        case 0: // Clear Badge
        {
            [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:nil];   
            
            // Reset the application badge number also
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            break;
        }
        case 1: // Increment Badge
        {
            int badgeCount = [[[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] badgeValue] intValue];
            badgeCount += 1;
            [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:[NSString stringWithFormat:@"%d",badgeCount]];
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;

            break;
        }
        case 2: // Update Badge from DB Count
        {
            int badgeCount = [AppUtilities countAlertNotificationItems];
            if ( badgeCount == 0 ) {
                [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:nil];   
                
                // Reset the application badge number also
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            }
            else {
                [[[[[APP_DELEGATE tabBarController] tabBar ] items] objectAtIndex:ALERT_TAB_IDX] setBadgeValue:[NSString stringWithFormat:@"%d",badgeCount]];
                [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
                
            }
            break;
        }
        default:
            break;
    }
}

+ (int) countAlertNotificationItems
{
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
   
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"Task" inManagedObjectContext:context]];
    // TODO: To avoid repeated fetch of recourring intervals fetching only repeat interval = 0. it is a workaround for now. Push notification to fix this.
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(alert_notification == 1) || (remind_on <= %@ && remind_repeat_interval == 0)", [NSDate date]]];
    
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Count %d",count);
    
    return count;
}


+ (void) saveContext {
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    NSError *error;
    if (![context save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

+ (BOOL) deleteTask: (Task *) task {
    [AppUtilities deleteLogic:task];
    
    [AppUtilities saveContext];
    return YES;
}

+ (BOOL) saveTask: (Task *) task {
    
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    ;
    
    // PUt a temporary check for debugging
    NSCalendarUnit nscuRemindRepeatInterval = [task.remind_repeat_interval intValue];
    if (nscuRemindRepeatInterval == NSDayCalendarUnit || nscuRemindRepeatInterval == NSWeekCalendarUnit || nscuRemindRepeatInterval == NSMonthCalendarUnit || nscuRemindRepeatInterval == NSYearCalendarUnit || nscuRemindRepeatInterval == NSEraCalendarUnit || nscuRemindRepeatInterval == 0 ) {
        
    }
    else {
        ULog(@"Invalid Remind Repeat Interval: %d", nscuRemindRepeatInterval);
        DLog(@"ERROR - Invalid Repeat Interval: %d", nscuRemindRepeatInterval);
    }

    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return YES;
}

+ (Task *) addTask:(NSString *) name Gain:(int) gain DueOn:(NSDate *)dueOn RemindOn:remindOn Note:(NSString *)note Flag:(int) flag Group:(Group *)group EstimCountIdx: (int) estimCountIdx EstimUnitIdx: (int) estimUnitIdx Tags: (NSMutableSet *) selTags RemindInterval:(NSCalendarUnit) nscuRemindRepeatInterval {
    Task *newTaskManagedObject = (Task *)[NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];
    
    [newTaskManagedObject setName:name];
    [newTaskManagedObject setGain:[NSNumber numberWithInt:gain]];
    [newTaskManagedObject setDone:[NSNumber numberWithInt:0]];
    [newTaskManagedObject setCreatedOn:[NSDate date]];
    [newTaskManagedObject setDueOn:dueOn];
    [newTaskManagedObject setGroup:group];
    [newTaskManagedObject setProjectName:group.name];
    [newTaskManagedObject setNote:note];
    [newTaskManagedObject setFlag:[NSNumber numberWithInt:flag]];
    [newTaskManagedObject setEffort_time:[NSNumber numberWithInt:estimCountIdx]];
    [newTaskManagedObject setEffort_time_unit:[NSNumber numberWithInt:estimUnitIdx]];
    [newTaskManagedObject setTagS:selTags];
    
    // Convert Selected CustomDays array to string (to store for the task)
    NSString *customDaysString = [[AppSharedData getInstance].reminderCustomDaysArray componentsJoinedByString:@","];
    
    [newTaskManagedObject setRemindOnCustomDays:customDaysString];
    // Reset the seconds part to make the reminder come exactly on the minute set.
    [newTaskManagedObject setRemind_on: [self getDateWithSecondsResetForDate:remindOn]];
    [newTaskManagedObject setRemind_repeat_interval:[NSNumber numberWithInt:nscuRemindRepeatInterval]];
    
    // PUt a temporary check for debugging
    if (nscuRemindRepeatInterval == NSDayCalendarUnit || nscuRemindRepeatInterval == NSWeekCalendarUnit || nscuRemindRepeatInterval == NSMonthCalendarUnit || nscuRemindRepeatInterval == NSYearCalendarUnit || nscuRemindRepeatInterval == NSEraCalendarUnit || nscuRemindRepeatInterval == 0 ) {
        
    }
    else {
        ULog(@"Invalid Remind Repeat Interval: %d", nscuRemindRepeatInterval);
        DLog(@"ERROR - Invalid Repeat Interval: %d", nscuRemindRepeatInterval);
    }
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    // Schedule the Alarm if applicable
    if (newTaskManagedObject.remind_on) {
        
        NSString *taskObjectID = [[[newTaskManagedObject objectID] URIRepresentation] absoluteString];
        
        [AppUtilities scheduleAlarm:newTaskManagedObject.group.name AlarmDate:newTaskManagedObject.remind_on  TaskName:newTaskManagedObject.name TaskObjectID:taskObjectID RemindInterval:nscuRemindRepeatInterval];
    }
    
    return newTaskManagedObject;
}

+ (NSDate *) getDateWithSecondsResetForDate: (NSDate *) date {
    
    if (date == nil)
        return date;
    
    NSDate              *   dateWithoutSeconds = date;
    NSDateComponents    *   startSeconds = [[NSCalendar currentCalendar] components: NSSecondCalendarUnit fromDate: dateWithoutSeconds];
    [startSeconds setSecond: -[startSeconds second]];
    dateWithoutSeconds = [[NSCalendar currentCalendar] dateByAddingComponents: startSeconds toDate: dateWithoutSeconds options: 0];
    
    return dateWithoutSeconds;
    
}

+ (NSString *)getSelectedTagsLabelText: (NSSet *) tags {
    NSString *tagLabelText = nil;
    
    // Move more code to here, after you done with updates in subsequent views rather using FINISH
    NSMutableArray *tagNamesArray = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for (Tag *tag in tags) {
        [tagNamesArray addObject:tag.name];
    }
    
    tagLabelText  = [tagNamesArray componentsJoinedByString:@", "];
    
    if (tagLabelText == nil || [tagLabelText length] == 0) {
        tagLabelText = @"Not Set";
    }
    
    return tagLabelText;
}


// Customize the appearance of table view cells.
+ (void)configureCell:(TaskViewCell *)mvaCell Task: (Task*) activityAtIndex
{
    
    
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    }
    
    
    [[mvaCell titleLabel] setText:activityAtIndex.name];
    
    NSString *dueOnText = [formatter stringFromDate:activityAtIndex.dueOn];
    if ([dueOnText length] == 0 )
        dueOnText = @"Never";
    [mvaCell.subtitleLabel setText: dueOnText];
    
    int progressAsInt =(int)([activityAtIndex.progress floatValue] + 0.5f);
    progressAsInt = progressAsInt*5;
    NSString *newText =[[NSString alloc] initWithFormat:@"%d",progressAsInt];
    [[mvaCell infoLabel1] setText: [newText stringByAppendingString:@"%"]];
    
    //    DLog(@"Gain: %@", activityAtIndex.gain);
    switch ([activityAtIndex.gain integerValue]) {
        case 0:
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"highGain"]] forState:UIControlStateNormal];
            //             mvaCell.statusButton2.backgroundColor = [UIColor redColor];
            break;
        case 1:
            //            mvaCell.statusButton2.backgroundColor = [UIColor blueColor];
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"mediumGain"]] forState:UIControlStateNormal];
            break;
        case 2:
            //            mvaCell.statusButton2.backgroundColor = [UIColor grayColor];
            //            mvaCell.statusButton2.titleLabel.text = @"B";//.backgroundColor = [UIColor redColor];
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"normalGain"]] forState:UIControlStateNormal];
            break;
        case 3:
            //            mvaCell.statusButton2.backgroundColor = [UIColor greenColor];
            //            cell.statusButton2.backgroundColor = [UIColor blueColor];
            [mvaCell.statusButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"lowGain"]] forState:UIControlStateNormal];
            break;
        default:
            mvaCell.statusButton2.backgroundColor = [UIColor redColor];
            break;
    }
    
    switch ([activityAtIndex.done intValue]) {
        case 0: {
            [mvaCell.statusButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOff"]] forState:UIControlStateNormal];
            [mvaCell.statusButton1 setSelected:NO];
            
            // Make the title, duedate and the progress font colors, to show it is still pending.
            [mvaCell titleLabel].font = [UIFont boldSystemFontOfSize:16];
            [mvaCell titleLabel].textColor = [UIColor blackColor];
            [mvaCell subtitleLabel].font = [UIFont systemFontOfSize:12];
            [mvaCell subtitleLabel].textColor = [UIColor blackColor];
            [mvaCell infoLabel1].font = [UIFont boldSystemFontOfSize:14];
            [mvaCell infoLabel1].textColor = [UIColor blackColor];
            break;}
        case 1: {
            [mvaCell.statusButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"checkOn"]] forState:UIControlStateSelected];
            [mvaCell.statusButton1 setSelected:YES];
            
            //To change the color to gray to indicate this task was completed.
            [mvaCell titleLabel].font = [UIFont italicSystemFontOfSize:16];
            [mvaCell titleLabel].textColor = [UIColor grayColor];
            [mvaCell subtitleLabel].font = [UIFont italicSystemFontOfSize:12];
            [mvaCell subtitleLabel].textColor = [UIColor grayColor];
            [mvaCell infoLabel1].font = [UIFont italicSystemFontOfSize:14];
            [mvaCell infoLabel1].textColor = [UIColor grayColor];
            break;}
        default:
            break;
    }
    
    if ( activityAtIndex.remind_on != nil ) {
        [mvaCell.infoButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"alarmOn"]] forState:UIControlStateSelected];
        [mvaCell.infoButton1 setSelected:YES];
    }
    else {
        [mvaCell.infoButton1 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.infoButton1 setSelected:NO];
    }
    
    if ( [activityAtIndex.flag intValue] == 1 ) {
        [mvaCell.infoButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOn"]] forState:UIControlStateSelected];
        [mvaCell.infoButton2 setSelected:YES];
    }
    else {
        [mvaCell.infoButton2 setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.infoButton2 setSelected:NO];
    }
    
    if ( activityAtIndex.note == nil || activityAtIndex.note.length == 0 ) {
        [mvaCell.notesButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        [mvaCell.notesButton setSelected:NO];
    }
    else {
        [mvaCell.notesButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"notesOn"]] forState:UIControlStateSelected];
        [mvaCell.notesButton setSelected:YES];
    }
    
    // TODO: See why setting hte text label did not work
    if (activityAtIndex.tagS == nil || [activityAtIndex.tagS count] == 0 ) {
        mvaCell.tagsButton.titleLabel.text = @"";
        [mvaCell.tagsButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"none"]] forState:UIControlStateNormal];
        
        [mvaCell.tagsButton setSelected:NO];
    }
    else {
        mvaCell.tagsButton.titleLabel.text = @"T";
        [mvaCell.tagsButton setImage:[UIImage imageNamed:[AppUtilities getImageName:@"tagSOn"]] forState:UIControlStateNormal];
        [mvaCell.tagsButton setSelected:YES];
    }
    
    //    [cell.statusButton addTarget:self action:@selector(changeStatus:) forControlEvents:UIControlEventTouchDown];
    // Set Self as delegate to allow calling the button operations to parent
    mvaCell.task = activityAtIndex;
}

+ (NSString *) cleanDictSetPrint: (NSMutableDictionary *) localDictWithSet {
    //    return [localDictWithSet description];
    NSString *formatedStr = @"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t{";
    
    NSString *aakey;
    for (aakey in localDictWithSet) {
        
        NSString *key = [NSString stringWithFormat:@"%@", aakey];
        formatedStr = [formatedStr stringByAppendingString:key];
        formatedStr = [formatedStr stringByAppendingString:@" = ["];
        
        NSMutableSet *myset = [localDictWithSet objectForKey:aakey];
        for (NSNumber *aItem in [myset allObjects]) {
            formatedStr = [formatedStr stringByAppendingString:[ aItem stringValue] ];
            formatedStr = [formatedStr stringByAppendingString:@", "];
        }
        
        formatedStr = [formatedStr stringByAppendingString:@"]  "];
    }
    
    formatedStr = [formatedStr stringByAppendingString:@"}"];
    
    return formatedStr;
}

+ (NSPredicate *) generatePredicate {
    // default predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
    BOOL _excludeConditionFinishedTasks = YES;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"realToDo.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: path])
    {
        NSMutableDictionary *savedDefaultValues = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
        
        if ([[savedDefaultValues objectForKey:@"ShowCompletedTask"] intValue] == 1) {
            _excludeConditionFinishedTasks = NO;
        }
        
    }
    
    
    if (_excludeConditionFinishedTasks) {
        predicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
    }
    else {
        predicate = [NSPredicate predicateWithFormat:@"(done == 0 || done == 1)"];
    }
    
    return predicate;
}

@end
