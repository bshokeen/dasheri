//
//  DynamicViewHelper.m
//  realtodo
//
//  Created by Me on 12/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//


#import "DynamicViewData.h"

@interface DynamicViewData ()



@property (nonatomic, strong) NSMutableArray* collapsibleOnInternalValArr;
@property (nonatomic, strong) NSMutableArray* collapsibleOnInternalSORTValArr;
@property (nonatomic, strong) NSMutableArray* collapsibleOnInternalSORTDIRECTIONValArr;

@property (nonatomic, strong) NSMutableArray* orderByInternalValArr;

@end


@implementation DynamicViewData

@synthesize collapsibleOnIdxPrevSelected = _collapsibleOnIdxPrevSelected;
@synthesize collapsibleOnIdx = _collapsibleOnIdx;
@synthesize orderByIdx =_orderByIdx;
@synthesize displayTypeIdx = _displayTypeIdx;
@synthesize sortCategoriesIdx =_sortCategoriesIdx;
@synthesize defaultValuesChanged = _defaultValuesChanged;
@synthesize orderByDirection = _orderByDirection;
@synthesize excludeConditionFinishedTasks = _excludeConditionFinishedTasks;
@synthesize autoDeleteFinishedTasks = _autoDeleteFinishedTasks;
@synthesize tagSet = _tagSet;
@synthesize groupSet = _groupSet;

@synthesize reloadUI = _reloadUI;

@synthesize collapsibleOnUIValueArr = _collapsibleOnUIValueArr;
@synthesize collapsibleOnInternalValArr = _collapsibleOnInternalValArr;
@synthesize collapsibleOnInternalSORTValArr = _collapsibleOnInternalSORTValArr;
@synthesize collapsibleOnInternalSORTDIRECTIONValArr = _collapsibleOnInternalSORTDIRECTIONValArr;
@synthesize orderByUIValueArr = _orderByUIValueArr;
@synthesize orderByInternalValArr = _orderByInternalValArr;
//@synthesize displayTypeUIValArr = _displayTypeUIValArr;
//@synthesize displayTypeInternalValArr = _displayTypeInternalValArr;
@synthesize displayTypeValArr = _displayTypeValArr;
@synthesize sortCategoriesValArr = _sortCategoriesValArr;
@synthesize searchText = _searchText;
@synthesize landscapeWidthFactor =_landscapeWidthFactor;

@synthesize reminderWeekDaysArr = _reminderWeekDaysArr;


- (id) init {
    self = [super init];
    
    //    self.configurationDict = [[NSMutableDictionary alloc] init];
    
    _collapsibleOnUIValueArr = [[NSMutableArray alloc] initWithObjects:
                                @"Group",
                                @"Due On",
                                @"Gain",
                                @"Percentage %",
                                @"Status", nil];
    _collapsibleOnInternalValArr = [[NSMutableArray alloc] initWithObjects:
                                    @"group.sort_order",
                                    @"dueOnSectionName",
                                    @"gainSectionName",
                                    @"progressSectionName",
                                    @"doneSectionName", nil];
    _collapsibleOnInternalSORTValArr = [[NSMutableArray alloc] initWithObjects:
                                        @"group.sort_order",
                                        @"dueOn",
                                        @"gain",
                                        @"progress",
                                        @"done", nil];
    _collapsibleOnInternalSORTDIRECTIONValArr = [[NSMutableArray alloc] initWithObjects:
                                                 @"YES",
                                                 @"NO",
                                                 @"YES",
                                                 @"NO",
                                                 @"YES", nil];
    _orderByUIValueArr = [[NSMutableArray alloc] initWithObjects:
                          @"Due On",
                          @"Gain",
                          @"Percentage %",
                          @"Status",
                          @"Remind", nil];
    _orderByInternalValArr = [[NSMutableArray alloc] initWithObjects:
                              @"dueOn",
                              @"gain",
                              @"progress",
                              @"done",
                              @"remind_on", nil];
    
    _displayTypeValArr = [[NSMutableArray alloc] initWithObjects:
                          DISPLAYTYPE_ALL,
                          DISPLAYTYPE_TODAY,
                          DISPLAYTYPE_WEEK,
                          DISPLAYTYPE_OVERDUE,
                          DISPLAYTYPE_NODUE,
                          DISPLAYTYPE_STAR, nil];
    
    _sortCategoriesValArr = [[NSMutableArray alloc] initWithObjects:
                             @"Collapse On",
                             @"Display Type",
                             @"Order By",
                             @"Tags",
                             @"Reset", nil];
    
    _reminderWeekDaysArr = [[NSMutableArray alloc] initWithObjects:
                            @"Sun",
                            @"Mon",
                            @"Tue",
                            @"Wed",
                            @"Thu",
                            @"Fri",
                            @"Sat", nil];
    

    
    // During load reset to Defaults
    [self resetToDefaultValues];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"realToDo.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: path])
    {
        NSMutableDictionary *savedDefaultValues = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
        
        if ([[savedDefaultValues objectForKey:@"ShowCompletedTask"] intValue] == 1) {
            _excludeConditionFinishedTasks = NO;
        }
        if ([[savedDefaultValues objectForKey:@"AutoDeleteCompletedTask"] intValue] == 0) {
            _autoDeleteFinishedTasks = NO;
        }
    }
    
    return self;
}

             
- (NSFetchedResultsController *) genFetchReqCtrlFromSelConfig: (NSManagedObjectContext *) mgdObjContext {
    
    NSFetchedResultsController *nsFetchRqstCtrlr = nil;
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:mgdObjContext];
    [fetchRequest setEntity:entity];
    
    NSArray *sortDescriptors = nil;
    NSString *collapseOnSectionName = [self.collapsibleOnInternalValArr objectAtIndex:self.collapsibleOnIdx];
    NSString *collapseOnOrderByField = [self.collapsibleOnInternalSORTValArr objectAtIndex:self.collapsibleOnIdx];
    BOOL collapseOnOrderDirection = [[self.collapsibleOnInternalSORTDIRECTIONValArr objectAtIndex:self.collapsibleOnIdx] boolValue];
    
    NSString *orderBy            = [self.orderByInternalValArr objectAtIndex:self.orderByIdx];
    BOOL orderByDirection        = self.orderByDirection;
    
    
    
    // Create the sort descriptors array.
    NSSortDescriptor *collapseOnDescriptor = [[NSSortDescriptor alloc]initWithKey:collapseOnOrderByField ascending:collapseOnOrderDirection];
    NSSortDescriptor *orderByDescriptor = [[NSSortDescriptor alloc] initWithKey:orderBy ascending:orderByDirection];
    
    sortDescriptors = [[NSArray alloc] initWithObjects:
                       collapseOnDescriptor,
                       orderByDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    // FILTER CLAUSE GENERATION - Prepare the required Predicates
    {
        NSMutableArray *predicates = [[NSMutableArray alloc] init ];
        
        // Filter Based on Exlusion Conditions: Exlcude Finished Task
        if (self.excludeConditionFinishedTasks) {
            NSPredicate *exclusionPredicate = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [predicates addObject:exclusionPredicate];
        }
        
        // Filter Records Based on the Selected Display Types - Today, Due, OverDue...
        NSString *displayType = [self.displayTypeValArr objectAtIndex:self.displayTypeIdx];
        NSPredicate *displayTypePredicate = [self getFilterConditionFromDisplayType:displayType];
        if (displayTypePredicate != nil)
            [predicates addObject:displayTypePredicate];
        
        // Filter Search Text: In task name and notes
        if ([self.searchText length] > 0 ) {
            NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ || note CONTAINS[cd] %@", self.searchText, self.searchText];
            [predicates addObject:searchPredicate];
        }
        
        // Filter Selected Tags: Any of the selected tag attached to the task
        if (self.tagSet != nil && [self.tagSet count] > 0 ) {
            NSPredicate *tagSetPredicate = [NSPredicate predicateWithFormat:@" ANY tagS IN [cd] %@", self.tagSet];
            [predicates addObject:tagSetPredicate];
            
            /* TODO: Try to if we can find a record having all selected tags in stead of any of hte selected tags
             NSPredicate *countPredicate = [NSPredicate predicateWithFormat:@"tagS.@count >0"];
             NSMutableArray *tagName = [[NSMutableArray alloc] init];
             for (Tag *tag in self.tagSet) {
             [tagName addObject:tag.name];
             }
             NSPredicate *tagSetPredicate = [NSPredicate predicateWithFormat:@" ANY tagS.name IN [cd] %@", tagName];
             */
        }
        
        if (self.groupSet != nil && [self.groupSet count] > 0 ) {
            NSPredicate *groupSetPredicate = [NSPredicate predicateWithFormat:@" ANY group IN [cd] %@", self.groupSet];
            [predicates addObject:groupSetPredicate];
        }
        
        // Compound Predicates as AND Conditions
        if ([predicates count] > 0 ) {
            NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
            [fetchRequest setPredicate:p];
        }
    }
    
    // TODO: Use cacheName
    // Create and initialize the fetch results controller.
    nsFetchRqstCtrlr = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                           managedObjectContext:mgdObjContext
                                                             sectionNameKeyPath:collapseOnSectionName
                                                                      cacheName:nil];
    return nsFetchRqstCtrlr;
}

- (NSPredicate *) getFilterConditionFromDisplayType: (NSString *) displayType {
    NSPredicate *returnPredicate = nil;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    // get year, month and day parts
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    
    // create a date object using custom dateComponents against current calendar
    NSDateComponents *todayCmpnt = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
    
    if ( [displayType isEqualToString:DISPLAYTYPE_ALL] ) {
        //        [parr addObject: [NSPredicate predicateWithFormat:@" "]];
        returnPredicate = nil;
    }
    
    else if ([displayType isEqualToString: DISPLAYTYPE_TODAY] )    {
        
        NSDate *today = [calendar dateFromComponents:todayCmpnt];
        
        //    NSDateFormatter *dformatter = [self dateFormatter];
        today = [dateFormatter dateFromString:
                 [NSString stringWithFormat:@"%i-%i-%i 00:00:00 +0000",
                  [todayCmpnt year],
                  [todayCmpnt month],
                  [todayCmpnt day]]];
        
        NSDate *tomorrow = [dateFormatter dateFromString:
                            [NSString stringWithFormat:@"%i-%i-%i 00:00:00 +0000",
                             [todayCmpnt year],
                             [todayCmpnt month],
                             [todayCmpnt day] +1]];
        
        NSPredicate *todayPredicate = [NSPredicate predicateWithFormat:@"(dueOn >= %@ && dueOn < %@)", today, tomorrow];
        
        //        [parr addObject:todayPredicate];
        returnPredicate = todayPredicate;
        
        DLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
        
    }
    
    else if ( [displayType isEqualToString:DISPLAYTYPE_WEEK] ) {
        
    }
    
    else if ( [displayType isEqualToString:DISPLAYTYPE_OVERDUE] ) {
        NSDate *today = [calendar dateFromComponents:todayCmpnt];
        
        //    NSDateFormatter *dformatter = [self dateFormatter];
        today = [dateFormatter dateFromString:
                 [NSString stringWithFormat:@"%i-%i-%i 00:00:00 +0000",
                  [todayCmpnt year],
                  [todayCmpnt month],
                  [todayCmpnt day]]];
        
        NSPredicate *delayPredicate = [NSPredicate predicateWithFormat:@"(done == 0) && (dueOn <= %@)", today];
        
        //        [parr addObject:delayPredicate];
        returnPredicate = delayPredicate;
        
        DLog(@"Filter: OverDue: %@", today);
    }
    
    else if ( [displayType isEqualToString:DISPLAYTYPE_NODUE] ) {
        
        NSPredicate *noDuePredicate = [NSPredicate predicateWithFormat:@" (dueOn == nil)"];
        
        //        [parr addObject:noDuePredicate];
        returnPredicate = noDuePredicate;
        DLog(@"Filter: No Due Set");
    }
    
    else if ( [displayType isEqualToString:DISPLAYTYPE_STAR] ) {
        NSPredicate *genPredicate = [NSPredicate predicateWithFormat:@" flag == 1"];
        //        [parr addObject:genPredicate];
        returnPredicate  = genPredicate;
        
    }
    
    return returnPredicate;
}

- (int) getAllCollapsibleOnCount {
    return  [self.collapsibleOnUIValueArr count];
}


- (void) resetToDefaultValues {
    
    // Set the default Values: Later will be fetch from DB
    _orderByIdx = 0;
    _collapsibleOnIdx =0;
    _displayTypeIdx = 0;
    _orderByDirection = NO;
    _excludeConditionFinishedTasks = YES;
    _autoDeleteFinishedTasks = YES;
    _defaultValuesChanged = NO;
    _tagSet = [[NSSet alloc]init];
    _groupSet = [[NSSet alloc] init];
    
}

-(BOOL) checkForDefaults {
    
    if ((_collapsibleOnIdx != 0) || (_orderByIdx != 0) || (_displayTypeIdx != 0) || (_orderByDirection) || ([_tagSet count] != 0) || ([_groupSet count] != 0)) {
        _defaultValuesChanged = YES;
    }
    else {
        _defaultValuesChanged = NO;
    }
    return _defaultValuesChanged;
}

@end


