//
//  GroupEditViewController.m
//  realtodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "FilterGroupSelectviewController.h"
#import "Group.h"
#import "AppUtilities.h"
#import "AppSharedData.h"

@interface FilterGroupSelectviewController ()
    @property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
    @property (nonatomic, strong) Group *groupSelected;
    @property (nonatomic, strong) NSIndexPath *lastSelectedGroupName;
    @property BOOL canEditSelection;

@end

@implementation FilterGroupSelectviewController

@synthesize groupNameText = _groupNameText;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize groupSelected = _groupSelected;
@synthesize seletedGroupSet = _seletedGroupSet;
@synthesize lastSelectedGroupName = _lastSelectedGroupIndexPath;
@synthesize isSelectMode =_isSelectMode;
@synthesize canEditSelection = _canEditSelection;

@synthesize delegate = _delegate;


#pragma Enable Edit/Add Mode
-(void) enableEditGroupName : (NSIndexPath *) indexPath {
    if (indexPath) {
        Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        self.groupNameText.text = group.name;
        
        self.lastSelectedGroupName = indexPath;
        self.groupSelected = group;
        
        self.groupNameText.placeholder = @"Enter New Group Name...";// Select/Deselect Item to Edit/Add...";  
        
    }else {
        
        [self enableAddGroupName];
    }
}

- (void) enableAddGroupName {
    
    self.groupNameText.text = @"";
    self.groupNameText.placeholder = @"Enter New Group Name...";
    self.groupSelected = nil;
    self.lastSelectedGroupName = nil;
}

#pragma mark - InitWith
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) initializeWithSelectMode:(BOOL) selectMode LastSetGroup: (Group *) lastSetGroup
  Context:(NSManagedObjectContext *)managedContext Delegate:(id<FilterGroupSelectviewControllerDelegate>)delegate {
    
    // If UI is not called for existing task there is no use of hte done button
    self.isSelectMode = selectMode;
    
    self.managedObjectContext = managedContext;
    self.delegate = delegate;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.seletedGroupSet = [[NSMutableSet alloc]init ];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     
    NSSet *selectedGroups = nil;
    selectedGroups = [AppSharedData getInstance].dynamicViewData.groupSet;            
    
    for (Group *group in selectedGroups) {    
        [_seletedGroupSet addObject:group];   
    }
    
    [self fetchData];    
    [self.groupNameText setDelegate:self];
    
    
    if (self.isSelectMode) {
        [self enableAddGroupName];
//        self.navigationItem.rightBarButtonItem = nil;        
    }
    else {
//        [self enableEditGroupName:nil]; // can do edit and add
       
    }
//     self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    self.navigationItem.rightBarButtonItem = nil;
    
    [self.groupNameText addTarget:self
                          action:@selector(startEditing:)
                forControlEvents:UIControlEventEditingDidBegin];
    
//    [self.tableView setEditing:YES];
}

- (void) startEditing: (UITextField *)textField 
{
    if (self.tableView.indexPathForSelectedRow == nil) {
        self.lastSelectedGroupName = nil;
        self.groupSelected = nil;
    }
}

- (void)viewDidUnload
{
    [self setGroupNameText:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![_fetchedResultsController.managedObjectContext save:&error]) {
            NSLog(@"Could not save context: %@", error);
        }
    }
    
}


- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated]; 
    
//    self.task.group = self.groupSelected;
    
    // NOt saving the relationship as it has to be saved in the delegate
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return YES;
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    ItemDetailsCell *gaevCell = (ItemDetailsCell *) cell;
    
    Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([_seletedGroupSet containsObject:group]) {        
        gaevCell.accessoryType = UITableViewCellAccessoryCheckmark;        
    }
    else {
        gaevCell.accessoryType = UITableViewCellAccessoryNone;
    }    
    gaevCell.itemNameLabel.text = group.name;      
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"ItemDetailsCell";
  /* Old way
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] 
                initWithStyle:UITableViewCellStyleDefault 
                reuseIdentifier:CellIdentifier];
    }    
   */
    ItemDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /* This is not working with xib. learn later and try again
    if (cell == nil) {
        NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (ItemDetailsCell *) [nib objectAtIndex:0];
    } */

    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // if we are in add/update the groups mode, no need to perform selection/deselection
    if (self.isSelectMode) {
        
        UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:YES];                    
        
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark; 
            [_seletedGroupSet addObject:group];
            // remember current IndexPath now and the corresponding group
            //self.lastSelectedGroupName = indexPath;
            //self.groupSelected = group;
        }    
        else if (cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
            cell.accessoryType = UITableViewCellAccessoryNone;     
            [_seletedGroupSet removeObject:group];
            [self enableAddGroupName];
        }
        else {
            DLog(@"Some error");
            [self enableAddGroupName];
        }
        
       // [self finishSelection];
    }
    else {

        if ([self.lastSelectedGroupName isEqual:indexPath]) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            [self enableAddGroupName];
        } else {

            [self enableEditGroupName: indexPath];
            // not selecting group as it is not needed in this case
        }
    }
}


#pragma mark - Result controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }        
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];    
    
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"Group" 
                                   inManagedObjectContext:self.managedObjectContext];    
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"sort_order" 
                                        ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]  initWithFetchRequest:fetchRequest 
                                                                                                 managedObjectContext:self.managedObjectContext      
                                                                                                   sectionNameKeyPath:nil 
                                                                                                            cacheName:nil];
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Error Fetching Group %@, %@", error, [error userInfo]);
	    abort();
	}    
    
    // Without this autoupdate will not work.
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}


- (void) fetchData {
    
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    [self.tableView reloadData];
}


- (IBAction) finishSelection {
    [self.delegate groupEditViewControllerDidFinish:self SelectedGroup:_seletedGroupSet];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
    [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}




@end
