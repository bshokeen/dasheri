//
//  EffortViewController.h
//  realtodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtilities.h"
#import "AppSharedData.h"
#import "CommonDTO.h"

@protocol EffortViewControllerDelegate;

@interface EffortViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource>

// UI Controls Outlet
@property (weak, nonatomic) IBOutlet UILabel *effortCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *effortUnitLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *effortNumberPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *effortUnitPickerView;


// Attributes expected to be provided from caller
@property (strong, nonatomic) NSString *keyName;
@property (nonatomic) int effortCountIdx;
@property (nonatomic) int effortUnitIdx;
//@property (strong, nonatomic) CommonDTO *commonDTO;
- (void) initForKeyName: (NSString *)keyName WithCountIdx: (int) countIdx WithUnitIdx: (int) unitIdx Delegate:(id<EffortViewControllerDelegate>) delegate;

// Action Operations supported by this controller
- (IBAction)resetBtn:(id)sender;
//- (IBAction)resetBtnClick:(id)sender;
- (IBAction)saveNavgBtnClicked:(id)sender;

// Delegate
@property (weak, nonatomic) id <EffortViewControllerDelegate> delegate;



@end

@protocol EffortViewControllerDelegate <NSObject> 

- (void) effortViewControllerDidFinish:(EffortViewController *) controller WithKeyName: (NSString *) keyName WithEffortCount: (int) countIdx WithEffortUnit: (int) unitIdx;
@end