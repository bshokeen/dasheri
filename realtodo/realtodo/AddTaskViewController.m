//
//  AddTaskViewController.m
//  realtodo
//
//  Created by Me on 16/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "AddTaskViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppSharedData.h"

@interface AddTaskViewController ()
@property (nonatomic, strong) Group *selectedGroup;

// Attributes to store the selected values are stored here. To avoid processing the UI text labels additionally storing the index values.
@property (nonatomic) int selEstimCountIdx;
@property (nonatomic) int selEstimUnitIdx;

// Attributes to store data before that is written over the task object. Maintaining both variables to avoid parsing/processing the UI Label Text.
@property (nonatomic, strong) NSDate *selRemindOn;
@property (nonatomic) NSCalendarUnit selNSCURemindRepeatInterval;

@end

@implementation AddTaskViewController
@synthesize gainOptions = _gainOptions;
@synthesize dueOnLabel = _dueOnLabel;
@synthesize remindOnLabel = _remindOnLabel;
@synthesize noteLabel = _noteLabel;
@synthesize estimUnitLabel = _estimatedEffortLabel;
@synthesize estimatCountLabel = _estimatedEffortCountLabel;
@synthesize tagSlabel = _tagSlabel;
@synthesize contextLabel = _contextLabel;
@synthesize groupNameLabel = _groupNameLabel;
@synthesize taskNameText = _activityNameInput;
@synthesize doneBtn = _doneBtn;
@synthesize flagBtn = _flagBtn;
@synthesize selectedGroup = _selectedGroup;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize gainImage = _gainImage;

@synthesize selEstimCountIdx = _selEstimCountIdx;
@synthesize selEstimUnitIdx = _selEstimUnitIdx;

// Reminder Options
@synthesize selRemindOn = _selRemindOn, selNSCURemindRepeatInterval = _selNSCURemindRepeatInterval;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedGroup = APP_SHARED_DATA.lastSelectedGroup;
    self.groupNameLabel.text = self.selectedGroup.name;
    [self.taskNameText becomeFirstResponder];
    
    // Initialize selValues and UIlabel to reuse the last selected values
    self.selEstimCountIdx = APP_SHARED_DATA.lastSelEffortCount;
    self.selEstimUnitIdx = APP_SHARED_DATA.lastSelEffortUnit;
    self.estimatCountLabel.text = [APP_SHARED_DATA uiTextFromCountIdx:self.selEstimCountIdx];
    self.estimUnitLabel.text  = [APP_SHARED_DATA uiTextFromUnitIdx:self.selEstimUnitIdx];
    self.gainOptions.selectedSegmentIndex = APP_SHARED_DATA.lastSelGain;
    
    [self.gainOptions addTarget:self action:@selector(gainChanged) forControlEvents:UIControlEventValueChanged];
    
    [self setGainButtonImage:APP_SHARED_DATA.lastSelGain]; // set the gain image according to the default gain
    
    // Load default values, nil => NEVER/Not Set, Interval = 2 => Specified date or not specified
    self.selRemindOn = nil;
    self.selNSCURemindRepeatInterval = NSEraCalendarUnit;
    
    // Before loading we need to remove the last set of tags used by previous add task operation.
    [AppSharedData getInstance].lastSelectedTagsOnNewTask = nil;
}

- (void)viewDidUnload
{
    [self setTaskNameText:nil];
    //    [self setProjectNameText:nil];
    [self setGainOptions:nil];
    [self setGroupNameLabel:nil];
    [self setDoneBtn:nil];
    [self setFlagBtn:nil];
    [self setDueOnLabel:nil];
    [self setRemindOnLabel:nil];
    [self setNoteLabel:nil];
    [self setEstimUnitLabel:nil];
    [self setEstimatCountLabel:nil];
    [self setTagSlabel:nil];
    [self setContextLabel:nil];
    
    [super viewDidUnload];
}

- (void) gainChanged {
    // HIDE keyboard when moving to different views
    [self.taskNameText resignFirstResponder];
    
    // Perform required operation now
    int selIdx = [self.gainOptions selectedSegmentIndex];
    [self setGainButtonImage:selIdx];
}

-(void) setGainButtonImage:(NSUInteger) gainIndex {
    
    switch (gainIndex) {
            
        case 0:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"highGain"]] forState:UIControlStateNormal];
            break;
        case 1:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"mediumGain"]] forState:UIControlStateNormal];
            break;
        case 2:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"normalGain"]] forState:UIControlStateNormal];
            break;
        case 3:
            [_gainImage setImage:[UIImage imageNamed:[AppUtilities getImageName:@"lowGain"]] forState:UIControlStateNormal];
            break;
        default:
            _gainImage.backgroundColor = [UIColor redColor];
            break;
    }
    
}

- (void) viewWillAppear:(BOOL)animated {
    // Using a specific shared variable for Add UI to pass on selected tags between Add & Tag View
    NSSet *tags = [AppSharedData getInstance].lastSelectedTagsOnNewTask;
    self.tagSlabel.text = [AppUtilities getSelectedTagsLabelText:tags];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}



- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if ( textField == self.taskNameText) {
        [textField resignFirstResponder];
    }
    
    return YES;
}

/*
 Manage row selection: If a row is selected, create a new editing view controller to edit the property associated with the selected row.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
        case 0: {
            switch (indexPath.row) {
                case 0: {
                    // To prevent this cell from geting selected
                    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
                    [cell setSelected:NO animated:NO];
                    break;
                }
                case 1:
                    [self performSegueWithIdentifier:@"ShowGroupEditViewController" sender:self];
                    break;
                default:
                    break;
            }

            break;
        }
        case 1: {
            switch (indexPath.row) {
                case 1:
                    [self performSegueWithIdentifier:@"ShowMoreTextViewController" sender:self];
                    break;//
                case 2:
                    [AppSharedData getInstance].reminderCustomDaysArray = [NSMutableArray arrayWithObjects: nil]; // TODO: this statement is not on update view why???
                    [self performSegueWithIdentifier:@"ShowReminderViewController" sender:self];
                    break;
                case 3:
                    [self performSegueWithIdentifier:@"ShowNotesViewController" sender:self];
                    break;                
                default: {
                    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
                    [cell setSelected:NO animated:NO];
                    break;
                }
            }
            break;
        }
        case 2: {
            // Effort Section
            [self performSegueWithIdentifier:@"ShowEffortViewController" sender:self];
            break;
        }
        case 3: {
            switch (indexPath.row) {
                case 0:
                    [self performSegueWithIdentifier:@"ShowTagViewController" sender:self];
                    break;
                    
                default:
                    break;
            }
            break;
        }
        case 4: {
            [self saveTask];
            break;
        }
        default:
            break;
    }
    //    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    //    [cell setSelected:NO animated:YES];
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // HIDE keyboard when moving to different views
    [self.taskNameText resignFirstResponder];
    
    if ([[segue identifier] isEqualToString:@"ShowMoreTextViewController"]) {
        
        DueDateViewController *controller = (DueDateViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        CommonDTO *commonDTO = [[CommonDTO alloc] initWithValue:self.taskNameText.text
                                                           Note:self.noteLabel.text
                                                          DueOn:[AppUtilities uiUNFormatedDate:self.dueOnLabel.text WithDayAdd:YES]
                                                       RemindOn:[AppUtilities uiUNFormatedDate:self.remindOnLabel.text WithDayAdd:YES]
                                                          Group:self.selectedGroup
                                                   TaskObjectID:nil];
        switch (indexPath.row) {
            case 0: // It was commented earlier, so i need to check if this id is correct.
                commonDTO.editFieldType = name;
                break;
            case 1:
                commonDTO.editFieldType = dueOn;
                break;
            case 2:
                commonDTO.editFieldType = remindOn;
                break;
            case 3:
                commonDTO.editFieldType = note;
                break;
            default:
                break;
        }
        
        controller.commonDTOObj = commonDTO;
        
        // to get the events, had to set the delegate as self.
        controller.delegate = self;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowNotesViewController"]) {
        
        NotesViewController *controller = (NotesViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        
        
        CommonDTO *commonDTO = [[CommonDTO alloc] initWithValue: self.taskNameText.text
                                                           Note:self.noteLabel.text
                                                          DueOn:nil
                                                       RemindOn:nil
                                                          Group:nil
                                                   TaskObjectID:nil];
        
        switch (indexPath.row) {
            case 0: // It was commented earlier, so i need to check if this id is correct.
                commonDTO.editFieldType = name;
                break;
            case 3:
                commonDTO.editFieldType = note;
                break;
            default:
                break;
        }
        
        controller.commonDTOObj = commonDTO;
        
        // to get the events, had to set the delegate as self.
        controller.delegate = self;
    }
    else if ([[segue identifier] isEqualToString:@"ShowReminderViewController"]) {

        ReminderViewController *remindCtrl = (ReminderViewController *) [segue destinationViewController];
        
        // Get a date from NIL/Never, have 1 day extra in it with seconds stripped out
        NSDate *date = [AppUtilities getDateWithSecondsResetForDate:[AppUtilities uiUNFormatedDate:self.remindOnLabel.text WithDayAdd:YES]];
        [remindCtrl initWithArgumentsRemindOn:date
                                                RemindRepeatInterval:0];
        remindCtrl.delegate = self;
    }
    else if ( [[segue identifier] isEqualToString:@"ShowGroupEditViewController"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        switch (indexPath.row) {
            case 1: {
                TabGroupsViewController *grpEditViewCntlr = (TabGroupsViewController *)[segue destinationViewController];
                //                grpEditViewCntlr.task = self.task;
                
                [grpEditViewCntlr initializeWithMode: ReadOnlySingleSelection
                                              LastSelGroupSet: [[NSMutableSet alloc] initWithObjects:self.selectedGroup, nil]
                                                   Context:self.managedObjectContext
                                                  Delegate:self];
                
                /*
                 // Create a new managed object context for allowing saving a new group. If updates happen to current task it should be not be saved there rather go to parent view of this controller.
                 //                NSManagedObjectContext *addingContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
                 //                [addingContext setParentContext:[self.task managedObjectContext]];
                 //                grpEditViewCntlr.managedObjectContext = addingContext;
                 */
                break;}
            default:
                break;
        }
    }
    else if ( [[segue identifier] isEqualToString:@"ShowEffortViewController"] ) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        EffortViewController *evCtrl = (EffortViewController *) [segue destinationViewController];
        evCtrl.delegate = self;
        
        if (indexPath.row == 0)
            [evCtrl initForKeyName:@"effort" WithCountIdx:self.selEstimCountIdx WithUnitIdx:self.selEstimUnitIdx Delegate:self];
        //        else if (indexPath.row == 1)
        //            [evCtrl initForKeyName:@"actual" WithCountIdx:self.selActualCountIdx WithUnitIdx:self.selActualUnitIdx Delegate:self];
        
    }
    else if ( [[segue identifier] isEqualToString:@"ShowTagViewController"] ) {
        
        TabTagsViewController *tvCtrl = (TabTagsViewController *) [segue destinationViewController];
        [tvCtrl initWithoutTask:SOURCE_UI_ADDUI];
    }
}

- (void) RemindViewControllerDidFinish:(NSDate *)remindOn RepeatInterval:(NSCalendarUnit)nscuRemindRepeatInterval FormatedLabelText:(NSString *)formatedLabelText 
{
    self.selRemindOn = remindOn;
    self.selNSCURemindRepeatInterval = nscuRemindRepeatInterval;
    
    self.remindOnLabel.text = formatedLabelText;
}

#pragma MoreTextViewController Delegate API
- (void) moreTextViewControllerDidCancel:(__autoreleasing id *)controller {
    
}
- (void) moreTextViewControllerDidFinish:(CommonDTO *)commonDTO: (NSString *)FormatedDate
{
    
    if (commonDTO.editFieldType == name )
        self.taskNameText.text = commonDTO.name;
    
    else if ((commonDTO.editFieldType == dueOn) && (![FormatedDate isEqualToString:NEVER])) {
        self.dueOnLabel.text = [AppUtilities uiFormatedDate:commonDTO.dueOn];
        
        NSDate *remindOn = commonDTO.remindOn;
        if (![self.remindOnLabel.text isEqualToString:NEVER] && [self.dueOnLabel.text isEqualToString:NEVER]) {
            self.dueOnLabel.text = [AppUtilities uiFormatedDate:remindOn];
        }
    }
    
    else if ([FormatedDate isEqualToString:NEVER]) {
        self.dueOnLabel.text = NEVER;
    }
    
    else if (commonDTO.editFieldType == note)
        self.noteLabel.text = commonDTO.note;
    
    else if (commonDTO.editFieldType == remindOn) {
        NSDate *remindOn = commonDTO.remindOn;
        // Ensure not null and check if selected remindOn date is greater than the current date
        //        if (remindOn != nil && ([remindOn compare:[NSDate date]] == NSOrderedDescending)) {
        if (remindOn != nil && [remindOn timeIntervalSinceNow] > 0) {
            remindOn = [AppUtilities getDateWithSecondsResetForDate:remindOn];
            self.remindOnLabel.text = [AppUtilities uiFormatedDate:remindOn];
        } else {
            self.remindOnLabel.text = [AppUtilities uiFormatedDate:nil];
        }
        
        if (remindOn != nil && [self.dueOnLabel.text isEqualToString:@"Never"]) {
            self.dueOnLabel.text = [AppUtilities uiFormatedDate:remindOn];
        }
    }
    
    else if (commonDTO.editFieldType == group) {
        self.selectedGroup = commonDTO.group;
        self.groupNameLabel.text = commonDTO.group.name;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Notes Controller Operations
-(void) notesViewControllerDidFinish:(CommonDTO *)commonDTO
{
    if (commonDTO.editFieldType == name )
        self.taskNameText.text = commonDTO.name;
    
    else if (commonDTO.editFieldType == note)
        self.noteLabel.text = commonDTO.note;
    
    [self.navigationController popViewControllerAnimated:YES];   
}

#pragma mark - GroupEditViewControllerDelegate API Implementation
-(void) groupEditViewControllerDidFinish:(id)controller SelectedGroup:(Group *) group
{
    self.selectedGroup = group;
    self.groupNameLabel.text = [AppUtilities handleEmptyString:group.name];
    [self.navigationController popViewControllerAnimated:YES];
    
    // To have some indication of which row we operated trying to do a deselect manually. Not doing great job but something is there
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:newPath];
    [cell setSelected:NO animated:YES];

}

- (void) groupEditViewControllerDidCancel:(__autoreleasing id *)controller {
    //    //no need to save any input when the user taps Cancel, so this method simply dismisses the add scene
    //    [self dismissViewControllerAnimated:YES completion:nil];
    // Don't pass current value to the edited object, just pop.
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) saveTask
{
    NSString *taskName = self.taskNameText.text;
    Group *group = self.selectedGroup;
    
    int gain = [self.gainOptions selectedSegmentIndex];
    
    NSDate *dueOn = nil;
    if (![self.dueOnLabel.text isEqualToString:@"Never"]) {
        dueOn = [AppUtilities uiUNFormatedDate:self.dueOnLabel.text];
    }
    
    // Alarm is created in add task
    // Values for remindOn & remindRepeatInterval is taken up from local variables instead of regenerating from the label text.
    
    int flag = 0;
    if ( [self.flagBtn isSelected]) {
        flag = 1;
    }
    
    NSString *note = self.noteLabel.text;
    
    int estimCountIdx = self.selEstimCountIdx;
    int estimUnitIdx  = self.selEstimUnitIdx;
    
    if (taskName.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add New Task" message:@"Please enter task name." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    else {
        [AppUtilities addTask:taskName Gain:gain DueOn:dueOn RemindOn:self.selRemindOn Note:note Flag:flag Group:group EstimCountIdx:estimCountIdx EstimUnitIdx:estimUnitIdx Tags: [AppSharedData getInstance].lastSelectedTagsOnNewTask RemindInterval:self.selNSCURemindRepeatInterval];
    
        [self dismissModalViewControllerAnimated:YES];
    }
}

#pragma mark - Done Cancel Operations
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //    [[self delegate] addActivityViewControllerDidCancel:self];
}

- (IBAction)done:(id)sender {
    [self saveTask];
}


- (IBAction)flagBtnClicked:(id)sender {
    
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOff"]] forState:UIControlStateNormal];
        [sender setSelected:NO];
    }else {
        [sender setImage:[UIImage imageNamed:[AppUtilities getImageName:@"starOn"]] forState:UIControlStateSelected];
        [sender setSelected:YES];
    }
}
#pragma mark - Effort Controller Operations

- (void) effortViewControllerDidFinish:(EffortViewController *)controller WithKeyName:(NSString *)keyName WithEffortCount:(int)countIdx WithEffortUnit:(int)unitIdx {
    
    if ([keyName isEqualToString:@"effort"]) {
        self.selEstimCountIdx = countIdx;
        self.selEstimUnitIdx = unitIdx;
        
        self.estimatCountLabel.text = [APP_SHARED_DATA uiTextFromCountIdx:self.selEstimCountIdx];
        self.estimUnitLabel.text  = [APP_SHARED_DATA uiTextFromUnitIdx:self.selEstimUnitIdx];
    }
    else if ( [keyName isEqualToString:@"actual"] ) {
        // Not needed on Add UI
    }
}

@end
