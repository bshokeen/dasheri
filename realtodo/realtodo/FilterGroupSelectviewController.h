//
//  GroupEditViewController.h
//  realtodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Task.h"
#import "Group.h"
#import "ItemDetailsCell.h"

@protocol FilterGroupSelectviewControllerDelegate;

@interface FilterGroupSelectviewController : UITableViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *groupNameText;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableSet *seletedGroupSet;
@property(weak, nonatomic) id <FilterGroupSelectviewControllerDelegate> delegate;

- (void) initializeWithSelectMode: (BOOL) selectMode LastSetGroup: (Group*) lastSetGroup Context:(NSManagedObjectContext *) managedContext Delegate: (id<FilterGroupSelectviewControllerDelegate>) delegate;
- (NSFetchedResultsController *)fetchedResultsController;
// This is set while calling this view to indicate if we need to allow edit/update of existing group. If called from edit/add task it is select mode only.
@property BOOL isSelectMode;

@end

@protocol FilterGroupSelectviewControllerDelegate <NSObject>

-(void) groupEditViewControllerDidCancel: (id *) controller;
-(void) groupEditViewControllerDidFinish:(id) controller SelectedGroup:(NSMutableSet *)group;
@end
