//
//  EmailViewController.m
//  realtodo
//
//  Created by Me on 02/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "EmailViewController.h"
#import "AppDelegate.h"

@interface EmailViewController ()

@end

@implementation EmailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
        return YES;
}

- (IBAction)exportTasksToEmail:(id)sender {
 
    // MOVE This code the TaskDataController
    NSManagedObjectContext *moc = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];

    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *taskEntity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:moc];
    
    [request setEntity:taskEntity];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"dueOn" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error= nil;
    
    NSMutableArray *mutableFetchResults = [[moc executeFetchRequest:request error:&error]  mutableCopy];
    
    NSString *fetchedData = @"GropName, TaskName, DueOn, Gain, Done, progress, CreatedOn, remind_on, notes \n";
     
    for (Task *taskItem in mutableFetchResults) {
        
        fetchedData = [ fetchedData stringByAppendingString:[self prepareForDump:taskItem.group.name]];
        fetchedData = [ fetchedData stringByAppendingString:@"\t, "];
        fetchedData = [ fetchedData stringByAppendingString:[self prepareForDump:taskItem.name]];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];        
        fetchedData = [ fetchedData stringByAppendingString:[self handleNull:[taskItem.dueOn description]]];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];        
        fetchedData = [ fetchedData stringByAppendingString:[NSString stringWithFormat:@"%i", [taskItem.gain intValue] ] ];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];        
        fetchedData = [ fetchedData stringByAppendingString:[NSString stringWithFormat:@"%i", [taskItem.done  intValue]] ];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];        
        fetchedData = [ fetchedData stringByAppendingString:[NSString stringWithFormat:@"%d", [taskItem.progress doubleValue]] ];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];       
        fetchedData = [ fetchedData stringByAppendingString:[self handleNull: [taskItem.createdOn description]]];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];       
        fetchedData = [ fetchedData stringByAppendingString:[self handleNull: [taskItem.remind_on description]]];
        fetchedData = [ fetchedData stringByAppendingString:@", \t"];   
        
        // Include the Note Field
        fetchedData = [ fetchedData stringByAppendingString:[self prepareForDump:taskItem.note]];
        
        fetchedData = [ fetchedData stringByAppendingString:@"\n"];
    }
        
//    NSLog(@"Count of data: %d", [mutableFetchResults count]);
//    UIAlertView *someError = [[UIAlertView alloc] initWithTitle: @"Network error" message: @"Error sending your info to the server" delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];

    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"realtodo: Tasks Backup!"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"bshokeen@gmail.com"]];
        //        [mailCont setMessageBody:@"Don't ever want to give you up" isHTML:NO];
        [mailCont setMessageBody:fetchedData isHTML:NO];     
        [self presentModalViewController:mailCont animated:YES];
        
    }
    
    DLog(@"Email Sent...Log handling" );
    
}

- (NSString *) handleNull: (NSString *) str {
    if (str == nil|| [str length] == 0) {
        return @"";
    }
    else {
        return str;
    }
}
- (NSString *) prepareForDump: (NSString *) inputStr {
    
    NSString *NEWLINE = @"\n";
    NSString *LINEFEED= @"\t";
    
    NSString *COMMA = @",";
    NSString *SPACE = @" ";
    NSString *EMPTY = @"";
    
    NSString *tmpStr = inputStr;
    if (tmpStr == nil|| [tmpStr length] == 0) {
        tmpStr = EMPTY;    
    }
    else {
        tmpStr = [tmpStr stringByReplacingOccurrencesOfString:NEWLINE   withString:LINEFEED];
        tmpStr = [tmpStr stringByReplacingOccurrencesOfString:COMMA     withString:SPACE];
    }

    return  tmpStr;
}

    

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            DLog(@"Mail Sending - Canceled");
            break;
        case MFMailComposeResultSaved:
            DLog(@"Mail Sending - Saved");
            break;
        case MFMailComposeResultSent:
            DLog(@"Mail Sending - Sent");        
            break;
        case MFMailComposeResultFailed:
            DLog(@"Mail Sending - Failed");            
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error <img class=""wp-smiley"" alt="":-("" src=""%22http://blog.mugunthkumar.com/wp-includes/images/smilies/icon_sad.gif%22"" /> "
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
}

@end

