//
//  AFilterSettingsViewController.m
//  realtodo
//
//  Created by Balbir Shokeen on 9/3/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "FilterSettingsViewController.h"
#import "AppDelegate.h"
#import "AppSharedData.h"
#import "TagViewController.h"


@interface FilterSettingsViewController ()

@property(nonatomic, retain) NSArray *sortListArray;
@property (nonatomic, strong) NSString *selectedCateg;
//@property (nonatomic, strong) Group *selectedGroup;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation FilterSettingsViewController

@synthesize sortListArray, selectedCateg = _selectedCateg;

@synthesize selectedTags = _selectedTags;
@synthesize selectedOrderBy = _selectedOrderBy;
@synthesize selectedCollapseOn = _selectedCollapseOn;
@synthesize selectedDisplayType = _selectedDisplayType;
@synthesize orderBy_btn = _orderBy_btn;
@synthesize selectedGroups = _selectedGroups;
@synthesize fetchedResultsController = _fetchedResultsController, managedObjectContext = _managedObjectContext;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
	[super viewDidUnload];
}


- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Group"
                                   inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"sort_order"
                                        ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]  initWithFetchRequest:fetchRequest
                                                                                                 managedObjectContext:_managedObjectContext
                                                                                                   sectionNameKeyPath:nil
                                                                                                            cacheName:nil];
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Error Fetching Group %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Without this autoupdate will not work.
    _fetchedResultsController.delegate = self;
    Group *fromGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:0];
    NSLog(@"Group: %@", fromGroup.name);
    return _fetchedResultsController;
}



-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    // get managed object context from App Delegate
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _managedObjectContext = [appDelegate managedObjectContext];
    
    
    self.navigationController.navigationBar.tintColor = nil;
    sortListArray = [AppSharedData getInstance].dynamicViewData.sortCategoriesValArr;
    
    //self.selectedGroup = APP_SHARED_DATA.lastSelectedGroup;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
            
        case 0: {
            switch (indexPath.row) {
                case 0:
                case 1:
                case 2: {
                    [self performSegueWithIdentifier:@"ShowFilterItemDetailsViewController" sender:self];
                    break;
                }
                case 3: {
                    // segue is defined directly on cell.
                    break;
                }
                case 4: {
                    [self performSegueWithIdentifier:@"ShowSelectGroupViewController" sender:self];
                    break;
                }
                default:
                    DLog(@"Error, Not expected to come here.");
                    break;
            }
            break;
        }
            
        case 1: {
            switch (indexPath.row) {
                case 0: {
                    
                    [self AnimateView];
                    
                    [[AppSharedData getInstance].dynamicViewData resetToDefaultValues];
                    [self viewWillAppear:YES];
                    break;
                }
                default:
                    break;
            }
        }
        default:
            break;
    }
}

-(void) AnimateView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.view cache:NO];
    [UIView commitAnimations];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowTagViewController"] ) {
        
        TagViewController *tvCtrl = (TagViewController *) [segue destinationViewController];
        [tvCtrl initWithoutTask:SOURCE_UI_FILTERUI];
    }
    
    else if ( [[segue identifier] isEqualToString:@"ShowSelectGroupViewController"] ) {
        
        FilterGroupSelectviewController *grpEditViewCntlr = (FilterGroupSelectviewController *)[segue destinationViewController];
        
        [grpEditViewCntlr initializeWithSelectMode: YES
                                      LastSetGroup: nil
                                           Context: _managedObjectContext
                                          Delegate:self];
        
    }
    
    else if ([[segue identifier] isEqualToString:@"ShowFilterItemDetailsViewController"]) {
        FilterItemDetailsViewController *fiCtrl = (FilterItemDetailsViewController *) [segue destinationViewController];
        fiCtrl.delegate = self;
        
        NSIndexPath *selIdxP = [self.tableView indexPathForSelectedRow];
        if (selIdxP && selIdxP.section == 0) {
            int sortItemsIdx = selIdxP.row;
            [AppSharedData getInstance].dynamicViewData.sortCategoriesIdx = sortItemsIdx;
            
            NSString *selectedSectionTitle = [[AppSharedData getInstance].dynamicViewData.sortCategoriesValArr objectAtIndex:sortItemsIdx];
            
            [fiCtrl setSectionTitle:selectedSectionTitle];
            [fiCtrl setSortItemIdx:sortItemsIdx];
        }
    }
}


- (void) viewWillAppear:(BOOL)animated {
    
    NSSet *tags = [AppSharedData getInstance].dynamicViewData.tagSet;
    _selectedTags.text = [AppUtilities getSelectedTagsLabelText:tags];
    
    NSSet *groups = [AppSharedData getInstance].dynamicViewData.groupSet;
    _selectedGroups.text = [AppUtilities getSelectedTagsLabelText:groups];
    
    int orderByIdx = [AppSharedData getInstance].dynamicViewData.orderByIdx;
    _selectedOrderBy.text = [[AppSharedData getInstance].dynamicViewData.orderByUIValueArr objectAtIndex:orderByIdx];
    
    _selectedCollapseOn.text = [[AppSharedData getInstance].dynamicViewData.collapsibleOnUIValueArr objectAtIndex:[AppSharedData getInstance].dynamicViewData.collapsibleOnIdx];
    
    _selectedDisplayType.text = [[AppSharedData getInstance].dynamicViewData.displayTypeValArr objectAtIndex:[AppSharedData getInstance].dynamicViewData.displayTypeIdx];
    
    _selectedGroups.text = [AppUtilities getSelectedTagsLabelText:[AppSharedData getInstance].dynamicViewData.groupSet];
    
    [self setOrderBtnImage];
}

#pragma mark - GroupEditViewControllerDelegate API Implementation
-(void) groupEditViewControllerDidFinish:(id)controller SelectedGroup:(NSMutableSet *) groupSet
{
    //self.selectedGroup = group;
    
    [AppSharedData getInstance].dynamicViewData.groupSet = groupSet;
    //    for (int i=0; i<group.count; i++) {
    //        NSLog(@"FilterSettingsViewController groupEditVCDidFinish: %@", [group allObjects]);
    //    }
    //_selectedGroups.text = [AppUtilities handleEmptyString:group.name];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) groupEditViewControllerDidCancel:(__autoreleasing id *)controller {
    //    //no need to save any input when the user taps Cancel, so this method simply dismisses the add scene
    //    [self dismissViewControllerAnimated:YES completion:nil];
    // Don't pass current value to the edited object, just pop.
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - FilterItemViewControllerDelegate API Implementation
- (void)filterItemTVControllerDidCancel:(FilterItemDetailsViewController *)controller
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


-(void) setOrderBtnImage {
    
    if ( [self.orderBy_btn isSelected]) {
        
    }
    if ([AppSharedData getInstance].dynamicViewData.orderByDirection)
        [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderYES"]] forState:UIControlStateNormal];
    
    else
        [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderNO"]] forState:UIControlStateNormal];
}

- (IBAction)setOrderDirection:(id)sender {
    
    if ([AppSharedData getInstance].dynamicViewData.orderByDirection) {
        [AppSharedData getInstance].dynamicViewData.orderByDirection = NO;
        [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderNO"]] forState:UIControlStateNormal];
    }
    else {
        [AppSharedData getInstance].dynamicViewData.orderByDirection = YES;
        [_orderBy_btn setImage:[UIImage imageNamed:[AppUtilities getImageName:@"OrderYES"]] forState:UIControlStateNormal];
    }
    
}

@end

