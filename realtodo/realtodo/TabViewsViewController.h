//
//  TabViewsViewController.h
//  realtodo
//
//  Created by Me on 18/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"


@interface TabViewsViewController : CommonViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *groupSegment;

- (IBAction)editDoneBtnClick:(id)sender;

@end
