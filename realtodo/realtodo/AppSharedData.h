//
//  AppSharedData.h
//  realtodo
//
//  Created by Me on 01/08/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Group.h"
#import "DynamicViewData.h"
#import "AppUtilities.h"

#define APP_SHARED_DATA ([AppSharedData getInstance])

@interface AppSharedData : NSObject

// Remember all the values across different UI Screen Operations
@property (nonatomic, strong) NSString *lastUsedGroup;
@property (nonatomic, strong) Group *lastSelectedGroup;

// To set the Reminder, save whether it is daily or weekly reminder
@property (nonatomic, strong) NSMutableArray *reminderCustomDaysArray;

// To prepopulate the Groups, Tags and Tasks.
@property (nonatomic, strong) NSMutableArray *preCreateGroups;
@property (nonatomic, strong) NSMutableArray *preCreateTags;
@property (nonatomic, strong) NSMutableArray *preCreateTasks;


/*
 * Array with values, last selected values, API to get text from Idx and default Idx values are stored here.
 */
// For the Effort UI storing the default values
@property (nonatomic, strong) NSMutableArray *effortCountArray;
@property (nonatomic, strong) NSMutableArray *effortUnitArray;
@property (nonatomic) int lastSelEffortCount;
@property (nonatomic) int lastSelEffortUnit;
@property (nonatomic) int lastSelGain;
// Default attributes owned by this view
@property (nonatomic) int defaultEffortCountIdx;
@property (nonatomic) int defaultEffortUnitIdx;
@property (nonatomic) int defaultGainIdx;


// For Add New Task UI, shared variables
@property (nonatomic, strong) NSMutableSet *lastSelectedTagsOnNewTask;

- (NSString *) uiTextFromCountIdx: (int) countIdx;
- (NSString *) uiTextFromUnitIdx:  (int) unitIdx;

// For Dynamic View Storing the required information across operation
@property (nonatomic, strong) DynamicViewData *dynamicViewData;

+ (AppSharedData *) getInstance;

// API to support Reminder Static Array
// To get the UI Label for display for the specified calendar unit
//- (NSString *) uiLabelFromNSCalendarUnit : (NSCalendarUnit) remindRepeatInterval;
- (int) getSegmentIdxForCalenderUnit: (NSCalendarUnit) nscuRepeatInterval;
- (NSCalendarUnit) getCalenderUnitForSegmentIndex: (int) selIdx;
- (NSString *) getRemindOnLabelTextFromSelSegIdx:   (int) selIdx RemindDate: (NSDate *) remindOnDate;
- (NSString *) getRemindOnLabelTextFromCalendarUnit: (NSCalendarUnit) nscuRepeatInterval RemindDate: (NSDate *) remindOnDate;
@end