//
//  ViewController.h
//  calendar
//
//  Created by iMobile on 10/3/12.
//  Copyright (c) 2012 iMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TapkuLibrary.h"
#import "AppDelegate.h"


@interface CalendarViewController : TKCalendarMonthTableViewController<NSFetchedResultsControllerDelegate> {
	NSMutableArray *dataArray;
	NSMutableDictionary *dataDictionary;
}

@property (retain,nonatomic) NSMutableArray *dataArray;
@property (retain,nonatomic) NSMutableDictionary *dataDictionary;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;


- (void) generateRandomDataForStartDate:(NSDate*)start endDate:(NSDate*)end;

@end
