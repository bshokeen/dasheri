//
//  CommonDTO.m
//  realtodo
//
//  Created by Me on 18/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "CommonDTO.h"

@implementation CommonDTO


@synthesize editFieldType = _editFieldType;
@synthesize editFieldName = _editFieldName;

@synthesize name = _taskName;
@synthesize note = _note;
@synthesize dueOn =_dueOn;
@synthesize remindOn = _remindOn;
//@synthesize groupName = _groupName;
@synthesize taskObjectID = _taskObjectID;
@synthesize group = _group;

//@synthesize effortUnitIdx = _effortUnitIdx;
//@synthesize effortCountIndx = _effortCountIndx;

-(id) initWithValue: (NSString *)name Note:(NSString *)note DueOn:(NSDate *)dueOn RemindOn:(NSDate *)remindOn Group:(Group *)group TaskObjectID:(NSString *)taskObjectID {

    if (self = [super init]) {
        self.name = name;
        self.note = note;
        self.dueOn = dueOn;
        self.remindOn = remindOn;
        self.group = group;
        self.taskObjectID = taskObjectID;
    }
    return self;
}

- (void) setEditFieldValue: (id) value {
    if (self.editFieldType == name )
        self.name = (NSString *) value;
    else if (self.editFieldType == note)
        self.note = (NSString *) value;
    else if (self.editFieldType == dueOn)
        self.dueOn = (NSDate *) value;
    else if (self.editFieldType == remindOn)
        self.remindOn = (NSDate *) value;
    else if (self.editFieldType == group)
        self.group = (Group *) value;
    else
        DLog(@"Unexpected to reach code here");
}

- (id) getEditFieldValue
{
    id returnValue = nil;
    
    if (self.editFieldType == name )
        returnValue = self.name;
    
    else if (self.editFieldType == note)
        returnValue = self.note;
    
    else if (self.editFieldType == dueOn)
        returnValue = self.dueOn;

    else if (self.editFieldType == remindOn)
        returnValue = self.remindOn;

    else if (self.editFieldType == group)
        returnValue = self.group;
    else
        DLog(@"Unexpected to reach code here");
    
    return returnValue;
}


- (NSString *) getEditFieldName {
    
    NSString *returnValue = nil;
    
    if (self.editFieldType == name )
            returnValue = @"Name";
    
    else if (self.editFieldType == dueOn)
        returnValue = @"Due On";
    
    else if (self.editFieldType == note)
        returnValue = @"Note";
    
    else if (self.editFieldType == remindOn)
        returnValue = @"Remind On";
    
    else if (self.editFieldType == group)
        returnValue = @"Group";
    
    else
        returnValue = @"...";
    
    return returnValue;
}
@end
