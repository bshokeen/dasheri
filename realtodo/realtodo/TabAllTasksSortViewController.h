//
//  TabAllTasksSortViewController.h
//  realtodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdateTaskViewController.h"
#import "AddTaskViewController.h"
#import "TaskViewCell.h"
#import "Task.h"
#import <QuartzCore/QuartzCore.h>
#import "Group.h"


@interface TabAllTasksSortViewController : UITableViewController 
                                            <UpdateTaskViewControllerDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;


@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegment;


- (NSFetchedResultsController *)fetchedResultsController;

@end


