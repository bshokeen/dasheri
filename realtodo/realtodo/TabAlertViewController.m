//
//  AlertViewController.m
//  realtodo
//
//  Created by Me on 14/10/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "TabAlertViewController.h"

@interface TabAlertViewController ()

@end

@implementation TabAlertViewController

@synthesize fetchedResultsControllerObj = _fetchedResultsControllerObj;
@synthesize mgdObjContext = _mgdObjContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    
    // Load the data on first run
    [self forceReloadThisUI];
    
//    int badgeCount = _fetchedResultsControllerObj.fetchedObjects.count;
    // TODO: Option to cut down a DB call here. Use teh badgeCount from fetchresults
    [AppUtilities updateAlertBadge:2];
   }

- (void) viewDidAppear:(BOOL)animated {
    
    if ( [AppSharedData getInstance].dynamicViewData.reloadUI ) {
        [[AppSharedData getInstance].dynamicViewData setReloadUI:NO];
        
        [self forceReloadThisUI];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    // Release any properties that are loaded in viewDidLoad or can be recreated lazily.
    self.fetchedResultsControllerObj = nil;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = LANDSCAPE_WIDTH_FACTOR;
    else {
        [AppSharedData getInstance].dynamicViewData.landscapeWidthFactor = 0;
    }
    [self forceReloadThisUI];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //    return IS_PAD || (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    return YES;
}


#pragma mark Table view data source methods
/*
 The data source methods are handled primarily by the fetch results controller
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    if ([sectionInfo numberOfObjects] == 0) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    return [sectionInfo numberOfObjects];
}

// Customize the appearance of table view cells.

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    // Configure the cell to show the Tasks's details
    TaskViewCell *mvaCell = (TaskViewCell *) cell;
    [mvaCell resetInitialText];
    
    Task *activityAtIndex =     [[self fetchedResultsController] objectAtIndexPath:indexPath];
    [AppUtilities configureCell:mvaCell Task:activityAtIndex];   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"TaskViewCell";
    
    TaskViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    /*  // This is not being invoked in my case but still retained as code here.
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (TaskViewCell *) [nib objectAtIndex:0];
     }*/
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma mark Table view editing

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Instead of deleting the task, clear up the alert on it.
        Task *task = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        switch ([task.remind_repeat_interval intValue]) {
            case 0: {
                task.remind_on = nil;
                task.alert_notification = 0;
                task.alert_notification_on = nil;
                break;
            }
            case NSDayCalendarUnit:
            case NSWeekCalendarUnit:
            case NSMonthCalendarUnit:
            case NSYearCalendarUnit: 
            case NSEraCalendarUnit:{
                // In these cases we reseting the notification to prevent them for appearing again. Code is by default excluding them to show on this UI as pending alerts as their date will always be of past. We can do a query of all notifications for this app and query to show those records.
                task.alert_notification = 0;
                task.alert_notification = nil;
                break;
            }
            default:
                DLog(@"AlertVIewController: ERROR - Should not have come here");
                break;
        }
        
        [AppUtilities saveTask:task];
        
        //    int badgeCount = _fetchedResultsControllerObj.fetchedObjects.count;
        // TODO: Option to cut down a DB call here. Use teh badgeCount from fetchresults
        [AppUtilities updateAlertBadge:2];
        
        DLog(@"AlertViewController: commitEditingStyle: Finished Delete");
        
    } 
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Clear";
}

#pragma mark Fetched results controller

/*
 NSFetchedResultsController delegate methods to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    UITableView *tableView = self.tableView;
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

- (void) fetchData {
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        ALog(@"AlertViewController: Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void) forceReloadThisUI 
{    
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    [self fetchData];
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *) fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:self.mgdObjContext];
    [fetchRequest setEntity:entity];
    
    // TODO: We need to use push notifications instead of local to avoid missing notifications sent on teh lock screen. For now using the remind_on field to do so.
    // TODO: To avoid repeated fetch of recourring intervals fetching only repeat interval = 0. it is a workaround for now. Push notification to fix this.
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(alert_notification == 1) || (remind_on <= %@ and remind_repeat_interval == 0) ", [NSDate date]];
    [fetchRequest setPredicate:filterPredicate];
    
    
    // Create the sort descriptors array.
    NSSortDescriptor *grpSortOrderDescriptor = [[NSSortDescriptor alloc]initWithKey:@"alert_notification_on" ascending:NO];
    
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:
                                grpSortOrderDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Create and initialize the fetch results controller.
    self.fetchedResultsControllerObj = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                                                           managedObjectContext:self.mgdObjContext 
                                                                             sectionNameKeyPath:nil
                                                                                      cacheName:nil];
    self.fetchedResultsControllerObj.delegate = self;
    
    DLog(@"AlertViewController: getFetchResultsCtroller: RE - QUERY");
    // Memory management.
    return self.fetchedResultsControllerObj;
}  


#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ShowUpdateActivityViewController"] ) 
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Task *task = (Task *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        UpdateTaskViewController *updateActivityViewController = [segue destinationViewController];
        updateActivityViewController.delegate = self;
        updateActivityViewController.task = task;
        
    }
    else if ([[segue identifier] isEqualToString:@"ShowAddActivityViewController"]) 
    {
        AddTaskViewController *addActivityViewController = (AddTaskViewController *) [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        
        addActivityViewController.managedObjectContext = self.mgdObjContext;
    }
}

#pragma mark - Add/Update controller delegate
- (void) updateActivityViewControllerDidCancel:(UpdateTaskViewController *)controler {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) updateActivityViewControllerDidFinish:(UpdateTaskViewController *)controller task:(Task *)task WithDelete:(BOOL)isDeleted
{
    if (task) {
        if (isDeleted) {
            [AppUtilities deleteTask:task];
        }
        else {
            [AppUtilities saveTask:task];
        }
    }

    [self.navigationController popViewControllerAnimated:YES];
    
    [self forceReloadThisUI];
}


#pragma mark - Action
- (IBAction)clearBtnClick:(id)sender {
    
    [AppUtilities clearAllAlertNotification];
}



@end
