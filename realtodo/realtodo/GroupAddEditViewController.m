//
//  GroupEditViewController.m
//  realtodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import "GroupAddEditViewController.h"
#import "Group.h"
#import "AppUtilities.h"
#import "AppSharedData.h"

@interface GroupAddEditViewController ()
    @property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
    @property (nonatomic, strong) Group *groupSelected;
    @property (nonatomic, strong) NSIndexPath *lastSelectedGroupName;
    @property BOOL canEditSelection;

@end

@implementation GroupAddEditViewController

@synthesize headerView = _headerView;
@synthesize groupNameText = _groupNameText;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;
//@synthesize task = _task;
@synthesize lastSetGroup = _lastSetGroup;
@synthesize doneBtn = _doneBtn;
@synthesize groupSelected = _groupSelected;
@synthesize lastSelectedGroupName = _lastSelectedGroupIndexPath;
@synthesize isSelectMode =_isSelectMode;
@synthesize canEditSelection = _canEditSelection;

@synthesize delegate = _delegate;


#pragma Enable Edit/Add Mode
-(void) enableEditGroupName : (NSIndexPath *) indexPath {
    if (indexPath) {
        Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        self.groupNameText.text = group.name;
        
        self.lastSelectedGroupName = indexPath;
        self.groupSelected = group;
        
        self.groupNameText.placeholder = @"Enter New Group Name...";// Select/Deselect Item to Edit/Add...";  
        
    }else {
        
        [self enableAddGroupName];
    }
}

- (void) enableAddGroupName {
    
    self.groupNameText.text = @"";
    self.groupNameText.placeholder = @"Enter New Group Name...";
    self.groupSelected = nil;
    self.lastSelectedGroupName = nil;
}

#pragma mark - InitWith
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) initializeWithSelectMode:(BOOL) selectMode LastSetGroup: (Group *) lastSetGroup
  Context:(NSManagedObjectContext *)managedContext Delegate:(id<GroupAddEditViewControllerDelegate>)delegate {
    
    // If UI is not called for existing task there is no use of hte done button
    self.isSelectMode = selectMode;
    
    self.managedObjectContext = managedContext;
    self.delegate = delegate;
    self.lastSetGroup = lastSetGroup;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.seletedGroupSet = [[NSMutableSet alloc]init ];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     
    
    [self fetchData];    
    [self.groupNameText setDelegate:self];
    
    
    if (self.isSelectMode) {
        [self enableAddGroupName];
//        self.navigationItem.rightBarButtonItem = nil;        
    }
    else {
//        [self enableEditGroupName:nil]; // can do edit and add
       
    }
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    self.navigationItem.rightBarButtonItem = nil;
    
    [self.groupNameText addTarget:self
                          action:@selector(startEditing:)
                forControlEvents:UIControlEventEditingDidBegin];
    
//    [self.tableView setEditing:YES];
}

- (void) startEditing: (UITextField *)textField 
{
    if (self.tableView.indexPathForSelectedRow == nil) {
        self.lastSelectedGroupName = nil;
        self.groupSelected = nil;
    }
}

- (void)viewDidUnload
{
    [self setHeaderView:nil];
    [self setGroupNameText:nil];
    [self setDoneBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![_fetchedResultsController.managedObjectContext save:&error]) {
            NSLog(@"Could not save context: %@", error);
        }
    }
    
}


- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated]; 
    
//    self.task.group = self.groupSelected;
    
    // NOt saving the relationship as it has to be saved in the delegate
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return YES;
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{    
    ItemDetailsCell *gaevCell = (ItemDetailsCell *) cell;
    
    Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([self.lastSetGroup.name isEqualToString:group.name]) {        
        gaevCell.accessoryType = UITableViewCellAccessoryCheckmark;        
    }
    else {
        gaevCell.accessoryType = UITableViewCellAccessoryNone;
    }
    gaevCell.itemNameLabel.text = group.name;      
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"ItemDetailsCell";
  /* Old way
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] 
                initWithStyle:UITableViewCellStyleDefault 
                reuseIdentifier:CellIdentifier];
    }    
   */
    ItemDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /* This is not working with xib. learn later and try again
    if (cell == nil) {
        NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (ItemDetailsCell *) [nib objectAtIndex:0];
    } */

    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the managed object.
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             */
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



- (void) resetSortOrder {
    
    
    }
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    /*[self resetSortOrder];
    NSInteger items = [[_fetchedResultsController fetchedObjects] count];
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=0; i< items; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+1];
        
        
        //        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
        Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
        movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row + 1];
    NSError *error;
    if (![_fetchedResultsController.managedObjectContext save:&error]) {
        NSLog(@"Could not save context: %@", error);
    }
 */
    
     NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Group *fromGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Group *toGroup = (Group *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    DLog(@"itemMoved: %@ to: %@ FsortOrder: %d toSO: %d FromIdx: %d to: %d", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], fromIndexPath.row, toIndexPath.row);
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Group *curObj = (Group *) [_fetchedResultsController objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];

        
//        DLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
                curObj.sort_order = newPosition;
    }
    
    Group *movedObj = (Group *) [_fetchedResultsController objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];

    // Save of this operation will be done on click of Edit Button as Done
}

 
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma NSFetchedResultsController delegate methods to respond to additions, removals and so on.

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    UITableView *tableView = self.tableView;
    //    NSLog(@"NSFetchType: %d", type);
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self enableAddGroupName];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
    
}

#pragma mark - Custom Editing Options in this controller
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     Group *group = (Group *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // if we are in add/update the groups mode, no need to perform selection/deselection
    if (self.isSelectMode) {
       
        UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:YES];                    
        
        
        // Reset Previous Selected Index if any
        if (self.lastSelectedGroupName && ![self.lastSelectedGroupName isEqual:indexPath]) {
            UITableViewCell * lastCell = [self.tableView  cellForRowAtIndexPath:self.lastSelectedGroupName];
            [lastCell setSelected:NO animated:YES];  
            lastCell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            // Set current cell as checked
            cell.accessoryType = UITableViewCellAccessoryCheckmark; 
            
            // remember current IndexPath now and the corresponding group
            self.lastSelectedGroupName = indexPath;
            self.groupSelected = group;
        }    
        else if (cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
            cell.accessoryType = UITableViewCellAccessoryNone;     
            
            [self enableAddGroupName];
        }
        else {
            DLog(@"Some error");
            [self enableAddGroupName];
        }
        
        [self finishSelection];
    }
    else {
//        NSLog(@"LastSelcted: %@", self.lastSelectedGroupName);
//        NSLog(@"CurrenSeled: %@", indexPath);
        if ([self.lastSelectedGroupName isEqual:indexPath]) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            [self enableAddGroupName];
        } else {

            [self enableEditGroupName: indexPath];
            // not selecting group as it is not needed in this case
        }
    }
}


#pragma mark - Result controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }        
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];    
    
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"Group" 
                                   inManagedObjectContext:self.managedObjectContext];    
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"sort_order" 
                                        ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]  initWithFetchRequest:fetchRequest 
                                                                                                 managedObjectContext:self.managedObjectContext      
                                                                                                   sectionNameKeyPath:nil 
                                                                                                            cacheName:nil];
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Error Fetching Group %@, %@", error, [error userInfo]);
	    abort();
	}    
    
    // Without this autoupdate will not work.
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

#pragma TextField Delegate Handling
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if ( textField == self.groupNameText) {
        
        NSString *groupName = [AppUtilities trim:textField.text];
        
        if (groupName.length == 0) {
            [textField resignFirstResponder];
        }
        else {
            if ([ self ifGroupAlreadyExist:groupName ]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                                message:@"Group Alreay Exist." 
                                                               delegate:self cancelButtonTitle:@"Ok" 
                                                      otherButtonTitles:nil];
                [alert show];
                self.groupNameText.text= @"";
            } 
            else {
                if (self.isSelectMode) {
                    // Edit is not allowed so no need to update and direclty go for add.
                    [self createNewGroup:textField.text];    
                    self.groupNameText.text= @"";
                }
                else {
                    // Edit of existing is allowed hence see if need to update
                    if (self.lastSelectedGroupName == nil) {
                        [self createNewGroup:textField.text];
                        self.groupNameText.text= @"";
                    } else {
                        self.groupSelected.name = groupName;
                    }
                }
                
                [textField resignFirstResponder];
            }
        }
    }    
    
    return YES;
}

- (void) fetchData {
    
    self.fetchedResultsController = nil;
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    [self.tableView reloadData];
}

-(void) saveUpdatedGroup: (Group *) existingGroup {
    NSError *error = nil;
    if (![existingGroup.managedObjectContext save:&error]) {        
        DLog(@"Error Saving Group %@, %@", error, [error userInfo]);
        
        abort();
    } 
}

-(void) createNewGroup: (NSString *)groupName {
    // Planning to add a new context object, so referring to fetrscntrl managed context specifically
    
    Group *newGroup = [NSEntityDescription insertNewObjectForEntityForName:@"Group"
                                                    inManagedObjectContext:self.fetchedResultsController.managedObjectContext];
    newGroup.name = groupName;
    newGroup.sort_order = [NSNumber numberWithInt:[self.fetchedResultsController.fetchedObjects count] + 1 ];
        
    NSError *error = nil;
    if (![newGroup.managedObjectContext save:&error]) {        
        DLog(@"Error Saving Group %@, %@", error, [error userInfo]);
        
        abort();
    } 
}

-(BOOL) ifGroupAlreadyExist: (NSString *)groupName {
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     
    [NSEntityDescription entityForName:@"Group" inManagedObjectContext:self.managedObjectContext]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(name == %@)", groupName]];

    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [self.managedObjectContext
                                       countForFetchRequest:fetchRequest error:&error];
//    NSLog(@"Count %d",count);
    
    if (count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void) finishSelection {
    [self.delegate groupEditViewControllerDidFinish:self SelectedGroup:self.groupSelected];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
    [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma Save Operation
- (IBAction)done:(id)sender {
    
    [self.delegate groupEditViewControllerDidFinish:self SelectedGroup:self.groupSelected];
    
    AppSharedData *appSharedData = [AppSharedData getInstance];
    [appSharedData setLastUsedGroup:self.groupSelected.name];
    [appSharedData setLastSelectedGroup:self.groupSelected];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
