//
//  EffortViewController.m
//  realtodo
//
//  Created by Me on 19/08/12.
//  Copyright (c) 2012 dasheri. All rights reserved.
//

#import "EffortViewController.h"


@interface EffortViewController ()


@end

@implementation EffortViewController
@synthesize effortCountLabel;
@synthesize effortUnitLabel;
@synthesize effortNumberPickerView;
@synthesize effortUnitPickerView;
//@synthesize commonDTO = _commonDTO;
@synthesize keyName = _keyName;
@synthesize effortUnitIdx = _effortUnitIdx;
@synthesize effortCountIdx = _effortCountIdx;
@synthesize delegate = _delegate;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) initForKeyName: (NSString *) keyName WithCountIdx: (int) countIdx WithUnitIdx: (int) unitIdx  Delegate: (id<EffortViewControllerDelegate>) delegate {
    
//    if (self) {
//        self.effortCountLabel.text = [APP_SHARED_DATA.effortCountArray objectAtIndex:countIdx];
//        self.effortUnitLabel.text = [APP_SHARED_DATA.effortUnitArray objectAtIndex:unitIdx];
//    }
    self.keyName = keyName;
    self.effortCountIdx = countIdx;
    self.effortUnitIdx = unitIdx;
    self.delegate = delegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    [self.effortNumberPickerView selectedRowInComponent:1];
    
//    int cntIdx = self.commonDTO.effortCountIndx;
//    int unitIdx= self.commonDTO.effortUnitIdx;
//    
//    self.effortCountLabel.text = [APP_SHARED_DATA.effortCountArray objectAtIndex:cntIdx];
//    self.effortUnitLabel.text = [APP_SHARED_DATA.effortUnitArray objectAtIndex:unitIdx];
    
//    [pickerView selectRow:1 inComponent:0 animated:NO];
    
    
    [self setEffortCount:self.effortCountIdx];
    [self setEffortUnit:self.effortUnitIdx];

}

- (void)viewDidUnload
{
    [self setEffortCountLabel:nil];
    [self setEffortNumberPickerView:nil];
    [self setEffortUnitPickerView:nil];
    [self setEffortUnitLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    switch (indexPath.section) {
        case 1: {
//            NSLog(@"%i", indexPath.row);
            switch (indexPath.row) {
                case 0: {
                    //[self resetBtn];
                    break;
                }
                default:
                     [self.navigationController popViewControllerAnimated:YES];
                    break;
            }
        }
    }
}

#pragma mark - UIPickerView Events

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    if (pickerView.tag == TAG_EFFORT_COUNT_PICKERVIEW ) 
//        return APP_SHARED_DATA.effortCountArray.count ;
        return 1;
    else if (pickerView.tag == TAG_EFFORT_UNIT_PICKERVIEW )
//        return APP_SHARED_DATA.effortUnitArray.count;
        return 1;
    else {
        return 0;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    mlabel.text=    [arrayNo objectAtIndex:row];
    if (pickerView.tag == TAG_EFFORT_COUNT_PICKERVIEW ) 
        self.effortCountLabel.text = [APP_SHARED_DATA.effortCountArray objectAtIndex:row];
    else if (pickerView.tag == TAG_EFFORT_UNIT_PICKERVIEW )
        self.effortUnitLabel.text = [APP_SHARED_DATA.effortUnitArray objectAtIndex:row];
    else {
        self.effortUnitLabel.text = @"Undefined";
        self.effortCountLabel.text = @"Undefined";
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    if (pickerView.tag == TAG_EFFORT_COUNT_PICKERVIEW ) 
        return APP_SHARED_DATA.effortCountArray.count ;
    else if (pickerView.tag == TAG_EFFORT_UNIT_PICKERVIEW )
        return APP_SHARED_DATA.effortUnitArray.count;
    else {
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    if (pickerView.tag == TAG_EFFORT_COUNT_PICKERVIEW ) 
        return [APP_SHARED_DATA.effortCountArray objectAtIndex:row];
    else if (pickerView.tag == TAG_EFFORT_UNIT_PICKERVIEW )
        return [APP_SHARED_DATA.effortUnitArray objectAtIndex:row];
    else {
        return @"Undefined";
    }
}

#pragma mark - Private API

- (void) setEffortCount: (int) countIdx {
    self.effortCountLabel.text = [APP_SHARED_DATA.effortCountArray objectAtIndex:countIdx];
    
    [self.effortNumberPickerView selectRow:countIdx inComponent:0 animated:YES];
   
}
- (void) setEffortUnit: (int) effortIdx {
    self.effortUnitLabel.text = [APP_SHARED_DATA.effortUnitArray objectAtIndex:effortIdx];
    
    [self.effortUnitPickerView selectRow:effortIdx inComponent:0 animated:YES];

}

- (void) saveBtnClicked {
    APP_SHARED_DATA.lastSelEffortCount = [self.effortNumberPickerView selectedRowInComponent:0];
    APP_SHARED_DATA.lastSelEffortUnit = [self.effortUnitPickerView selectedRowInComponent:0];
    
    [self.delegate effortViewControllerDidFinish:self 
                                     WithKeyName:self.keyName 
                                 WithEffortCount:[self.effortNumberPickerView selectedRowInComponent:0] 
                                  WithEffortUnit:[self.effortUnitPickerView selectedRowInComponent:0]];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Action Operations
//- (IBAction)resetBtnClick:(id)sender {
//    [self setEffortCount:APP_SHARED_DATA.defaultEffortCountIdx];
//     [self setEffortUnit:APP_SHARED_DATA.defaultEffortUnitIdx];
//}

- (IBAction)resetBtn:(id)sender {
    [self setEffortCount:APP_SHARED_DATA.defaultEffortCountIdx];
    [self setEffortUnit:APP_SHARED_DATA.defaultEffortUnitIdx];
}

- (IBAction)saveNavgBtnClicked:(id)sender {
    [self saveBtnClicked];
}


@end
