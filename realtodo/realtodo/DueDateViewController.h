//
//  MoreTextViewController.h
//  realtodo
//
//  Created by Me on 17/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDTO.h"

@protocol MoreTextViewControllerDelegate;

@interface DueDateViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextView *dataTextView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *enableDateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *enableDateSwitch;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;

@property (weak, nonatomic) IBOutlet UISegmentedControl *dueOnChoiceSegment;

@property (nonatomic, strong) CommonDTO *commonDTOObj;


@property(weak, nonatomic) id <MoreTextViewControllerDelegate> delegate;

- (IBAction)save:(id)sender;
- (IBAction)resetBtnClick:(id)sender;


@end

@protocol MoreTextViewControllerDelegate <NSObject>

-(void) moreTextViewControllerDidCancel: (id *) controller;
-(void) moreTextViewControllerDidFinish:(CommonDTO *) commonDTO: (NSString *)FormatedDate;

@end
