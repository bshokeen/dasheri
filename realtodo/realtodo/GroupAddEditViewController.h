//
//  GroupEditViewController.h
//  realtodo
//
//  Created by Me on 25/07/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Task.h"
#import "Group.h"
#import "ItemDetailsCell.h"

@protocol GroupAddEditViewControllerDelegate;

@interface GroupAddEditViewController : UITableViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITextField *groupNameText;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
//@property (nonatomic, weak) Task *task;
@property (nonatomic, strong) Group *lastSetGroup;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBtn;

@property(weak, nonatomic) id <GroupAddEditViewControllerDelegate> delegate;
- (IBAction)done:(id)sender;

- (void) initializeWithSelectMode: (BOOL) selectMode LastSetGroup: (Group*) lastSetGroup Context:(NSManagedObjectContext *) managedContext Delegate: (id<GroupAddEditViewControllerDelegate>) delegate;

// This is set while calling this view to indicate if we need to allow edit/update of existing group. If called from edit/add task it is select mode only.
@property BOOL isSelectMode;

@end

@protocol GroupAddEditViewControllerDelegate <NSObject>

-(void) groupEditViewControllerDidCancel: (id *) controller;
-(void) groupEditViewControllerDidFinish:(id) controller SelectedGroup:(Group *)group;
@end
