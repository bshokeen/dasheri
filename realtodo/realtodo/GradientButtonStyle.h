//
//  AppDelegate.h
//  realtodo
//
//  Created by Me on 10/06/12.
//  Copyright (c) 2012 Airtel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface GradientButtonStyle : UIButton {
    CAGradientLayer *shineLayer;
    CALayer         *highlightLayer;
}

@end
